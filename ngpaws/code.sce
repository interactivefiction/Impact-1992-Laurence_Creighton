






;#define const aLight       = 0
;#define const aWearable    = 1
;#define const aContainer   = 2
;#define const aNPC         = 3
;#define const aConcealed   = 4
;#define const aEdible      = 5
;#define const aDrinkable   = 6
;#define const aEnterable   = 7
;#define const aFemale      = 8
;#define const aLockable    = 9
;#define const aLocked      = 10
;#define const aMale        = 11
;#define const aNeuter      = 12
;#define const aOpenable    = 13
;#define const aOpen        = 14
;#define const aPluralName  = 15
;#define const aTransparent = 16
;#define const aScenery     = 17
;#define const aSupporter   = 18
;#define const aSwitchable  = 19
;#define const aOn          = 20
;#define const aStatic      = 21


;#define const object_0_lamp = 0
;#define const object_1_lamp = 1
;#define const object_2 = 2
;#define const object_3 = 3
;#define const object_4_padd = 4
;#define const object_5_box = 5
;#define const object_6_box = 6
;#define const object_7 = 7
;#define const object_8_galo = 8
;#define const object_9_coin = 9
;#define const object_10_nail = 10
;#define const object_11_need = 11
;#define const object_12_comp = 12
;#define const object_13 = 13
;#define const object_14_trou = 14
;#define const object_15 = 15
;#define const object_16_maga = 16
;#define const object_17_scre = 17
;#define const object_18_jar = 18
;#define const object_19_bar = 19
;#define const object_20 = 20
;#define const object_21 = 21
;#define const object_22_rod = 22
;#define const object_23 = 23
;#define const object_24_barr = 24
;#define const object_25 = 25
;#define const object_26_noti = 26
;#define const object_27_dagg = 27
;#define const object_28 = 28
;#define const object_29 = 29
;#define const object_30 = 30
;#define const object_31_para = 31
;#define const object_32 = 32
;#define const object_33_spad = 33
;#define const object_34_rope = 34
;#define const object_35 = 35
;#define const object_36 = 36
;#define const object_37 = 37
;#define const object_38_rope = 38
;#define const object_39_bone = 39
;#define const object_40 = 40
;#define const object_41 = 41
;#define const object_42 = 42
;#define const object_43 = 43
;#define const object_44_egg = 44
;#define const object_45 = 45
;#define const object_46 = 46
;#define const object_47_disc = 47
;#define const object_48 = 48
;#define const object_49_shee = 49
;#define const object_50_broo = 50
;#define const object_51_lift = 51
;#define const object_52 = 52
;#define const object_53 = 53
;#define const object_54_book = 54
;#define const object_55 = 55
;#define const object_56_cott = 56
;#define const object_57_plie = 57
;#define const object_58 = 58
;#define const object_59 = 59
;#define const object_60 = 60
;#define const object_61 = 61
;#define const object_62 = 62
;#define const object_63_scro = 63
;#define const object_64_spic = 64
;#define const object_65_key = 65
;#define const object_66 = 66
;#define const object_67_glov = 67
;#define const object_68 = 68
;#define const object_69 = 69
;#define const object_70 = 70
;#define const object_71_hook = 71
;#define const object_72 = 72


;#define const room_0 = 0
;#define const room_1 = 1
;#define const room_2 = 2
;#define const room_3 = 3
;#define const room_4 = 4
;#define const room_5 = 5
;#define const room_6 = 6
;#define const room_7 = 7
;#define const room_8 = 8
;#define const room_9 = 9
;#define const room_10 = 10
;#define const room_11 = 11
;#define const room_12 = 12
;#define const room_13 = 13
;#define const room_14 = 14
;#define const room_15 = 15
;#define const room_16 = 16
;#define const room_17 = 17
;#define const room_18 = 18
;#define const room_19 = 19
;#define const room_20 = 20
;#define const room_21 = 21
;#define const room_22 = 22
;#define const room_23 = 23
;#define const room_24 = 24
;#define const room_25 = 25
;#define const room_26 = 26
;#define const room_27 = 27
;#define const room_28 = 28
;#define const room_29 = 29
;#define const room_30 = 30
;#define const room_31 = 31
;#define const room_32 = 32
;#define const room_33 = 33
;#define const room_34 = 34
;#define const room_35 = 35
;#define const room_36 = 36
;#define const room_37 = 37
;#define const room_38 = 38
;#define const room_39 = 39
;#define const room_40 = 40
;#define const room_41 = 41
;#define const room_42 = 42
;#define const room_43 = 43
;#define const room_44 = 44
;#define const room_45 = 45
;#define const room_46 = 46
;#define const room_47 = 47
;#define const room_48 = 48
;#define const room_49 = 49
;#define const room_50 = 50
;#define const room_51 = 51
;#define const room_52 = 52
;#define const room_53 = 53
;#define const room_54 = 54

;#define const room_noncreated = 252 ; destroying an object moves it to this room
;#define const room_worn = 253 ; wearing an object moves it to this room
;#define const room_carried = 254 ; carrying an object moves it to this room
;#define const room_here = 255 ; not used by any Quill condacts










;#define const yesno_is_dark                       =   0
;#define const total_carried                       =   1
;#define const countdown_location_description      =   2
;#define const countdown_location_description_dark =   3
;#define const countdown_object_description_unlit  =   4
;#define const countdown_player_input_1            =   5
;#define const countdown_player_input_2            =   6
;#define const countdown_player_input_3            =   7
;#define const countdown_player_input_4            =   8
;#define const countdown_player_input_dark         =   9
;#define const countdown_player_input_unlit        =  10
;#define const game_flag_11                        = 111 ; ngPAWS assigns a special meaning to flag 11
;#define const game_flag_12                        = 112 ; ngPAWS assigns a special meaning to flag 12
;#define const game_flag_13                        = 113 ; ngPAWS assigns a special meaning to flag 13
;#define const game_flag_14                        = 114 ; ngPAWS assigns a special meaning to flag 14
;#define const game_flag_15                        = 115 ; ngPAWS assigns a special meaning to flag 15
;#define const game_flag_16                        = 116 ; ngPAWS assigns a special meaning to flag 16
;#define const game_flag_17                        =  17
;#define const game_flag_18                        =  18
;#define const game_flag_19                        =  19
;#define const game_flag_20                        =  20
;#define const game_flag_21                        =  21
;#define const game_flag_22                        =  22
;#define const game_flag_23                        =  23
;#define const game_flag_24                        =  24
;#define const game_flag_25                        =  25
;#define const game_flag_26                        =  26
;#define const game_flag_27                        =  27
;#define const pause_parameter                     =  28
;#define const bitset_graphics_status              =  29
;#define const total_player_score                  =  30
;#define const total_turns_lower                   =  31
;#define const total_turns_higher                  =  32
;#define const word_verb                           =  33
;#define const word_noun                           =  34
;#define const room_number                         =  38 ; (35 in The Quill, but 38 in ngPAWS)




;#define const pause_sound_effect_tone_increasing = 1 ; increasing tone (value is the duration)
;#define const pause_sound_effect_telephone       = 2 ; ringing telephone (value is the number of rings)
;#define const pause_sound_effect_tone_decreasing = 3 ; decreasing tone (opposite of effect 1)
;#define const pause_sound_effect_white_noise     = 5 ; white noise (resembles an audience clapping, value is the number of repetitions)
;#define const pause_sound_effect_machine_gun     = 6 ; machine gun noise (value is the duration)

;#define const pause_flash = 4 ; flash the screen and the border (value is the number of flashes)
;#define const pause_box_wipe = 9 ; a series of coloured boxes expands out of the centre of the screen at speed

;#define const pause_font_default = 7 ; switch to the default font
;#define const pause_font_alternate = 8 ; switch to the alternative font.

;#define const pause_change_youcansee = 10 ; Change the "You can also see" message to the message number passed to PAUSE

;#define const pause_restart = 12 ; Restart the game without warning.
;#define const pause_reboot = 13 ; Reboot the Spectrum without warning
;#define const pause_ability = 11 ; Set the maximum number of objects carried at once to the value passed to PAUSE
;#define const pause_ability_plus = 14 ; Increase number of objects that can be carried at once by the amount passed to PAUSE
;#define const pause_ability_minus = 15 ; Decrease number of objects that can be carried at once by the amount passed to PAUSE
;#define const pause_toggle_graphics = 19 ; switch graphics off (for PAUSE 255) or on (for any other value)
;#define const pause_ram_save_load = 21 ; RAMload (for PAUSE 50) or RAMsave (for any other value)
;#define const pause_set_identity_byte = 22 ; Set the identity in saved games to the value passed to PAUSE

;#define const pause_click_effect_1 = 16
;#define const pause_click_effect_2 = 17
;#define const pause_click_effect_3 = 18









/CTL






/VOC






n         1    verb
north     1    verb
s         2    verb
south     2    verb
e         3    verb
east      3    verb
w         4    verb
west      4    verb
northeast 5    verb
ne        5    verb
nw        6    verb
se        7    verb
southwest 8    verb
sw        8    verb
u         9    noun


up        9    verb
d        10    noun


desc     10    verb

down     10    verb
ente     11    noun
enter    11    verb

in       11    verb

insi     11    verb
exit     12    verb
leav     12    verb
leave    12    verb
o        12    verb
out      12    verb
cano     13    noun






wave     14    verb
exam     15    verb
x        15    verb
p        16    verb
wait     16    verb
padd     17    noun
foun     18    noun
tier     18    noun
wate     19    noun
box      20    noun
matc     20    noun
corn     21    noun
pit      22    noun
sear     23    verb
coin     24    noun
poun     24    noun
comp     25    noun ; computer compass

gate     26    noun
open     27    verb
unlo     27    verb
farm     28    noun
man      28    noun
chat     29    verb
spea     29    verb
talk     29    verb
give     30    verb
offe     30    verb
maga     31    noun
unde     32    noun
scre     33    noun
read     34    verb
pock     35    noun
ward     36    noun
pant     37    noun
trou     37    noun
unsc     38    verb
lamp     39    noun
drie     40    noun
drye     40    noun
door     42    noun
inse     43    verb
plac     44    noun


put      44    verb
tabl     45    noun
hone     46    noun
jar      46    noun
pres     47    verb
pull     47    verb
push     47    verb
bar      48    noun
crow     48    noun
brea     49    verb
smas     49    verb






chai     50    noun
call     51    verb
cupb     52    noun
rod      53    noun
leve     54    noun
barr     55    noun
ratt     56    verb
shak     56    verb
need     57    noun
noti     58    noun
nail     59    noun
make     60    verb
dagg     61    noun
pier     62    verb
pric     62    verb
stab     62    verb
lid      63    noun
ladd     64    noun
all      65    noun
lay      66    verb
lie      66    verb
asce     67    verb
clim     67    verb
tree     68    noun
trun     68    noun
grap     69    noun
hook     69    noun
atta     70    verb
tie      70    verb
para     71    noun
ridg     72    noun
jump     73    verb
leap     73    verb
raft     74    noun
heap     75    noun
pile     75    noun
rubb     75    noun
dig      76    verb
spad     77    noun
rope     79    noun
stra     79    noun
undo     80    verb
unti     80    verb
cut      81    verb
dog      82    noun
hot      82    noun
light    83    noun
ligh     83    verb

on       83    verb
onto     83    noun

exti     84    verb
off      84    verb
fire     85    noun
fireplace 85   noun
pris     86    verb
pry      86    verb
boar     87    verb
feed     88    verb
thro     89    verb
bear     90    noun
sign     91    noun
behi     92    noun
star     93    verb
bone     94    noun

burn     95    verb
ignition 95    noun
igni     95    verb
nich     96    noun
guar     97    noun
egg      98    noun
g       100    verb
get     100    verb
t       100    verb
take    100    verb
drop    101    verb
remo    102    verb
wear    103    verb
i       104    verb
inve    104    verb
l       105    verb
look    105    verb
r       105    verb
rede    105    verb
qq      106    verb
quit    106    verb

save    107    verb

load    108    verb
bark    109    noun
move    110    verb
turn    110    verb
till    111    noun
disc    112    noun
disk    112    noun
swin    113    verb
pape    114    noun
shee    114    noun
broo    115    noun
stic    115    noun

lift    116    verb
ledg    117    noun
red     118    noun
gree    119    noun
swit    120    noun
switch  120    verb

trip    120    verb
wall    121    noun
mach    122    noun
book    123    noun
note    123    noun
28s     124    verb
172w    125    noun
butt    126    noun
swim    128    verb
pot     129    noun
bloc    130    verb
plug    130    verb
ear     131    noun
ears    131    noun
patt    132    noun
car     133    noun
knob    134    noun
plie    135    noun
wife    136    noun
kiss    137    verb
scro    138    noun
scroll  138    noun
trac    139    noun
dry     140    verb
pay     141    verb
hut     142    noun
cros    143    verb
pool    144    noun
dism    145    verb
coor    146    noun
gard    147    noun
herb    148    noun
spic    148    noun
eat     149    verb
key     150    noun
ram     151    noun
rs      152    verb
rl      153    verb
font    154    verb
1       155    noun
2       156    noun
drin    157    verb
hand    158    noun
loud    159    noun
shou    160    verb
gene    161    noun

hole    162    noun
kill    163    verb
lean    164    verb
prop    164    verb
stan    164    verb
plan    165    noun
peel    167    verb
smel    168    verb
snif    168    verb
vacu    169    noun
brib    170    verb
desp    171    verb
send    171    verb
clue    172    verb
h       172    verb
help    172    verb
hint    172    verb
tip     172    verb
scor    173    verb
stat    173    verb
play    174    verb
pat     175    verb
pet     175    verb
stro    175    verb
hotp    176    noun
kett    176    noun
plat    176    noun
stov    176    noun
shaf    177    noun
stal    178    noun
clea    179    verb
wash    179    verb
grab    180    verb
stea    180    verb
swip    180    verb
fill    181    verb
sket    182    noun
noth    183    noun
rip     184    verb
tear    184    verb
high    185    noun
road    185    noun
stre    185    noun
galo    200    noun
cott    201    noun
piec    201    noun
wool    201    noun
glov    202    noun
lifter  203    noun ; not 106
tripswitch 204 noun ; not 120




AND          2    conjunction
THEN         2    conjunction






/STX





/0
It's too dark to see anything.<br>
/1
\nYOU NOTICE:<br>
/2 ; TODO: check the following message matches the tone of your game:
What do you want to do now?
/3 ; TODO: check the following message matches the tone of your game:
{EXITS|@38|1000}
/4 ; TODO: check the following message matches the tone of your game:
Please enter your next order
/5 ; TODO: check the following message matches the tone of your game:
Please type in your orders
/6
Sorry, not understood.<br>
/7
No path leads in that direction.<br>
/8
Command can't be executed.<br>
/9
INVENTORY:
/10
 (worn)
/11
Nothing<br>
/12
Are you certain?<br>
/13
\nTry again?<br>
/14
Thanks for playing a game from  {CLASS|center|ZENOBI SOFTWARE\n\n}<br>
/15
DONE<br>
/16
                               ?<br>
/17 ; TODO: check the following message matches the tone of your game:
You have typed<br>
/18
 turns have been attempted.
/19 ; TODO: check the following message matches the tone of your game:
s<br>
/20 ; TODO: check the following message matches the tone of your game:
.<br>
/21
\nYour rating is 
/22
 per cent.<br>
/23
You're not wearing it.<br>
/24
You can't remove it as both your hands are full.<br>
/25
Are there two?<br>
/26
Do you see it ?<br>
/27
You're carrying the limit.<br>
/28
Command can't be executed.<br>
/29
You're already wearing it.<br>
/30
Y<br>
/31
N<br>
/32 ; TODO: check the following message matches the tone of your game:
More...<br>
/33 ; TODO: check the following message matches the tone of your game:
><br>
/34 ; TODO: check the following message matches the tone of your game:
<br>
/35 ; TODO: check the following message matches the tone of your game:
Time passes...<br>
/36 ; TODO: check the following message matches the tone of your game:

/37 ; TODO: check the following message matches the tone of your game:

/38 ; TODO: check the following message matches the tone of your game:

/39 ; TODO: check the following message matches the tone of your game:

/40 ; TODO: check the following message matches the tone of your game:
Command can't be executed.<br>
/41 ; TODO: check the following message matches the tone of your game:
Command can't be executed.<br>
/42 ; TODO: check the following message matches the tone of your game:
You can't remove it as both your hands are full.<br>
/43 ; TODO: check the following message matches the tone of your game:
You're carrying the limit.<br>
/44 ; TODO: check the following message matches the tone of your game:
You put {OREF} into<br>
/45 ; TODO: check the following message matches the tone of your game:
{OREF} is not into<br>
/46 ; TODO: check the following message matches the tone of your game:
<br>
/47 ; TODO: check the following message matches the tone of your game:
<br>
/48 ; TODO: check the following message matches the tone of your game:
<br>
/49 ; TODO: check the following message matches the tone of your game:
Command can't be executed.<br>
/50 ; TODO: check the following message matches the tone of your game:
Command can't be executed.<br>
/51 ; TODO: check the following message matches the tone of your game:
.<br>
/52 ; TODO: check the following message matches the tone of your game:
That is not into<br>
/53 ; TODO: check the following message matches the tone of your game:
nothing at all<br>
/54 ; TODO: check the following message matches the tone of your game:
File not found.<br>
/55 ; TODO: check the following message matches the tone of your game:
File corrupt.<br>
/56 ; TODO: check the following message matches the tone of your game:
I/O error. File not saved.<br>
/57 ; TODO: check the following message matches the tone of your game:
Directory full.<br>
/58 ; TODO: check the following message matches the tone of your game:
Please enter savegame name you used when saving the game status.
/59 ; TODO: check the following message matches the tone of your game:
Invalid savegame name. Please check the name you entered is correct, and make sure you are trying to load the game from the same browser you saved it.<br>
/60 ; TODO: check the following message matches the tone of your game:
Please enter savegame name. Remember to note down the name you choose, as it will be requested in order to restore the game status.
/61 ; TODO: check the following message matches the tone of your game:
<br>
/62 ; TODO: check the following message matches the tone of your game:
Sorry? Please try other words.<br>
/63 ; TODO: check the following message matches the tone of your game:
Here<br>
/64 ; TODO: check the following message matches the tone of your game:
you can see<br>
/65 ; TODO: check the following message matches the tone of your game:
you can see<br>
/66 ; TODO: check the following message matches the tone of your game:
inside you see<br>
/67 ; TODO: check the following message matches the tone of your game:
on top you see<br>






/MTX


/1000 ; TODO: check the following message matches the tone of your game:
Exits: 
/1001 ; TODO: check the following message matches the tone of your game:
You can't see any exits

/1003
{ACTION|nort|nort}
/1004
{ACTION|sout|sout}
/1005
{ACTION|east|east}
/1006
{ACTION|west|west}
/1007
{ACTION|ne|ne}
/1009
{ACTION|se|se}
/1010
{ACTION|sw|sw}
/1012
{ACTION|down|down}
/1013
{ACTION|insi|insi}
/1014
{ACTION|out|out}






/OTX
/0 ; sust(object_0_lamp//0)
A LIT paraffin lamp
/1 ; sust(object_1_lamp//1)
A paraffin lamp
/2 ; sust(object_2//2)
A canoe
/3 ; sust(object_3//3)
A man standing on the far side
/4 ; sust(object_4_padd//4)
A paddle
/5 ; sust(object_5_box//5)
A box of matches
/6 ; sust(object_6_box//6)
A box of matches
/7 ; sust(object_7//7)
A hole
/8 ; sust(object_8_galo//8)
A pair of galoshes
/9 ; sust(object_9_coin//9)
A `1 coin
/10 ; sust(object_10_nail//10)
A rusty nail
/11 ; sust(object_11_need//11)
A needle
/12 ; sust(object_12_comp//12)
A crude compass
/13 ; sust(object_13//13)
A farmer
/14 ; sust(object_14_trou//14)
A pair of trousers
/15 ; sust(object_15//15)
An open gate
/16 ; sust(object_16_maga//16)
A magazine
/17 ; sust(object_17_scre//17)
A screwdriver
/18 ; sust(object_18_jar//18)
A jar of honey
/19 ; sust(object_19_bar//19)
A crowbar
/20 ; sust(object_20//20)
An open door
/21 ; sust(object_21//21)
An open cupboard
/22 ; sust(object_22_rod//22)
An iron rod
/23 ; sust(object_23//23)
An iron lever
/24 ; sust(object_24_barr//24)
A small wooden barrel
/25 ; sust(object_25//25)
A notice nailed to the wall
/26 ; sust(object_26_noti//26)
A notice
/27 ; sust(object_27_dagg//27)
A short dagger
/28 ; sust(object_28//28)
A ladder
/29 ; sust(object_29//29)
A ladder over the stalagmites
/30 ; sust(object_30//30)
A grappling hook (in the rock)
/31 ; sust(object_31_para//31)
A pink parasol
/32 ; sust(object_32//32)
A small raft
/33 ; sust(object_33_spad//33)
A spade
/34 ; sust(object_34_rope//34)
A rope
/35 ; sust(object_35//35)
An open steel door
/36 ; sust(object_36//36)
A mixture of glass and honey
/37 ; sust(object_37//37)
A bear
/38 ; sust(object_38_rope//38)
A long rope with grappling hook
/39 ; sust(object_39_bone//39)
A bone
/40 ; sust(object_40//40)
Hot, the dog
/41 ; sust(object_41//41)
A pile of rubble
/42 ; sust(object_42//42)
A flattened pile of rubble
/43 ; sust(object_43//43)
A guard
/44 ; sust(object_44_egg//44)
An egg
/45 ; sust(object_45//45)
A broken egg
/46 ; sust(object_46//46)
An opening in the trunk
/47 ; sust(object_47_disc//47)
A computer disc
/48 ; sust(object_48//48)
A dangling rope
/49 ; sust(object_49_shee//49)
A sheet of paper
/50 ; sust(object_50_broo//50)
An old broomstick
/51 ; sust(object_51_lift//51)
A paper-lifter#The &#34;up and down&#34; variety.
/52 ; sust(object_52//52)
That the ceiling has opened
/53 ; sust(object_53//53)
A metal tripswitch
/54 ; sust(object_54_book//54)
A notebook
/55 ; sust(object_55//55)
that the computer is booted up
/56 ; sust(object_56_cott//56)
Pieces of cotton wool
/57 ; sust(object_57_plie//57)
A pair of pliers
/58 ; sust(object_58//58)
that the coordinates have been entered.
/59 ; sust(object_59//59)
The farmer's wife
/60 ; sust(object_60//60)
A smashed lamp
/61 ; sust(object_61//61)
The computer awaits a disc....
/62 ; sust(object_62//62)
The farmer's wife
/63 ; sust(object_63_scro//63)
A battered scroll
/64 ; sust(object_64_spic//64)
A rare herb
/65 ; sust(object_65_key//65)
A key
/66 ; sust(object_66//66)
An open door
/67 ; sust(object_67_glov//67)
A rubber glove
/68 ; sust(object_68//68)
A large plant
/69 ; sust(object_69//69)
A narrow ventilation shaft.
/70 ; sust(object_70//70)
A steel door (slightly ajar)
/71 ; sust(object_71_hook//71)
A grappling hook
/72 ; sust(object_72//72)
That the overhead lights are off






/LTX
/0 ; sust(room_0//0)
{CLASS|center|IMPACT\n}The exhausted scientists looked up wearily from their equations and calculations, and looking at each other nodded a silent concurrence.\nstAn asteroid measuring almost t enmiles in diameter is in an orb itwhich will cause an impact wit h the city of London.\nA laser beam is tried unsuccess- fully to destroy it.\nIt has been reported that a repelling beam machine has been built by a now-dead inventor. You must find it and deflect the asteroid away from Earth before it destroys most of London.\n\nby Laurence Creighton June 1992 using the Quill  the Press
/1 ; TODO: consider adding relevant links: {ACTION|clea car|clea car}, {ACTION|cros road|cros road}, {ACTION|in car|in car}, {ACTION|w|w}, {ACTION|x car|x car} ; sust(room_1//1)
You are next to your car on a busy highway. The road itself runs north/south, and a narrow path leads off to the {ACTION|east|east}. Exit: {ACTION|east|east}
/2 ; sust(room_2//2)
You are on a narrow path.\nExits: {ACTION|west|west} and {ACTION|east|east}
/3 ; TODO: consider adding relevant links: {ACTION|in cano|in cano}, {ACTION|talk man|talk man}, {ACTION|x cano|x cano} ; sust(room_3//3)
You are beside a small dam. The path ends abruptly at the water, and continues on the far side. A small canoe can be seen.\nExit: {ACTION|west|west}
/4 ; TODO: consider adding relevant links: {ACTION|clim foun|clim foun}, {ACTION|drin wate|drin wate}, {ACTION|fill barr|fill barr}, {ACTION|in foun|in foun}, {ACTION|x foun|x foun}, {ACTION|x wate|x wate} ; sust(room_4//4)
You are in a very small field. In the centre stands a large stone fountain over which water cascades from one tier to the next. The dam is to the west. Exit: {ACTION|east|east}
/5 ; sust(room_5//5)
You are at some crossroads. To the {ACTION|north|north} you can see the outskirts of a dense forest in which you could easily get lost. To the {ACTION|east|east} is a service station and a short distance to the {ACTION|south|south} a small shop can be seen. Exits: {ACTION|north|north}, {ACTION|south|south}, {ACTION|west|west}, {ACTION|east|east}
/6 ; TODO: consider adding relevant links: {ACTION|d|d}, {ACTION|jump|jump}, {ACTION|x pit|x pit} ; sust(room_6//6)
You're inside a service station. The floor is covered with oil, and steps leading down to the service pit look very slippery. Exit: {ACTION|west|west} and down
/7 ; TODO: consider adding relevant links: {ACTION|ne|ne} ; sust(room_7//7)
You are at a junction of paths. Exits: {ACTION|west|west}, {ACTION|east|east} and northeast
/8 ; TODO: consider adding relevant links: {ACTION|brea chai|brea chai}, {ACTION|brea door|brea door}, {ACTION|in|in}, {ACTION|inse bar|inse bar}, {ACTION|open door|open door}, {ACTION|pry chai|pry chai}, {ACTION|x chai|x chai} ; sust(room_8//8)
You have come to the grounds of a disused mine.  Off to one side is a small room.\nExits: {ACTION|west|west}, {ACTION|east|east} and in
/9 ; TODO: consider adding relevant links: {ACTION|x lift|x lift} ; sust(room_9//9)
You are by a mineshaft. A lift stands ready to descend to the lower depths.\nExits: {ACTION|west|west} and {ACTION|in|in}
/10 ; TODO: consider adding relevant links: {ACTION|clim rubb|clim rubb}, {ACTION|dig|dig}, {ACTION|dig rubb|dig rubb} ; sust(room_10//10)
The path comes to a dead end. This is a small but very high cavern and appears to be a dump for rubble.\nA path leads {ACTION|west|west}
/11 ; TODO: consider adding relevant links: {ACTION|in|in} ; sust(room_11//11)
You're in an underground passage with a lift ready to ascend to the surface.\nExits: {ACTION|west|west}, {ACTION|east|east} and in
/12 ; TODO: consider adding relevant links: {ACTION|h|h}, {ACTION|open door|open door}, {ACTION|pres door|pres door}, {ACTION|send dog|send dog}, {ACTION|w|w}, {ACTION|x door|x door} ; sust(room_12//12)
You are in a damp underground passage. You can see a large steel door.\nExits: {ACTION|east|east} and west
/13 ; TODO: consider adding relevant links: {ACTION|dig|dig}, {ACTION|e|e} ; sust(room_13//13)
You have come to a new digging. Exits: {ACTION|south|south} and east
/14 ; TODO: consider adding relevant links: {ACTION|dig|dig}, {ACTION|pres nail|pres nail}, {ACTION|remo nail|remo nail}, {ACTION|rip noti|rip noti}, {ACTION|x nail|x nail} ; sust(room_14//14)
You are at the southern side of the new digging.\nExit: {ACTION|north|north}
/15 ; sust(room_15//15)
You are on a forked path.\nExits: {ACTION|north|north}, {ACTION|south|south} and {ACTION|east|east}
/16 ; TODO: consider adding relevant links: {ACTION|clea till|clea till}, {ACTION|open till|open till}, {ACTION|r unde|r unde}, {ACTION|x till|x till} ; sust(room_16//16)
You are in a general dealer's store. The shelves are filled with merchandise and an ancient till full of dust is obviously broken.\nExit: {ACTION|north|north}
/17 ; TODO: consider adding relevant links: {ACTION|sw|sw} ; sust(room_17//17)
You are on a farm road wending from southwest to {ACTION|north|north}.
/18 ; TODO: consider adding relevant links: {ACTION|clim gate|clim gate}, {ACTION|h|h}, {ACTION|n|n}, {ACTION|open gate|open gate}, {ACTION|shou|shou}, {ACTION|talk loud|talk loud}, {ACTION|talk man|talk man}, {ACTION|w|w} ; sust(room_18//18)
You are at a high wooden gate. Beyond can be seen the farmhouse and westwards, a shed.\nExits: north, {ACTION|south|south} and west
/19 ; TODO: consider adding relevant links: {ACTION|clim on|clim on}, {ACTION|in trac|in trac}, {ACTION|r unde|r unde}, {ACTION|x trac|x trac} ; sust(room_19//19)
You are in a tractor shed. Next to you is a large red tractor which almost fills the entire shed.\nExit: {ACTION|east|east}
/20 ; TODO: consider adding relevant links: {ACTION|give key|give key}, {ACTION|in|in}, {ACTION|talk man|talk man} ; sust(room_20//20)
You are standing at an open door which leads into the farmhouse. Exits: {ACTION|south|south} and in
/21 ; TODO: consider adding relevant links: {ACTION|o|o} ; sust(room_21//21)
You are in the hallway. Doors lead off in every direction. Exits: {ACTION|north|north}, {ACTION|west|west}, {ACTION|east|east}, out
/22 ; TODO: consider adding relevant links: {ACTION|g lamp|g lamp}, {ACTION|unsc lamp|unsc lamp} ; sust(room_22//22)
You are in a cozy living room. Armchairs are in a semicircle around a low coffee table.\nExit: {ACTION|south|south}
/23 ; TODO: consider adding relevant links: {ACTION|inse box|inse box}, {ACTION|put box|put box}, {ACTION|x drye|x drye}, {ACTION|ne|ne} ; sust(room_23//23)
You are standing in a room which is being used as a laundry. The rubber pipe leading to the washing machine is broken, but the tumble drier has not met the same fate and seems to be in good order.\nExits: {ACTION|south|south} and northeast
/24 ; TODO: consider adding relevant links: {ACTION|give spic|give spic}, {ACTION|h|h}, {ACTION|x fire|x fire}, {ACTION|x pot|x pot}, {ACTION|x put|x put}, {ACTION|x tabl|x tabl} ; sust(room_24//24)
You're in a typical farm kitchen with a huge pot bubbling over a roaring fire. A large table is standing in the centre in front of the fireplace.\nExits: {ACTION|north|north} and {ACTION|west|west}
/25 ; TODO: consider adding relevant links: {ACTION|open ward|open ward}, {ACTION|x ward|x ward} ; sust(room_25//25)
You're in a bedroom. Long velvet curtains cover the wide window, and a large wardrobe is built into the wall to the side of the bed.\nExit: {ACTION|east|east}
/26 ; TODO: consider adding relevant links: {ACTION|h|h}, {ACTION|nw|nw}, {ACTION|east|east}, {ACTION|ne|ne}, {ACTION|se|se}, {ACTION|sw|sw}, {ACTION|west|west} ; sust(room_26//26)
You are on the outskirts of an impenetrable forest. A well- defined path leads {ACTION|south|south} and a rudimentary path swings off to the {ACTION|northwest|northwest}, but you're apt to go round in a circle if you don't know where you're going...
/27 ; TODO: consider adding relevant links: {ACTION|drop|drop}, {ACTION|pres d|pres d}, {ACTION|pres u|pres u} ; sust(room_27//27)
You're inside the lift at the surface.  Two buttons marked U and D can be seen.\nExit: {ACTION|out|out}
/28 ; TODO: consider adding relevant links: {ACTION|sear pit|sear pit}, {ACTION|u|u}, {ACTION|x pit|x pit} ; sust(room_28//28)
You are in a service pit. Steps lead up to the interior of the service station.\nExit: up
/29 ; TODO: consider adding relevant links: {ACTION|boar raft|boar raft}, {ACTION|burn rope|burn rope}, {ACTION|cut rope|cut rope}, {ACTION|remo rope|remo rope}, {ACTION|se|se}, {ACTION|unti rope|unti rope}, {ACTION|x rope|x rope} ; sust(room_29//29)
You are on the shore of a large lake.\nExit: southeast
/30 ; TODO: consider adding relevant links: {ACTION|drop|drop}, {ACTION|o|o}, {ACTION|pres d|pres d}, {ACTION|pres u|pres u} ; sust(room_30//30)
You're inside the lift deep inside the mine.  Two buttons marked U and D can be seen. Exit: out
/31 ; TODO: consider adding relevant links: {ACTION|gene on|gene on}, {ACTION|inse rod|inse rod}, {ACTION|on gene|on gene}, {ACTION|r behi|r behi}, {ACTION|remo rod|remo rod}, {ACTION|star gene|star gene}, {ACTION|x gene|x gene} ; sust(room_31//31)
You are in a small room housing a powerful generator. In the front of the generator is a 2cm square-shaped recess, and behind the generator are wires and lots of dust.\nExit: {ACTION|out|out}
/32 ; TODO: consider adding relevant links: {ACTION|call man|call man}, {ACTION|open cupb|open cupb}, {ACTION|pry cupb|pry cupb}, {ACTION|sear corn|sear corn}, {ACTION|x corn|x corn}, {ACTION|x cupb|x cupb}, {ACTION|sw|sw} ; sust(room_32//32)
This is obviously the farmer's workshop. A cupboard marked &#34;BITS  PIECES&#34; stands alongsidea  dark corner.                  E xit: southwest
/33 ; TODO: consider adding relevant links: {ACTION|boar raft|boar raft}, {ACTION|n|n} ; sust(room_33//33)
You are on the far shore of the lake.\nExits: north and {ACTION|west|west}
/34 ; TODO: consider adding relevant links: {ACTION|e|e}, {ACTION|feed bear|feed bear}, {ACTION|give jar|give jar}, {ACTION|h|h}, {ACTION|put jar|put jar}, {ACTION|read sign|read sign}, {ACTION|s|s}, {ACTION|thro jar|thro jar} ; sust(room_34//34)
You are by the entrance to a cave. A flashing red sign tries to tell you something.\nExits: south and east
/35 ; TODO: consider adding relevant links: {ACTION|e|e}, {ACTION|h|h}, {ACTION|w|w} ; sust(room_35//35)
You are inside the cave.  The floor to the east is covered with sharp, pointed stalagmites. Exits: west and east
/36 ; TODO: consider adding relevant links: {ACTION|clim ladd|clim ladd}, {ACTION|g hook|g hook}, {ACTION|jump|jump}, {ACTION|pres hook|pres hook}, {ACTION|w|w} ; sust(room_36//36)
You are in a high-walled cavern. The walls are slimy and too slippery to climb, and the only path seems to run west - over the stalagmites.
/37 ; TODO: consider adding relevant links: {ACTION|give coin|give coin}, {ACTION|h|h}, {ACTION|in hut|in hut}, {ACTION|w|w} ; sust(room_37//37)
You are on a west?{ACTION|east|east} path. A tollgate stands next to a small wooden hut.
/38 ; TODO: consider adding relevant links: {ACTION|drop egg|drop egg} ; sust(room_38//38)
You are on a ladder at the top of the tree.  Looking around and admiring the view, you wonder whether the effort was worth it! Exit: {ACTION|down|down}
/39 ; TODO: consider adding relevant links: {ACTION|clim ladd|clim ladd}, {ACTION|clim tree|clim tree}, {ACTION|g bark|g bark}, {ACTION|h|h}, {ACTION|in|in}, {ACTION|peel bark|peel bark}, {ACTION|turn bark|turn bark}, {ACTION|x bark|x bark}, {ACTION|x patt|x patt}, {ACTION|x tree|x tree} ; sust(room_39//39)
You are at a large tree. The bark seems to have a mystical pattern, and high above a nest can be seen.\nExit: {ACTION|east|east}
/40 ; TODO: consider adding relevant links: {ACTION|cros pool|cros pool}, {ACTION|g rope|g rope}, {ACTION|swin|swin}, {ACTION|thro hook|thro hook}, {ACTION|thro rope|thro rope}, {ACTION|u|u}, {ACTION|w|w}, {ACTION|x pool|x pool} ; sust(room_40//40)
You're in an underground cavern. To the west is an icy pool above which is a rocky protuberance. Beyond the pool you can see that the path continues. A steep path leads up out of the tree.\nExit: {ACTION|north|north}
/41 ; sust(room_41//41)
You are in an icy pool. A nearby thermometer reads 2@C - which explains why your legs and arms have cramped and you feel your- self drowning...
/42 ; TODO: consider adding relevant links: {ACTION|d|d}, {ACTION|g shee|g shee}, {ACTION|h|h}, {ACTION|lift shee|lift shee}, {ACTION|swin|swin}, {ACTION|x ledg|x ledg}, {ACTION|x shee|x shee} ; sust(room_42//42)
You are on a path which goes {ACTION|north|north}. A very steep path leads down to a flimsy-looking ledge&#59; and to the east is the icy pool.
/43 ; TODO: consider adding relevant links: {ACTION|h|h}, {ACTION|in|in}, {ACTION|open door|open door}, {ACTION|x door|x door} ; sust(room_43//43)
You are before the door of a big round building.\nExits: in and {ACTION|south|south}
/44 ; TODO: consider adding relevant links: {ACTION|u|u} ; sust(room_44//44)
You are on a rocky ledge. It is groaning under your weight and you realize that the end is upon you!\nExit: up
/45 ; TODO: consider adding relevant links: {ACTION|d|d}, {ACTION|drop|drop}, {ACTION|g hook|g hook}, {ACTION|pres hook|pres hook}, {ACTION|remo hook|remo hook} ; sust(room_45//45)
You're on top of the ladder, high above the floor.  Next to you is a grappling hook which is embedded in the rock.\nExit: down
/46 ; TODO: consider adding relevant links: {ACTION|jump|jump}, {ACTION|x ridg|x ridg} ; sust(room_46//46)
You are dangling high above the cave&#59; your fingers tenaciously gripping the crumbling ridge....
/47 ; TODO: consider adding relevant links: {ACTION|drop|drop}, {ACTION|x nich|x nich} ; sust(room_47//47)
You are atop a pile of rubble. Next to you is a small niche. Exit: {ACTION|down|down}
/48 ; TODO: consider adding relevant links: {ACTION|comp on|comp on}, {ACTION|e|e}, {ACTION|inse disc|inse disc}, {ACTION|on comp|on comp}, {ACTION|trip on|trip on}, {ACTION|x comp|x comp} ; sust(room_48//48)
You are in a small side room. It appears to be an office, and on the desk stands a computer. Exit: east
/49 ; TODO: consider adding relevant links: {ACTION|e|e}, {ACTION|o|o}, {ACTION|plug ear|plug ear}, {ACTION|pres butt|pres butt}, {ACTION|turn knob|turn knob}, {ACTION|u|u}, {ACTION|w|w}, {ACTION|x mach|x mach} ; sust(room_49//49)
You're in a large observatory. A telescope points at the ceiling, and next to it is the repelling machine, also pointing at the ceiling. High above your head, is a catwalk which is accessed by a narrow flight of steps. Exits: west, east, out and up
/50 ; TODO: consider adding relevant links: {ACTION|w|w}, {ACTION|x stov|x stov}, {ACTION|x wall|x wall} ; sust(room_50//50)
You are in a tiny kitchenette. There is a kettle and a hotplate against the wall - probably for the long nights of observing. Exit: west
/51 ; TODO: consider adding relevant links: {ACTION|d|d}, {ACTION|pres gree|pres gree}, {ACTION|pres red|pres red} ; sust(room_51//51)
You're on a narrow catwalk high above the observatory floor. You notice the ceiling is bisected, and next to you are two buttons: one red, the other green.\nExit: down
/52 ; TODO: consider adding relevant links: {ACTION|r behi|r behi}, {ACTION|smel plan|smel plan}, {ACTION|x gard|x gard}, {ACTION|x plan|x plan} ; sust(room_52//52)
You're in an underground herb garden. The aroma fills your nostrils with delight.\nExit: {ACTION|south|south}
/53 ; TODO: consider adding relevant links: {ACTION|star trac|star trac}, {ACTION|x burn|x burn} ; sust(room_53//53)
You are sitting on the driver's seat of the tractor. In front of you is the ignition but the key is missing.\nExit: {ACTION|down|down}
/54 ; sust(room_54//54)








/CON
/0 ; sust(room_0//0)

/1 ; sust(room_1//1)
e 2 ; sust(room_2/2)

/2 ; sust(room_2//2)
e 3 ; sust(room_3/3)
w 1 ; sust(room_1/1)

/3 ; sust(room_3//3)
w 2 ; sust(room_2/2)

/4 ; sust(room_4//4)
e 5 ; sust(room_5/5)

/5 ; sust(room_5//5)
e 6 ; sust(room_6/6)
n 26 ; sust(room_26/26)
s 15 ; sust(room_15/15)
w 4 ; sust(room_4/4)

/6 ; sust(room_6//6)
w 5 ; sust(room_5/5)

/7 ; sust(room_7//7)
e 8 ; sust(room_8/8)
ne 17 ; sust(room_17/17)
w 15 ; sust(room_15/15)

/8 ; sust(room_8//8)
e 9 ; sust(room_9/9)
w 7 ; sust(room_7/7)

/9 ; sust(room_9//9)
in 27 ; sust(room_27/27)
w 8 ; sust(room_8/8)

/10 ; sust(room_10//10)
w 11 ; sust(room_11/11)

/11 ; sust(room_11//11)
e 10 ; sust(room_10/10)
w 12 ; sust(room_12/12)

/12 ; sust(room_12//12)
e 11 ; sust(room_11/11)

/13 ; sust(room_13//13)
s 14 ; sust(room_14/14)

/14 ; sust(room_14//14)
n 13 ; sust(room_13/13)

/15 ; sust(room_15//15)
e 7 ; sust(room_7/7)
n 5 ; sust(room_5/5)
s 16 ; sust(room_16/16)

/16 ; sust(room_16//16)
n 15 ; sust(room_15/15)

/17 ; sust(room_17//17)
n 18 ; sust(room_18/18)
sw 7 ; sust(room_7/7)

/18 ; sust(room_18//18)
s 17 ; sust(room_17/17)

/19 ; sust(room_19//19)
e 18 ; sust(room_18/18)

/20 ; sust(room_20//20)
s 18 ; sust(room_18/18)

/21 ; sust(room_21//21)
e 24 ; sust(room_24/24)
n 22 ; sust(room_22/22)
w 25 ; sust(room_25/25)

/22 ; sust(room_22//22)
s 21 ; sust(room_21/21)

/23 ; sust(room_23//23)
ne 32 ; sust(room_32/32)
s 24 ; sust(room_24/24)

/24 ; sust(room_24//24)
n 23 ; sust(room_23/23)
w 21 ; sust(room_21/21)

/25 ; sust(room_25//25)
e 21 ; sust(room_21/21)

/26 ; sust(room_26//26)
e 26 ; sust(room_26/26)
n 26 ; sust(room_26/26)
ne 26 ; sust(room_26/26)
s 5 ; sust(room_5/5)
se 26 ; sust(room_26/26)
sw 26 ; sust(room_26/26)
w 26 ; sust(room_26/26)

/27 ; sust(room_27//27)
o 9 ; sust(room_9/9)

/28 ; sust(room_28//28)

/29 ; sust(room_29//29)

/30 ; sust(room_30//30)

/31 ; sust(room_31//31)
o 8 ; sust(room_8/8)

/32 ; sust(room_32//32)
sw 23 ; sust(room_23/23)

/33 ; sust(room_33//33)
w 37 ; sust(room_37/37)

/34 ; sust(room_34//34)

/35 ; sust(room_35//35)

/36 ; sust(room_36//36)

/37 ; sust(room_37//37)
e 33 ; sust(room_33/33)

/38 ; sust(room_38//38)
d 39 ; sust(room_39/39)

/39 ; sust(room_39//39)
e 37 ; sust(room_37/37)

/40 ; sust(room_40//40)
n 52 ; sust(room_52/52)

/41 ; sust(room_41//41)

/42 ; sust(room_42//42)
n 43 ; sust(room_43/43)

/43 ; sust(room_43//43)
s 42 ; sust(room_42/42)

/44 ; sust(room_44//44)

/45 ; sust(room_45//45)

/46 ; sust(room_46//46)

/47 ; sust(room_47//47)
d 10 ; sust(room_10/10)

/48 ; sust(room_48//48)

/49 ; sust(room_49//49)

/50 ; sust(room_50//50)

/51 ; sust(room_51//51)

/52 ; sust(room_52//52)
s 40 ; sust(room_40/40)

/53 ; sust(room_53//53)
d 19 ; sust(room_19/19)

/54 ; sust(room_54//54)







/OBJ

/0  252      1    _    _       10000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_0_lamp//0) ; sust(room_noncreated/252) ; sust(ght/ATTR)
/1          22      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_1_lamp//1) ; sust(room_22/22) ; sust(ATTR/ATTR)
/2       252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_2//2) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/3       252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_3//3) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/4  252      1 padd _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_4_padd//4) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/5   252      1 box  _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_5_box//5) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/6   252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_6_box//6) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/7       252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_7//7) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/8  252      1 galo _       01000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_8_galo//8) ; sust(room_noncreated/252) ; sust(arable/ATTR)
/9  252      1 coin _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_9_coin//9) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/10 252      1 nail _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_10_nail//10) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/11 252      1 need _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_11_need//11) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/12 252      1 comp _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_12_comp//12) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/13              18      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_13//13) ; sust(room_18/18) ; sust(ATTR/ATTR)
/14         25      1 trou _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_14_trou//14) ; sust(room_25/25) ; sust(ATTR/ATTR)
/15      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_15//15) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/16         19      1 maga _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_16_maga//16) ; sust(room_19/19) ; sust(ATTR/ATTR)
/17 252      1 scre _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_17_scre//17) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/18          24      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_18_jar//18) ; sust(room_24/24) ; sust(ATTR/ATTR)
/19  252      1 bar  _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_19_bar//19) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/20      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_20//20) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/21      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_21//21) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/22  252      1 rod  _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_22_rod//22) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/23      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_23//23) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/24         16      1 barr _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_24_barr//24) ; sust(room_16/16) ; sust(ATTR/ATTR)
/25              14      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_25//25) ; sust(room_14/14) ; sust(ATTR/ATTR)
/26 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_26_noti//26) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/27 252      1 dagg _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_27_dagg//27) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/28              39      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_28//28) ; sust(room_39/39) ; sust(ATTR/ATTR)
/29      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_29//29) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/30              36      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_30//30) ; sust(room_36/36) ; sust(ATTR/ATTR)
/31 252      1 para _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_31_para//31) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/32              29      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_32//32) ; sust(room_29/29) ; sust(ATTR/ATTR)
/33 252      1 spad _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_33_spad//33) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/34 252      1 rope _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_34_rope//34) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/35      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_35//35) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/36      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_36//36) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/37              34      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_37//37) ; sust(room_34/34) ; sust(ATTR/ATTR)
/38 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_38_rope//38) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/39 252      1 bone _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_39_bone//39) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/40      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_40//40) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/41              10      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_41//41) ; sust(room_10/10) ; sust(ATTR/ATTR)
/42      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_42//42) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/43              37      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_43//43) ; sust(room_37/37) ; sust(ATTR/ATTR)
/44          38      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_44_egg//44) ; sust(room_38/38) ; sust(ATTR/ATTR)
/45      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_45//45) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/46      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_46//46) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/47 252      1 disc _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_47_disc//47) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/48      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_48//48) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/49         44      1 shee _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_49_shee//49) ; sust(room_44/44) ; sust(ATTR/ATTR)
/50         32      1 broo _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_50_broo//50) ; sust(room_32/32) ; sust(ATTR/ATTR)
/51 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_51_lift//51) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/52      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_52//52) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/53      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_53//53) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/54         48      1 book _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_54_book//54) ; sust(room_48/48) ; sust(ATTR/ATTR)
/55      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_55//55) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/56 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_56_cott//56) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/57 252      1 plie _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_57_plie//57) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/58      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_58//58) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/59              24      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_59//59) ; sust(room_24/24) ; sust(ATTR/ATTR)
/60      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_60//60) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/61      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_61//61) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/62      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_62//62) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/63          1      1 scro _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_63_scro//63) ; sust(room_1/1) ; sust(ATTR/ATTR)
/64 252      1 spic _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_64_spic//64) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/65  252      1 key  _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_65_key//65) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/66      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_66//66) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/67 252      1 glov _       01000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_67_glov//67) ; sust(room_noncreated/252) ; sust(arable/ATTR)
/68              52      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_68//68) ; sust(room_52/52) ; sust(ATTR/ATTR)
/69              13      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_69//69) ; sust(room_13/13) ; sust(ATTR/ATTR)
/70              12      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_70//70) ; sust(room_12/12) ; sust(ATTR/ATTR)
/71 252      1 hook _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_71_hook//71) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/72              49      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_72//72) ; sust(room_49/49) ; sust(ATTR/ATTR)






/PRO 0








_ _
 hook    "RESPONSE_START"


_ _
 hook    "RESPONSE_USER"

n _
 at      18 ; sust(room_18/18)
 absent  15 ; sust(object_15/15)
 writeln "The path is on the other side of the locked gate."
 done

n _
 at      18 ; sust(room_18/18)
 present 15 ; sust(object_15/15)
 goto    20 ; sust(room_20/20)
 desc

n _
 at      33 ; sust(room_33/33)
 let     7 4 ; sust(countdown_player_input_3/7)
 goto    34 ; sust(room_34/34)
 desc

s _
 at      34 ; sust(room_34/34)
 clear   7 ; sust(countdown_player_input_3/7)
 goto    33 ; sust(room_33/33)
 desc

e _
 at      35 ; sust(room_35/35)
 absent  29 ; sust(object_29/29)
 writeln "OOPS! Stalagmites pierce your feet and you start to bleed to death."
 score
 end

e _
 at      35 ; sust(room_35/35)
 present 29 ; sust(object_29/29)
 writeln "The ladder's rungs protect your feet from the lethal stalagmites"
 place   29 36 ; sust(object_29/29) ; sust(room_36/36)
 goto    36 ; sust(room_36/36)
 anykey
 desc

e _
 present 37 ; sust(object_37/37)
 writeln "You can't do that...yet."
 done

e _
 at      34 ; sust(room_34/34)
 absent  37 ; sust(object_37/37)
 goto    35 ; sust(room_35/35)
 set     0 ; sust(yesno_is_dark/0)
 let     10 3 ; sust(countdown_player_input_unlit/10)
 desc

e _
 at      34 ; sust(room_34/34)
 present 37 ; sust(object_37/37)
 writeln "Don't be crazy!"
 done

e _
 at      13 ; sust(room_13/13)
 present 35 ; sust(object_35/35)
 place   35 12 ; sust(object_35/35) ; sust(room_12/12)
 goto    12 ; sust(room_12/12)
 desc

e _
 at      48 ; sust(room_48/48)
 present 72 ; sust(object_72/72)
 place   72 49 ; sust(object_72/72) ; sust(room_49/49)
 goto    49 ; sust(room_49/49)
 desc

e _
 at      48 ; sust(room_48/48)
 goto    49 ; sust(room_49/49)
 desc

e _
 at      49 ; sust(room_49/49)
 present 72 ; sust(object_72/72)
 place   72 50 ; sust(object_72/72) ; sust(room_50/50)
 goto    50 ; sust(room_50/50)
 desc

e _
 at      49 ; sust(room_49/49)
 goto    50 ; sust(room_50/50)
 desc

w _
 at      18 ; sust(room_18/18)
 absent  15 ; sust(object_15/15)
 writeln "The path is on the other side of the locked gate."
 done

w _
 present 15 ; sust(object_15/15)
 goto    19 ; sust(room_19/19)
 desc

w _
 at      36 ; sust(room_36/36)
 absent  29 ; sust(object_29/29)
 writeln "OOPS! Stalagmites pierce your feet and you start to bleed to death."
 score
 end

w _
 at      36 ; sust(room_36/36)
 present 29 ; sust(object_29/29)
 writeln "The ladder's rungs protect your feet from the lethal stalagmites"
 place   29 35 ; sust(object_29/29) ; sust(room_35/35)
 goto    35 ; sust(room_35/35)
 anykey
 desc

w _
 at      12 ; sust(room_12/12)
 present 35 ; sust(object_35/35)
 goto    13 ; sust(room_13/13)
 place   35 13 ; sust(object_35/35) ; sust(room_13/13)
 desc

w _
 at      12 ; sust(room_12/12)
 absent  35 ; sust(object_35/35)
 writeln "You can't do that...yet."
 done

w _
 at      37 ; sust(room_37/37)
 present 43 ; sust(object_43/43)
 writeln "The guard squares his shoulders and gives you an icy stare."
 done

w _
 at      37 ; sust(room_37/37)
 absent  43 ; sust(object_43/43)
 goto    39 ; sust(room_39/39)
 desc

w _
 at      35 ; sust(room_35/35)
 goto    34 ; sust(room_34/34)
 clear   0 ; sust(yesno_is_dark/0)
 clear   10 ; sust(countdown_player_input_unlit/10)
 desc

w _
 at      40 ; sust(room_40/40)
 writeln "How ?"
 done

w _
 at      49 ; sust(room_49/49)
 present 72 ; sust(object_72/72)
 place   72 48 ; sust(object_72/72) ; sust(room_48/48)
 goto    48 ; sust(room_48/48)
 desc

w _
 at      49 ; sust(room_49/49)
 goto    48 ; sust(room_48/48)
 desc

w _
 at      50 ; sust(room_50/50)
 present 72 ; sust(object_72/72)
 place   72 49 ; sust(object_72/72) ; sust(room_49/49)
 goto    49 ; sust(room_49/49)
 desc

w _
 at      50 ; sust(room_50/50)
 goto    49 ; sust(room_49/49)
 desc

w _
 at      1 ; sust(room_1/1)
 writeln "\nstOh dear, you misjudge the spee d of an oncoming Porsche and you                 are sent into ob livion!"
 score
 end

nw _
 at      26 ; sust(room_26/26)
 notcarr 12 ; sust(object_12_comp/12)
 writeln "You'll get lost if you do."
 pause   100
 desc

nw _
 at      26 ; sust(room_26/26)
 carried 12 ; sust(object_12_comp/12)
 writeln "With the aid of the compass, you manage to get through the forest and find yourself on the shore of a large lake."
 anykey
 goto    29 ; sust(room_29/29)
 desc

se _
 at      29 ; sust(room_29/29)
 notcarr 12 ; sust(object_12_comp/12)
 writeln "You'll get lost if you do."
 pause   100
 desc

se _
 at      29 ; sust(room_29/29)
 carried 12 ; sust(object_12_comp/12)
 writeln "Using the compass to help you, you retrace your steps back to the outskirts of the forest."
 goto    26 ; sust(room_26/26)
 anykey
 desc

u _
 at      28 ; sust(room_28/28)
 goto    6 ; sust(room_6/6)
 desc

u _
 at      40 ; sust(room_40/40)
 clear   0 ; sust(yesno_is_dark/0)
 goto    39 ; sust(room_39/39)
 desc

u _
 at      44 ; sust(room_44/44)
 clear   8 ; sust(countdown_player_input_4/8)
 goto    42 ; sust(room_42/42)
 desc

u _
 at      49 ; sust(room_49/49)
 present 52 ; sust(object_52/52)
 absent  72 ; sust(object_72/72)
 goto    51 ; sust(room_51/51)
 place   52 51 ; sust(object_52/52) ; sust(room_51/51)
 desc

u _
 at      49 ; sust(room_49/49)
 absent  52 ; sust(object_52/52)
 present 72 ; sust(object_72/72)
 place   72 51 ; sust(object_72/72) ; sust(room_51/51)
 goto    51 ; sust(room_51/51)
 desc

u _
 at      49 ; sust(room_49/49)
 absent  52 ; sust(object_52/52)
 absent  72 ; sust(object_72/72)
 goto    51 ; sust(room_51/51)
 desc

d _
 at      6 ; sust(room_6/6)
 notworn 8 ; sust(object_8_galo/8)
 writeln "You slip on the oily steps. With a severely bruised ego, you climb back out."
 anykey
 desc

d _
 at      6 ; sust(room_6/6)
 worn    8 ; sust(object_8_galo/8)
 goto    28 ; sust(room_28/28)
 writeln "The galoshes prevent you from slipping as you enter the pit."
 anykey
 desc

d _
 at      45 ; sust(room_45/45)
 goto    36 ; sust(room_36/36)
 desc

d _
 at      42 ; sust(room_42/42)
 let     8 2 ; sust(countdown_player_input_4/8)
 goto    44 ; sust(room_44/44)
 desc

d _
 at      51 ; sust(room_51/51)
 present 52 ; sust(object_52/52)
 place   52 49 ; sust(object_52/52) ; sust(room_49/49)
 goto    49 ; sust(room_49/49)
 desc

d _
 at      51 ; sust(room_51/51)
 absent  52 ; sust(object_52/52)
 present 72 ; sust(object_72/72)
 place   72 49 ; sust(object_72/72) ; sust(room_49/49)
 goto    49 ; sust(room_49/49)
 desc

d _
 at      51 ; sust(room_51/51)
 absent  52 ; sust(object_52/52)
 absent  72 ; sust(object_72/72)
 goto    49 ; sust(room_49/49)
 desc

in cano
 at      3 ; sust(room_3/3)
 absent  2 ; sust(object_2/2)
 writeln "stYou can't do that yet, it's on                 the far side of the dam."
 done

in cano
 present 2 ; sust(object_2/2)
 writeln "stEntering the canoe, you paddle                 to the other sid e...                            "
 destroy 2 ; sust(object_2/2)
 goto    4 ; sust(room_4/4)
 anykey
 writeln "The canoe hits a hidden rock and sinks. You leap onto the bank just in time."
 anykey
 desc

in foun
 at      4 ; sust(room_4/4)
 writeln "Don't be crazy!"
 done

in tree
 present 46 ; sust(object_46/46)
 writeln "You enter the trunk, and stumble down a very steep path to an underground cavern."
 set     0 ; sust(yesno_is_dark/0)
 anykey
 goto    40 ; sust(room_40/40)
 desc

in car
 at      1 ; sust(room_1/1)
 writeln "What for, you've just started?!"
 done

in trac
 at      19 ; sust(room_19/19)
 goto    53 ; sust(room_53/53)
 desc

in hut
 at      37 ; sust(room_37/37)
 writeln "Why ?"
 done

in coor
 present 55 ; sust(object_55/55)
 absent  58 ; sust(object_58/58)
 writeln "Could you put that another way ?"
 done

in shaf
 present 69 ; sust(object_69/69)
 writeln "stYou can't - it's far too narro w."
 done

in _
 at      20 ; sust(room_20/20)
 present 13 ; sust(object_13/13)
 writeln "The farmer says you may only enter when you find his missing key."
 done

in _
 at      20 ; sust(room_20/20)
 absent  13 ; sust(object_13/13)
 goto    21 ; sust(room_21/21)
 desc

in _
 at      8 ; sust(room_8/8)
 absent  20 ; sust(object_20/20)
 writeln "There's a strong chain holding the door closed."
 done

in _
 at      8 ; sust(room_8/8)
 present 20 ; sust(object_20/20)
 goto    31 ; sust(room_31/31)
 desc

in _
 at      11 ; sust(room_11/11)
 clear   0 ; sust(yesno_is_dark/0)
 clear   10 ; sust(countdown_player_input_unlit/10)
 goto    30 ; sust(room_30/30)
 desc

in _
 at      39 ; sust(room_39/39)
 present 46 ; sust(object_46/46)
 writeln "Please be more specific..."
 done

in _
 at      43 ; sust(room_43/43)
 absent  66 ; sust(object_66/66)
 writeln "You can't do that...yet."
 done

in _
 at      43 ; sust(room_43/43)
 present 66 ; sust(object_66/66)
 goto    49 ; sust(room_49/49)
 desc

o _
 at      21 ; sust(room_21/21)
 carried 14 ; sust(object_14_trou/14)
 notcarr 9 ; sust(object_9_coin/9)
 writeln "&#34;Oy, I didn't say you could take me trousers,&#34; roars the farmer taking them from you. Sheepishly you leave."
 destroy 14 ; sust(object_14_trou/14)
 goto    20 ; sust(room_20/20)
 anykey
 desc

o _
 at      21 ; sust(room_21/21)
 carried 9 ; sust(object_9_coin/9)
 carried 14 ; sust(object_14_trou/14)
 writeln "&#34;Oy, I didn't say you could take me trousers,&#34; roars the farmer taking them from you. Sheepishly you leave."
 writeln "st&#34;And you can return my pound,&#34;                 he scowls, takin g it from you."
 destroy 9 ; sust(object_9_coin/9)
 destroy 14 ; sust(object_14_trou/14)
 goto    20 ; sust(room_20/20)
 anykey
 desc

o _
 at      21 ; sust(room_21/21)
 goto    20 ; sust(room_20/20)
 desc

o _
 at      30 ; sust(room_30/30)
 set     0 ; sust(yesno_is_dark/0)
 let     10 3 ; sust(countdown_player_input_unlit/10)
 goto    11 ; sust(room_11/11)
 desc

o _
 at      49 ; sust(room_49/49)
 set     0 ; sust(yesno_is_dark/0)
 let     10 3 ; sust(countdown_player_input_unlit/10)
 goto    43 ; sust(room_43/43)
 desc

wave _
 present 3 ; sust(object_3/3)
 create  2 ; sust(object_2/2)
 destroy 3 ; sust(object_3/3)
 create  4 ; sust(object_4_padd/4)
 get     4 ; sust(object_4_padd/4)
 writeln "The man waves back, climbs into the canoe and paddles across. He gives you the paddle and wishes you luck with your quest."
 plus    30 1 ; sust(total_player_score/30)
 anykey
 desc

wave _
 absent  3 ; sust(object_3/3)
 writeln "Why ?"
 done

x cano
 present 2 ; sust(object_2/2)
 zero    20 ; sust(game_flag_20/20)
 writeln "Its mooring rope dangles in the water."
 done

x cano
 at      3 ; sust(room_3/3)
 absent  2 ; sust(object_2/2)
 writeln "stYou can't do that yet, it's on                 the far side of the dam."
 done

x foun
 at      4 ; sust(room_4/4)
 writeln "Cascading water over the tiers is almost mesmerizing."
 done

x wate
 at      4 ; sust(room_4/4)
 eq      111 10 ; sust(game_flag_11/111)
 let     111 100 ; sust(game_flag_11/111)
 create  5 ; sust(object_5_box/5)
 writeln "Floating in the water is a soggy box of matches."
 pause   200
 plus    30 1 ; sust(total_player_score/30)
 desc

x wate
 at      4 ; sust(room_4/4)
 eq      111 100 ; sust(game_flag_11/111)
 writeln "You've done that."
 done

x box
 carried 5 ; sust(object_5_box/5)
 writeln "stWet and soggy."
 done

x box
 carried 6 ; sust(object_6_box/6)
 writeln "Nice and dry."
 done

x corn
 at      32 ; sust(room_32/32)
 writeln "It's just what it says it is...."
 done

x pit
 at      6 ; sust(room_6/6)
 writeln "You can't do that...yet."
 done

x pit
 at      28 ; sust(room_28/28)
 writeln "It's just what it says it is...."
 done

x coin
 carried 9 ; sust(object_9_coin/9)
 writeln "Standard Royal Mint issue."
 done

x comp
 present 55 ; sust(object_55/55)
 writeln "On-screen instructions instruct you to enter the coordinates thus:-\n\nstLeave one space between the tw o coordinates  e.g. 123N 123E"
 done

x comp
 at      48 ; sust(room_48/48)
 gt      24 199 ; sust(game_flag_24/24)
 lt      24 255 ; sust(game_flag_24/24)
 writeln "The lit screen is blank."
 done

x comp
 at      48 ; sust(room_48/48)
 lt      24 200 ; sust(game_flag_24/24)
 writeln "It hasn't been switched on yet."
 done

x comp
 carried 12 ; sust(object_12_comp/12)
 writeln "Suitable for getting you through a forest."
 done

x man
 atgt    17 ; sust(room_17/17)
 atlt    21 ; sust(room_21/21)
 present 13 ; sust(object_13/13)
 writeln "He's holding a reel from a fish- ing rod in his hand, which he seems to be mending."
 done

x maga
 carried 16 ; sust(object_16_maga/16)
 writeln "It's titled: &#34;FISHING DIGEST&#34;. At the top left hand corner is a sketch of a reddish-coloured fish."
 done

x scre
 present 17 ; sust(object_17_scre/17)
 writeln "You really want a description?"
 done

x pock
 carried 14 ; sust(object_14_trou/14)
 zero    114 ; sust(game_flag_14/114)
 let     114 100 ; sust(game_flag_14/114)
 create  9 ; sust(object_9_coin/9)
 writeln "You've found something..."
 pause   75
 plus    30 3 ; sust(total_player_score/30)
 desc

x pock
 carried 14 ; sust(object_14_trou/14)
 eq      114 100 ; sust(game_flag_14/114)
 writeln "You've done that."
 done

x ward
 at      25 ; sust(room_25/25)
 writeln "It's locked."
 done

x trou
 carried 14 ; sust(object_14_trou/14)
 writeln "They belong to the farmer."
 done

x lamp
 present 1 ; sust(object_1_lamp/1)
 gt      29 0 ; sust(bitset_graphics_status/29)
 writeln "A small tag tells you that there is only a little paraffin in the lamp."
 done

x lamp
 present 0 ; sust(object_0_lamp/0)
 gt      29 0 ; sust(bitset_graphics_status/29)
 writeln "A small tag tells you that there is only a little paraffin in the lamp."
 done

x lamp
 present 1 ; sust(object_1_lamp/1)
 eq      29 0 ; sust(bitset_graphics_status/29)
 writeln "It's empty."
 done

x drye
 at      23 ; sust(room_23/23)
 writeln "Has a hinged front door through which clothing is placed to dry."
 done

x door
 at      12 ; sust(room_12/12)
 absent  35 ; sust(object_35/35)
 writeln "A small heap of soil on the other side stops it opening wide enough to enter. The gap is a mere few inches."
 done

x door
 at      43 ; sust(room_43/43)
 absent  66 ; sust(object_66/66)
 writeln "It needs a key."
 done

x fireplace ; put
 at      24 ; sust(room_24/24)
 zero    112 ; sust(game_flag_12/112)
 let     112 100 ; sust(game_flag_12/112)
 create  19 ; sust(object_19_bar/19)
 writeln "Standing alongside the fireplace is a crowbar."
 plus    30 1 ; sust(total_player_score/30)
 anykey
 desc

x fireplace ; put
 at      24 ; sust(room_24/24)
 eq      112 100 ; sust(game_flag_12/112)
 writeln "You've done that."
 done

x tabl
 at      24 ; sust(room_24/24)
 writeln "&#34;Stop fiddling about with my table.&#34; complains the wife."
 done

x jar
 carried 18 ; sust(object_18_jar/18)
 lt      19 220 ; sust(game_flag_19/19)
 writeln "A label states &#34;BEST QUALITY&#34;   {CLASS|center|           VACUUM SEALED}           Break vacuum to open."
 done

x jar
 carried 18 ; sust(object_18_jar/18)
 eq      19 220 ; sust(game_flag_19/19)
 writeln "A label states &#34;BEST QUALITY&#34;   {CLASS|center|           VACUUM SEALED}           Break vacuum to open."
 writeln "The lid has been pierced and removed."
 done

x chai
 at      8 ; sust(room_8/8)
 absent  20 ; sust(object_20/20)
 writeln "Looks very strong."
 done

x cupb
 present 21 ; sust(object_21/21)
 eq      115 50 ; sust(game_flag_15/115)
 let     115 100 ; sust(game_flag_15/115)
 create  22 ; sust(object_22_rod/22)
 writeln "You've found something..."
 plus    30 1 ; sust(total_player_score/30)
 pause   75
 desc

x cupb
 present 21 ; sust(object_21/21)
 eq      115 100 ; sust(game_flag_15/115)
 writeln "You've done that."
 done

x cupb
 at      32 ; sust(room_32/32)
 absent  21 ; sust(object_21/21)
 writeln "Closed with a simple lock."
 done

x rod
 carried 22 ; sust(object_22_rod/22)
 writeln "About 30cm long and has a 2cm, square end."
 done

x barr
 present 24 ; sust(object_24_barr/24)
 writeln "stSmall and light."
 done

x need
 carried 11 ; sust(object_11_need/11)
 writeln "There is an stN at the end, and a 2mm hole underneath."
 done

x nail
 carried 10 ; sust(object_10_nail/10)
 writeln "It is a 2mm nail."
 done

x nail
 at      14 ; sust(room_14/14)
 present 25 ; sust(object_25/25)
 writeln "It is a 2mm nail."
 done

x dagg
 carried 27 ; sust(object_27_dagg/27)
 writeln "Very sharp and pointed."
 done

x ladd
 present 28 ; sust(object_28/28)
 writeln "It's just what it says it is...."
 done

x tree
 at      39 ; sust(room_39/39)
 writeln "stGiving the tree a sound tap, y oudeduce (correctly) that it is hollow."
 done

x para
 present 31 ; sust(object_31_para/31)
 writeln "A sunshade very much like an umbrella."
 done

x ridg
 at      46 ; sust(room_46/46)
 eq      18 50 ; sust(game_flag_18/18)
 let     18 100 ; sust(game_flag_18/18)
 create  31 ; sust(object_31_para/31)
 writeln "You've found something..."
 plus    30 3 ; sust(total_player_score/30)
 pause   75
 desc

x ridg
 at      46 ; sust(room_46/46)
 eq      18 100 ; sust(game_flag_18/18)
 writeln "You've done that."
 done

x raft
 present 32 ; sust(object_32/32)
 writeln "stUnlikely to fall apart or sink !"
 done

x rubb
 present 41 ; sust(object_41/41)
 writeln "It's just a tall heap of rubble which appears worthless."
 done

x spad
 present 33 ; sust(object_33_spad/33)
 writeln "It's just what it says it is...."
 done

x rope
 at      29 ; sust(room_29/29)
 eq      21 100 ; sust(game_flag_21/21)
 writeln "It has been cut through, except for one last resistant strand."
 done

x rope
 carried 34 ; sust(object_34_rope/34)
 writeln "It's just what it says it is...."
 done

x dog
 present 40 ; sust(object_40/40)
 writeln "The adventurer's best friend!"
 done

x fire
 at      24 ; sust(room_24/24)
 zero    112 ; sust(game_flag_12/112)
 let     112 100 ; sust(game_flag_12/112)
 create  19 ; sust(object_19_bar/19)
 writeln "Standing alongside the fireplace is a crowbar."
 plus    30 1 ; sust(total_player_score/30)
 anykey
 desc

x fire
 at      24 ; sust(room_24/24)
 eq      112 100 ; sust(game_flag_12/112)
 writeln "You've done that."
 done

x bear
 present 37 ; sust(object_37/37)
 writeln "Big and mean."
 done

x bone
 present 39 ; sust(object_39_bone/39)
 writeln "Not much meat on it."
 done

x ignition
 at      53 ; sust(room_53/53)
 writeln "It needs a key."
 done

x nich
 at      47 ; sust(room_47/47)
 eq      20 100 ; sust(game_flag_20/20)
 set     20 ; sust(game_flag_20/20)
 create  67 ; sust(object_67_glov/67)
 writeln "You've found something..."
 plus    30 3 ; sust(total_player_score/30)
 pause   75
 desc

x nich
 at      47 ; sust(room_47/47)
 eq      20 255 ; sust(game_flag_20/20)
 writeln "You've done that."
 done

x guar
 present 43 ; sust(object_43/43)
 writeln "Nothing less than a Sherman tank will get past him!"
 done

x egg
 present 44 ; sust(object_44_egg/44)
 writeln "Would make a nice omelette."
 done

x bark
 at      39 ; sust(room_39/39)
 writeln "stIt seems to be movable."
 done

x till
 at      16 ; sust(room_16/16)
 writeln "Ancient. Mounted on thick stubby legs, and is stuck on 4/7&#124;d."
 done

x disc
 carried 47 ; sust(object_47_disc/47)
 writeln "For booting up a computer."
 done

x shee
 at      42 ; sust(room_42/42)
 zero    23 ; sust(game_flag_23/23)
 writeln "It has what appears to be some coordinates written on it."
 done

x shee
 present 49 ; sust(object_49_shee/49)
 writeln "It has what appears to be some coordinates written on it."
 done

x broo
 present 50 ; sust(object_50_broo/50)
 writeln "It has two holes: one near the bottom, and the other dead centre at the bottom.  Perhaps something should go in one hole and out the other..."
 done

x lifter
 present 51 ; sust(object_51_lift/51)
 writeln "Has a long nail sticking out the bottom."
 done

x lifter
 at      9 ; sust(room_9/9)
 writeln "The &#34;up and down&#34; variety."
 done

x ledg
 at      42 ; sust(room_42/42)
 zero    23 ; sust(game_flag_23/23)
 writeln "stOn it lies a sheet of paper."
 done

x tripswitch
 present 53 ; sust(object_53/53)
 writeln "Old and rusty. Looks decidedly unsafe, especially for the naked hand."
 done

x wall
 at      50 ; sust(room_50/50)
 zero    24 ; sust(game_flag_24/24)
 let     24 100 ; sust(game_flag_24/24)
 create  53 ; sust(object_53/53)
 writeln "You've found something..."
 plus    30 2 ; sust(total_player_score/30)
 pause   75
 desc

x wall
 at      50 ; sust(room_50/50)
 eq      24 100 ; sust(game_flag_24/24)
 writeln "You've done that."
 done

x mach
 at      49 ; sust(room_49/49)
 writeln "It's the infamous Repelling-Beam Machine. On its side is a bright red button and a fine-adjustment knob."
 done

x pot
 at      24 ; sust(room_24/24)
 writeln "It contains a stew and is fragrant with many spices."
 done

x patt
 at      39 ; sust(room_39/39)
 writeln "There are many scuff marks on the pattern, intimating that the tree is not your normal variety!"
 done

x car
 at      1 ; sust(room_1/1)
 writeln "Dirty: needs a good wash."
 done

x plie
 carried 57 ; sust(object_57_plie/57)
 writeln "The &#34;long nose&#34; sort.  Can reach where your fingers can't."
 done

x scro
 present 63 ; sust(object_63_scro/63)
 writeln "Seems to be some form of papyrus"
 done

x trac
 at      19 ; sust(room_19/19)
 writeln "It seems to fill the whole shed. Its wheels are huge, giving it plenty of clearance over bumpy terrain. There is a foothold to facilitate climbing onto the tractor."
 done

x pool
 at      40 ; sust(room_40/40)
 writeln "A nearby thermometer reads 2@C."
 done

x gard
 at      52 ; sust(room_52/52)
 writeln "You see many different herbs\nall neatly labelled."
 done

x spic
 carried 64 ; sust(object_64_spic/64)
 writeln "It would enhance any respectable dish."
 done

x key
 carried 65 ; sust(object_65_key/65)
 writeln "It's marked &#34;OBSERVATORY&#34;"
 done

x gene
 at      31 ; sust(room_31/31)
 lt      115 255 ; sust(game_flag_15/115)
 writeln "Powerful enough to drive a lift"
 writeln "but it is off at present."
 done

x gene
 at      31 ; sust(room_31/31)
 eq      115 255 ; sust(game_flag_15/115)
 writeln "Powerful enough to drive a lift"
 writeln "and it is now working."
 done

x hole
 present 7 ; sust(object_7/7)
 writeln "It's just what it says it is...."
 done

x plan
 at      52 ; sust(room_52/52)
 writeln "It is very fragrant."
 done

x stov
 at      50 ; sust(room_50/50)
 writeln "stFor use during the long wee hours of work."
 done

x shaf
 present 69 ; sust(object_69/69)
 writeln "Just wide enough for the dog."
 done

x stal
 atgt    34 ; sust(room_34/34)
 atlt    37 ; sust(room_37/37)
 writeln "stPointed and sharp&#59; they could easily puncture your shoes and pierce the soles of your feet."
 done

x sket
 carried 16 ; sust(object_16_maga/16)
 writeln "It's just what it says it is...."
 done

x galo
 present 8 ; sust(object_8_galo/8)
 writeln "Made of supple rubber."
 done

x cott
 carried 56 ; sust(object_56_cott/56)
 writeln "The farmer used them to exclude unwanted noise."
 done

x glov
 present 67 ; sust(object_67_glov/67)
 writeln "Something's going right: it's your size!"
 done

x _
 writeln "Nothing further is revealed."
 done

p _
 writeln "Old Father Time taps his foot patiently..."
 done

sear corn
 at      32 ; sust(room_32/32)
 zero    27 ; sust(game_flag_27/27)
 let     27 100 ; sust(game_flag_27/27)
 create  33 ; sust(object_33_spad/33)
 writeln "You've found something..."
 plus    30 2 ; sust(total_player_score/30)
 pause   75
 desc

sear corn
 at      32 ; sust(room_32/32)
 eq      27 100 ; sust(game_flag_27/27)
 writeln "You've done that."
 done

sear pit
 at      28 ; sust(room_28/28)
 eq      27 220 ; sust(game_flag_27/27)
 set     27 ; sust(game_flag_27/27)
 create  57 ; sust(object_57_plie/57)
 writeln "You've found something..."
 plus    30 2 ; sust(total_player_score/30)
 pause   75
 desc

sear pit
 at      28 ; sust(room_28/28)
 eq      27 255 ; sust(game_flag_27/27)
 writeln "You've done that."
 done

sear pock
 carried 14 ; sust(object_14_trou/14)
 zero    114 ; sust(game_flag_14/114)
 let     114 100 ; sust(game_flag_14/114)
 create  9 ; sust(object_9_coin/9)
 writeln "You've found something..."
 pause   75
 plus    30 3 ; sust(total_player_score/30)
 desc

sear pock
 carried 14 ; sust(object_14_trou/14)
 eq      114 100 ; sust(game_flag_14/114)
 writeln "You've done that."
 done

sear _
 writeln "Nothing further is revealed."
 done

on comp
 at      48 ; sust(room_48/48)
 writeln "Could you put that another way ?"
 done

on comp
 at      48 ; sust(room_48/48)
 lt      24 200 ; sust(game_flag_24/24)
 writeln "Could you put that another way ?"
 done

open gate
 at      18 ; sust(room_18/18)
 absent  15 ; sust(object_15/15)
 present 13 ; sust(object_13/13)
 writeln "&#34;Oy, I didn't say you could come onto my property,&#34; cries the farmer.  You flee as he aims a shotgun at you."
 goto    4 ; sust(room_4/4)
 anykey
 desc

open maga
 carried 16 ; sust(object_16_maga/16)
 writeln "stNo thanks, looks too boring."
 done

open ward
 at      25 ; sust(room_25/25)
 writeln "Adventures should be SO simple!"
 done

open door
 at      8 ; sust(room_8/8)
 absent  20 ; sust(object_20/20)
 writeln "How ?"
 done

open door
 at      12 ; sust(room_12/12)
 absent  35 ; sust(object_35/35)
 writeln "How ?"
 done

open door
 at      12 ; sust(room_12/12)
 absent  35 ; sust(object_35/35)
 writeln "How ?"
 done

open door
 at      43 ; sust(room_43/43)
 absent  66 ; sust(object_66/66)
 notcarr 65 ; sust(object_65_key/65)
 writeln "With what ?"
 done

open door
 at      43 ; sust(room_43/43)
 absent  66 ; sust(object_66/66)
 carried 65 ; sust(object_65_key/65)
 create  66 ; sust(object_66/66)
 writeln "You insert the key in the lock and turn it.  The door creaks open...."

 anykey
 desc

open jar
 carried 18 ; sust(object_18_jar/18)
 zero    17 ; sust(game_flag_17/17)
 writeln "How ?"
 done

open cupb
 at      32 ; sust(room_32/32)
 absent  21 ; sust(object_21/21)
 let     115 1 ; sust(game_flag_15/115)
 writeln "With what ?"
 done

open para
 carried 31 ; sust(object_31_para/31)
 let     18 150 ; sust(game_flag_18/18)
 plus    30 3 ; sust(total_player_score/30)
 ok

open till
 at      16 ; sust(room_16/16)
 writeln "Nothing further is revealed."
 done

talk man
 at      20 ; sust(room_20/20)
 present 13 ; sust(object_13/13)
 writeln "The farmer says you may only enter when you find his missing key."
 done

talk man
 at      18 ; sust(room_18/18)
 present 13 ; sust(object_13/13)
 writeln "The farmer seems not to have heard you."
 done

talk man
 at      3 ; sust(room_3/3)
 present 3 ; sust(object_3/3)
 writeln "You can't do that...yet."
 done

talk guar
 present 43 ; sust(object_43/43)
 eq      114 100 ; sust(game_flag_14/114)
 writeln "&#34;Are you the one who's going to save London?&#34; he asks.  Seeing you nod humbly he goes on:\n&#34;It's usually `2 to pass through here, but Zenobi travellers can go through for a quid.&#34;"
 plus    114 10 ; sust(game_flag_14/114)
 anykey
 desc

talk guar
 present 43 ; sust(object_43/43)
 eq      114 110 ; sust(game_flag_14/114)
 writeln "&#34;Chatty sort, aren't ya?&#34; says the guard. &#34;`1 or stay here...&#34;"
 done

talk wife
 present 59 ; sust(object_59/59)
 writeln "She ignores you and gets on with her chores."
 done

talk loud
 at      18 ; sust(room_18/18)
 present 13 ; sust(object_13/13)
 create  15 ; sust(object_15/15)
 create  56 ; sust(object_56_cott/56)
 place   13 20 ; sust(object_13/13) ; sust(room_20/20)
 writeln "&#34;Ah, now I can hear you,&#34; says the farmer removing some pieces of cotton wool from his ears. &#34;If you can help me find the key to my tractor, you may enter my house&#59; meanwhile you may come on to my land.&#34;  And with that he opens the gate and bids you enter."
 plus    30 3 ; sust(total_player_score/30)
 anykey
 desc

talk _
 writeln "Nothing to say."
 done

give coin
 present 43 ; sust(object_43/43)
 carried 9 ; sust(object_9_coin/9)
 writeln "st&#34;Thanks guv,&#34; says the guard a s               he pockets the c oin, &#34;I'm off   duty now&#59; you ca n come and go asyou please.&#34;"
 set     114 ; sust(game_flag_14/114)
 destroy 43 ; sust(object_43/43)
 destroy 9 ; sust(object_9_coin/9)
 plus    30 2 ; sust(total_player_score/30)
 anykey
 desc

give coin
 at      37 ; sust(room_37/37)
 present 43 ; sust(object_43/43)
 notcarr 9 ; sust(object_9_coin/9)
 writeln "With what ?"
 done

give maga
 carried 16 ; sust(object_16_maga/16)
 present 13 ; sust(object_13/13)
 destroy 13 ; sust(object_13/13)
 destroy 16 ; sust(object_16_maga/16)
 writeln "The farmer accepts the magazine and opens it. There is a tinkle as the tractor key falls out of the magazine onto the ground. Smiling sheepishly as he stoops to retrieve it, he says: &#34;I forgot I'd wedged it in there&#59; you may enter my house, and call me if I can be of service.&#34;"
 pause   35

 plus    30 4 ; sust(total_player_score/30)
 anykey
 desc

give trou
 carried 14 ; sust(object_14_trou/14)
 present 13 ; sust(object_13/13)
 destroy 14 ; sust(object_14_trou/14)
 writeln "&#34;What the blazes are you doing with my trousers?&#34; grumbles the farmer taking them from you."
 done

give jar
 at      34 ; sust(room_34/34)
 carried 18 ; sust(object_18_jar/18)
 writeln "Could you put that another way ?"
 done

give spic
 present 59 ; sust(object_59/59)
 carried 64 ; sust(object_64_spic/64)
 swap    64 65 ; sust(object_64_spic/64) ; sust(object_65_key/65)
 writeln "You give the rare herb to the farmer's wife. She thanks you profusely and adds it to the pot over the fire.\nst&#34;Take this key - it should hel p you.  The professor lodged her e and I found it in his room aft erthe poor man died.&#34;"
 plus    30 2 ; sust(total_player_score/30)
 anykey
 desc

give spic
 present 62 ; sust(object_62/62)
 carried 64 ; sust(object_64_spic/64)
 swap    64 65 ; sust(object_64_spic/64) ; sust(object_65_key/65)
 writeln "You give the rare herb to the farmer's wife. She thanks you profusely and adds it to the pot over the fire.\nst&#34;Take this key - it should hel p you.  The professor lodged her e and I found it in his room aft erthe poor man died.&#34;"
 plus    30 2 ; sust(total_player_score/30)
 anykey
 desc

give spic
 at      24 ; sust(room_24/24)
 notcarr 64 ; sust(object_64_spic/64)
 writeln "&#34;I don't see it,&#34; she cries."
 done

give key
 at      20 ; sust(room_20/20)
 present 13 ; sust(object_13/13)
 writeln "Adventures should be SO simple!"
 done

give _
 writeln "Not wanted."
 done

read maga
 carried 16 ; sust(object_16_maga/16)
 writeln "stNo thanks, looks too boring."
 done

read noti
 present 25 ; sust(object_25/25)
 writeln "Do not dig - leaking Methane gas"
 done

read noti
 present 26 ; sust(object_26_noti/26)
 writeln "Do not dig - leaking Methane gas"
 done

read sign
 at      34 ; sust(room_34/34)
 writeln "PLEASE DON'T FEED THE ANIMALS."
 done

read shee
 present 49 ; sust(object_49_shee/49)
 notcarr 49 ; sust(object_49_shee/49)
 writeln "You can't do that...yet."
 done

read shee
 carried 49 ; sust(object_49_shee/49)
 writeln "{CLASS|center|28@S  172@W}"
 done

read book
 carried 54 ; sust(object_54_book/54)
 cls
 writeln "Inside, in a neat handwriting is a page of instructions:-\n\n1. Punch the coordinates into\n   the computer.\n\n2. Open the dome.\n\n3. Switch on machine.\n\n4. Turn fine adjustment knob\n\n5. Don't forget to bl\n\nThe advisory stops dead, and you wonder what is missing..."
 anykey
 desc

read scro
 present 63 ; sust(object_63_scro/63)
 cls
 writeln "QQ   =  QUIT    I   =  INVENTORY R?L  =  LOOK    G?T =  GET?TAKE X    =  EXAMINE O   =  OUT?LEAVE P    =  WAIT\nRS   =  RAM SAVE  RL=  RAM LOAD\n\nNOTE:-\n\nThis game employs occasional inputs of THREE words (although only the first two are processed by the Quill).\n\n\nTyping   FONT 1    and    FONT 2 gives alternative typeface.\n\n\nHandle fragile objects carefully"
 anykey
 desc

read coor
 carried 49 ; sust(object_49_shee/49)
 writeln "{CLASS|center|28@S  172@W}"
 done

unsc lamp
 at      22 ; sust(room_22/22)
 notcarr 17 ; sust(object_17_scre/17)
 lt      113 200 ; sust(game_flag_13/113)
 writeln "With what ?"
 done

unsc lamp
 at      22 ; sust(room_22/22)
 carried 17 ; sust(object_17_scre/17)
 absent  13 ; sust(object_13/13)
 lt      113 200 ; sust(game_flag_13/113)
 let     113 200 ; sust(game_flag_13/113)
 writeln "As surreptitiously as possible you unscrew the lamp."
 plus    30 1 ; sust(total_player_score/30)
 anykey
 desc

unsc lamp
 at      22 ; sust(room_22/22)
 carried 17 ; sust(object_17_scre/17)
 present 13 ; sust(object_13/13)
 lt      113 200 ; sust(game_flag_13/113)
 writeln "&#34;What the hell are you doing to my lamp?&#34; says the farmer.  He chases you off his land and says never to return."
 anykey
 goto    18 ; sust(room_18/18)
 destroy 15 ; sust(object_15/15)
 desc

unsc lamp
 at      22 ; sust(room_22/22)
 carried 17 ; sust(object_17_scre/17)
 present 1 ; sust(object_1_lamp/1)
 notcarr 1 ; sust(object_1_lamp/1)
 eq      113 200 ; sust(game_flag_13/113)
 writeln "You've done that."
 done

inse box
 at      23 ; sust(room_23/23)
 carried 5 ; sust(object_5_box/5)
 writeln "You insert the wet box into the drier and close the door.  The tumble drier spins around for a while, and when it stops you open the door and remove the box which is now dry."
 swap    5 6 ; sust(object_5_box/5) ; sust(object_6_box/6)
 plus    30 2 ; sust(total_player_score/30)
 anykey
 desc

inse box
 at      23 ; sust(room_23/23)
 carried 6 ; sust(object_6_box/6)
 writeln "Why ?"
 pause   50
 writeln "You've done that."
 done

inse bar
 at      8 ; sust(room_8/8)
 carried 19 ; sust(object_19_bar/19)
 absent  20 ; sust(object_20/20)
 create  20 ; sust(object_20/20)
 writeln "Inserting the crowbar under the chain, you give a mighty push and the chain snaps&#59; the door creaks open...."
 plus    30 2 ; sust(total_player_score/30)
 anykey

 desc

inse bar
 at      8 ; sust(room_8/8)
 present 20 ; sust(object_20/20)
 carried 19 ; sust(object_19_bar/19)
 writeln "You've done that."
 done

inse rod
 at      31 ; sust(room_31/31)
 carried 22 ; sust(object_22_rod/22)
 swap    22 23 ; sust(object_22_rod/22) ; sust(object_23/23)
 drop    23 ; sust(object_23/23)

 writeln "You fit the rod into the recess, effectively making a lever."
 plus    30 1 ; sust(total_player_score/30)
 anykey
 desc

inse nail
 carried 50 ; sust(object_50_broo/50)
 carried 10 ; sust(object_10_nail/10)
 destroy 10 ; sust(object_10_nail/10)
 swap    50 51 ; sust(object_50_broo/50) ; sust(object_51_lift/51)
 writeln "stPushing the nail into the hole                 on the broomstic k, it emerges   out the bottom m aking a useful  paper-lifter."
 anykey
 desc

inse disc
 at      48 ; sust(room_48/48)
 carried 47 ; sust(object_47_disc/47)
 lt      24 200 ; sust(game_flag_24/24)
 writeln "You can't do that...yet."
 done

inse disc
 at      48 ; sust(room_48/48)
 carried 47 ; sust(object_47_disc/47)
 eq      24 200 ; sust(game_flag_24/24)
 destroy 47 ; sust(object_47_disc/47)
 set     24 ; sust(game_flag_24/24)
 destroy 61 ; sust(object_61/61)
 create  55 ; sust(object_55/55)
 pause   50
 let     28 5 ; sust(pause_parameter/28) ; sust(pause_sound_effect_white_noise/5)
 pause   3
 plus    30 2 ; sust(total_player_score/30)
 desc

inse hand
 present 24 ; sust(object_24_barr/24)
 writeln "Nice try, but nothing happens!"
 done

inse cott
 carried 56 ; sust(object_56_cott/56)
 notworn 56 ; sust(object_56_cott/56)
 writeln "Could you put that another way ?"
 done

put box
 at      23 ; sust(room_23/23)
 carried 5 ; sust(object_5_box/5)
 writeln "Could you put that another way ?"
 done

put lamp
 carried 1 ; sust(object_1_lamp/1)
 drop    1 ; sust(object_1_lamp/1)
 writeln "Carefully you put it down."
 pause   100
 desc

put lamp
 carried 0 ; sust(object_0_lamp/0)
 drop    0 ; sust(object_0_lamp/0)
 writeln "Carefully you put it down."
 pause   100
 desc

put jar
 notat   34 ; sust(room_34/34)
 carried 18 ; sust(object_18_jar/18)
 drop    18 ; sust(object_18_jar/18)
 writeln "Carefully you put it down."
 pause   100
 desc

put jar
 at      34 ; sust(room_34/34)
 lt      19 220 ; sust(game_flag_19/19)
 writeln "stYou place the jar near the bea r               and retreat. You  see the bear   vainly attemptin g to remove the lid."
 drop    18 ; sust(object_18_jar/18)
 anykey
 desc

put jar
 at      34 ; sust(room_34/34)
 carried 18 ; sust(object_18_jar/18)
 eq      19 220 ; sust(game_flag_19/19)
 destroy 37 ; sust(object_37/37)
 destroy 18 ; sust(object_18_jar/18)
 writeln "You place the jar gently on the ground and take a few steps back.  The bear picks up the jar and lopes out of sight to enjoy his feast."
 plus    30 2 ; sust(total_player_score/30)
 anykey
 desc

put ladd
 atgt    34 ; sust(room_34/34)
 atlt    37 ; sust(room_37/37)
 carried 28 ; sust(object_28/28)
 swap    28 29 ; sust(object_28/28) ; sust(object_29/29)
 drop    29 ; sust(object_29/29)
 let     28 11 ; sust(pause_parameter/28) ; sust(pause_ability/11)
 pause   6
 writeln "You lay the ladder over the stalagmites."
 pause   200
 desc

put ladd
 carried 28 ; sust(object_28/28)
 drop    28 ; sust(object_28/28)
 let     28 11 ; sust(pause_parameter/28) ; sust(pause_ability/11)
 pause   6
 writeln "You stand the ladder in front of you."
 pause   200
 desc

put egg
 carried 44 ; sust(object_44_egg/44)
 drop    44 ; sust(object_44_egg/44)
 writeln "Carefully you put it down."
 pause   100
 desc

pres u
 at      27 ; sust(room_27/27)
 writeln "Nice try, but nothing happens!"
 done

pres u
 at      30 ; sust(room_30/30)
 writeln "stThe lift begins to move."
 pause   20
 let     28 5 ; sust(pause_parameter/28) ; sust(pause_sound_effect_white_noise/5)
 pause   5
 pause   20
 goto    27 ; sust(room_27/27)
 desc

pres d
 at      27 ; sust(room_27/27)
 lt      115 255 ; sust(game_flag_15/115)
 writeln "There is no power."
 done

pres d
 at      27 ; sust(room_27/27)
 eq      115 255 ; sust(game_flag_15/115)
 writeln "stThe lift begins to move."
 pause   20
 let     28 5 ; sust(pause_parameter/28) ; sust(pause_sound_effect_white_noise/5)
 pause   5
 pause   20
 goto    30 ; sust(room_30/30)
 desc

pres d
 at      30 ; sust(room_30/30)
 writeln "Nice try, but nothing happens!"
 done

pres door
 at      12 ; sust(room_12/12)
 absent  35 ; sust(object_35/35)
 writeln "Adventures should be SO simple!"
 done

pres leve
 present 23 ; sust(object_23/23)
 eq      115 100 ; sust(game_flag_15/115)
 set     115 ; sust(game_flag_15/115)
 writeln "The generator hums into life, indicating that the lift now has power."


 plus    30 1 ; sust(total_player_score/30)
 anykey
 desc

pres leve
 present 23 ; sust(object_23/23)
 eq      115 255 ; sust(game_flag_15/115)
 writeln "You've done that."
 done

pres noti
 present 25 ; sust(object_25/25)
 writeln "Adventures should be SO simple!"
 done

pres nail
 at      14 ; sust(room_14/14)
 notcarr 57 ; sust(object_57_plie/57)
 writeln "With what ?"
 done

pres nail
 at      14 ; sust(room_14/14)
 carried 57 ; sust(object_57_plie/57)
 present 25 ; sust(object_25/25)
 swap    25 26 ; sust(object_25/25) ; sust(object_26_noti/26)
 create  10 ; sust(object_10_nail/10)
 writeln "You pull the nail out of the wall with the pliers."
 plus    30 2 ; sust(total_player_score/30)
 anykey
 desc

pres nail
 at      14 ; sust(room_14/14)
 carried 57 ; sust(object_57_plie/57)
 absent  25 ; sust(object_25/25)
 writeln "You've done that."
 done

pres hook
 at      45 ; sust(room_45/45)
 zero    18 ; sust(game_flag_18/18)
 present 30 ; sust(object_30/30)
 let     18 50 ; sust(game_flag_18/18)
 swap    30 71 ; sust(object_30/30) ; sust(object_71_hook/71)
 place   71 36 ; sust(object_71_hook/71) ; sust(room_36/36)
 goto    46 ; sust(room_46/46)
 let     6 4 ; sust(countdown_player_input_2/6)
 writeln "stStanding on top of the ladder,                 you pull the hoo k out of the    rock. The inerti a from doing so,causes the ladde r to slip from  under you and yo u drop the hook.Displaying a spl it-second reflexaction, you grab  hold of a      convenient ridge , but you can   feel your finger s slipping...."
 plus    30 2 ; sust(total_player_score/30)
 anykey
 desc

pres hook
 at      36 ; sust(room_36/36)
 present 30 ; sust(object_30/30)
 writeln "How ?"
 done

pres red
 at      51 ; sust(room_51/51)
 absent  52 ; sust(object_52/52)
 writeln "Nice try, but nothing happens!"
 done

pres red
 at      51 ; sust(room_51/51)
 present 52 ; sust(object_52/52)
 destroy 52 ; sust(object_52/52)
 writeln "There is a rumble as the ceiling moves."
 minus   25 10 ; sust(game_flag_25/25)
 let     28 5 ; sust(pause_parameter/28) ; sust(pause_sound_effect_white_noise/5)
 pause   4
 desc

pres gree
 at      51 ; sust(room_51/51)
 lt      24 150 ; sust(game_flag_24/24)
 absent  52 ; sust(object_52/52)
 writeln "There is no power."
 done

pres gree
 at      51 ; sust(room_51/51)
 gt      24 149 ; sust(game_flag_24/24)
 absent  52 ; sust(object_52/52)
 create  52 ; sust(object_52/52)
 writeln "There is a rumble as the ceiling moves."
 plus    25 10 ; sust(game_flag_25/25)
 let     28 5 ; sust(pause_parameter/28) ; sust(pause_sound_effect_white_noise/5)
 pause   4
 desc

pres tripswitch
 present 53 ; sust(object_53/53)
 writeln "Could you put that another way ?"
 done

pres butt
 at      49 ; sust(room_49/49)
 lt      24 150 ; sust(game_flag_24/24)
 writeln "There is no power."
 done

pres butt
 at      49 ; sust(room_49/49)
 gt      24 149 ; sust(game_flag_24/24)
 lt      25 30 ; sust(game_flag_25/25)
 writeln "You can't do that...yet."
 done

pres butt
 at      49 ; sust(room_49/49)
 gt      24 149 ; sust(game_flag_24/24)
 eq      25 30 ; sust(game_flag_25/25)
 set     25 ; sust(game_flag_25/25)
 writeln "stThe machine starts up with a l owgrowl, and quickly builds up t o               a high-pitched s cream."
 done

brea door
 at      8 ; sust(room_8/8)
 absent  20 ; sust(object_20/20)
 writeln "Nice try, but nothing happens!"
 done

brea chai
 at      8 ; sust(room_8/8)
 absent  20 ; sust(object_20/20)
 notcarr 19 ; sust(object_19_bar/19)
 writeln "With what ?"
 done

brea chai
 at      8 ; sust(room_8/8)
 carried 19 ; sust(object_19_bar/19)
 absent  20 ; sust(object_20/20)
 writeln "How ?"
 done

brea vacu
 carried 18 ; sust(object_18_jar/18)
 lt      19 220 ; sust(game_flag_19/19)
 writeln "How ?"
 done

call man
 at      32 ; sust(room_32/32)
 eq      115 1 ; sust(game_flag_15/115)
 absent  21 ; sust(object_21/21)
 writeln "&#34;I see you've been trying to open my cupboard,&#34; the farmer says, responding to your call. &#34;Heard you shaking it a mile away - it's simpler with a key!&#34; He produces a key and opens the cupboard for you."
 create  21 ; sust(object_21/21)
 create  13 ; sust(object_13/13)
 let     115 50 ; sust(game_flag_15/115)
 plus    30 3 ; sust(total_player_score/30)
 anykey
 desc

call man
 atgt    17 ; sust(room_17/17)
 atlt    33 ; sust(room_33/33)
 gt      115 1 ; sust(game_flag_15/115)
 writeln "The farmer appears.\n&#34;You don't need my help&#59; stop wasting my time.&#34;"
 create  13 ; sust(object_13/13)
 anykey
 desc

call man
 atgt    17 ; sust(room_17/17)
 atlt    33 ; sust(room_33/33)
 zero    115 ; sust(game_flag_15/115)
 writeln "The farmer appears.\n&#34;You don't need my help&#59; stop wasting my time.&#34;"
 done

shak maga
 carried 16 ; sust(object_16_maga/16)
 writeln "Why ?"
 done

shak barr
 carried 24 ; sust(object_24_barr/24)
 zero    116 ; sust(game_flag_16/116)
 set     116 ; sust(game_flag_16/116)
 create  11 ; sust(object_11_need/11)
 writeln "Giving the seemingly empty barrel a good shake, you are rewarded by an object falling out..."
 anykey
 plus    30 3 ; sust(total_player_score/30)
 desc

shak barr
 carried 24 ; sust(object_24_barr/24)
 eq      116 255 ; sust(game_flag_16/116)
 writeln "You've done that."
 done

make comp
 carried 10 ; sust(object_10_nail/10)
 carried 11 ; sust(object_11_need/11)
 destroy 10 ; sust(object_10_nail/10)
 swap    11 12 ; sust(object_11_need/11) ; sust(object_12_comp/12)
 writeln "Wobbling the compass needle on the rusty nail, you make a crude compass."
 anykey
 desc

make comp
 notcarr 12 ; sust(object_12_comp/12)
 writeln "With what ?"
 done

pier jar
 carried 18 ; sust(object_18_jar/18)
 carried 27 ; sust(object_27_dagg/27)
 swap    18 36 ; sust(object_18_jar/18) ; sust(object_36/36)
 drop    36 ; sust(object_36/36)
 writeln "Oops!"
 pause   75
 desc

pier lid
 carried 18 ; sust(object_18_jar/18)
 carried 27 ; sust(object_27_dagg/27)
 zero    19 ; sust(game_flag_19/19)
 let     19 220 ; sust(game_flag_19/19)
 writeln "You pierce the lid with the dagger.  There is a hiss as air fills the vacuum, and the lid unscrews easily which you discard."
 let     28 5 ; sust(pause_parameter/28) ; sust(pause_sound_effect_white_noise/5)
 pause   2
 plus    30 3 ; sust(total_player_score/30)
 anykey
 desc

pier lid
 carried 18 ; sust(object_18_jar/18)
 carried 27 ; sust(object_27_dagg/27)
 eq      19 220 ; sust(game_flag_19/19)
 writeln "You've done that."
 done

pier lid
 carried 18 ; sust(object_18_jar/18)
 notcarr 27 ; sust(object_27_dagg/27)
 zero    19 ; sust(game_flag_19/19)
 writeln "With what ?"
 done

lay ladd
 atgt    34 ; sust(room_34/34)
 atlt    37 ; sust(room_37/37)
 carried 28 ; sust(object_28/28)
 swap    28 29 ; sust(object_28/28) ; sust(object_29/29)
 drop    29 ; sust(object_29/29)
 let     28 11 ; sust(pause_parameter/28) ; sust(pause_ability/11)
 pause   6
 writeln "You lay the ladder over the stalagmites."
 pause   200
 desc

lay ladd
 carried 28 ; sust(object_28/28)
 writeln "Not here."
 done

clim foun
 at      4 ; sust(room_4/4)
 writeln "Why ?"
 done

clim gate
 at      18 ; sust(room_18/18)
 absent  15 ; sust(object_15/15)
 present 13 ; sust(object_13/13)
 writeln "&#34;Oy, I didn't say you could come onto my property,&#34; cries the farmer.  You flee as he aims a shotgun at you."
 goto    4 ; sust(room_4/4)
 anykey
 desc

clim ladd
 at      39 ; sust(room_39/39)
 present 28 ; sust(object_28/28)
 notcarr 28 ; sust(object_28/28)
 goto    38 ; sust(room_38/38)
 desc

clim ladd
 at      36 ; sust(room_36/36)
 zero    18 ; sust(game_flag_18/18)
 present 28 ; sust(object_28/28)
 notcarr 28 ; sust(object_28/28)
 goto    45 ; sust(room_45/45)
 place   30 45 ; sust(object_30/30) ; sust(room_45/45)
 desc

clim ladd
 at      36 ; sust(room_36/36)
 notzero 18 ; sust(game_flag_18/18)
 writeln "Why ?"
 done

clim ladd
 present 28 ; sust(object_28/28)
 notcarr 28 ; sust(object_28/28)
 writeln "Not here."
 done

clim ladd
 at      36 ; sust(room_36/36)
 carried 28 ; sust(object_28/28)
 writeln "stWhile you're holding it ?"
 done

clim ladd
 at      39 ; sust(room_39/39)
 carried 28 ; sust(object_28/28)
 writeln "stWhile you're holding it ?"
 done

clim tree
 at      39 ; sust(room_39/39)
 absent  28 ; sust(object_28/28)
 writeln "With what ?"
 done

clim rubb
 at      10 ; sust(room_10/10)
 present 41 ; sust(object_41/41)
 goto    47 ; sust(room_47/47)
 writeln "Taking a deep breath, you trudge up the steep slope to the very top."
 anykey
 desc

clim rubb
 present 42 ; sust(object_42/42)
 writeln "Too late!"
 done

clim rope
 present 48 ; sust(object_48/48)
 writeln "Why ?"
 done

clim _ ; on
 at      19 ; sust(room_19/19)
 goto    53 ; sust(room_53/53)
 desc

tie hook
 carried 71 ; sust(object_71_hook/71)
 carried 34 ; sust(object_34_rope/34)
 writeln "Could you put that another way ?"
 done

tie rope
 carried 71 ; sust(object_71_hook/71)
 carried 34 ; sust(object_34_rope/34)
 destroy 71 ; sust(object_71_hook/71)
 swap    34 38 ; sust(object_34_rope/34) ; sust(object_38_rope/38)
 writeln "You tie the rope to the hook."
 pause   150
 desc

tie rope
 carried 38 ; sust(object_38_rope/38)
 writeln "You've done that."
 done

tie bear
 present 37 ; sust(object_37/37)
 writeln "With what ?"
 done

jump _
 at      46 ; sust(room_46/46)
 writeln "If you insist."
 pause   200
 writeln "\nstYou fall to the ground breakin g your legs and arms.\n                "
 score
 end

jump _
 atgt    39 ; sust(room_39/39)
 atlt    43 ; sust(room_43/43)
 writeln "If you insist."
 let     28 3 ; sust(pause_parameter/28) ; sust(pause_sound_effect_tone_decreasing/3)
 pause   200
 goto    41 ; sust(room_41/41)
 desc

jump _
 at      36 ; sust(room_36/36)
 zero    18 ; sust(game_flag_18/18)
 writeln "You do an Olympic jump, but miss the hook by a whisker!"
 done

jump _
 at      6 ; sust(room_6/6)
 writeln "Don't be crazy!"
 done

dig rubb
 at      10 ; sust(room_10/10)
 carried 33 ; sust(object_33_spad/33)
 present 41 ; sust(object_41/41)
 swap    41 42 ; sust(object_41/41) ; sust(object_42/42)
 create  27 ; sust(object_27_dagg/27)
 writeln "stYou reduce the pile of rubble toa worthless heap, and in doing                 so uncover a sho rt dagger."
 plus    30 2 ; sust(total_player_score/30)
 anykey
 desc

dig rubb
 at      10 ; sust(room_10/10)
 notcarr 33 ; sust(object_33_spad/33)
 writeln "With what ?"
 done

dig rubb
 at      10 ; sust(room_10/10)
 present 42 ; sust(object_42/42)
 carried 33 ; sust(object_33_spad/33)
 writeln "You've done that."
 done

dig _
 notcarr 33 ; sust(object_33_spad/33)
 writeln "With what ?"
 done

dig _
 at      14 ; sust(room_14/14)
 carried 33 ; sust(object_33_spad/33)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   2
 writeln "BOOM! A badly-sealed methane pipe explodes, blowing off your feet."
 score
 end

dig _
 at      10 ; sust(room_10/10)
 present 41 ; sust(object_41/41)
 carried 33 ; sust(object_33_spad/33)
 writeln "Please be more specific..."
 done

dig _
 at      13 ; sust(room_13/13)
 carried 33 ; sust(object_33_spad/33)
 eq      27 100 ; sust(game_flag_27/27)
 let     27 200 ; sust(game_flag_27/27)
 create  7 ; sust(object_7/7)
 writeln "You dig a hole and find nothing."
 pause   150
 desc

dig _
 at      13 ; sust(room_13/13)
 eq      27 200 ; sust(game_flag_27/27)
 carried 33 ; sust(object_33_spad/33)
 let     27 220 ; sust(game_flag_27/27)
 create  8 ; sust(object_8_galo/8)
 writeln "Digging deeper you find a pair of old galoshes."
 plus    30 2 ; sust(total_player_score/30)
 anykey
 desc

dig _
 at      13 ; sust(room_13/13)
 carried 33 ; sust(object_33_spad/33)
 gt      27 219 ; sust(game_flag_27/27)
 writeln "You've done that."
 done

dig _
 writeln "Not here."
 done

unti rope
 present 2 ; sust(object_2/2)
 zero    20 ; sust(game_flag_20/20)
 let     20 100 ; sust(game_flag_20/20)
 create  34 ; sust(object_34_rope/34)
 get     34 ; sust(object_34_rope/34)
 writeln "You untie the rope and pick it up."
 pause   100
 desc

unti rope
 at      29 ; sust(room_29/29)
 zero    21 ; sust(game_flag_21/21)
 writeln "The knot's too tight, you will need to cut it."
 done

unti rope
 present 2 ; sust(object_2/2)
 eq      20 100 ; sust(game_flag_20/20)
 writeln "You've done that."
 done

cut rope
 present 2 ; sust(object_2/2)
 zero    20 ; sust(game_flag_20/20)
 writeln "With what ?"
 done

cut rope
 at      29 ; sust(room_29/29)
 zero    21 ; sust(game_flag_21/21)
 notcarr 27 ; sust(object_27_dagg/27)
 writeln "With what ?"
 done

cut rope
 at      29 ; sust(room_29/29)
 carried 27 ; sust(object_27_dagg/27)
 zero    21 ; sust(game_flag_21/21)
 let     21 100 ; sust(game_flag_21/21)
 writeln "The dagger slices through most of the rope. There is however, a resistant strand that will not succumb to the dagger's edge."
 anykey
 desc

cut rope
 at      29 ; sust(room_29/29)
 carried 27 ; sust(object_27_dagg/27)
 eq      21 100 ; sust(game_flag_21/21)
 writeln "Nice try, but nothing happens!"
 done

on box
 carried 6 ; sust(object_6_box/6)
 writeln "Could you put that another way ?"
 done

on box
 carried 5 ; sust(object_5_box/5)
 writeln "You can't do that...yet."
 done

on comp
 at      48 ; sust(room_48/48)
 lt      24 150 ; sust(game_flag_24/24)
 writeln "There is no power."
 done

on comp
 at      48 ; sust(room_48/48)
 eq      24 150 ; sust(game_flag_24/24)
 let     24 200 ; sust(game_flag_24/24)
 create  61 ; sust(object_61/61)
 pause   25
 plus    30 2 ; sust(total_player_score/30)
 desc

on lamp
 carried 1 ; sust(object_1_lamp/1)
 gt      29 0 ; sust(bitset_graphics_status/29)
 carried 6 ; sust(object_6_box/6)
 swap    0 1 ; sust(object_0_lamp/0) ; sust(object_1_lamp/1)
 clear   10 ; sust(countdown_player_input_unlit/10)
 writeln "You light the lamp."
 pause   75
 desc

on lamp
 carried 1 ; sust(object_1_lamp/1)
 carried 5 ; sust(object_5_box/5)
 writeln "You can't do that...yet."
 done

on lamp
 carried 1 ; sust(object_1_lamp/1)
 notcarr 6 ; sust(object_6_box/6)
 writeln "With what ?"
 done

on lamp
 carried 1 ; sust(object_1_lamp/1)
 eq      29 0 ; sust(bitset_graphics_status/29)
 carried 6 ; sust(object_6_box/6)
 writeln "The paraffin is finished."
 done






on gene
 at      31 ; sust(room_31/31)
 lt      115 255 ; sust(game_flag_15/115)
 writeln "How ?"
 done

off lamp
 carried 0 ; sust(object_0_lamp/0)
 swap    0 1 ; sust(object_0_lamp/0) ; sust(object_1_lamp/1)
 let     10 3 ; sust(countdown_player_input_unlit/10)
 writeln "You extinguish the lamp."
 pause   75
 desc

pry chai
 at      8 ; sust(room_8/8)
 carried 19 ; sust(object_19_bar/19)
 absent  20 ; sust(object_20/20)
 writeln "Could you put that another way ?"
 done

pry cupb
 at      32 ; sust(room_32/32)
 carried 19 ; sust(object_19_bar/19)
 pause   25

 writeln "stThe farmer appears in a rage. &#34;Oy, what do you think you're doing?  You can't just walk into people's homes and smash their bleedin' cupboards.&#34;\nHe chucks you off his property."
 goto    18 ; sust(room_18/18)
 destroy 15 ; sust(object_15/15)
 anykey
 desc

pry nail
 present 25 ; sust(object_25/25)
 writeln "stYou'll need to pull it out - n otprise it."
 done

boar cano
 present 2 ; sust(object_2/2)
 writeln "Could you put that another way ?"
 done

boar raft
 at      29 ; sust(room_29/29)
 lt      21 255 ; sust(game_flag_21/21)
 writeln "The mooring rope is still tied."
 done

boar raft
 at      29 ; sust(room_29/29)
 eq      21 255 ; sust(game_flag_21/21)
 notcarr 4 ; sust(object_4_padd/4)
 writeln "What's the point of that without a paddle?"
 done

boar raft
 at      29 ; sust(room_29/29)
 carried 4 ; sust(object_4_padd/4)
 writeln "Clambering onto the raft, you paddle it across to the other side of the lake."
 goto    33 ; sust(room_33/33)
 place   32 33 ; sust(object_32/32) ; sust(room_33/33)
 anykey
 desc

boar raft
 at      33 ; sust(room_33/33)
 notcarr 4 ; sust(object_4_padd/4)
 writeln "What's the point of that without a paddle?"
 done

boar raft
 at      33 ; sust(room_33/33)
 carried 4 ; sust(object_4_padd/4)
 goto    29 ; sust(room_29/29)
 place   32 29 ; sust(object_32/32) ; sust(room_29/29)
 writeln "Clambering onto the raft, you paddle it across to the other side of the lake."
 anykey
 desc

feed bear
 at      34 ; sust(room_34/34)
 carried 18 ; sust(object_18_jar/18)
 writeln "\nThe bear lifts you like a toy and crushes you to death."
 score
 end

thro jar
 at      34 ; sust(room_34/34)
 carried 18 ; sust(object_18_jar/18)
 writeln "KLONK! The jar hits the bear on the head. The bear gives a loud roar of pain and chases you. With one powerful squeeze it breaks every bone in your body."
 score
 end

thro hook
 at      40 ; sust(room_40/40)
 carried 38 ; sust(object_38_rope/38)
 writeln "Could you put that another way ?"
 done

thro rope
 at      40 ; sust(room_40/40)
 carried 38 ; sust(object_38_rope/38)
 swap    38 48 ; sust(object_38_rope/38) ; sust(object_48/48)
 drop    48 ; sust(object_48/48)
 writeln "stThe hook catches onto the rock y               protuberance, an d the rope      dangles just wit hin reach."
 let     28 1 ; sust(pause_parameter/28) ; sust(pause_sound_effect_tone_increasing/1)
 pause   250
 plus    30 3 ; sust(total_player_score/30)
 anykey
 desc

thro rope
 notat   40 ; sust(room_40/40)
 writeln "Why ?"
 done

thro rope
 at      40 ; sust(room_40/40)
 carried 34 ; sust(object_34_rope/34)
 destroy 34 ; sust(object_34_rope/34)
 writeln "If you insist."
 pause   75
 writeln "That was dumb.  You throw the rope into the air and it lands in the pool with a splash."
 let     28 1 ; sust(pause_parameter/28) ; sust(pause_sound_effect_tone_increasing/1)
 pause   200
 anykey
 desc

thro tripswitch
 present 53 ; sust(object_53/53)
 worn    67 ; sust(object_67_glov/67)
 eq      24 100 ; sust(game_flag_24/24)
 let     24 150 ; sust(game_flag_24/24)
 clear   0 ; sust(yesno_is_dark/0)
 destroy 72 ; sust(object_72/72)
 writeln "With the glove protecting your hand, you throw the tripswitch to the &#34;ON&#34; position. The power is restored and all the lights come on."
 plus    30 2 ; sust(total_player_score/30)
 anykey
 desc

thro tripswitch
 present 53 ; sust(object_53/53)
 notworn 67 ; sust(object_67_glov/67)
 eq      24 100 ; sust(game_flag_24/24)
 let     28 4 ; sust(pause_parameter/28) ; sust(pause_flash/4)
 pause   4
 writeln "stYou receive a violent electric                 shock from the t ripswitch."
 score
 end

thro tripswitch
 present 53 ; sust(object_53/53)
 eq      24 150 ; sust(game_flag_24/24)
 writeln "You've done that."
 done

star trac
 at      53 ; sust(room_53/53)
 writeln "How ?"
 pause   100
 writeln "With what ?"
 pause   100
 writeln "Why ?"
 done

star gene
 at      31 ; sust(room_31/31)
 lt      115 255 ; sust(game_flag_15/115)
 writeln "How ?"
 done

burn rope
 at      29 ; sust(room_29/29)
 zero    21 ; sust(game_flag_21/21)
 carried 6 ; sust(object_6_box/6)
 writeln "A match is not going to burn through such a thick rope."
 done

burn rope
 at      29 ; sust(room_29/29)
 carried 6 ; sust(object_6_box/6)
 eq      21 100 ; sust(game_flag_21/21)
 set     21 ; sust(game_flag_21/21)
 writeln "Striking a match, you hold it beneath the resistant strand, which smoulders...and finally breaks."
 plus    30 2 ; sust(total_player_score/30)
 anykey
 desc

burn rope
 at      29 ; sust(room_29/29)
 carried 6 ; sust(object_6_box/6)
 eq      21 255 ; sust(game_flag_21/21)
 writeln "You've done that."
 done

g cano
 present 2 ; sust(object_2/2)
 writeln "Don't be crazy!"
 done

g box
 present 6 ; sust(object_6_box/6)
 get     6 ; sust(object_6_box/6)
 ok

g lamp
 at      22 ; sust(room_22/22)
 lt      113 200 ; sust(game_flag_13/113)
 writeln "stIt's screwed to the table."
 done

g lamp
 at      22 ; sust(room_22/22)
 eq      113 200 ; sust(game_flag_13/113)
 get     1 ; sust(object_1_lamp/1)
 ok

g lamp
 present 1 ; sust(object_1_lamp/1)
 get     1 ; sust(object_1_lamp/1)
 ok

g lamp
 present 0 ; sust(object_0_lamp/0)
 get     0 ; sust(object_0_lamp/0)
 ok

g lamp
 present 60 ; sust(object_60/60)
 writeln "Why ?"
 done

g jar
 present 36 ; sust(object_36/36)
 writeln "Don't be crazy!"
 done

g jar
 present 37 ; sust(object_37/37)
 present 18 ; sust(object_18_jar/18)
 notcarr 18 ; sust(object_18_jar/18)
 writeln "Not with that mad bear around!"
 done

g jar
 present 18 ; sust(object_18_jar/18)
 notcarr 18 ; sust(object_18_jar/18)
 zero    5 ; sust(countdown_player_input_1/5)
 present 59 ; sust(object_59/59)
 writeln "&#34;You leave that jar alone,&#34; says the farmer's wife chasing you out of the kitchen."
 anykey
 goto    21 ; sust(room_21/21)
 desc

g jar
 present 62 ; sust(object_62/62)
 gt      5 1 ; sust(countdown_player_input_1/5)
 plus    30 3 ; sust(total_player_score/30)
 get     18 ; sust(object_18_jar/18)
 writeln "stAs soon as her back is turned,                 you realize that  this is your   golden opportuni ty. You reach   out and take the  jar of honey.  But watch out if  she catches youwith it...."
 anykey
 desc

g jar
 present 18 ; sust(object_18_jar/18)
 notcarr 18 ; sust(object_18_jar/18)
 zero    5 ; sust(countdown_player_input_1/5)
 present 62 ; sust(object_62/62)
 writeln "&#34;You leave that jar alone,&#34; says the farmer's wife chasing you out of the kitchen."
 anykey
 goto    21 ; sust(room_21/21)
 desc

g jar
 get     18 ; sust(object_18_jar/18)
 ok

g noti
 present 25 ; sust(object_25/25)
 writeln "You can't do that...yet."
 done

g noti
 present 26 ; sust(object_26_noti/26)
 get     26 ; sust(object_26_noti/26)
 ok

g nail
 present 25 ; sust(object_25/25)
 writeln "How ?"
 done

g ladd
 present 28 ; sust(object_28/28)
 gt      1 2 ; sust(total_carried/1)
 writeln "It's large and heavy&#59; you need to drop something first."
 done

g ladd
 present 28 ; sust(object_28/28)
 get     28 ; sust(object_28/28)
 let     28 11 ; sust(pause_parameter/28) ; sust(pause_ability/11)
 pause   3
 ok

g ladd
 present 29 ; sust(object_29/29)
 gt      1 2 ; sust(total_carried/1)
 writeln "It's large and heavy&#59; you need to drop something first."
 done

g ladd
 present 29 ; sust(object_29/29)
 swap    28 29 ; sust(object_28/28) ; sust(object_29/29)
 get     28 ; sust(object_28/28)
 let     28 11 ; sust(pause_parameter/28) ; sust(pause_ability/11)
 pause   3
 ok

g all
 writeln "stThis facility is regrettably n otavailable on The Quill - sorry !"
 done

g hook
 at      36 ; sust(room_36/36)
 zero    18 ; sust(game_flag_18/18)
 writeln "You can't reach it&#59; it's high above you embedded in the rock."
 done

g hook
 at      45 ; sust(room_45/45)
 present 30 ; sust(object_30/30)
 writeln "How ?"
 done

g rope
 present 38 ; sust(object_38_rope/38)
 get     38 ; sust(object_38_rope/38)
 ok

g rope
 present 2 ; sust(object_2/2)
 zero    20 ; sust(game_flag_20/20)
 writeln "Please be more specific..."
 done

g rope
 at      40 ; sust(room_40/40)
 present 48 ; sust(object_48/48)
 writeln "Don't be crazy!"
 done

g rope
 present 48 ; sust(object_48/48)
 writeln "stAfter all this trouble?! Leave                 it be - you'll n eed it."
 done

g dog
 present 40 ; sust(object_40/40)
 writeln "Why ?"
 done

g egg
 present 44 ; sust(object_44_egg/44)
 get     44 ; sust(object_44_egg/44)
 ok

g egg
 present 45 ; sust(object_45/45)
 writeln "Why ?"
 done

g bark
 at      39 ; sust(room_39/39)
 writeln "Why ?"
 done

g shee
 at      42 ; sust(room_42/42)
 zero    23 ; sust(game_flag_23/23)
 writeln "How ?"
 done

g lifter
 present 51 ; sust(object_51_lift/51)
 get     51 ; sust(object_51_lift/51)
 ok

g plan
 present 68 ; sust(object_68/68)
 writeln "st&#34;Please don't touch the flora, &#34;               booms a voice fr om above."
 done

g cott
 get     56 ; sust(object_56_cott/56)
 ok

g _
 autog
 ok

drop box
 carried 6 ; sust(object_6_box/6)
 drop    6 ; sust(object_6_box/6)
 ok

drop lamp
 carried 1 ; sust(object_1_lamp/1)
 swap    1 60 ; sust(object_1_lamp/1) ; sust(object_60/60)
 drop    60 ; sust(object_60/60)
 writeln "Oops!"
 pause   25
 writeln "That wasn't very bright..."
 pause   120
 desc

drop lamp
 carried 0 ; sust(object_0_lamp/0)
 swap    0 60 ; sust(object_0_lamp/0) ; sust(object_60/60)
 drop    60 ; sust(object_60/60)
 writeln "Oops!"
 pause   25
 writeln "That wasn't very bright..."
 pause   120
 desc

drop jar
 carried 18 ; sust(object_18_jar/18)
 swap    18 36 ; sust(object_18_jar/18) ; sust(object_36/36)
 drop    36 ; sust(object_36/36)
 writeln "Oops!"
 pause   25
 writeln "That wasn't very bright..."
 pause   120
 desc

drop noti
 carried 26 ; sust(object_26_noti/26)
 drop    26 ; sust(object_26_noti/26)
 ok

drop ladd
 carried 28 ; sust(object_28/28)
 drop    28 ; sust(object_28/28)
 let     28 11 ; sust(pause_parameter/28) ; sust(pause_ability/11)
 pause   6
 writeln "Carefully you put it down."
 anykey
 desc

drop all
 gt      1 0 ; sust(total_carried/1)
 dropall
 let     28 11 ; sust(pause_parameter/28) ; sust(pause_ability/11)
 pause   6
 writeln "stYou carefully put everything o n               the ground."
 pause   150
 desc

drop all
 eq      1 0 ; sust(total_carried/1)
 notworn 8 ; sust(object_8_galo/8)
 notworn 67 ; sust(object_67_glov/67)
 writeln "Nice try, but nothing happens!"
 done

drop rope
 carried 38 ; sust(object_38_rope/38)
 drop    38 ; sust(object_38_rope/38)
 ok

drop egg
 at      38 ; sust(room_38/38)
 carried 44 ; sust(object_44_egg/44)
 swap    44 45 ; sust(object_44_egg/44) ; sust(object_45/45)
 drop    45 ; sust(object_45/45)
 place   45 39 ; sust(object_45/45) ; sust(room_39/39)
 writeln "Oops!"
 pause   50
 desc

drop egg
 notat   38 ; sust(room_38/38)
 carried 44 ; sust(object_44_egg/44)
 swap    44 45 ; sust(object_44_egg/44) ; sust(object_45/45)
 drop    45 ; sust(object_45/45)
 writeln "Oops!"
 pause   50
 desc

drop lifter
 carried 51 ; sust(object_51_lift/51)
 drop    51 ; sust(object_51_lift/51)
 ok

drop noth
 eq      1 0 ; sust(total_carried/1)
 writeln "Don't be crazy!"
 done

drop cott
 drop    56 ; sust(object_56_cott/56)
 ok

drop _
 at      27 ; sust(room_27/27)
 writeln "Not here."
 done

drop _
 at      30 ; sust(room_30/30)
 writeln "Not here."
 done

drop _
 at      45 ; sust(room_45/45)
 writeln "Not here."
 done

drop _
 at      47 ; sust(room_47/47)
 writeln "Not here."
 done

drop _
 autod
 ok

remo rod
 at      31 ; sust(room_31/31)
 present 23 ; sust(object_23/23)
 writeln "Too late!"
 done

remo need
 carried 12 ; sust(object_12_comp/12)
 create  11 ; sust(object_11_need/11)
 swap    10 12 ; sust(object_10_nail/10) ; sust(object_12_comp/12)
 writeln "You remove the needle from the nail and put it on the ground."
 anykey
 desc

remo noti
 present 25 ; sust(object_25/25)
 writeln "How ?"
 done

remo nail
 carried 51 ; sust(object_51_lift/51)
 create  10 ; sust(object_10_nail/10)
 swap    50 51 ; sust(object_50_broo/50) ; sust(object_51_lift/51)
 drop    50 ; sust(object_50_broo/50)
 get     10 ; sust(object_10_nail/10)
 writeln "You remove the nail from the broom."
 anykey
 desc

remo nail
 at      14 ; sust(room_14/14)
 notcarr 57 ; sust(object_57_plie/57)
 present 25 ; sust(object_25/25)
 writeln "How ?"
 done

remo lid
 carried 18 ; sust(object_18_jar/18)
 lt      19 220 ; sust(game_flag_19/19)
 writeln "How ?"
 done

remo hook
 at      45 ; sust(room_45/45)
 zero    18 ; sust(game_flag_18/18)
 writeln "How ?"
 done

remo rope
 at      29 ; sust(room_29/29)
 lt      21 255 ; sust(game_flag_21/21)
 writeln "Adventures should be SO simple!"
 done

remo cott
 worn    56 ; sust(object_56_cott/56)
 remove  56 ; sust(object_56_cott/56)
 ok

remo _
 autor
 ok

wear trou
 carried 14 ; sust(object_14_trou/14)
 writeln "They're about four sizes too big for you."
 done

wear cott
 carried 56 ; sust(object_56_cott/56)
 writeln "Could you put that another way ?"
 done

wear _
 autow
 ok

i _
 inven

r _ ; TODO: Check r in
 PROCESS 3

r unde
 at      19 ; sust(room_19/19)
 zero    113 ; sust(game_flag_13/113)
 let     113 100 ; sust(game_flag_13/113)
 create  17 ; sust(object_17_scre/17)
 writeln "You've found something..."
 plus    30 2 ; sust(total_player_score/30)
 pause   75
 desc

r unde
 at      16 ; sust(room_16/16)
 zero    22 ; sust(game_flag_22/22)
 let     22 100 ; sust(game_flag_22/22)
 create  47 ; sust(object_47_disc/47)
 writeln "You've found something..."
 pause   75
 plus    30 2 ; sust(total_player_score/30)
 desc

r unde
 at      19 ; sust(room_19/19)
 eq      113 100 ; sust(game_flag_13/113)
 writeln "You've done that."
 done

r unde
 at      16 ; sust(room_16/16)
 eq      22 100 ; sust(game_flag_22/22)
 writeln "You've done that."
 done

r unde
 writeln "Nothing further is revealed."
 done

r behi
 at      52 ; sust(room_52/52)
 zero    17 ; sust(game_flag_17/17)
 set     17 ; sust(game_flag_17/17)
 create  64 ; sust(object_64_spic/64)
 writeln "You've found something..."
 plus    30 3 ; sust(total_player_score/30)
 pause   75
 desc

r behi
 at      31 ; sust(room_31/31)
 eq      112 100 ; sust(game_flag_12/112)
 let     112 200 ; sust(game_flag_12/112)
 create  39 ; sust(object_39_bone/39)
 writeln "You find an old bone behind the generator."
 plus    30 3 ; sust(total_player_score/30)
 pause   150
 desc

r behi
 at      52 ; sust(room_52/52)
 eq      17 255 ; sust(game_flag_17/17)
 writeln "You've done that."
 done

r behi
 at      31 ; sust(room_31/31)
 eq      112 200 ; sust(game_flag_12/112)
 writeln "You've done that."
 done

r behi
 writeln "Nothing further is revealed."
 done

r _
 desc

qq _
 quit
 turns
 end

save _
 save

load _
 load

turn lid
 carried 18 ; sust(object_18_jar/18)
 lt      19 220 ; sust(game_flag_19/19)
 writeln "Adventures should be SO simple!"
 done

turn bark
 at      39 ; sust(room_39/39)
 absent  46 ; sust(object_46/46)
 create  46 ; sust(object_46/46)
 writeln "Throwing your arms around the large tree, you move the bark and reveal an opening..."
 plus    30 2 ; sust(total_player_score/30)
 anykey
 desc

turn bark
 at      39 ; sust(room_39/39)
 present 46 ; sust(object_46/46)
 writeln "You've done that."
 done

turn knob
 at      49 ; sust(room_49/49)
 notcarr 57 ; sust(object_57_plie/57)
 writeln "It's deeply recessed&#59; you can't reach it with your fingers."
 done

turn knob
 at      49 ; sust(room_49/49)
 carried 57 ; sust(object_57_plie/57)
 zero    26 ; sust(game_flag_26/26)
 set     26 ; sust(game_flag_26/26)
 plus    25 10 ; sust(game_flag_25/25)
 writeln "You turn the knurled knob with the pair of pliers."
 plus    30 3 ; sust(total_player_score/30)
 done

turn knob
 at      49 ; sust(room_49/49)
 eq      26 255 ; sust(game_flag_26/26)
 writeln "You've done that."
 done

turn _
 turns
 done

swin _
 at      40 ; sust(room_40/40)
 absent  48 ; sust(object_48/48)
 writeln "With what ?"
 done

swin _
 at      40 ; sust(room_40/40)
 present 48 ; sust(object_48/48)
 goto    42 ; sust(room_42/42)
 writeln "You swing yourself over the icy pool and land on the other side. You secure the rope in readiness for a possible return journey."
 place   48 42 ; sust(object_48/48) ; sust(room_42/42)
 let     28 5 ; sust(pause_parameter/28) ; sust(pause_sound_effect_white_noise/5)
 pause   3
 anykey
 desc

swin _
 at      42 ; sust(room_42/42)
 writeln "You swing yourself over the icy pool and land on the other side. You secure the rope in readiness for a possible return journey."
 place   48 40 ; sust(object_48/48) ; sust(room_40/40)
 goto    40 ; sust(room_40/40)
 let     28 5 ; sust(pause_parameter/28) ; sust(pause_sound_effect_white_noise/5)
 pause   3
 anykey
 desc

lift shee
 at      42 ; sust(room_42/42)
 zero    23 ; sust(game_flag_23/23)
 carried 51 ; sust(object_51_lift/51)
 let     23 10 ; sust(game_flag_23/23)
 create  49 ; sust(object_49_shee/49)
 drop    51 ; sust(object_51_lift/51)
 get     49 ; sust(object_49_shee/49)
 writeln "You lower the paper-lifter over the edge and lift the sheet of paper with the nail.  Removing the paper from the nail, you drop the paper-lifter, having no further use for it."
 plus    30 3 ; sust(total_player_score/30)
 anykey
 desc

lift shee
 at      42 ; sust(room_42/42)
 eq      23 10 ; sust(game_flag_23/23)
 writeln "You've done that."
 done

trip _
  PROCESS 4 ; trip on

28s 172w
 absent  55 ; sust(object_55/55)
 writeln "You can't do that...yet."
 done

28s 172w
 present 55 ; sust(object_55/55)
 absent  58 ; sust(object_58/58)
 create  58 ; sust(object_58/58)
 plus    25 10 ; sust(game_flag_25/25)
 writeln "You enter the coordinates into the computer.  The machine in the adjoining room swings into the correct position."
 let     28 5 ; sust(pause_parameter/28) ; sust(pause_sound_effect_white_noise/5)
 pause   4
 done

swim _
 atgt    2 ; sust(room_2/2)
 atlt    5 ; sust(room_5/5)
 writeln "stNo thanks! Water looks too col d."
 done

swim _
 atgt    39 ; sust(room_39/39)
 atlt    43 ; sust(room_43/43)
 writeln "stNo thanks! Water looks too col d."
 done

plug ear
 notat   49 ; sust(room_49/49)
 carried 56 ; sust(object_56_cott/56)
 writeln "Not here."
 done

plug ear
 at      49 ; sust(room_49/49)
 carried 56 ; sust(object_56_cott/56)
 wear    56 ; sust(object_56_cott/56)
 writeln "You plug your ears with the two pieces of cotton wool."
 plus    30 3 ; sust(total_player_score/30)
 done

kiss wife
 present 59 ; sust(object_59/59)
 writeln "&#34;Well I never,&#34; declares the wife, &#34;you have swept me off my feet - but you still can't have my honey!&#34;"
 done

kiss wife
 present 62 ; sust(object_62/62)
 present 18 ; sust(object_18_jar/18)
 notcarr 18 ; sust(object_18_jar/18)
 writeln "&#34;Well I never,&#34; declares the wife, &#34;you have swept me off my feet - but you still can't have my honey!&#34;"
 done

kiss wife
 present 62 ; sust(object_62/62)
 absent  18 ; sust(object_18_jar/18)
 writeln "If you insist."
 done

dry box
 carried 5 ; sust(object_5_box/5)
 writeln "How ?"
 done

pay guar
 present 43 ; sust(object_43/43)
 carried 9 ; sust(object_9_coin/9)
 writeln "Could you put that another way ?"
 done

cros pool
 at      40 ; sust(room_40/40)
 writeln "How ?"
 done

cros road
 at      1 ; sust(room_1/1)
 writeln "\nstOh dear, you misjudge the spee d of an oncoming Porsche and you                 are sent into ob livion!"
 end

dism comp
 carried 12 ; sust(object_12_comp/12)
 writeln "Could you put that another way ?"
 done

eat jar
 carried 18 ; sust(object_18_jar/18)
 lt      19 220 ; sust(game_flag_19/19)
 writeln "You can't do that...yet."
 done

eat jar
 carried 18 ; sust(object_18_jar/18)
 eq      19 220 ; sust(game_flag_19/19)
 writeln "You're sweet enough&#59; you don't need the honey!"
 done

eat spic
 carried 64 ; sust(object_64_spic/64)
 destroy 64 ; sust(object_64_spic/64)
 writeln "stThat was delicious."
 pause   100
 desc

save ram
 writeln "Data placed in memory."
 pause   50
 let     28 21 ; sust(pause_parameter/28) ; sust(pause_ram_save_load/21)
 pause   1

load ram
 writeln "Data recalled."
 pause   50
 let     28 21 ; sust(pause_parameter/28) ; sust(pause_ram_save_load/21)
 pause   50

rs _
 writeln "Data placed in memory."
 pause   50
 let     28 21 ; sust(pause_parameter/28) ; sust(pause_ram_save_load/21)
 pause   1

rl _
 writeln "Data recalled."
 pause   50
 let     28 21 ; sust(pause_parameter/28) ; sust(pause_ram_save_load/21)
 pause   50

font 1
 let     28 7 ; sust(pause_parameter/28) ; sust(pause_font_default/7)
 pause   1
 desc

font 2
 let     28 8 ; sust(pause_parameter/28) ; sust(pause_font_alternate/8)
 pause   1
 desc

drin wate
 at      4 ; sust(room_4/4)
 writeln "Just what's needed on a hot day!"
 done

shou _
 at      18 ; sust(room_18/18)
 present 13 ; sust(object_13/13)
 writeln "st&#34;Oy, why you shouting?  I'm no t               that deaf&#34; says the farmer."
 done

shou _
 writeln "Why ?"
 done

on gene
 at      31 ; sust(room_31/31)
 lt      115 255 ; sust(game_flag_15/115)
 writeln "How ?"
 done

kill bear
 present 37 ; sust(object_37/37)
 writeln "With what ?"
 done

kill _
 writeln "stIt is this writer's considered                 opinion that all  adventurers    have homicidal t endencies!"
 done

stan ladd
 atgt    32 ; sust(room_32/32)
 atlt    40 ; sust(room_40/40)
 carried 28 ; sust(object_28/28)
 drop    28 ; sust(object_28/28)
 let     28 11 ; sust(pause_parameter/28) ; sust(pause_ability/11)
 pause   6
 writeln "You stand the ladder in front of you."
 pause   150
 desc

peel bark
 at      39 ; sust(room_39/39)
 writeln "Why ?"
 done

smel plan
 at      52 ; sust(room_52/52)
 writeln "It is very fragrant."
 done

brib guar
 present 43 ; sust(object_43/43)
 carried 9 ; sust(object_9_coin/9)
 writeln "Don't be crazy!"
 done

send dog
 at      12 ; sust(room_12/12)
 eq      112 200 ; sust(game_flag_12/112)
 present 70 ; sust(object_70/70)
 present 40 ; sust(object_40/40)
 swap    35 70 ; sust(object_35/35) ; sust(object_70/70)
 destroy 40 ; sust(object_40/40)
 writeln "stHot takes his bone and squeeze s               through the gap.  He gnaws on it for a while, the n digs away at  the heap looking  for another. Byso doing, he lev els the heap andthe door swings open.           Hot notices the ventilation pipethrough which he 'd arrived. He  disappears into it, and with a  yelp is gone."
 plus    30 2 ; sust(total_player_score/30)
 anykey
 desc

send dog
 at      12 ; sust(room_12/12)
 absent  35 ; sust(object_35/35)
 lt      112 200 ; sust(game_flag_12/112)
 writeln "You can't do that...yet."
 done

h _
 at      18 ; sust(room_18/18)
 present 13 ; sust(object_13/13)
 writeln "Raise your voice&#59; he's deaf but he won't admit it!"
 done

h _
 at      12 ; sust(room_12/12)
 present 40 ; sust(object_40/40)
 writeln "stHot could probably flatten tha t pile if he could be persuaded tosqueeze through the door again ."
 done

h _
 at      26 ; sust(room_26/26)
 carried 11 ; sust(object_11_need/11)
 notcarr 10 ; sust(object_10_nail/10)
 writeln "You'll need to wobble the needle on something...."
 done

h _
 at      34 ; sust(room_34/34)
 present 37 ; sust(object_37/37)
 carried 18 ; sust(object_18_jar/18)
 writeln "Just leave the jar of honey for him!"
 done

h _
 at      35 ; sust(room_35/35)
 absent  29 ; sust(object_29/29)
 writeln "Make an improvised pathway."
 done

h _
 at      37 ; sust(room_37/37)
 notcarr 9 ; sust(object_9_coin/9)
 present 43 ; sust(object_43/43)
 writeln "Now where would we usually find a coin?"
 done

h _
 at      42 ; sust(room_42/42)
 zero    23 ; sust(game_flag_23/23)
 writeln "Tidy up like a park-keeper."
 done

h _
 at      43 ; sust(room_43/43)
 absent  66 ; sust(object_66/66)
 writeln "Ingratiate yourself to the lady of the house...."
 done

h _
 at      24 ; sust(room_24/24)
 present 18 ; sust(object_18_jar/18)
 zero    5 ; sust(countdown_player_input_1/5)
 writeln "Wait until she's distracted."
 done

h _
 at      12 ; sust(room_12/12)
 absent  40 ; sust(object_40/40)
 absent  35 ; sust(object_35/35)
 writeln "Have you checked the generator room thoroughly?"
 done

h _
 at      39 ; sust(room_39/39)
 absent  46 ; sust(object_46/46)
 writeln "The bark must be moved out of the way..."
 done

h _
 writeln "Not here."
 done

scor _
 score
 done

play dog
 present 40 ; sust(object_40/40)
 writeln "stHot raises a paw. Seems it's t heonly trick he knows."
 done

pet dog
 present 40 ; sust(object_40/40)
 writeln "stLick lick he responds!"
 done

clea till
 at      16 ; sust(room_16/16)
 writeln "Nice try, but nothing happens!"
 done

clea car
 at      1 ; sust(room_1/1)
 writeln "Old Father Time taps his foot patiently..."
 done

stea jar
 present 18 ; sust(object_18_jar/18)
 notcarr 18 ; sust(object_18_jar/18)
 zero    5 ; sust(countdown_player_input_1/5)
 present 59 ; sust(object_59/59)
 writeln "&#34;You leave that jar alone,&#34; says the farmer's wife chasing you out of the kitchen."
 anykey
 goto    21 ; sust(room_21/21)
 desc

stea jar
 present 18 ; sust(object_18_jar/18)
 notcarr 18 ; sust(object_18_jar/18)
 zero    5 ; sust(countdown_player_input_1/5)
 present 62 ; sust(object_62/62)
 writeln "&#34;You leave that jar alone,&#34; says the farmer's wife chasing you out of the kitchen."
 anykey
 goto    21 ; sust(room_21/21)
 desc

fill barr
 at      4 ; sust(room_4/4)
 carried 24 ; sust(object_24_barr/24)
 writeln "Why ?"
 done

rip noti
 at      14 ; sust(room_14/14)
 present 25 ; sust(object_25/25)
 writeln "Why ?"
 done


_ _
 hook    "RESPONSE_DEFAULT_START"


_ _
 hook    "RESPONSE_DEFAULT_END"







/PRO 1


_ _
 hook "PRO1"

_ _
 at     0
 bclear 12 5                      ; Set language to English

_ _
 islight
 listobj                        ; Lists present objects
 listnpc @38                    ; Lists present NPCs







/PRO 2


_ _
 eq      31 0 ; sust(total_turns_lower/31)
 eq      32 0 ; sust(total_turns_higher/32)
 ability 6 6


_ _
 hook    "PRO2"

_ _
 at      3 ; sust(room_3/3)
 present 3 ; sust(object_3/3)
 eq      32 0 ; sust(total_turns_higher/32)
 gt      31 20 ; sust(total_turns_lower/31)
 writeln "\nstThe man turns around and leave s."
 anykey
 destroy 3 ; sust(object_3/3)
 plus    31 1 ; sust(total_turns_lower/31)
 desc

_ _
 at      3 ; sust(room_3/3)
 zero    111 ; sust(game_flag_11/111)
 absent  3 ; sust(object_3/3)
 create  3 ; sust(object_3/3)
 let     111 10 ; sust(game_flag_11/111)
 writeln "\nstAcross the dam a man appears&#59; helooks in your direction."
 anykey
 desc

_ _
 atgt    20 ; sust(room_20/20)
 atlt    23 ; sust(room_23/23)
 absent  13 ; sust(object_13/13)
 chance  20
 create  13 ; sust(object_13/13)
 writeln "st\nThe farmer appears."
 pause   75
 desc

_ _
 atgt    20 ; sust(room_20/20)
 atlt    23 ; sust(room_23/23)
 present 13 ; sust(object_13/13)
 chance  25
 destroy 13 ; sust(object_13/13)
 writeln "\nThe farmer wanders off."
 pause   75
 desc

_ _
 carried 1 ; sust(object_1_lamp/1)
 present 13 ; sust(object_13/13)
 writeln "\n&#34;What the hades are you holding? My lovely lamp from the lounge?&#34; screams the farmer.  &#34;Get off my land you thief you!&#34;"
 goto    18 ; sust(room_18/18)
 destroy 13 ; sust(object_13/13)
 destroy 15 ; sust(object_15/15)
 destroy 1 ; sust(object_1_lamp/1)
 anykey
 desc

_ _
 carried 0 ; sust(object_0_lamp/0)
 present 13 ; sust(object_13/13)
 writeln "\n&#34;What the hades are you holding? My lovely lamp from the lounge?&#34; screams the farmer.  &#34;Get off my land you thief you!&#34;"
 goto    18 ; sust(room_18/18)
 destroy 13 ; sust(object_13/13)
 destroy 15 ; sust(object_15/15)
 destroy 0 ; sust(object_0_lamp/0)
 anykey
 desc

_ _
 present 0 ; sust(object_0_lamp/0)
 minus   29 1 ; sust(bitset_graphics_status/29)

_ _
 present 0 ; sust(object_0_lamp/0)
 gt      29 1 ; sust(bitset_graphics_status/29)
 lt      29 6 ; sust(bitset_graphics_status/29)
 writeln "\nstThe lamp is getting dimmer..."

_ _
 present 0 ; sust(object_0_lamp/0)
 eq      29 1 ; sust(bitset_graphics_status/29)
 swap    0 1 ; sust(object_0_lamp/0) ; sust(object_1_lamp/1)
 let     10 3 ; sust(countdown_player_input_unlit/10)
 clear   29 ; sust(bitset_graphics_status/29)
 desc

_ _
 eq      10 1 ; sust(countdown_player_input_unlit/10)
 writeln "\nstYou walk into a wall and crack{CLASS|center|           your skull.}"
 score
 end

_ _
 eq      29 88 ; sust(bitset_graphics_status/29)
 writeln "\nA needle on the lamp points to the &#124; mark.  Half your paraffin has been used."

_ _
 at      49 ; sust(room_49/49)
 absent  72 ; sust(object_72/72)
 clear   0 ; sust(yesno_is_dark/0)

_ _
 at      24 ; sust(room_24/24)
 present 18 ; sust(object_18_jar/18)
 notcarr 18 ; sust(object_18_jar/18)
 present 59 ; sust(object_59/59)
 zero    5 ; sust(countdown_player_input_1/5)
 chance  14
 writeln "\nst&#34;Oh dear, my stew is burning,&#34;                 cries the farmer 's wife.  She   turns to tend to  the pot."
 let     5 3 ; sust(countdown_player_input_1/5)
 swap    59 62 ; sust(object_59/59) ; sust(object_62/62)
 anykey
 desc

_ _
 at      24 ; sust(room_24/24)
 eq      5 1 ; sust(countdown_player_input_1/5)
 writeln "\nstHaving saved the stew, the goo d               lady returns to her chores."
 clear   5 ; sust(countdown_player_input_1/5)
 anykey
 desc

_ _
 gt      5 1 ; sust(countdown_player_input_1/5)
 writeln "\nThe farmer's wife is preoccupied with saving her stew..."

_ _
 at      24 ; sust(room_24/24)
 zero    5 ; sust(countdown_player_input_1/5)
 carried 18 ; sust(object_18_jar/18)
 pause   10
 writeln "\n&#34;Oh, so you're the one who stole my jar of honey,&#34; scolds the farmer's wife.  &#34;I'll have it back if you don't mind - and this time I'm going to lock it away.&#34;  She takes the jar from you, muttering something about adventurers who go into people's homes and just take things as soon as a back is turned."
 destroy 18 ; sust(object_18_jar/18)
 anykey
 desc

_ _
 gt      6 1 ; sust(countdown_player_input_2/6)
 lt      6 5 ; sust(countdown_player_input_2/6)
 writeln "\nstYour fingers are slipping..."

_ _
 eq      6 1 ; sust(countdown_player_input_2/6)
 lt      18 150 ; sust(game_flag_18/18)
 writeln "\nstYou fall to the ground breakin g your legs and arms.\n                "
 let     28 3 ; sust(pause_parameter/28) ; sust(pause_sound_effect_tone_decreasing/3)
 pause   200
 score
 end

_ _
 eq      6 1 ; sust(countdown_player_input_2/6)
 eq      18 150 ; sust(game_flag_18/18)
 writeln "\nWheeeeee!! Look at Mary Poppins floating down to safety!"
 goto    36 ; sust(room_36/36)
 let     28 3 ; sust(pause_parameter/28) ; sust(pause_sound_effect_tone_decreasing/3)
 pause   250
 anykey
 clear   6 ; sust(countdown_player_input_2/6)
 desc

_ _
 at      41 ; sust(room_41/41)
 writeln "\nBefore long you die a painful death resulting from hypothermia"
 score
 end

_ _
 at      41 ; sust(room_41/41)
 score
 end

_ _
 at      12 ; sust(room_12/12)
 carried 0 ; sust(object_0_lamp/0)
 carried 39 ; sust(object_39_bone/39)
 absent  40 ; sust(object_40/40)
 destroy 39 ; sust(object_39_bone/39)
 create  40 ; sust(object_40/40)
 writeln "\nA small dog suddenly comes into your life by squeezing through the partly-open steel door. He jumps up and grabs the bone from out of your hand.  Proud of himself, he sits there panting and waiting...."
 anykey
 desc

_ _
 present 37 ; sust(object_37/37)
 gt      7 1 ; sust(countdown_player_input_3/7)
 writeln "st\nThe bear advances towards you..."

_ _
 present 37 ; sust(object_37/37)
 eq      7 1 ; sust(countdown_player_input_3/7)
 writeln "\nThe bear lifts you like a toy and crushes you to death."
 score
 end

_ _
 zero    32 ; sust(total_turns_higher/32)
 eq      31 200 ; sust(total_turns_lower/31)
 cls
 writeln "\nThe asteroid is getting closer!!"
 pause   250
 plus    31 1 ; sust(total_turns_lower/31)
 desc

_ _
 eq      32 1 ; sust(total_turns_higher/32)
 eq      31 144 ; sust(total_turns_lower/31)
 cls
 writeln "\nThe asteroid is getting closer!!"
 pause   250
 plus    31 1 ; sust(total_turns_lower/31)
 desc

_ _
 eq      32 2 ; sust(total_turns_higher/32)
 eq      31 88 ; sust(total_turns_lower/31)
 cls
 writeln "\nThe asteroid is getting closer!!"
 pause   250
 plus    31 1 ; sust(total_turns_lower/31)
 desc

_ _
 eq      32 3 ; sust(total_turns_higher/32)
 eq      31 32 ; sust(total_turns_lower/31)
 cls
 writeln "\nThe asteroid is getting closer!!"
 pause   250
 plus    31 1 ; sust(total_turns_lower/31)
 desc

_ _
 eq      32 3 ; sust(total_turns_higher/32)
 eq      31 232 ; sust(total_turns_lower/31)
 cls
 writeln "\nThe asteroid is getting closer!!"
 pause   250
 plus    31 1 ; sust(total_turns_lower/31)
 desc

_ _
 eq      32 4 ; sust(total_turns_higher/32)
 eq      31 176 ; sust(total_turns_lower/31)
 cls
 writeln "\nThe asteroid is getting closer!!"
 pause   250
 plus    31 1 ; sust(total_turns_lower/31)
 desc

_ _
 eq      32 5 ; sust(total_turns_higher/32)
 eq      31 120 ; sust(total_turns_lower/31)
 cls
 writeln "\nThe asteroid is getting closer!!"
 pause   250
 plus    31 1 ; sust(total_turns_lower/31)
 desc

_ _
 eq      8 1 ; sust(countdown_player_input_4/8)
 writeln "st\nThe ledge creaks and groans and finally snaps. You fall into a dark and bottomless abyss."
 score
 end

_ _
 at      49 ; sust(room_49/49)
 eq      25 255 ; sust(game_flag_25/25)
 notworn 56 ; sust(object_56_cott/56)
 writeln "\nIt tears at your unprotected ear drums. You collapse with pain as the shriek goes far beyond the normal threshold of pain."

 score
 end

_ _
 at      49 ; sust(room_49/49)
 eq      25 255 ; sust(game_flag_25/25)
 worn    56 ; sust(object_56_cott/56)

 anykey
 cls
 writeln "The machine comes to life with a supersonic scream that could burst an eardrum, but the cotton wool protects you.  An invisible beam is sent skywards towards the errant asteroid.\nstThe repelling beam hits the asteroid, sending it into a new orbit away from Earth.\nPicking up a nearby phone, you report your success. You beam with pride as congratulations on your success shower on you. London has been saved, and your credibility left intact.\n\n{CLASS|center|CONGRATULATIONS}"
 plus    30 4 ; sust(total_player_score/30)
 score
 end

_ _
 at      5 ; sust(room_5/5)
 present 8 ; sust(object_8_galo/8)
 carried 57 ; sust(object_57_plie/57)
 writeln "\nstA passing tramp stops you and begs for the galoshes. Having no further need for them, you give them to him."
 destroy 8 ; sust(object_8_galo/8)
 anykey
 desc

_ _
 at      0 ; sust(room_0/0)
 goto    1 ; sust(room_1/1)
 let     29 200 ; sust(bitset_graphics_status/29)
 anykey
 desc


/PRO 3


in _
 carried 24 ; sust(object_24_barr/24)
 writeln "stIt seems to be empty, but you can't be sure."
 done

in _
 writeln "Please be more specific..."
 done

/PRO 4


on _
 at      48 ; sust(room_48/48)
 lt      24 200 ; sust(game_flag_24/24)
 writeln "Could you put that another way ?"
 done
