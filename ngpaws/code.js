// This file is (C) Carlos Sanchez 2014, released under the MIT license


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// GLOBAL VARIABLES AND CONSTANTS ///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// CONSTANTS 
var VOCABULARY_ID = 0;
var VOCABULARY_WORD = 1;
var VOCABULARY_TYPE = 2;

var WORDTYPE_VERB = 0;
var WORDTYPE_NOUN = 1
var WORDTYPE_ADJECT = 2;
var WORDTYPE_ADVERB = 3;
var WORDTYPE_PRONOUN = 4;
var WORDTYPE_CONJUNCTION = 5;
var WORDTYPE_PREPOSITION = 6;

var TIMER_MILLISECONDS  = 40;

var RESOURCE_TYPE_IMG = 1;
var RESOURCE_TYPE_SND = 2;

var PROCESS_RESPONSE = 0;
var PROCESS_DESCRIPTION = 1;
var PROCESS_TURN = 2;

var DIV_TEXT_SCROLL_STEP = 40;


// Aux
var SET_VALUE = 255; // Value assigned by SET condact
var EMPTY_WORD = 255; // Value for word types when no match is found (as for  sentences without adjective or name)
var MAX_WORD_LENGHT = 10;  // Number of characters considered per word
var FLAG_COUNT = 256;  // Number of flags
var NUM_CONNECTION_VERBS = 14; // Number of verbs used as connection, from 0 to N - 1
var NUM_CONVERTIBLE_NOUNS = 20;
var NUM_PROPER_NOUNS = 50; // Number of proper nouns, can't be used as pronoun reference
var EMPTY_OBJECT = 255; // To remark there is no object when the action requires a objno parameter
var NO_EXIT = 255;  // If an exit does not exist, its value is this value
var MAX_CHANNELS = 17; // Number of SFX channels
var RESOURCES_DIR='dat/';


//Attributes
var ATTR_LIGHT=0;			// Object produces light
var ATTR_WEARABLE=1;		// Object is wearable
var ATTR_CONTAINER=2;       // Object is a container
var ATTR_NPC=3;             // Object is actually an NPC
var ATTR_CONCEALED = 4; /// Present but not visible
var ATTR_EDIBLE = 5;   /// Can be eaten
var ATTR_DRINKABLE=6;
var ATTR_ENTERABLE = 7;
var ATTR_FEMALE = 8;
var ATTR_LOCKABLE = 9;
var ATTR_LOCKED = 10;
var ATTR_MALE = 11;
var ATTR_NEUTER=12;
var ATTR_OPENABLE =13;
var ATTR_OPEN=14;
var ATTR_PLURALNAME = 15;
var ATTR_TRANSPARENT=16;
var ATTR_SCENERY=17;
var ATTR_SUPPORTER = 18;
var ATTR_SWITCHABLE=19;
var ATTR_ON  =20;
var ATTR_STATIC  =21;



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// INTERNAL STRINGS ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General messages & strings
var STR_NEWLINE = '<br />';
var STR_PROMPT_START = '<span class="feedback">&gt; ';
var STR_PROMPT_END = '</span>';
var STR_RAMSAVE_FILENAME = 'RAMSAVE_SAVEGAME';



// Runtime error messages
var STR_WRONG_SYSMESS = 'WARNING: System message requested does not exist.'; 
var STR_WRONG_LOCATION = 'WARNING: Location requested does not exist.'; 
var STR_WRONG_MESSAGE = 'WARNING: Message requested does not exist.'; 
var STR_WRONG_PROCESS = 'WARNING: Process requested does not exist.' 
var STR_RAMLOAD_ERROR= 'WARNING: You can\'t restore game as it has not yet been saved.'; 
var STR_RUNTIME_VERSION  = 'ngPAWS runtime (C) 2014 Carlos Sanchez.  Released under {URL|http://www.opensource.org/licenses/MIT| MIT license}.\nBuzz sound libray (C) Jay Salvat. Released under the {URL|http://www.opensource.org/licenses/MIT| MIT license} \n jQuery (C) jQuery Foundation. Released under the {URL|https://jquery.org/license/| MIT license}.';
var STR_TRANSCRIPT = 'To copy the transcript to your clipboard, press Ctrl+C, then press Enter';

var STR_INVALID_TAG_SEQUENCE = 'Invalid tag sequence: ';
var STR_INVALID_TAG_SEQUENCE_EMPTY = 'Invalid tag sequence.';
var STR_INVALID_TAG_SEQUENCE_BADPARAMS = 'Invalid tag sequence: bad parameters.';
var STR_INVALID_TAG_SEQUENCE_BADTAG = 'Invalid tag sequence: unknown tag.';
var STR_BADIE = 'You are using a very old version of Internet Explorer. Some features of this product won\'t be avaliable, and other may not work properly. For a better experience please upgrade your browser or install some other one like Firefox, Chrome or Opera.\n\nIt\'s up to you to continue but be warned your experience may be affected.';
var STR_INVALID_OBJECT = 'WARNING: Trying to access object that does not exist'


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////     FLAGS     ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


var FLAG_LIGHT = 0;
var FLAG_OBJECTS_CARRIED_COUNT = 1;
var FLAG_AUTODEC2 = 2; 
var FLAG_AUTODEC3 = 3;
var FLAG_AUTODEC4 = 4;
var FLAG_AUTODEC5 = 5;
var FLAG_AUTODEC6 = 6;
var FLAG_AUTODEC7 = 7;
var FLAG_AUTODEC8 = 8;
var FLAG_AUTODEC9 = 9;
var FLAG_AUTODEC10 = 10;
var FLAG_ESCAPE = 11;
var FLAG_PARSER_SETTINGS = 12;
var FLAG_PICTURE_SETTINGS = 29
var FLAG_SCORE = 30;
var FLAG_TURNS_LOW = 31;
var FLAG_TURNS_HIGH = 32;
var FLAG_VERB = 33;
var FLAG_NOUN1 =34;
var FLAG_ADJECT1 = 35;
var FLAG_ADVERB = 36;
var FLAG_MAXOBJECTS_CARRIED = 37;
var FLAG_LOCATION = 38;
var FLAG_TOPLINE = 39;   // deprecated
var FLAG_MODE = 40;  // deprecated
var FLAG_PROTECT = 41;   // deprecated
var FLAG_PROMPT = 42; 
var FLAG_PREP = 43;
var FLAG_NOUN2 = 44;
var FLAG_ADJECT2 = 45;
var FLAG_PRONOUN = 46;
var FLAG_PRONOUN_ADJECT = 47;
var FLAG_TIMEOUT_LENGTH = 48;
var FLAG_TIMEOUT_SETTINGS = 49; 
var FLAG_DOALL_LOC = 50;
var FLAG_REFERRED_OBJECT = 51;
var FLAG_MAXWEIGHT_CARRIED = 52;
var FLAG_OBJECT_LIST_FORMAT = 53;
var FLAG_REFERRED_OBJECT_LOCATION = 54;
var FLAG_REFERRED_OBJECT_WEIGHT = 55;
var FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES = 56;
var FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES = 57;
var FLAG_EXPANSION1 = 58;
var FLAG_EXPANSION2 = 59;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// SPECIAL LOCATIONS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var LOCATION_WORN = 253;
var LOCATION_CARRIED = 254;
var LOCATION_NONCREATED = 252;
var LOCATION_HERE = 255;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////  SYSTEM MESSAGES  ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



var SYSMESS_ISDARK = 0;
var SYSMESS_YOUCANSEE = 1;
var SYSMESS_PROMPT0 = 2;
var SYSMESS_PROMPT1 = 3;
var SYSMESS_PROMPT2 = 4
var SYSMESS_PROMPT3= 5;
var SYSMESS_IDONTUNDERSTAND = 6;
var SYSMESS_WRONGDIRECTION = 7
var SYSMESS_CANTDOTHAT = 8;
var SYSMESS_YOUARECARRYING = 9;
var SYSMESS_WORN = 10;
var SYSMESS_CARRYING_NOTHING = 11;
var SYSMESS_AREYOUSURE = 12;
var SYSMESS_PLAYAGAIN = 13;
var SYSMESS_FAREWELL = 14;
var SYSMESS_OK = 15;
var SYSMESS_PRESSANYKEY = 16;
var SYSMESS_TURNS_START = 17;
var SYSMESS_TURNS_CONTINUE = 18;
var SYSMESS_TURNS_PLURAL = 19;
var SYSMESS_TURNS_END = 20;
var SYSMESS_SCORE_START= 21;
var SYSMESS_SCORE_END =22;
var SYSMESS_YOURENOTWEARINGTHAT = 23;
var SYSMESS_YOUAREALREADYWEARINGTHAT = 24;
var SYSMESS_YOUALREADYHAVEOBJECT = 25;
var SYSMESS_CANTSEETHAT = 26;
var SYSMESS_CANTCARRYANYMORE = 27;
var SYSMESS_YOUDONTHAVETHAT = 28;
var SYSMESS_YOUAREALREADYWAERINGOBJECT = 29;
var SYSMESS_YES = 30;
var SYSMESS_NO = 31;
var SYSMESS_MORE = 32;
var SYSMESS_CARET = 33;
var SYSMESS_TIMEOUT=35;
var SYSMESS_YOUTAKEOBJECT = 36;
var SYSMESS_YOUWEAROBJECT = 37;
var SYSMESS_YOUREMOVEOBJECT = 38;
var SYSMESS_YOUDROPOBJECT = 39;
var SYSMESS_YOUCANTWEAROBJECT = 40;
var SYSMESS_YOUCANTREMOVEOBJECT = 41;
var SYSMESS_CANTREMOVE_TOOMANYOBJECTS = 42;
var SYSMESS_WEIGHSTOOMUCH = 43;
var SYSMESS_YOUPUTOBJECTIN = 44;
var SYSMESS_YOUCANTTAKEOBJECTOUTOF = 45;
var SYSMESS_LISTSEPARATOR = 46;
var SYSMESS_LISTLASTSEPARATOR = 47;
var SYSMESS_LISTEND = 48;
var SYSMESS_YOUDONTHAVEOBJECT = 49;
var SYSMESS_YOUARENOTWEARINGOBJECT = 50;
var SYSMESS_PUTINTAKEOUTTERMINATION = 51;
var SYSMESS_THATISNOTIN = 52;
var SYSMESS_EMPTYOBJECTLIST = 53;
var SYSMESS_FILENOTFOUND = 54;
var SYSMESS_CORRUPTFILE = 55;
var SYSMESS_IOFAILURE = 56;
var SYSMESS_DIRECTORYFULL = 57;
var SYSMESS_LOADFILE = 58;
var SYSMESS_FILENOTFOUND = 59;
var SYSMESS_SAVEFILE = 60;
var SYSMESS_SORRY = 61;
var SYSMESS_NONSENSE_SENTENCE = 62;
var SYSMESS_NPCLISTSTART = 63;
var SYSMESS_NPCLISTCONTINUE = 64;
var SYSMESS_NPCLISTCONTINUE_PLURAL = 65;
var SYSMESS_INSIDE_YOUCANSEE = 66;
var SYSMESS_OVER_YOUCANSEE = 67;
var SYSMESS_YOUPUTOBJECTON = 68;
var SYSMESS_YOUCANTTAKEOBJECTFROM = 69;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// GLOBAL VARS //////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Parser vars
var last_player_orders = [];   // Store last player orders, to be able to restore it when pressing arrow up
var last_player_orders_pointer = 0;
var parser_word_found;
var player_order_buffer = '';
var player_order = ''; // Current player order
var previous_verb = EMPTY_WORD;
var previous_noun = EMPTY_WORD;
var previous_adject = EMPTY_WORD;
var pronoun_suffixes = [];


//Settings
var graphicsON = true; 
var soundsON = true; 
var interruptDisabled = false;
var showWarnings = true;

// waitkey commands callback function
var waitkey_callback_function = [];

//PAUSE
var inPause=false;
var pauseRemainingTime = 0;



// Transcript
var inTranscript = false;
var transcript = '';


// Block
var inBlock = false;
var unblock_process = null;


// END
var inEND = false;

//QUIT
var inQUIT = false;

//ANYKEY
var inAnykey = false;

//GETKEY
var inGetkey = false;
var getkey_return_flag = null;

// Status flags
var done_flag;
var describe_location_flag;
var in_response;
var success;

// doall control
var doall_flag;
var process_in_doall;
var entry_for_doall	= '';
var current_process;


var timeout_progress = 0;
var ramsave_value = null;
var num_objects;


// The flags
var flags = new Array();


// The sound channels
var soundChannels = [];
var soundLoopCount = [];

//The last free object attribute
var nextFreeAttr = 22;

//Autocomplete array
var autocomplete = new Array();
var autocompleteStep = 0;
var autocompleteBaseWord = '';
// PROCESSES

interruptProcessExists = false;

function pro000()
{
process_restart=true;
pro000_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p000e0000:
	{
 		if (skipdoall('p000e0000')) break p000e0000;
 		ACChook(0);
		if (done_flag) break pro000_restart;
		{}

	}

	// _ _
	p000e0001:
	{
 		if (skipdoall('p000e0001')) break p000e0001;
 		ACChook(1);
		if (done_flag) break pro000_restart;
		{}

	}

	// N _
	p000e0002:
	{
 		if (skipdoall('p000e0002')) break p000e0002;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0002;
 		}
		if (!CNDat(18)) break p000e0002;
		if (!CNDabsent(15)) break p000e0002;
 		ACCwriteln(2);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// N _
	p000e0003:
	{
 		if (skipdoall('p000e0003')) break p000e0003;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0003;
 		}
		if (!CNDat(18)) break p000e0003;
		if (!CNDpresent(15)) break p000e0003;
 		ACCgoto(20);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// N _
	p000e0004:
	{
 		if (skipdoall('p000e0004')) break p000e0004;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0004;
 		}
		if (!CNDat(33)) break p000e0004;
 		ACClet(7,4);
 		ACCgoto(34);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// S _
	p000e0005:
	{
 		if (skipdoall('p000e0005')) break p000e0005;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0005;
 		}
		if (!CNDat(34)) break p000e0005;
 		ACCclear(7);
 		ACCgoto(33);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// E _
	p000e0006:
	{
 		if (skipdoall('p000e0006')) break p000e0006;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0006;
 		}
		if (!CNDat(35)) break p000e0006;
		if (!CNDabsent(29)) break p000e0006;
 		ACCwriteln(3);
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// E _
	p000e0007:
	{
 		if (skipdoall('p000e0007')) break p000e0007;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0007;
 		}
		if (!CNDat(35)) break p000e0007;
		if (!CNDpresent(29)) break p000e0007;
 		ACCwriteln(4);
 		ACCplace(29,36);
 		ACCgoto(36);
 		ACCanykey();
 		function anykey00000() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00000);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// E _
	p000e0008:
	{
 		if (skipdoall('p000e0008')) break p000e0008;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0008;
 		}
		if (!CNDpresent(37)) break p000e0008;
 		ACCwriteln(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// E _
	p000e0009:
	{
 		if (skipdoall('p000e0009')) break p000e0009;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0009;
 		}
		if (!CNDat(34)) break p000e0009;
		if (!CNDabsent(37)) break p000e0009;
 		ACCgoto(35);
 		ACCset(0);
 		ACClet(10,3);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// E _
	p000e0010:
	{
 		if (skipdoall('p000e0010')) break p000e0010;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0010;
 		}
		if (!CNDat(34)) break p000e0010;
		if (!CNDpresent(37)) break p000e0010;
 		ACCwriteln(6);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// E _
	p000e0011:
	{
 		if (skipdoall('p000e0011')) break p000e0011;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0011;
 		}
		if (!CNDat(13)) break p000e0011;
		if (!CNDpresent(35)) break p000e0011;
 		ACCplace(35,12);
 		ACCgoto(12);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// E _
	p000e0012:
	{
 		if (skipdoall('p000e0012')) break p000e0012;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0012;
 		}
		if (!CNDat(48)) break p000e0012;
		if (!CNDpresent(72)) break p000e0012;
 		ACCplace(72,49);
 		ACCgoto(49);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// E _
	p000e0013:
	{
 		if (skipdoall('p000e0013')) break p000e0013;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0013;
 		}
		if (!CNDat(48)) break p000e0013;
 		ACCgoto(49);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// E _
	p000e0014:
	{
 		if (skipdoall('p000e0014')) break p000e0014;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0014;
 		}
		if (!CNDat(49)) break p000e0014;
		if (!CNDpresent(72)) break p000e0014;
 		ACCplace(72,50);
 		ACCgoto(50);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// E _
	p000e0015:
	{
 		if (skipdoall('p000e0015')) break p000e0015;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0015;
 		}
		if (!CNDat(49)) break p000e0015;
 		ACCgoto(50);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// W _
	p000e0016:
	{
 		if (skipdoall('p000e0016')) break p000e0016;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0016;
 		}
		if (!CNDat(18)) break p000e0016;
		if (!CNDabsent(15)) break p000e0016;
 		ACCwriteln(7);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// W _
	p000e0017:
	{
 		if (skipdoall('p000e0017')) break p000e0017;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0017;
 		}
		if (!CNDpresent(15)) break p000e0017;
 		ACCgoto(19);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// W _
	p000e0018:
	{
 		if (skipdoall('p000e0018')) break p000e0018;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0018;
 		}
		if (!CNDat(36)) break p000e0018;
		if (!CNDabsent(29)) break p000e0018;
 		ACCwriteln(8);
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// W _
	p000e0019:
	{
 		if (skipdoall('p000e0019')) break p000e0019;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0019;
 		}
		if (!CNDat(36)) break p000e0019;
		if (!CNDpresent(29)) break p000e0019;
 		ACCwriteln(9);
 		ACCplace(29,35);
 		ACCgoto(35);
 		ACCanykey();
 		function anykey00001() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00001);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// W _
	p000e0020:
	{
 		if (skipdoall('p000e0020')) break p000e0020;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0020;
 		}
		if (!CNDat(12)) break p000e0020;
		if (!CNDpresent(35)) break p000e0020;
 		ACCgoto(13);
 		ACCplace(35,13);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// W _
	p000e0021:
	{
 		if (skipdoall('p000e0021')) break p000e0021;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0021;
 		}
		if (!CNDat(12)) break p000e0021;
		if (!CNDabsent(35)) break p000e0021;
 		ACCwriteln(10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// W _
	p000e0022:
	{
 		if (skipdoall('p000e0022')) break p000e0022;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0022;
 		}
		if (!CNDat(37)) break p000e0022;
		if (!CNDpresent(43)) break p000e0022;
 		ACCwriteln(11);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// W _
	p000e0023:
	{
 		if (skipdoall('p000e0023')) break p000e0023;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0023;
 		}
		if (!CNDat(37)) break p000e0023;
		if (!CNDabsent(43)) break p000e0023;
 		ACCgoto(39);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// W _
	p000e0024:
	{
 		if (skipdoall('p000e0024')) break p000e0024;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0024;
 		}
		if (!CNDat(35)) break p000e0024;
 		ACCgoto(34);
 		ACCclear(0);
 		ACCclear(10);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// W _
	p000e0025:
	{
 		if (skipdoall('p000e0025')) break p000e0025;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0025;
 		}
		if (!CNDat(40)) break p000e0025;
 		ACCwriteln(12);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// W _
	p000e0026:
	{
 		if (skipdoall('p000e0026')) break p000e0026;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0026;
 		}
		if (!CNDat(49)) break p000e0026;
		if (!CNDpresent(72)) break p000e0026;
 		ACCplace(72,48);
 		ACCgoto(48);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// W _
	p000e0027:
	{
 		if (skipdoall('p000e0027')) break p000e0027;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0027;
 		}
		if (!CNDat(49)) break p000e0027;
 		ACCgoto(48);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// W _
	p000e0028:
	{
 		if (skipdoall('p000e0028')) break p000e0028;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0028;
 		}
		if (!CNDat(50)) break p000e0028;
		if (!CNDpresent(72)) break p000e0028;
 		ACCplace(72,49);
 		ACCgoto(49);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// W _
	p000e0029:
	{
 		if (skipdoall('p000e0029')) break p000e0029;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0029;
 		}
		if (!CNDat(50)) break p000e0029;
 		ACCgoto(49);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// W _
	p000e0030:
	{
 		if (skipdoall('p000e0030')) break p000e0030;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0030;
 		}
		if (!CNDat(1)) break p000e0030;
 		ACCwriteln(13);
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// NW _
	p000e0031:
	{
 		if (skipdoall('p000e0031')) break p000e0031;
 		if (in_response)
		{
			if (!CNDverb(6)) break p000e0031;
 		}
		if (!CNDat(26)) break p000e0031;
		if (!CNDnotcarr(12)) break p000e0031;
 		ACCwriteln(14);
 		ACCpause(100);
 		function anykey00002() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00002);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// NW _
	p000e0032:
	{
 		if (skipdoall('p000e0032')) break p000e0032;
 		if (in_response)
		{
			if (!CNDverb(6)) break p000e0032;
 		}
		if (!CNDat(26)) break p000e0032;
		if (!CNDcarried(12)) break p000e0032;
 		ACCwriteln(15);
 		ACCanykey();
 		function anykey00003() 
		{
 		ACCgoto(29);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00003);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SE _
	p000e0033:
	{
 		if (skipdoall('p000e0033')) break p000e0033;
 		if (in_response)
		{
			if (!CNDverb(7)) break p000e0033;
 		}
		if (!CNDat(29)) break p000e0033;
		if (!CNDnotcarr(12)) break p000e0033;
 		ACCwriteln(16);
 		ACCpause(100);
 		function anykey00004() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00004);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SE _
	p000e0034:
	{
 		if (skipdoall('p000e0034')) break p000e0034;
 		if (in_response)
		{
			if (!CNDverb(7)) break p000e0034;
 		}
		if (!CNDat(29)) break p000e0034;
		if (!CNDcarried(12)) break p000e0034;
 		ACCwriteln(17);
 		ACCgoto(26);
 		ACCanykey();
 		function anykey00005() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00005);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// UP _
	p000e0035:
	{
 		if (skipdoall('p000e0035')) break p000e0035;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0035;
 		}
		if (!CNDat(28)) break p000e0035;
 		ACCgoto(6);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// UP _
	p000e0036:
	{
 		if (skipdoall('p000e0036')) break p000e0036;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0036;
 		}
		if (!CNDat(40)) break p000e0036;
 		ACCclear(0);
 		ACCgoto(39);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// UP _
	p000e0037:
	{
 		if (skipdoall('p000e0037')) break p000e0037;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0037;
 		}
		if (!CNDat(44)) break p000e0037;
 		ACCclear(8);
 		ACCgoto(42);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// UP _
	p000e0038:
	{
 		if (skipdoall('p000e0038')) break p000e0038;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0038;
 		}
		if (!CNDat(49)) break p000e0038;
		if (!CNDpresent(52)) break p000e0038;
		if (!CNDabsent(72)) break p000e0038;
 		ACCgoto(51);
 		ACCplace(52,51);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// UP _
	p000e0039:
	{
 		if (skipdoall('p000e0039')) break p000e0039;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0039;
 		}
		if (!CNDat(49)) break p000e0039;
		if (!CNDabsent(52)) break p000e0039;
		if (!CNDpresent(72)) break p000e0039;
 		ACCplace(72,51);
 		ACCgoto(51);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// UP _
	p000e0040:
	{
 		if (skipdoall('p000e0040')) break p000e0040;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0040;
 		}
		if (!CNDat(49)) break p000e0040;
		if (!CNDabsent(52)) break p000e0040;
		if (!CNDabsent(72)) break p000e0040;
 		ACCgoto(51);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0041:
	{
 		if (skipdoall('p000e0041')) break p000e0041;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0041;
 		}
		if (!CNDat(6)) break p000e0041;
		if (!CNDnotworn(8)) break p000e0041;
 		ACCwriteln(18);
 		ACCanykey();
 		function anykey00006() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00006);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0042:
	{
 		if (skipdoall('p000e0042')) break p000e0042;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0042;
 		}
		if (!CNDat(6)) break p000e0042;
		if (!CNDworn(8)) break p000e0042;
 		ACCgoto(28);
 		ACCwriteln(19);
 		ACCanykey();
 		function anykey00007() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00007);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0043:
	{
 		if (skipdoall('p000e0043')) break p000e0043;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0043;
 		}
		if (!CNDat(45)) break p000e0043;
 		ACCgoto(36);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0044:
	{
 		if (skipdoall('p000e0044')) break p000e0044;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0044;
 		}
		if (!CNDat(42)) break p000e0044;
 		ACClet(8,2);
 		ACCgoto(44);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0045:
	{
 		if (skipdoall('p000e0045')) break p000e0045;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0045;
 		}
		if (!CNDat(51)) break p000e0045;
		if (!CNDpresent(52)) break p000e0045;
 		ACCplace(52,49);
 		ACCgoto(49);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0046:
	{
 		if (skipdoall('p000e0046')) break p000e0046;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0046;
 		}
		if (!CNDat(51)) break p000e0046;
		if (!CNDabsent(52)) break p000e0046;
		if (!CNDpresent(72)) break p000e0046;
 		ACCplace(72,49);
 		ACCgoto(49);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// DESC _
	p000e0047:
	{
 		if (skipdoall('p000e0047')) break p000e0047;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0047;
 		}
		if (!CNDat(51)) break p000e0047;
		if (!CNDabsent(52)) break p000e0047;
		if (!CNDabsent(72)) break p000e0047;
 		ACCgoto(49);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTER CANO
	p000e0048:
	{
 		if (skipdoall('p000e0048')) break p000e0048;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0048;
			if (!CNDnoun1(13)) break p000e0048;
 		}
		if (!CNDat(3)) break p000e0048;
		if (!CNDabsent(2)) break p000e0048;
 		ACCwriteln(20);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTER CANO
	p000e0049:
	{
 		if (skipdoall('p000e0049')) break p000e0049;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0049;
			if (!CNDnoun1(13)) break p000e0049;
 		}
		if (!CNDpresent(2)) break p000e0049;
 		ACCwriteln(21);
 		ACCdestroy(2);
 		ACCgoto(4);
 		ACCanykey();
 		function anykey00009() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00008() 
		{
 		ACCwriteln(22);
 		ACCanykey();
 		waitKey(anykey00009);
		}
 		waitKey(anykey00008);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// ENTER FOUN
	p000e0050:
	{
 		if (skipdoall('p000e0050')) break p000e0050;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0050;
			if (!CNDnoun1(18)) break p000e0050;
 		}
		if (!CNDat(4)) break p000e0050;
 		ACCwriteln(23);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTER TREE
	p000e0051:
	{
 		if (skipdoall('p000e0051')) break p000e0051;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0051;
			if (!CNDnoun1(68)) break p000e0051;
 		}
		if (!CNDpresent(46)) break p000e0051;
 		ACCwriteln(24);
 		ACCset(0);
 		ACCanykey();
 		function anykey00010() 
		{
 		ACCgoto(40);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00010);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// ENTER CAR
	p000e0052:
	{
 		if (skipdoall('p000e0052')) break p000e0052;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0052;
			if (!CNDnoun1(133)) break p000e0052;
 		}
		if (!CNDat(1)) break p000e0052;
 		ACCwriteln(25);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTER TRAC
	p000e0053:
	{
 		if (skipdoall('p000e0053')) break p000e0053;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0053;
			if (!CNDnoun1(139)) break p000e0053;
 		}
		if (!CNDat(19)) break p000e0053;
 		ACCgoto(53);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTER HUT
	p000e0054:
	{
 		if (skipdoall('p000e0054')) break p000e0054;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0054;
			if (!CNDnoun1(142)) break p000e0054;
 		}
		if (!CNDat(37)) break p000e0054;
 		ACCwriteln(26);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTER COOR
	p000e0055:
	{
 		if (skipdoall('p000e0055')) break p000e0055;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0055;
			if (!CNDnoun1(146)) break p000e0055;
 		}
		if (!CNDpresent(55)) break p000e0055;
		if (!CNDabsent(58)) break p000e0055;
 		ACCwriteln(27);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTER SHAF
	p000e0056:
	{
 		if (skipdoall('p000e0056')) break p000e0056;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0056;
			if (!CNDnoun1(177)) break p000e0056;
 		}
		if (!CNDpresent(69)) break p000e0056;
 		ACCwriteln(28);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTER _
	p000e0057:
	{
 		if (skipdoall('p000e0057')) break p000e0057;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0057;
 		}
		if (!CNDat(20)) break p000e0057;
		if (!CNDpresent(13)) break p000e0057;
 		ACCwriteln(29);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTER _
	p000e0058:
	{
 		if (skipdoall('p000e0058')) break p000e0058;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0058;
 		}
		if (!CNDat(20)) break p000e0058;
		if (!CNDabsent(13)) break p000e0058;
 		ACCgoto(21);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTER _
	p000e0059:
	{
 		if (skipdoall('p000e0059')) break p000e0059;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0059;
 		}
		if (!CNDat(8)) break p000e0059;
		if (!CNDabsent(20)) break p000e0059;
 		ACCwriteln(30);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTER _
	p000e0060:
	{
 		if (skipdoall('p000e0060')) break p000e0060;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0060;
 		}
		if (!CNDat(8)) break p000e0060;
		if (!CNDpresent(20)) break p000e0060;
 		ACCgoto(31);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTER _
	p000e0061:
	{
 		if (skipdoall('p000e0061')) break p000e0061;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0061;
 		}
		if (!CNDat(11)) break p000e0061;
 		ACCclear(0);
 		ACCclear(10);
 		ACCgoto(30);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTER _
	p000e0062:
	{
 		if (skipdoall('p000e0062')) break p000e0062;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0062;
 		}
		if (!CNDat(39)) break p000e0062;
		if (!CNDpresent(46)) break p000e0062;
 		ACCwriteln(31);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTER _
	p000e0063:
	{
 		if (skipdoall('p000e0063')) break p000e0063;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0063;
 		}
		if (!CNDat(43)) break p000e0063;
		if (!CNDabsent(66)) break p000e0063;
 		ACCwriteln(32);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTER _
	p000e0064:
	{
 		if (skipdoall('p000e0064')) break p000e0064;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0064;
 		}
		if (!CNDat(43)) break p000e0064;
		if (!CNDpresent(66)) break p000e0064;
 		ACCgoto(49);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0065:
	{
 		if (skipdoall('p000e0065')) break p000e0065;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0065;
 		}
		if (!CNDat(21)) break p000e0065;
		if (!CNDcarried(14)) break p000e0065;
		if (!CNDnotcarr(9)) break p000e0065;
 		ACCwriteln(33);
 		ACCdestroy(14);
 		ACCgoto(20);
 		ACCanykey();
 		function anykey00011() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00011);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0066:
	{
 		if (skipdoall('p000e0066')) break p000e0066;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0066;
 		}
		if (!CNDat(21)) break p000e0066;
		if (!CNDcarried(9)) break p000e0066;
		if (!CNDcarried(14)) break p000e0066;
 		ACCwriteln(34);
 		ACCwriteln(35);
 		ACCdestroy(9);
 		ACCdestroy(14);
 		ACCgoto(20);
 		ACCanykey();
 		function anykey00012() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00012);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0067:
	{
 		if (skipdoall('p000e0067')) break p000e0067;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0067;
 		}
		if (!CNDat(21)) break p000e0067;
 		ACCgoto(20);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0068:
	{
 		if (skipdoall('p000e0068')) break p000e0068;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0068;
 		}
		if (!CNDat(30)) break p000e0068;
 		ACCset(0);
 		ACClet(10,3);
 		ACCgoto(11);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0069:
	{
 		if (skipdoall('p000e0069')) break p000e0069;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0069;
 		}
		if (!CNDat(49)) break p000e0069;
 		ACCset(0);
 		ACClet(10,3);
 		ACCgoto(43);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// WAVE _
	p000e0070:
	{
 		if (skipdoall('p000e0070')) break p000e0070;
 		if (in_response)
		{
			if (!CNDverb(14)) break p000e0070;
 		}
		if (!CNDpresent(3)) break p000e0070;
 		ACCcreate(2);
 		ACCdestroy(3);
 		ACCcreate(4);
 		ACCget(4);
		if (!success) break pro000_restart;
 		ACCwriteln(36);
 		ACCplus(30,1);
 		ACCanykey();
 		function anykey00013() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00013);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// WAVE _
	p000e0071:
	{
 		if (skipdoall('p000e0071')) break p000e0071;
 		if (in_response)
		{
			if (!CNDverb(14)) break p000e0071;
 		}
		if (!CNDabsent(3)) break p000e0071;
 		ACCwriteln(37);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CANO
	p000e0072:
	{
 		if (skipdoall('p000e0072')) break p000e0072;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0072;
			if (!CNDnoun1(13)) break p000e0072;
 		}
		if (!CNDpresent(2)) break p000e0072;
		if (!CNDzero(20)) break p000e0072;
 		ACCwriteln(38);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CANO
	p000e0073:
	{
 		if (skipdoall('p000e0073')) break p000e0073;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0073;
			if (!CNDnoun1(13)) break p000e0073;
 		}
		if (!CNDat(3)) break p000e0073;
		if (!CNDabsent(2)) break p000e0073;
 		ACCwriteln(39);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM FOUN
	p000e0074:
	{
 		if (skipdoall('p000e0074')) break p000e0074;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0074;
			if (!CNDnoun1(18)) break p000e0074;
 		}
		if (!CNDat(4)) break p000e0074;
 		ACCwriteln(40);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM WATE
	p000e0075:
	{
 		if (skipdoall('p000e0075')) break p000e0075;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0075;
			if (!CNDnoun1(19)) break p000e0075;
 		}
		if (!CNDat(4)) break p000e0075;
		if (!CNDeq(111,10)) break p000e0075;
 		ACClet(111,100);
 		ACCcreate(5);
 		ACCwriteln(41);
 		ACCpause(200);
 		function anykey00014() 
		{
 		ACCplus(30,1);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00014);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// EXAM WATE
	p000e0076:
	{
 		if (skipdoall('p000e0076')) break p000e0076;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0076;
			if (!CNDnoun1(19)) break p000e0076;
 		}
		if (!CNDat(4)) break p000e0076;
		if (!CNDeq(111,100)) break p000e0076;
 		ACCwriteln(42);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOX
	p000e0077:
	{
 		if (skipdoall('p000e0077')) break p000e0077;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0077;
			if (!CNDnoun1(20)) break p000e0077;
 		}
		if (!CNDcarried(5)) break p000e0077;
 		ACCwriteln(43);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOX
	p000e0078:
	{
 		if (skipdoall('p000e0078')) break p000e0078;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0078;
			if (!CNDnoun1(20)) break p000e0078;
 		}
		if (!CNDcarried(6)) break p000e0078;
 		ACCwriteln(44);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CORN
	p000e0079:
	{
 		if (skipdoall('p000e0079')) break p000e0079;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0079;
			if (!CNDnoun1(21)) break p000e0079;
 		}
		if (!CNDat(32)) break p000e0079;
 		ACCwriteln(45);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PIT
	p000e0080:
	{
 		if (skipdoall('p000e0080')) break p000e0080;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0080;
			if (!CNDnoun1(22)) break p000e0080;
 		}
		if (!CNDat(6)) break p000e0080;
 		ACCwriteln(46);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PIT
	p000e0081:
	{
 		if (skipdoall('p000e0081')) break p000e0081;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0081;
			if (!CNDnoun1(22)) break p000e0081;
 		}
		if (!CNDat(28)) break p000e0081;
 		ACCwriteln(47);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM COIN
	p000e0082:
	{
 		if (skipdoall('p000e0082')) break p000e0082;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0082;
			if (!CNDnoun1(24)) break p000e0082;
 		}
		if (!CNDcarried(9)) break p000e0082;
 		ACCwriteln(48);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM COMP
	p000e0083:
	{
 		if (skipdoall('p000e0083')) break p000e0083;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0083;
			if (!CNDnoun1(25)) break p000e0083;
 		}
		if (!CNDpresent(55)) break p000e0083;
 		ACCwriteln(49);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM COMP
	p000e0084:
	{
 		if (skipdoall('p000e0084')) break p000e0084;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0084;
			if (!CNDnoun1(25)) break p000e0084;
 		}
		if (!CNDat(48)) break p000e0084;
		if (!CNDgt(24,199)) break p000e0084;
		if (!CNDlt(24,255)) break p000e0084;
 		ACCwriteln(50);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM COMP
	p000e0085:
	{
 		if (skipdoall('p000e0085')) break p000e0085;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0085;
			if (!CNDnoun1(25)) break p000e0085;
 		}
		if (!CNDat(48)) break p000e0085;
		if (!CNDlt(24,200)) break p000e0085;
 		ACCwriteln(51);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM COMP
	p000e0086:
	{
 		if (skipdoall('p000e0086')) break p000e0086;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0086;
			if (!CNDnoun1(25)) break p000e0086;
 		}
		if (!CNDcarried(12)) break p000e0086;
 		ACCwriteln(52);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM FARM
	p000e0087:
	{
 		if (skipdoall('p000e0087')) break p000e0087;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0087;
			if (!CNDnoun1(28)) break p000e0087;
 		}
		if (!CNDatgt(17)) break p000e0087;
		if (!CNDatlt(21)) break p000e0087;
		if (!CNDpresent(13)) break p000e0087;
 		ACCwriteln(53);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM MAGA
	p000e0088:
	{
 		if (skipdoall('p000e0088')) break p000e0088;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0088;
			if (!CNDnoun1(31)) break p000e0088;
 		}
		if (!CNDcarried(16)) break p000e0088;
 		ACCwriteln(54);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SCRE
	p000e0089:
	{
 		if (skipdoall('p000e0089')) break p000e0089;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0089;
			if (!CNDnoun1(33)) break p000e0089;
 		}
		if (!CNDpresent(17)) break p000e0089;
 		ACCwriteln(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM POCK
	p000e0090:
	{
 		if (skipdoall('p000e0090')) break p000e0090;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0090;
			if (!CNDnoun1(35)) break p000e0090;
 		}
		if (!CNDcarried(14)) break p000e0090;
		if (!CNDzero(114)) break p000e0090;
 		ACClet(114,100);
 		ACCcreate(9);
 		ACCwriteln(56);
 		ACCpause(75);
 		function anykey00015() 
		{
 		ACCplus(30,3);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00015);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// EXAM POCK
	p000e0091:
	{
 		if (skipdoall('p000e0091')) break p000e0091;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0091;
			if (!CNDnoun1(35)) break p000e0091;
 		}
		if (!CNDcarried(14)) break p000e0091;
		if (!CNDeq(114,100)) break p000e0091;
 		ACCwriteln(57);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM WARD
	p000e0092:
	{
 		if (skipdoall('p000e0092')) break p000e0092;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0092;
			if (!CNDnoun1(36)) break p000e0092;
 		}
		if (!CNDat(25)) break p000e0092;
 		ACCwriteln(58);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PANT
	p000e0093:
	{
 		if (skipdoall('p000e0093')) break p000e0093;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0093;
			if (!CNDnoun1(37)) break p000e0093;
 		}
		if (!CNDcarried(14)) break p000e0093;
 		ACCwriteln(59);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM LAMP
	p000e0094:
	{
 		if (skipdoall('p000e0094')) break p000e0094;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0094;
			if (!CNDnoun1(39)) break p000e0094;
 		}
		if (!CNDpresent(1)) break p000e0094;
		if (!CNDgt(29,0)) break p000e0094;
 		ACCwriteln(60);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM LAMP
	p000e0095:
	{
 		if (skipdoall('p000e0095')) break p000e0095;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0095;
			if (!CNDnoun1(39)) break p000e0095;
 		}
		if (!CNDpresent(0)) break p000e0095;
		if (!CNDgt(29,0)) break p000e0095;
 		ACCwriteln(61);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM LAMP
	p000e0096:
	{
 		if (skipdoall('p000e0096')) break p000e0096;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0096;
			if (!CNDnoun1(39)) break p000e0096;
 		}
		if (!CNDpresent(1)) break p000e0096;
		if (!CNDeq(29,0)) break p000e0096;
 		ACCwriteln(62);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DRIE
	p000e0097:
	{
 		if (skipdoall('p000e0097')) break p000e0097;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0097;
			if (!CNDnoun1(40)) break p000e0097;
 		}
		if (!CNDat(23)) break p000e0097;
 		ACCwriteln(63);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DOOR
	p000e0098:
	{
 		if (skipdoall('p000e0098')) break p000e0098;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0098;
			if (!CNDnoun1(42)) break p000e0098;
 		}
		if (!CNDat(12)) break p000e0098;
		if (!CNDabsent(35)) break p000e0098;
 		ACCwriteln(64);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DOOR
	p000e0099:
	{
 		if (skipdoall('p000e0099')) break p000e0099;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0099;
			if (!CNDnoun1(42)) break p000e0099;
 		}
		if (!CNDat(43)) break p000e0099;
		if (!CNDabsent(66)) break p000e0099;
 		ACCwriteln(65);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM FIRE
	p000e0100:
	{
 		if (skipdoall('p000e0100')) break p000e0100;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0100;
			if (!CNDnoun1(85)) break p000e0100;
 		}
		if (!CNDat(24)) break p000e0100;
		if (!CNDzero(112)) break p000e0100;
 		ACClet(112,100);
 		ACCcreate(19);
 		ACCwriteln(66);
 		ACCplus(30,1);
 		ACCanykey();
 		function anykey00016() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00016);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// EXAM FIRE
	p000e0101:
	{
 		if (skipdoall('p000e0101')) break p000e0101;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0101;
			if (!CNDnoun1(85)) break p000e0101;
 		}
		if (!CNDat(24)) break p000e0101;
		if (!CNDeq(112,100)) break p000e0101;
 		ACCwriteln(67);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM TABL
	p000e0102:
	{
 		if (skipdoall('p000e0102')) break p000e0102;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0102;
			if (!CNDnoun1(45)) break p000e0102;
 		}
		if (!CNDat(24)) break p000e0102;
 		ACCwriteln(68);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HONE
	p000e0103:
	{
 		if (skipdoall('p000e0103')) break p000e0103;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0103;
			if (!CNDnoun1(46)) break p000e0103;
 		}
		if (!CNDcarried(18)) break p000e0103;
		if (!CNDlt(19,220)) break p000e0103;
 		ACCwriteln(69);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HONE
	p000e0104:
	{
 		if (skipdoall('p000e0104')) break p000e0104;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0104;
			if (!CNDnoun1(46)) break p000e0104;
 		}
		if (!CNDcarried(18)) break p000e0104;
		if (!CNDeq(19,220)) break p000e0104;
 		ACCwriteln(70);
 		ACCwriteln(71);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CHAI
	p000e0105:
	{
 		if (skipdoall('p000e0105')) break p000e0105;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0105;
			if (!CNDnoun1(50)) break p000e0105;
 		}
		if (!CNDat(8)) break p000e0105;
		if (!CNDabsent(20)) break p000e0105;
 		ACCwriteln(72);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CUPB
	p000e0106:
	{
 		if (skipdoall('p000e0106')) break p000e0106;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0106;
			if (!CNDnoun1(52)) break p000e0106;
 		}
		if (!CNDpresent(21)) break p000e0106;
		if (!CNDeq(115,50)) break p000e0106;
 		ACClet(115,100);
 		ACCcreate(22);
 		ACCwriteln(73);
 		ACCplus(30,1);
 		ACCpause(75);
 		function anykey00017() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00017);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// EXAM CUPB
	p000e0107:
	{
 		if (skipdoall('p000e0107')) break p000e0107;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0107;
			if (!CNDnoun1(52)) break p000e0107;
 		}
		if (!CNDpresent(21)) break p000e0107;
		if (!CNDeq(115,100)) break p000e0107;
 		ACCwriteln(74);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CUPB
	p000e0108:
	{
 		if (skipdoall('p000e0108')) break p000e0108;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0108;
			if (!CNDnoun1(52)) break p000e0108;
 		}
		if (!CNDat(32)) break p000e0108;
		if (!CNDabsent(21)) break p000e0108;
 		ACCwriteln(75);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM ROD
	p000e0109:
	{
 		if (skipdoall('p000e0109')) break p000e0109;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0109;
			if (!CNDnoun1(53)) break p000e0109;
 		}
		if (!CNDcarried(22)) break p000e0109;
 		ACCwriteln(76);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BARR
	p000e0110:
	{
 		if (skipdoall('p000e0110')) break p000e0110;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0110;
			if (!CNDnoun1(55)) break p000e0110;
 		}
		if (!CNDpresent(24)) break p000e0110;
 		ACCwriteln(77);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM NEED
	p000e0111:
	{
 		if (skipdoall('p000e0111')) break p000e0111;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0111;
			if (!CNDnoun1(57)) break p000e0111;
 		}
		if (!CNDcarried(11)) break p000e0111;
 		ACCwriteln(78);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM NAIL
	p000e0112:
	{
 		if (skipdoall('p000e0112')) break p000e0112;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0112;
			if (!CNDnoun1(59)) break p000e0112;
 		}
		if (!CNDcarried(10)) break p000e0112;
 		ACCwriteln(79);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM NAIL
	p000e0113:
	{
 		if (skipdoall('p000e0113')) break p000e0113;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0113;
			if (!CNDnoun1(59)) break p000e0113;
 		}
		if (!CNDat(14)) break p000e0113;
		if (!CNDpresent(25)) break p000e0113;
 		ACCwriteln(80);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DAGG
	p000e0114:
	{
 		if (skipdoall('p000e0114')) break p000e0114;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0114;
			if (!CNDnoun1(61)) break p000e0114;
 		}
		if (!CNDcarried(27)) break p000e0114;
 		ACCwriteln(81);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM LADD
	p000e0115:
	{
 		if (skipdoall('p000e0115')) break p000e0115;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0115;
			if (!CNDnoun1(64)) break p000e0115;
 		}
		if (!CNDpresent(28)) break p000e0115;
 		ACCwriteln(82);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM TREE
	p000e0116:
	{
 		if (skipdoall('p000e0116')) break p000e0116;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0116;
			if (!CNDnoun1(68)) break p000e0116;
 		}
		if (!CNDat(39)) break p000e0116;
 		ACCwriteln(83);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PARA
	p000e0117:
	{
 		if (skipdoall('p000e0117')) break p000e0117;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0117;
			if (!CNDnoun1(71)) break p000e0117;
 		}
		if (!CNDpresent(31)) break p000e0117;
 		ACCwriteln(84);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM RIDG
	p000e0118:
	{
 		if (skipdoall('p000e0118')) break p000e0118;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0118;
			if (!CNDnoun1(72)) break p000e0118;
 		}
		if (!CNDat(46)) break p000e0118;
		if (!CNDeq(18,50)) break p000e0118;
 		ACClet(18,100);
 		ACCcreate(31);
 		ACCwriteln(85);
 		ACCplus(30,3);
 		ACCpause(75);
 		function anykey00018() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00018);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// EXAM RIDG
	p000e0119:
	{
 		if (skipdoall('p000e0119')) break p000e0119;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0119;
			if (!CNDnoun1(72)) break p000e0119;
 		}
		if (!CNDat(46)) break p000e0119;
		if (!CNDeq(18,100)) break p000e0119;
 		ACCwriteln(86);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM RAFT
	p000e0120:
	{
 		if (skipdoall('p000e0120')) break p000e0120;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0120;
			if (!CNDnoun1(74)) break p000e0120;
 		}
		if (!CNDpresent(32)) break p000e0120;
 		ACCwriteln(87);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HEAP
	p000e0121:
	{
 		if (skipdoall('p000e0121')) break p000e0121;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0121;
			if (!CNDnoun1(75)) break p000e0121;
 		}
		if (!CNDpresent(41)) break p000e0121;
 		ACCwriteln(88);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SPAD
	p000e0122:
	{
 		if (skipdoall('p000e0122')) break p000e0122;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0122;
			if (!CNDnoun1(77)) break p000e0122;
 		}
		if (!CNDpresent(33)) break p000e0122;
 		ACCwriteln(89);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM ROPE
	p000e0123:
	{
 		if (skipdoall('p000e0123')) break p000e0123;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0123;
			if (!CNDnoun1(79)) break p000e0123;
 		}
		if (!CNDat(29)) break p000e0123;
		if (!CNDeq(21,100)) break p000e0123;
 		ACCwriteln(90);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM ROPE
	p000e0124:
	{
 		if (skipdoall('p000e0124')) break p000e0124;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0124;
			if (!CNDnoun1(79)) break p000e0124;
 		}
		if (!CNDcarried(34)) break p000e0124;
 		ACCwriteln(91);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DOG
	p000e0125:
	{
 		if (skipdoall('p000e0125')) break p000e0125;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0125;
			if (!CNDnoun1(82)) break p000e0125;
 		}
		if (!CNDpresent(40)) break p000e0125;
 		ACCwriteln(92);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM FIRE
	p000e0126:
	{
 		if (skipdoall('p000e0126')) break p000e0126;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0126;
			if (!CNDnoun1(85)) break p000e0126;
 		}
		if (!CNDat(24)) break p000e0126;
		if (!CNDzero(112)) break p000e0126;
 		ACClet(112,100);
 		ACCcreate(19);
 		ACCwriteln(93);
 		ACCplus(30,1);
 		ACCanykey();
 		function anykey00019() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00019);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// EXAM FIRE
	p000e0127:
	{
 		if (skipdoall('p000e0127')) break p000e0127;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0127;
			if (!CNDnoun1(85)) break p000e0127;
 		}
		if (!CNDat(24)) break p000e0127;
		if (!CNDeq(112,100)) break p000e0127;
 		ACCwriteln(94);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BEAR
	p000e0128:
	{
 		if (skipdoall('p000e0128')) break p000e0128;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0128;
			if (!CNDnoun1(90)) break p000e0128;
 		}
		if (!CNDpresent(37)) break p000e0128;
 		ACCwriteln(95);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BONE
	p000e0129:
	{
 		if (skipdoall('p000e0129')) break p000e0129;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0129;
			if (!CNDnoun1(94)) break p000e0129;
 		}
		if (!CNDpresent(39)) break p000e0129;
 		ACCwriteln(96);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM IGNITION
	p000e0130:
	{
 		if (skipdoall('p000e0130')) break p000e0130;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0130;
			if (!CNDnoun1(95)) break p000e0130;
 		}
		if (!CNDat(53)) break p000e0130;
 		ACCwriteln(97);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM NICH
	p000e0131:
	{
 		if (skipdoall('p000e0131')) break p000e0131;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0131;
			if (!CNDnoun1(96)) break p000e0131;
 		}
		if (!CNDat(47)) break p000e0131;
		if (!CNDeq(20,100)) break p000e0131;
 		ACCset(20);
 		ACCcreate(67);
 		ACCwriteln(98);
 		ACCplus(30,3);
 		ACCpause(75);
 		function anykey00020() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00020);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// EXAM NICH
	p000e0132:
	{
 		if (skipdoall('p000e0132')) break p000e0132;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0132;
			if (!CNDnoun1(96)) break p000e0132;
 		}
		if (!CNDat(47)) break p000e0132;
		if (!CNDeq(20,255)) break p000e0132;
 		ACCwriteln(99);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GUAR
	p000e0133:
	{
 		if (skipdoall('p000e0133')) break p000e0133;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0133;
			if (!CNDnoun1(97)) break p000e0133;
 		}
		if (!CNDpresent(43)) break p000e0133;
 		ACCwriteln(100);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM EGG
	p000e0134:
	{
 		if (skipdoall('p000e0134')) break p000e0134;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0134;
			if (!CNDnoun1(98)) break p000e0134;
 		}
		if (!CNDpresent(44)) break p000e0134;
 		ACCwriteln(101);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BARK
	p000e0135:
	{
 		if (skipdoall('p000e0135')) break p000e0135;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0135;
			if (!CNDnoun1(109)) break p000e0135;
 		}
		if (!CNDat(39)) break p000e0135;
 		ACCwriteln(102);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM TILL
	p000e0136:
	{
 		if (skipdoall('p000e0136')) break p000e0136;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0136;
			if (!CNDnoun1(111)) break p000e0136;
 		}
		if (!CNDat(16)) break p000e0136;
 		ACCwriteln(103);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM DISC
	p000e0137:
	{
 		if (skipdoall('p000e0137')) break p000e0137;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0137;
			if (!CNDnoun1(112)) break p000e0137;
 		}
		if (!CNDcarried(47)) break p000e0137;
 		ACCwriteln(104);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PAPE
	p000e0138:
	{
 		if (skipdoall('p000e0138')) break p000e0138;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0138;
			if (!CNDnoun1(114)) break p000e0138;
 		}
		if (!CNDat(42)) break p000e0138;
		if (!CNDzero(23)) break p000e0138;
 		ACCwriteln(105);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PAPE
	p000e0139:
	{
 		if (skipdoall('p000e0139')) break p000e0139;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0139;
			if (!CNDnoun1(114)) break p000e0139;
 		}
		if (!CNDpresent(49)) break p000e0139;
 		ACCwriteln(106);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BROO
	p000e0140:
	{
 		if (skipdoall('p000e0140')) break p000e0140;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0140;
			if (!CNDnoun1(115)) break p000e0140;
 		}
		if (!CNDpresent(50)) break p000e0140;
 		ACCwriteln(107);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM LIFTER
	p000e0141:
	{
 		if (skipdoall('p000e0141')) break p000e0141;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0141;
			if (!CNDnoun1(203)) break p000e0141;
 		}
		if (!CNDpresent(51)) break p000e0141;
 		ACCwriteln(108);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM LIFTER
	p000e0142:
	{
 		if (skipdoall('p000e0142')) break p000e0142;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0142;
			if (!CNDnoun1(203)) break p000e0142;
 		}
		if (!CNDat(9)) break p000e0142;
 		ACCwriteln(109);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM LEDG
	p000e0143:
	{
 		if (skipdoall('p000e0143')) break p000e0143;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0143;
			if (!CNDnoun1(117)) break p000e0143;
 		}
		if (!CNDat(42)) break p000e0143;
		if (!CNDzero(23)) break p000e0143;
 		ACCwriteln(110);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM TRIPSWITCH
	p000e0144:
	{
 		if (skipdoall('p000e0144')) break p000e0144;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0144;
			if (!CNDnoun1(204)) break p000e0144;
 		}
		if (!CNDpresent(53)) break p000e0144;
 		ACCwriteln(111);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM WALL
	p000e0145:
	{
 		if (skipdoall('p000e0145')) break p000e0145;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0145;
			if (!CNDnoun1(121)) break p000e0145;
 		}
		if (!CNDat(50)) break p000e0145;
		if (!CNDzero(24)) break p000e0145;
 		ACClet(24,100);
 		ACCcreate(53);
 		ACCwriteln(112);
 		ACCplus(30,2);
 		ACCpause(75);
 		function anykey00021() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00021);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// EXAM WALL
	p000e0146:
	{
 		if (skipdoall('p000e0146')) break p000e0146;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0146;
			if (!CNDnoun1(121)) break p000e0146;
 		}
		if (!CNDat(50)) break p000e0146;
		if (!CNDeq(24,100)) break p000e0146;
 		ACCwriteln(113);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM MACH
	p000e0147:
	{
 		if (skipdoall('p000e0147')) break p000e0147;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0147;
			if (!CNDnoun1(122)) break p000e0147;
 		}
		if (!CNDat(49)) break p000e0147;
 		ACCwriteln(114);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM POT
	p000e0148:
	{
 		if (skipdoall('p000e0148')) break p000e0148;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0148;
			if (!CNDnoun1(129)) break p000e0148;
 		}
		if (!CNDat(24)) break p000e0148;
 		ACCwriteln(115);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PATT
	p000e0149:
	{
 		if (skipdoall('p000e0149')) break p000e0149;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0149;
			if (!CNDnoun1(132)) break p000e0149;
 		}
		if (!CNDat(39)) break p000e0149;
 		ACCwriteln(116);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CAR
	p000e0150:
	{
 		if (skipdoall('p000e0150')) break p000e0150;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0150;
			if (!CNDnoun1(133)) break p000e0150;
 		}
		if (!CNDat(1)) break p000e0150;
 		ACCwriteln(117);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PLIE
	p000e0151:
	{
 		if (skipdoall('p000e0151')) break p000e0151;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0151;
			if (!CNDnoun1(135)) break p000e0151;
 		}
		if (!CNDcarried(57)) break p000e0151;
 		ACCwriteln(118);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SCRO
	p000e0152:
	{
 		if (skipdoall('p000e0152')) break p000e0152;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0152;
			if (!CNDnoun1(138)) break p000e0152;
 		}
		if (!CNDpresent(63)) break p000e0152;
 		ACCwriteln(119);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM TRAC
	p000e0153:
	{
 		if (skipdoall('p000e0153')) break p000e0153;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0153;
			if (!CNDnoun1(139)) break p000e0153;
 		}
		if (!CNDat(19)) break p000e0153;
 		ACCwriteln(120);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM POOL
	p000e0154:
	{
 		if (skipdoall('p000e0154')) break p000e0154;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0154;
			if (!CNDnoun1(144)) break p000e0154;
 		}
		if (!CNDat(40)) break p000e0154;
 		ACCwriteln(121);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GARD
	p000e0155:
	{
 		if (skipdoall('p000e0155')) break p000e0155;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0155;
			if (!CNDnoun1(147)) break p000e0155;
 		}
		if (!CNDat(52)) break p000e0155;
 		ACCwriteln(122);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HERB
	p000e0156:
	{
 		if (skipdoall('p000e0156')) break p000e0156;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0156;
			if (!CNDnoun1(148)) break p000e0156;
 		}
		if (!CNDcarried(64)) break p000e0156;
 		ACCwriteln(123);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM KEY
	p000e0157:
	{
 		if (skipdoall('p000e0157')) break p000e0157;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0157;
			if (!CNDnoun1(150)) break p000e0157;
 		}
		if (!CNDcarried(65)) break p000e0157;
 		ACCwriteln(124);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GENE
	p000e0158:
	{
 		if (skipdoall('p000e0158')) break p000e0158;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0158;
			if (!CNDnoun1(161)) break p000e0158;
 		}
		if (!CNDat(31)) break p000e0158;
		if (!CNDlt(115,255)) break p000e0158;
 		ACCwriteln(125);
 		ACCwriteln(126);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GENE
	p000e0159:
	{
 		if (skipdoall('p000e0159')) break p000e0159;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0159;
			if (!CNDnoun1(161)) break p000e0159;
 		}
		if (!CNDat(31)) break p000e0159;
		if (!CNDeq(115,255)) break p000e0159;
 		ACCwriteln(127);
 		ACCwriteln(128);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HOLE
	p000e0160:
	{
 		if (skipdoall('p000e0160')) break p000e0160;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0160;
			if (!CNDnoun1(162)) break p000e0160;
 		}
		if (!CNDpresent(7)) break p000e0160;
 		ACCwriteln(129);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM PLAN
	p000e0161:
	{
 		if (skipdoall('p000e0161')) break p000e0161;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0161;
			if (!CNDnoun1(165)) break p000e0161;
 		}
		if (!CNDat(52)) break p000e0161;
 		ACCwriteln(130);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM HOTP
	p000e0162:
	{
 		if (skipdoall('p000e0162')) break p000e0162;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0162;
			if (!CNDnoun1(176)) break p000e0162;
 		}
		if (!CNDat(50)) break p000e0162;
 		ACCwriteln(131);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SHAF
	p000e0163:
	{
 		if (skipdoall('p000e0163')) break p000e0163;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0163;
			if (!CNDnoun1(177)) break p000e0163;
 		}
		if (!CNDpresent(69)) break p000e0163;
 		ACCwriteln(132);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM STAL
	p000e0164:
	{
 		if (skipdoall('p000e0164')) break p000e0164;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0164;
			if (!CNDnoun1(178)) break p000e0164;
 		}
		if (!CNDatgt(34)) break p000e0164;
		if (!CNDatlt(37)) break p000e0164;
 		ACCwriteln(133);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SKET
	p000e0165:
	{
 		if (skipdoall('p000e0165')) break p000e0165;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0165;
			if (!CNDnoun1(182)) break p000e0165;
 		}
		if (!CNDcarried(16)) break p000e0165;
 		ACCwriteln(134);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GALO
	p000e0166:
	{
 		if (skipdoall('p000e0166')) break p000e0166;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0166;
			if (!CNDnoun1(200)) break p000e0166;
 		}
		if (!CNDpresent(8)) break p000e0166;
 		ACCwriteln(135);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM COTT
	p000e0167:
	{
 		if (skipdoall('p000e0167')) break p000e0167;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0167;
			if (!CNDnoun1(201)) break p000e0167;
 		}
		if (!CNDcarried(56)) break p000e0167;
 		ACCwriteln(136);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM GLOV
	p000e0168:
	{
 		if (skipdoall('p000e0168')) break p000e0168;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0168;
			if (!CNDnoun1(202)) break p000e0168;
 		}
		if (!CNDpresent(67)) break p000e0168;
 		ACCwriteln(137);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM _
	p000e0169:
	{
 		if (skipdoall('p000e0169')) break p000e0169;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0169;
 		}
 		ACCwriteln(138);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// P _
	p000e0170:
	{
 		if (skipdoall('p000e0170')) break p000e0170;
 		if (in_response)
		{
			if (!CNDverb(16)) break p000e0170;
 		}
 		ACCwriteln(139);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR CORN
	p000e0171:
	{
 		if (skipdoall('p000e0171')) break p000e0171;
 		if (in_response)
		{
			if (!CNDverb(23)) break p000e0171;
			if (!CNDnoun1(21)) break p000e0171;
 		}
		if (!CNDat(32)) break p000e0171;
		if (!CNDzero(27)) break p000e0171;
 		ACClet(27,100);
 		ACCcreate(33);
 		ACCwriteln(140);
 		ACCplus(30,2);
 		ACCpause(75);
 		function anykey00022() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00022);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SEAR CORN
	p000e0172:
	{
 		if (skipdoall('p000e0172')) break p000e0172;
 		if (in_response)
		{
			if (!CNDverb(23)) break p000e0172;
			if (!CNDnoun1(21)) break p000e0172;
 		}
		if (!CNDat(32)) break p000e0172;
		if (!CNDeq(27,100)) break p000e0172;
 		ACCwriteln(141);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR PIT
	p000e0173:
	{
 		if (skipdoall('p000e0173')) break p000e0173;
 		if (in_response)
		{
			if (!CNDverb(23)) break p000e0173;
			if (!CNDnoun1(22)) break p000e0173;
 		}
		if (!CNDat(28)) break p000e0173;
		if (!CNDeq(27,220)) break p000e0173;
 		ACCset(27);
 		ACCcreate(57);
 		ACCwriteln(142);
 		ACCplus(30,2);
 		ACCpause(75);
 		function anykey00023() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00023);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SEAR PIT
	p000e0174:
	{
 		if (skipdoall('p000e0174')) break p000e0174;
 		if (in_response)
		{
			if (!CNDverb(23)) break p000e0174;
			if (!CNDnoun1(22)) break p000e0174;
 		}
		if (!CNDat(28)) break p000e0174;
		if (!CNDeq(27,255)) break p000e0174;
 		ACCwriteln(143);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR POCK
	p000e0175:
	{
 		if (skipdoall('p000e0175')) break p000e0175;
 		if (in_response)
		{
			if (!CNDverb(23)) break p000e0175;
			if (!CNDnoun1(35)) break p000e0175;
 		}
		if (!CNDcarried(14)) break p000e0175;
		if (!CNDzero(114)) break p000e0175;
 		ACClet(114,100);
 		ACCcreate(9);
 		ACCwriteln(144);
 		ACCpause(75);
 		function anykey00024() 
		{
 		ACCplus(30,3);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00024);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SEAR POCK
	p000e0176:
	{
 		if (skipdoall('p000e0176')) break p000e0176;
 		if (in_response)
		{
			if (!CNDverb(23)) break p000e0176;
			if (!CNDnoun1(35)) break p000e0176;
 		}
		if (!CNDcarried(14)) break p000e0176;
		if (!CNDeq(114,100)) break p000e0176;
 		ACCwriteln(145);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SEAR _
	p000e0177:
	{
 		if (skipdoall('p000e0177')) break p000e0177;
 		if (in_response)
		{
			if (!CNDverb(23)) break p000e0177;
 		}
 		ACCwriteln(146);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LIGH COMP
	p000e0178:
	{
 		if (skipdoall('p000e0178')) break p000e0178;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0178;
			if (!CNDnoun1(25)) break p000e0178;
 		}
		if (!CNDat(48)) break p000e0178;
 		ACCwriteln(147);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LIGH COMP
	p000e0179:
	{
 		if (skipdoall('p000e0179')) break p000e0179;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0179;
			if (!CNDnoun1(25)) break p000e0179;
 		}
		if (!CNDat(48)) break p000e0179;
		if (!CNDlt(24,200)) break p000e0179;
 		ACCwriteln(148);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN GATE
	p000e0180:
	{
 		if (skipdoall('p000e0180')) break p000e0180;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0180;
			if (!CNDnoun1(26)) break p000e0180;
 		}
		if (!CNDat(18)) break p000e0180;
		if (!CNDabsent(15)) break p000e0180;
		if (!CNDpresent(13)) break p000e0180;
 		ACCwriteln(149);
 		ACCgoto(4);
 		ACCanykey();
 		function anykey00025() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00025);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// OPEN MAGA
	p000e0181:
	{
 		if (skipdoall('p000e0181')) break p000e0181;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0181;
			if (!CNDnoun1(31)) break p000e0181;
 		}
		if (!CNDcarried(16)) break p000e0181;
 		ACCwriteln(150);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN WARD
	p000e0182:
	{
 		if (skipdoall('p000e0182')) break p000e0182;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0182;
			if (!CNDnoun1(36)) break p000e0182;
 		}
		if (!CNDat(25)) break p000e0182;
 		ACCwriteln(151);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0183:
	{
 		if (skipdoall('p000e0183')) break p000e0183;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0183;
			if (!CNDnoun1(42)) break p000e0183;
 		}
		if (!CNDat(8)) break p000e0183;
		if (!CNDabsent(20)) break p000e0183;
 		ACCwriteln(152);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0184:
	{
 		if (skipdoall('p000e0184')) break p000e0184;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0184;
			if (!CNDnoun1(42)) break p000e0184;
 		}
		if (!CNDat(12)) break p000e0184;
		if (!CNDabsent(35)) break p000e0184;
 		ACCwriteln(153);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0185:
	{
 		if (skipdoall('p000e0185')) break p000e0185;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0185;
			if (!CNDnoun1(42)) break p000e0185;
 		}
		if (!CNDat(12)) break p000e0185;
		if (!CNDabsent(35)) break p000e0185;
 		ACCwriteln(154);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0186:
	{
 		if (skipdoall('p000e0186')) break p000e0186;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0186;
			if (!CNDnoun1(42)) break p000e0186;
 		}
		if (!CNDat(43)) break p000e0186;
		if (!CNDabsent(66)) break p000e0186;
		if (!CNDnotcarr(65)) break p000e0186;
 		ACCwriteln(155);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0187:
	{
 		if (skipdoall('p000e0187')) break p000e0187;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0187;
			if (!CNDnoun1(42)) break p000e0187;
 		}
		if (!CNDat(43)) break p000e0187;
		if (!CNDabsent(66)) break p000e0187;
		if (!CNDcarried(65)) break p000e0187;
 		ACCcreate(66);
 		ACCwriteln(156);
 		ACCanykey();
 		function anykey00026() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00026);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// OPEN HONE
	p000e0188:
	{
 		if (skipdoall('p000e0188')) break p000e0188;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0188;
			if (!CNDnoun1(46)) break p000e0188;
 		}
		if (!CNDcarried(18)) break p000e0188;
		if (!CNDzero(17)) break p000e0188;
 		ACCwriteln(157);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN CUPB
	p000e0189:
	{
 		if (skipdoall('p000e0189')) break p000e0189;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0189;
			if (!CNDnoun1(52)) break p000e0189;
 		}
		if (!CNDat(32)) break p000e0189;
		if (!CNDabsent(21)) break p000e0189;
 		ACClet(115,1);
 		ACCwriteln(158);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN PARA
	p000e0190:
	{
 		if (skipdoall('p000e0190')) break p000e0190;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0190;
			if (!CNDnoun1(71)) break p000e0190;
 		}
		if (!CNDcarried(31)) break p000e0190;
 		ACClet(18,150);
 		ACCplus(30,3);
 		ACCok();
		break pro000_restart;
		{}

	}

	// OPEN TILL
	p000e0191:
	{
 		if (skipdoall('p000e0191')) break p000e0191;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0191;
			if (!CNDnoun1(111)) break p000e0191;
 		}
		if (!CNDat(16)) break p000e0191;
 		ACCwriteln(159);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHAT FARM
	p000e0192:
	{
 		if (skipdoall('p000e0192')) break p000e0192;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0192;
			if (!CNDnoun1(28)) break p000e0192;
 		}
		if (!CNDat(20)) break p000e0192;
		if (!CNDpresent(13)) break p000e0192;
 		ACCwriteln(160);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHAT FARM
	p000e0193:
	{
 		if (skipdoall('p000e0193')) break p000e0193;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0193;
			if (!CNDnoun1(28)) break p000e0193;
 		}
		if (!CNDat(18)) break p000e0193;
		if (!CNDpresent(13)) break p000e0193;
 		ACCwriteln(161);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHAT FARM
	p000e0194:
	{
 		if (skipdoall('p000e0194')) break p000e0194;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0194;
			if (!CNDnoun1(28)) break p000e0194;
 		}
		if (!CNDat(3)) break p000e0194;
		if (!CNDpresent(3)) break p000e0194;
 		ACCwriteln(162);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHAT GUAR
	p000e0195:
	{
 		if (skipdoall('p000e0195')) break p000e0195;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0195;
			if (!CNDnoun1(97)) break p000e0195;
 		}
		if (!CNDpresent(43)) break p000e0195;
		if (!CNDeq(114,100)) break p000e0195;
 		ACCwriteln(163);
 		ACCplus(114,10);
 		ACCanykey();
 		function anykey00027() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00027);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// CHAT GUAR
	p000e0196:
	{
 		if (skipdoall('p000e0196')) break p000e0196;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0196;
			if (!CNDnoun1(97)) break p000e0196;
 		}
		if (!CNDpresent(43)) break p000e0196;
		if (!CNDeq(114,110)) break p000e0196;
 		ACCwriteln(164);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHAT WIFE
	p000e0197:
	{
 		if (skipdoall('p000e0197')) break p000e0197;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0197;
			if (!CNDnoun1(136)) break p000e0197;
 		}
		if (!CNDpresent(59)) break p000e0197;
 		ACCwriteln(165);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CHAT LOUD
	p000e0198:
	{
 		if (skipdoall('p000e0198')) break p000e0198;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0198;
			if (!CNDnoun1(159)) break p000e0198;
 		}
		if (!CNDat(18)) break p000e0198;
		if (!CNDpresent(13)) break p000e0198;
 		ACCcreate(15);
 		ACCcreate(56);
 		ACCplace(13,20);
 		ACCwriteln(166);
 		ACCplus(30,3);
 		ACCanykey();
 		function anykey00028() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00028);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// CHAT _
	p000e0199:
	{
 		if (skipdoall('p000e0199')) break p000e0199;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0199;
 		}
 		ACCwriteln(167);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIVE COIN
	p000e0200:
	{
 		if (skipdoall('p000e0200')) break p000e0200;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0200;
			if (!CNDnoun1(24)) break p000e0200;
 		}
		if (!CNDpresent(43)) break p000e0200;
		if (!CNDcarried(9)) break p000e0200;
 		ACCwriteln(168);
 		ACCset(114);
 		ACCdestroy(43);
 		ACCdestroy(9);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00029() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00029);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// GIVE COIN
	p000e0201:
	{
 		if (skipdoall('p000e0201')) break p000e0201;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0201;
			if (!CNDnoun1(24)) break p000e0201;
 		}
		if (!CNDat(37)) break p000e0201;
		if (!CNDpresent(43)) break p000e0201;
		if (!CNDnotcarr(9)) break p000e0201;
 		ACCwriteln(169);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIVE MAGA
	p000e0202:
	{
 		if (skipdoall('p000e0202')) break p000e0202;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0202;
			if (!CNDnoun1(31)) break p000e0202;
 		}
		if (!CNDcarried(16)) break p000e0202;
		if (!CNDpresent(13)) break p000e0202;
 		ACCdestroy(13);
 		ACCdestroy(16);
 		ACCwriteln(170);
 		ACCpause(35);
 		function anykey00031() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00030() 
		{
 		ACCplus(30,4);
 		ACCanykey();
 		waitKey(anykey00031);
		}
 		waitKey(anykey00030);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// GIVE PANT
	p000e0203:
	{
 		if (skipdoall('p000e0203')) break p000e0203;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0203;
			if (!CNDnoun1(37)) break p000e0203;
 		}
		if (!CNDcarried(14)) break p000e0203;
		if (!CNDpresent(13)) break p000e0203;
 		ACCdestroy(14);
 		ACCwriteln(171);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIVE HONE
	p000e0204:
	{
 		if (skipdoall('p000e0204')) break p000e0204;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0204;
			if (!CNDnoun1(46)) break p000e0204;
 		}
		if (!CNDat(34)) break p000e0204;
		if (!CNDcarried(18)) break p000e0204;
 		ACCwriteln(172);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIVE HERB
	p000e0205:
	{
 		if (skipdoall('p000e0205')) break p000e0205;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0205;
			if (!CNDnoun1(148)) break p000e0205;
 		}
		if (!CNDpresent(59)) break p000e0205;
		if (!CNDcarried(64)) break p000e0205;
 		ACCswap(64,65);
 		ACCwriteln(173);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00032() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00032);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// GIVE HERB
	p000e0206:
	{
 		if (skipdoall('p000e0206')) break p000e0206;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0206;
			if (!CNDnoun1(148)) break p000e0206;
 		}
		if (!CNDpresent(62)) break p000e0206;
		if (!CNDcarried(64)) break p000e0206;
 		ACCswap(64,65);
 		ACCwriteln(174);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00033() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00033);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// GIVE HERB
	p000e0207:
	{
 		if (skipdoall('p000e0207')) break p000e0207;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0207;
			if (!CNDnoun1(148)) break p000e0207;
 		}
		if (!CNDat(24)) break p000e0207;
		if (!CNDnotcarr(64)) break p000e0207;
 		ACCwriteln(175);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIVE KEY
	p000e0208:
	{
 		if (skipdoall('p000e0208')) break p000e0208;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0208;
			if (!CNDnoun1(150)) break p000e0208;
 		}
		if (!CNDat(20)) break p000e0208;
		if (!CNDpresent(13)) break p000e0208;
 		ACCwriteln(176);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GIVE _
	p000e0209:
	{
 		if (skipdoall('p000e0209')) break p000e0209;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0209;
 		}
 		ACCwriteln(177);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// READ MAGA
	p000e0210:
	{
 		if (skipdoall('p000e0210')) break p000e0210;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0210;
			if (!CNDnoun1(31)) break p000e0210;
 		}
		if (!CNDcarried(16)) break p000e0210;
 		ACCwriteln(178);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// READ NOTI
	p000e0211:
	{
 		if (skipdoall('p000e0211')) break p000e0211;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0211;
			if (!CNDnoun1(58)) break p000e0211;
 		}
		if (!CNDpresent(25)) break p000e0211;
 		ACCwriteln(179);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// READ NOTI
	p000e0212:
	{
 		if (skipdoall('p000e0212')) break p000e0212;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0212;
			if (!CNDnoun1(58)) break p000e0212;
 		}
		if (!CNDpresent(26)) break p000e0212;
 		ACCwriteln(180);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// READ SIGN
	p000e0213:
	{
 		if (skipdoall('p000e0213')) break p000e0213;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0213;
			if (!CNDnoun1(91)) break p000e0213;
 		}
		if (!CNDat(34)) break p000e0213;
 		ACCwriteln(181);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// READ PAPE
	p000e0214:
	{
 		if (skipdoall('p000e0214')) break p000e0214;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0214;
			if (!CNDnoun1(114)) break p000e0214;
 		}
		if (!CNDpresent(49)) break p000e0214;
		if (!CNDnotcarr(49)) break p000e0214;
 		ACCwriteln(182);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// READ PAPE
	p000e0215:
	{
 		if (skipdoall('p000e0215')) break p000e0215;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0215;
			if (!CNDnoun1(114)) break p000e0215;
 		}
		if (!CNDcarried(49)) break p000e0215;
 		ACCwriteln(183);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// READ BOOK
	p000e0216:
	{
 		if (skipdoall('p000e0216')) break p000e0216;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0216;
			if (!CNDnoun1(123)) break p000e0216;
 		}
		if (!CNDcarried(54)) break p000e0216;
 		ACCcls();
 		ACCwriteln(184);
 		ACCanykey();
 		function anykey00034() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00034);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// READ SCRO
	p000e0217:
	{
 		if (skipdoall('p000e0217')) break p000e0217;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0217;
			if (!CNDnoun1(138)) break p000e0217;
 		}
		if (!CNDpresent(63)) break p000e0217;
 		ACCcls();
 		ACCwriteln(185);
 		ACCanykey();
 		function anykey00035() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00035);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// READ COOR
	p000e0218:
	{
 		if (skipdoall('p000e0218')) break p000e0218;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0218;
			if (!CNDnoun1(146)) break p000e0218;
 		}
		if (!CNDcarried(49)) break p000e0218;
 		ACCwriteln(186);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// UNSC LAMP
	p000e0219:
	{
 		if (skipdoall('p000e0219')) break p000e0219;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0219;
			if (!CNDnoun1(39)) break p000e0219;
 		}
		if (!CNDat(22)) break p000e0219;
		if (!CNDnotcarr(17)) break p000e0219;
		if (!CNDlt(113,200)) break p000e0219;
 		ACCwriteln(187);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// UNSC LAMP
	p000e0220:
	{
 		if (skipdoall('p000e0220')) break p000e0220;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0220;
			if (!CNDnoun1(39)) break p000e0220;
 		}
		if (!CNDat(22)) break p000e0220;
		if (!CNDcarried(17)) break p000e0220;
		if (!CNDabsent(13)) break p000e0220;
		if (!CNDlt(113,200)) break p000e0220;
 		ACClet(113,200);
 		ACCwriteln(188);
 		ACCplus(30,1);
 		ACCanykey();
 		function anykey00036() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00036);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// UNSC LAMP
	p000e0221:
	{
 		if (skipdoall('p000e0221')) break p000e0221;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0221;
			if (!CNDnoun1(39)) break p000e0221;
 		}
		if (!CNDat(22)) break p000e0221;
		if (!CNDcarried(17)) break p000e0221;
		if (!CNDpresent(13)) break p000e0221;
		if (!CNDlt(113,200)) break p000e0221;
 		ACCwriteln(189);
 		ACCanykey();
 		function anykey00037() 
		{
 		ACCgoto(18);
 		ACCdestroy(15);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00037);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// UNSC LAMP
	p000e0222:
	{
 		if (skipdoall('p000e0222')) break p000e0222;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0222;
			if (!CNDnoun1(39)) break p000e0222;
 		}
		if (!CNDat(22)) break p000e0222;
		if (!CNDcarried(17)) break p000e0222;
		if (!CNDpresent(1)) break p000e0222;
		if (!CNDnotcarr(1)) break p000e0222;
		if (!CNDeq(113,200)) break p000e0222;
 		ACCwriteln(190);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INSE BOX
	p000e0223:
	{
 		if (skipdoall('p000e0223')) break p000e0223;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0223;
			if (!CNDnoun1(20)) break p000e0223;
 		}
		if (!CNDat(23)) break p000e0223;
		if (!CNDcarried(5)) break p000e0223;
 		ACCwriteln(191);
 		ACCswap(5,6);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00038() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00038);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// INSE BOX
	p000e0224:
	{
 		if (skipdoall('p000e0224')) break p000e0224;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0224;
			if (!CNDnoun1(20)) break p000e0224;
 		}
		if (!CNDat(23)) break p000e0224;
		if (!CNDcarried(6)) break p000e0224;
 		ACCwriteln(192);
 		ACCpause(50);
 		function anykey00039() 
		{
 		ACCwriteln(193);
 		ACCdone();
		return;
		}
 		waitKey(anykey00039);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// INSE BAR
	p000e0225:
	{
 		if (skipdoall('p000e0225')) break p000e0225;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0225;
			if (!CNDnoun1(48)) break p000e0225;
 		}
		if (!CNDat(8)) break p000e0225;
		if (!CNDcarried(19)) break p000e0225;
		if (!CNDabsent(20)) break p000e0225;
 		ACCcreate(20);
 		ACCwriteln(194);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00040() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00040);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// INSE BAR
	p000e0226:
	{
 		if (skipdoall('p000e0226')) break p000e0226;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0226;
			if (!CNDnoun1(48)) break p000e0226;
 		}
		if (!CNDat(8)) break p000e0226;
		if (!CNDpresent(20)) break p000e0226;
		if (!CNDcarried(19)) break p000e0226;
 		ACCwriteln(195);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INSE ROD
	p000e0227:
	{
 		if (skipdoall('p000e0227')) break p000e0227;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0227;
			if (!CNDnoun1(53)) break p000e0227;
 		}
		if (!CNDat(31)) break p000e0227;
		if (!CNDcarried(22)) break p000e0227;
 		ACCswap(22,23);
 		ACCdrop(23);
		if (!success) break pro000_restart;
 		ACCwriteln(196);
 		ACCplus(30,1);
 		ACCanykey();
 		function anykey00041() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00041);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// INSE NAIL
	p000e0228:
	{
 		if (skipdoall('p000e0228')) break p000e0228;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0228;
			if (!CNDnoun1(59)) break p000e0228;
 		}
		if (!CNDcarried(50)) break p000e0228;
		if (!CNDcarried(10)) break p000e0228;
 		ACCdestroy(10);
 		ACCswap(50,51);
 		ACCwriteln(197);
 		ACCanykey();
 		function anykey00042() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00042);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// INSE DISC
	p000e0229:
	{
 		if (skipdoall('p000e0229')) break p000e0229;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0229;
			if (!CNDnoun1(112)) break p000e0229;
 		}
		if (!CNDat(48)) break p000e0229;
		if (!CNDcarried(47)) break p000e0229;
		if (!CNDlt(24,200)) break p000e0229;
 		ACCwriteln(198);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INSE DISC
	p000e0230:
	{
 		if (skipdoall('p000e0230')) break p000e0230;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0230;
			if (!CNDnoun1(112)) break p000e0230;
 		}
		if (!CNDat(48)) break p000e0230;
		if (!CNDcarried(47)) break p000e0230;
		if (!CNDeq(24,200)) break p000e0230;
 		ACCdestroy(47);
 		ACCset(24);
 		ACCdestroy(61);
 		ACCcreate(55);
 		ACCpause(50);
 		function anykey00044() 
		{
 		ACCplus(30,2);
 		ACCdesc();
		return;
		}
 		function anykey00043() 
		{
 		ACClet(28,5);
 		ACCpause(3);
 		waitKey(anykey00044);
		}
 		waitKey(anykey00043);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// INSE HAND
	p000e0231:
	{
 		if (skipdoall('p000e0231')) break p000e0231;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0231;
			if (!CNDnoun1(158)) break p000e0231;
 		}
		if (!CNDpresent(24)) break p000e0231;
 		ACCwriteln(199);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INSE COTT
	p000e0232:
	{
 		if (skipdoall('p000e0232')) break p000e0232;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0232;
			if (!CNDnoun1(201)) break p000e0232;
 		}
		if (!CNDcarried(56)) break p000e0232;
		if (!CNDnotworn(56)) break p000e0232;
 		ACCwriteln(200);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PUT BOX
	p000e0233:
	{
 		if (skipdoall('p000e0233')) break p000e0233;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0233;
			if (!CNDnoun1(20)) break p000e0233;
 		}
		if (!CNDat(23)) break p000e0233;
		if (!CNDcarried(5)) break p000e0233;
 		ACCwriteln(201);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PUT LAMP
	p000e0234:
	{
 		if (skipdoall('p000e0234')) break p000e0234;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0234;
			if (!CNDnoun1(39)) break p000e0234;
 		}
		if (!CNDcarried(1)) break p000e0234;
 		ACCdrop(1);
		if (!success) break pro000_restart;
 		ACCwriteln(202);
 		ACCpause(100);
 		function anykey00045() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00045);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PUT LAMP
	p000e0235:
	{
 		if (skipdoall('p000e0235')) break p000e0235;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0235;
			if (!CNDnoun1(39)) break p000e0235;
 		}
		if (!CNDcarried(0)) break p000e0235;
 		ACCdrop(0);
		if (!success) break pro000_restart;
 		ACCwriteln(203);
 		ACCpause(100);
 		function anykey00046() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00046);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PUT HONE
	p000e0236:
	{
 		if (skipdoall('p000e0236')) break p000e0236;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0236;
			if (!CNDnoun1(46)) break p000e0236;
 		}
		if (!CNDnotat(34)) break p000e0236;
		if (!CNDcarried(18)) break p000e0236;
 		ACCdrop(18);
		if (!success) break pro000_restart;
 		ACCwriteln(204);
 		ACCpause(100);
 		function anykey00047() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00047);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PUT HONE
	p000e0237:
	{
 		if (skipdoall('p000e0237')) break p000e0237;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0237;
			if (!CNDnoun1(46)) break p000e0237;
 		}
		if (!CNDat(34)) break p000e0237;
		if (!CNDlt(19,220)) break p000e0237;
 		ACCwriteln(205);
 		ACCdrop(18);
		if (!success) break pro000_restart;
 		ACCanykey();
 		function anykey00048() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00048);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PUT HONE
	p000e0238:
	{
 		if (skipdoall('p000e0238')) break p000e0238;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0238;
			if (!CNDnoun1(46)) break p000e0238;
 		}
		if (!CNDat(34)) break p000e0238;
		if (!CNDcarried(18)) break p000e0238;
		if (!CNDeq(19,220)) break p000e0238;
 		ACCdestroy(37);
 		ACCdestroy(18);
 		ACCwriteln(206);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00049() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00049);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PUT LADD
	p000e0239:
	{
 		if (skipdoall('p000e0239')) break p000e0239;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0239;
			if (!CNDnoun1(64)) break p000e0239;
 		}
		if (!CNDatgt(34)) break p000e0239;
		if (!CNDatlt(37)) break p000e0239;
		if (!CNDcarried(28)) break p000e0239;
 		ACCswap(28,29);
 		ACCdrop(29);
		if (!success) break pro000_restart;
 		ACClet(28,11);
 		ACCpause(6);
 		function anykey00051() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00050() 
		{
 		ACCwriteln(207);
 		ACCpause(200);
 		waitKey(anykey00051);
		}
 		waitKey(anykey00050);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PUT LADD
	p000e0240:
	{
 		if (skipdoall('p000e0240')) break p000e0240;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0240;
			if (!CNDnoun1(64)) break p000e0240;
 		}
		if (!CNDcarried(28)) break p000e0240;
 		ACCdrop(28);
		if (!success) break pro000_restart;
 		ACClet(28,11);
 		ACCpause(6);
 		function anykey00053() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00052() 
		{
 		ACCwriteln(208);
 		ACCpause(200);
 		waitKey(anykey00053);
		}
 		waitKey(anykey00052);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PUT EGG
	p000e0241:
	{
 		if (skipdoall('p000e0241')) break p000e0241;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0241;
			if (!CNDnoun1(98)) break p000e0241;
 		}
		if (!CNDcarried(44)) break p000e0241;
 		ACCdrop(44);
		if (!success) break pro000_restart;
 		ACCwriteln(209);
 		ACCpause(100);
 		function anykey00054() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00054);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES U
	p000e0242:
	{
 		if (skipdoall('p000e0242')) break p000e0242;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0242;
			if (!CNDnoun1(9)) break p000e0242;
 		}
		if (!CNDat(27)) break p000e0242;
 		ACCwriteln(210);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES U
	p000e0243:
	{
 		if (skipdoall('p000e0243')) break p000e0243;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0243;
			if (!CNDnoun1(9)) break p000e0243;
 		}
		if (!CNDat(30)) break p000e0243;
 		ACCwriteln(211);
 		ACCpause(20);
 		function anykey00057() 
		{
 		ACCgoto(27);
 		ACCdesc();
		return;
		}
 		function anykey00056() 
		{
 		ACCpause(20);
 		waitKey(anykey00057);
		}
 		function anykey00055() 
		{
 		ACClet(28,5);
 		ACCpause(5);
 		waitKey(anykey00056);
		}
 		waitKey(anykey00055);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES D
	p000e0244:
	{
 		if (skipdoall('p000e0244')) break p000e0244;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0244;
			if (!CNDnoun1(10)) break p000e0244;
 		}
		if (!CNDat(27)) break p000e0244;
		if (!CNDlt(115,255)) break p000e0244;
 		ACCwriteln(212);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES D
	p000e0245:
	{
 		if (skipdoall('p000e0245')) break p000e0245;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0245;
			if (!CNDnoun1(10)) break p000e0245;
 		}
		if (!CNDat(27)) break p000e0245;
		if (!CNDeq(115,255)) break p000e0245;
 		ACCwriteln(213);
 		ACCpause(20);
 		function anykey00060() 
		{
 		ACCgoto(30);
 		ACCdesc();
		return;
		}
 		function anykey00059() 
		{
 		ACCpause(20);
 		waitKey(anykey00060);
		}
 		function anykey00058() 
		{
 		ACClet(28,5);
 		ACCpause(5);
 		waitKey(anykey00059);
		}
 		waitKey(anykey00058);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES D
	p000e0246:
	{
 		if (skipdoall('p000e0246')) break p000e0246;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0246;
			if (!CNDnoun1(10)) break p000e0246;
 		}
		if (!CNDat(30)) break p000e0246;
 		ACCwriteln(214);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES DOOR
	p000e0247:
	{
 		if (skipdoall('p000e0247')) break p000e0247;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0247;
			if (!CNDnoun1(42)) break p000e0247;
 		}
		if (!CNDat(12)) break p000e0247;
		if (!CNDabsent(35)) break p000e0247;
 		ACCwriteln(215);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES LEVE
	p000e0248:
	{
 		if (skipdoall('p000e0248')) break p000e0248;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0248;
			if (!CNDnoun1(54)) break p000e0248;
 		}
		if (!CNDpresent(23)) break p000e0248;
		if (!CNDeq(115,100)) break p000e0248;
 		ACCset(115);
 		ACCwriteln(216);
 		ACCplus(30,1);
 		ACCanykey();
 		function anykey00061() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00061);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES LEVE
	p000e0249:
	{
 		if (skipdoall('p000e0249')) break p000e0249;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0249;
			if (!CNDnoun1(54)) break p000e0249;
 		}
		if (!CNDpresent(23)) break p000e0249;
		if (!CNDeq(115,255)) break p000e0249;
 		ACCwriteln(217);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES NOTI
	p000e0250:
	{
 		if (skipdoall('p000e0250')) break p000e0250;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0250;
			if (!CNDnoun1(58)) break p000e0250;
 		}
		if (!CNDpresent(25)) break p000e0250;
 		ACCwriteln(218);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES NAIL
	p000e0251:
	{
 		if (skipdoall('p000e0251')) break p000e0251;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0251;
			if (!CNDnoun1(59)) break p000e0251;
 		}
		if (!CNDat(14)) break p000e0251;
		if (!CNDnotcarr(57)) break p000e0251;
 		ACCwriteln(219);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES NAIL
	p000e0252:
	{
 		if (skipdoall('p000e0252')) break p000e0252;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0252;
			if (!CNDnoun1(59)) break p000e0252;
 		}
		if (!CNDat(14)) break p000e0252;
		if (!CNDcarried(57)) break p000e0252;
		if (!CNDpresent(25)) break p000e0252;
 		ACCswap(25,26);
 		ACCcreate(10);
 		ACCwriteln(220);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00062() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00062);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES NAIL
	p000e0253:
	{
 		if (skipdoall('p000e0253')) break p000e0253;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0253;
			if (!CNDnoun1(59)) break p000e0253;
 		}
		if (!CNDat(14)) break p000e0253;
		if (!CNDcarried(57)) break p000e0253;
		if (!CNDabsent(25)) break p000e0253;
 		ACCwriteln(221);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GRAP
	p000e0254:
	{
 		if (skipdoall('p000e0254')) break p000e0254;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0254;
			if (!CNDnoun1(69)) break p000e0254;
 		}
		if (!CNDat(45)) break p000e0254;
		if (!CNDzero(18)) break p000e0254;
		if (!CNDpresent(30)) break p000e0254;
 		ACClet(18,50);
 		ACCswap(30,71);
 		ACCplace(71,36);
 		ACCgoto(46);
 		ACClet(6,4);
 		ACCwriteln(222);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00063() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00063);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES GRAP
	p000e0255:
	{
 		if (skipdoall('p000e0255')) break p000e0255;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0255;
			if (!CNDnoun1(69)) break p000e0255;
 		}
		if (!CNDat(36)) break p000e0255;
		if (!CNDpresent(30)) break p000e0255;
 		ACCwriteln(223);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES RED
	p000e0256:
	{
 		if (skipdoall('p000e0256')) break p000e0256;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0256;
			if (!CNDnoun1(118)) break p000e0256;
 		}
		if (!CNDat(51)) break p000e0256;
		if (!CNDabsent(52)) break p000e0256;
 		ACCwriteln(224);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES RED
	p000e0257:
	{
 		if (skipdoall('p000e0257')) break p000e0257;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0257;
			if (!CNDnoun1(118)) break p000e0257;
 		}
		if (!CNDat(51)) break p000e0257;
		if (!CNDpresent(52)) break p000e0257;
 		ACCdestroy(52);
 		ACCwriteln(225);
 		ACCminus(25,10);
 		ACClet(28,5);
 		ACCpause(4);
 		function anykey00064() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00064);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES GREE
	p000e0258:
	{
 		if (skipdoall('p000e0258')) break p000e0258;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0258;
			if (!CNDnoun1(119)) break p000e0258;
 		}
		if (!CNDat(51)) break p000e0258;
		if (!CNDlt(24,150)) break p000e0258;
		if (!CNDabsent(52)) break p000e0258;
 		ACCwriteln(226);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GREE
	p000e0259:
	{
 		if (skipdoall('p000e0259')) break p000e0259;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0259;
			if (!CNDnoun1(119)) break p000e0259;
 		}
		if (!CNDat(51)) break p000e0259;
		if (!CNDgt(24,149)) break p000e0259;
		if (!CNDabsent(52)) break p000e0259;
 		ACCcreate(52);
 		ACCwriteln(227);
 		ACCplus(25,10);
 		ACClet(28,5);
 		ACCpause(4);
 		function anykey00065() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00065);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRES TRIPSWITCH
	p000e0260:
	{
 		if (skipdoall('p000e0260')) break p000e0260;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0260;
			if (!CNDnoun1(204)) break p000e0260;
 		}
		if (!CNDpresent(53)) break p000e0260;
 		ACCwriteln(228);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES BUTT
	p000e0261:
	{
 		if (skipdoall('p000e0261')) break p000e0261;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0261;
			if (!CNDnoun1(126)) break p000e0261;
 		}
		if (!CNDat(49)) break p000e0261;
		if (!CNDlt(24,150)) break p000e0261;
 		ACCwriteln(229);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES BUTT
	p000e0262:
	{
 		if (skipdoall('p000e0262')) break p000e0262;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0262;
			if (!CNDnoun1(126)) break p000e0262;
 		}
		if (!CNDat(49)) break p000e0262;
		if (!CNDgt(24,149)) break p000e0262;
		if (!CNDlt(25,30)) break p000e0262;
 		ACCwriteln(230);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES BUTT
	p000e0263:
	{
 		if (skipdoall('p000e0263')) break p000e0263;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0263;
			if (!CNDnoun1(126)) break p000e0263;
 		}
		if (!CNDat(49)) break p000e0263;
		if (!CNDgt(24,149)) break p000e0263;
		if (!CNDeq(25,30)) break p000e0263;
 		ACCset(25);
 		ACCwriteln(231);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BREA DOOR
	p000e0264:
	{
 		if (skipdoall('p000e0264')) break p000e0264;
 		if (in_response)
		{
			if (!CNDverb(49)) break p000e0264;
			if (!CNDnoun1(42)) break p000e0264;
 		}
		if (!CNDat(8)) break p000e0264;
		if (!CNDabsent(20)) break p000e0264;
 		ACCwriteln(232);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BREA CHAI
	p000e0265:
	{
 		if (skipdoall('p000e0265')) break p000e0265;
 		if (in_response)
		{
			if (!CNDverb(49)) break p000e0265;
			if (!CNDnoun1(50)) break p000e0265;
 		}
		if (!CNDat(8)) break p000e0265;
		if (!CNDabsent(20)) break p000e0265;
		if (!CNDnotcarr(19)) break p000e0265;
 		ACCwriteln(233);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BREA CHAI
	p000e0266:
	{
 		if (skipdoall('p000e0266')) break p000e0266;
 		if (in_response)
		{
			if (!CNDverb(49)) break p000e0266;
			if (!CNDnoun1(50)) break p000e0266;
 		}
		if (!CNDat(8)) break p000e0266;
		if (!CNDcarried(19)) break p000e0266;
		if (!CNDabsent(20)) break p000e0266;
 		ACCwriteln(234);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BREA VACU
	p000e0267:
	{
 		if (skipdoall('p000e0267')) break p000e0267;
 		if (in_response)
		{
			if (!CNDverb(49)) break p000e0267;
			if (!CNDnoun1(169)) break p000e0267;
 		}
		if (!CNDcarried(18)) break p000e0267;
		if (!CNDlt(19,220)) break p000e0267;
 		ACCwriteln(235);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CALL FARM
	p000e0268:
	{
 		if (skipdoall('p000e0268')) break p000e0268;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0268;
			if (!CNDnoun1(28)) break p000e0268;
 		}
		if (!CNDat(32)) break p000e0268;
		if (!CNDeq(115,1)) break p000e0268;
		if (!CNDabsent(21)) break p000e0268;
 		ACCwriteln(236);
 		ACCcreate(21);
 		ACCcreate(13);
 		ACClet(115,50);
 		ACCplus(30,3);
 		ACCanykey();
 		function anykey00066() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00066);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// CALL FARM
	p000e0269:
	{
 		if (skipdoall('p000e0269')) break p000e0269;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0269;
			if (!CNDnoun1(28)) break p000e0269;
 		}
		if (!CNDatgt(17)) break p000e0269;
		if (!CNDatlt(33)) break p000e0269;
		if (!CNDgt(115,1)) break p000e0269;
 		ACCwriteln(237);
 		ACCcreate(13);
 		ACCanykey();
 		function anykey00067() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00067);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// CALL FARM
	p000e0270:
	{
 		if (skipdoall('p000e0270')) break p000e0270;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0270;
			if (!CNDnoun1(28)) break p000e0270;
 		}
		if (!CNDatgt(17)) break p000e0270;
		if (!CNDatlt(33)) break p000e0270;
		if (!CNDzero(115)) break p000e0270;
 		ACCwriteln(238);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RATT MAGA
	p000e0271:
	{
 		if (skipdoall('p000e0271')) break p000e0271;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0271;
			if (!CNDnoun1(31)) break p000e0271;
 		}
		if (!CNDcarried(16)) break p000e0271;
 		ACCwriteln(239);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RATT BARR
	p000e0272:
	{
 		if (skipdoall('p000e0272')) break p000e0272;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0272;
			if (!CNDnoun1(55)) break p000e0272;
 		}
		if (!CNDcarried(24)) break p000e0272;
		if (!CNDzero(116)) break p000e0272;
 		ACCset(116);
 		ACCcreate(11);
 		ACCwriteln(240);
 		ACCanykey();
 		function anykey00068() 
		{
 		ACCplus(30,3);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00068);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// RATT BARR
	p000e0273:
	{
 		if (skipdoall('p000e0273')) break p000e0273;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0273;
			if (!CNDnoun1(55)) break p000e0273;
 		}
		if (!CNDcarried(24)) break p000e0273;
		if (!CNDeq(116,255)) break p000e0273;
 		ACCwriteln(241);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MAKE COMP
	p000e0274:
	{
 		if (skipdoall('p000e0274')) break p000e0274;
 		if (in_response)
		{
			if (!CNDverb(60)) break p000e0274;
			if (!CNDnoun1(25)) break p000e0274;
 		}
		if (!CNDcarried(10)) break p000e0274;
		if (!CNDcarried(11)) break p000e0274;
 		ACCdestroy(10);
 		ACCswap(11,12);
 		ACCwriteln(242);
 		ACCanykey();
 		function anykey00069() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00069);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// MAKE COMP
	p000e0275:
	{
 		if (skipdoall('p000e0275')) break p000e0275;
 		if (in_response)
		{
			if (!CNDverb(60)) break p000e0275;
			if (!CNDnoun1(25)) break p000e0275;
 		}
		if (!CNDnotcarr(12)) break p000e0275;
 		ACCwriteln(243);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PIER HONE
	p000e0276:
	{
 		if (skipdoall('p000e0276')) break p000e0276;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0276;
			if (!CNDnoun1(46)) break p000e0276;
 		}
		if (!CNDcarried(18)) break p000e0276;
		if (!CNDcarried(27)) break p000e0276;
 		ACCswap(18,36);
 		ACCdrop(36);
		if (!success) break pro000_restart;
 		ACCwriteln(244);
 		ACCpause(75);
 		function anykey00070() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00070);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PIER LID
	p000e0277:
	{
 		if (skipdoall('p000e0277')) break p000e0277;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0277;
			if (!CNDnoun1(63)) break p000e0277;
 		}
		if (!CNDcarried(18)) break p000e0277;
		if (!CNDcarried(27)) break p000e0277;
		if (!CNDzero(19)) break p000e0277;
 		ACClet(19,220);
 		ACCwriteln(245);
 		ACClet(28,5);
 		ACCpause(2);
 		function anykey00072() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00071() 
		{
 		ACCplus(30,3);
 		ACCanykey();
 		waitKey(anykey00072);
		}
 		waitKey(anykey00071);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PIER LID
	p000e0278:
	{
 		if (skipdoall('p000e0278')) break p000e0278;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0278;
			if (!CNDnoun1(63)) break p000e0278;
 		}
		if (!CNDcarried(18)) break p000e0278;
		if (!CNDcarried(27)) break p000e0278;
		if (!CNDeq(19,220)) break p000e0278;
 		ACCwriteln(246);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PIER LID
	p000e0279:
	{
 		if (skipdoall('p000e0279')) break p000e0279;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0279;
			if (!CNDnoun1(63)) break p000e0279;
 		}
		if (!CNDcarried(18)) break p000e0279;
		if (!CNDnotcarr(27)) break p000e0279;
		if (!CNDzero(19)) break p000e0279;
 		ACCwriteln(247);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LAY LADD
	p000e0280:
	{
 		if (skipdoall('p000e0280')) break p000e0280;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0280;
			if (!CNDnoun1(64)) break p000e0280;
 		}
		if (!CNDatgt(34)) break p000e0280;
		if (!CNDatlt(37)) break p000e0280;
		if (!CNDcarried(28)) break p000e0280;
 		ACCswap(28,29);
 		ACCdrop(29);
		if (!success) break pro000_restart;
 		ACClet(28,11);
 		ACCpause(6);
 		function anykey00074() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00073() 
		{
 		ACCwriteln(248);
 		ACCpause(200);
 		waitKey(anykey00074);
		}
 		waitKey(anykey00073);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// LAY LADD
	p000e0281:
	{
 		if (skipdoall('p000e0281')) break p000e0281;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0281;
			if (!CNDnoun1(64)) break p000e0281;
 		}
		if (!CNDcarried(28)) break p000e0281;
 		ACCwriteln(249);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASCE FOUN
	p000e0282:
	{
 		if (skipdoall('p000e0282')) break p000e0282;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0282;
			if (!CNDnoun1(18)) break p000e0282;
 		}
		if (!CNDat(4)) break p000e0282;
 		ACCwriteln(250);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASCE GATE
	p000e0283:
	{
 		if (skipdoall('p000e0283')) break p000e0283;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0283;
			if (!CNDnoun1(26)) break p000e0283;
 		}
		if (!CNDat(18)) break p000e0283;
		if (!CNDabsent(15)) break p000e0283;
		if (!CNDpresent(13)) break p000e0283;
 		ACCwriteln(251);
 		ACCgoto(4);
 		ACCanykey();
 		function anykey00075() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00075);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// ASCE LADD
	p000e0284:
	{
 		if (skipdoall('p000e0284')) break p000e0284;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0284;
			if (!CNDnoun1(64)) break p000e0284;
 		}
		if (!CNDat(39)) break p000e0284;
		if (!CNDpresent(28)) break p000e0284;
		if (!CNDnotcarr(28)) break p000e0284;
 		ACCgoto(38);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ASCE LADD
	p000e0285:
	{
 		if (skipdoall('p000e0285')) break p000e0285;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0285;
			if (!CNDnoun1(64)) break p000e0285;
 		}
		if (!CNDat(36)) break p000e0285;
		if (!CNDzero(18)) break p000e0285;
		if (!CNDpresent(28)) break p000e0285;
		if (!CNDnotcarr(28)) break p000e0285;
 		ACCgoto(45);
 		ACCplace(30,45);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ASCE LADD
	p000e0286:
	{
 		if (skipdoall('p000e0286')) break p000e0286;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0286;
			if (!CNDnoun1(64)) break p000e0286;
 		}
		if (!CNDat(36)) break p000e0286;
		if (!CNDnotzero(18)) break p000e0286;
 		ACCwriteln(252);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASCE LADD
	p000e0287:
	{
 		if (skipdoall('p000e0287')) break p000e0287;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0287;
			if (!CNDnoun1(64)) break p000e0287;
 		}
		if (!CNDpresent(28)) break p000e0287;
		if (!CNDnotcarr(28)) break p000e0287;
 		ACCwriteln(253);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASCE LADD
	p000e0288:
	{
 		if (skipdoall('p000e0288')) break p000e0288;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0288;
			if (!CNDnoun1(64)) break p000e0288;
 		}
		if (!CNDat(36)) break p000e0288;
		if (!CNDcarried(28)) break p000e0288;
 		ACCwriteln(254);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASCE LADD
	p000e0289:
	{
 		if (skipdoall('p000e0289')) break p000e0289;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0289;
			if (!CNDnoun1(64)) break p000e0289;
 		}
		if (!CNDat(39)) break p000e0289;
		if (!CNDcarried(28)) break p000e0289;
 		ACCwriteln(255);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASCE TREE
	p000e0290:
	{
 		if (skipdoall('p000e0290')) break p000e0290;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0290;
			if (!CNDnoun1(68)) break p000e0290;
 		}
		if (!CNDat(39)) break p000e0290;
		if (!CNDabsent(28)) break p000e0290;
 		ACCwriteln(256);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASCE HEAP
	p000e0291:
	{
 		if (skipdoall('p000e0291')) break p000e0291;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0291;
			if (!CNDnoun1(75)) break p000e0291;
 		}
		if (!CNDat(10)) break p000e0291;
		if (!CNDpresent(41)) break p000e0291;
 		ACCgoto(47);
 		ACCwriteln(257);
 		ACCanykey();
 		function anykey00076() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00076);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// ASCE HEAP
	p000e0292:
	{
 		if (skipdoall('p000e0292')) break p000e0292;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0292;
			if (!CNDnoun1(75)) break p000e0292;
 		}
		if (!CNDpresent(42)) break p000e0292;
 		ACCwriteln(258);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASCE ROPE
	p000e0293:
	{
 		if (skipdoall('p000e0293')) break p000e0293;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0293;
			if (!CNDnoun1(79)) break p000e0293;
 		}
		if (!CNDpresent(48)) break p000e0293;
 		ACCwriteln(259);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASCE _
	p000e0294:
	{
 		if (skipdoall('p000e0294')) break p000e0294;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0294;
 		}
		if (!CNDat(19)) break p000e0294;
 		ACCgoto(53);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ATTA GRAP
	p000e0295:
	{
 		if (skipdoall('p000e0295')) break p000e0295;
 		if (in_response)
		{
			if (!CNDverb(70)) break p000e0295;
			if (!CNDnoun1(69)) break p000e0295;
 		}
		if (!CNDcarried(71)) break p000e0295;
		if (!CNDcarried(34)) break p000e0295;
 		ACCwriteln(260);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ATTA ROPE
	p000e0296:
	{
 		if (skipdoall('p000e0296')) break p000e0296;
 		if (in_response)
		{
			if (!CNDverb(70)) break p000e0296;
			if (!CNDnoun1(79)) break p000e0296;
 		}
		if (!CNDcarried(71)) break p000e0296;
		if (!CNDcarried(34)) break p000e0296;
 		ACCdestroy(71);
 		ACCswap(34,38);
 		ACCwriteln(261);
 		ACCpause(150);
 		function anykey00077() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00077);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// ATTA ROPE
	p000e0297:
	{
 		if (skipdoall('p000e0297')) break p000e0297;
 		if (in_response)
		{
			if (!CNDverb(70)) break p000e0297;
			if (!CNDnoun1(79)) break p000e0297;
 		}
		if (!CNDcarried(38)) break p000e0297;
 		ACCwriteln(262);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ATTA BEAR
	p000e0298:
	{
 		if (skipdoall('p000e0298')) break p000e0298;
 		if (in_response)
		{
			if (!CNDverb(70)) break p000e0298;
			if (!CNDnoun1(90)) break p000e0298;
 		}
		if (!CNDpresent(37)) break p000e0298;
 		ACCwriteln(263);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// JUMP _
	p000e0299:
	{
 		if (skipdoall('p000e0299')) break p000e0299;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0299;
 		}
		if (!CNDat(46)) break p000e0299;
 		ACCwriteln(264);
 		ACCpause(200);
 		function anykey00078() 
		{
 		ACCwriteln(265);
 		ACCscore();
 		ACCend();
		return;
		}
 		waitKey(anykey00078);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// JUMP _
	p000e0300:
	{
 		if (skipdoall('p000e0300')) break p000e0300;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0300;
 		}
		if (!CNDatgt(39)) break p000e0300;
		if (!CNDatlt(43)) break p000e0300;
 		ACCwriteln(266);
 		ACClet(28,3);
 		ACCpause(200);
 		function anykey00079() 
		{
 		ACCgoto(41);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00079);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// JUMP _
	p000e0301:
	{
 		if (skipdoall('p000e0301')) break p000e0301;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0301;
 		}
		if (!CNDat(36)) break p000e0301;
		if (!CNDzero(18)) break p000e0301;
 		ACCwriteln(267);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// JUMP _
	p000e0302:
	{
 		if (skipdoall('p000e0302')) break p000e0302;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0302;
 		}
		if (!CNDat(6)) break p000e0302;
 		ACCwriteln(268);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIG HEAP
	p000e0303:
	{
 		if (skipdoall('p000e0303')) break p000e0303;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0303;
			if (!CNDnoun1(75)) break p000e0303;
 		}
		if (!CNDat(10)) break p000e0303;
		if (!CNDcarried(33)) break p000e0303;
		if (!CNDpresent(41)) break p000e0303;
 		ACCswap(41,42);
 		ACCcreate(27);
 		ACCwriteln(269);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00080() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00080);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DIG HEAP
	p000e0304:
	{
 		if (skipdoall('p000e0304')) break p000e0304;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0304;
			if (!CNDnoun1(75)) break p000e0304;
 		}
		if (!CNDat(10)) break p000e0304;
		if (!CNDnotcarr(33)) break p000e0304;
 		ACCwriteln(270);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIG HEAP
	p000e0305:
	{
 		if (skipdoall('p000e0305')) break p000e0305;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0305;
			if (!CNDnoun1(75)) break p000e0305;
 		}
		if (!CNDat(10)) break p000e0305;
		if (!CNDpresent(42)) break p000e0305;
		if (!CNDcarried(33)) break p000e0305;
 		ACCwriteln(271);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIG _
	p000e0306:
	{
 		if (skipdoall('p000e0306')) break p000e0306;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0306;
 		}
		if (!CNDnotcarr(33)) break p000e0306;
 		ACCwriteln(272);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIG _
	p000e0307:
	{
 		if (skipdoall('p000e0307')) break p000e0307;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0307;
 		}
		if (!CNDat(14)) break p000e0307;
		if (!CNDcarried(33)) break p000e0307;
 		ACClet(28,4);
 		ACCpause(2);
 		function anykey00081() 
		{
 		ACCwriteln(273);
 		ACCscore();
 		ACCend();
		return;
		}
 		waitKey(anykey00081);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DIG _
	p000e0308:
	{
 		if (skipdoall('p000e0308')) break p000e0308;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0308;
 		}
		if (!CNDat(10)) break p000e0308;
		if (!CNDpresent(41)) break p000e0308;
		if (!CNDcarried(33)) break p000e0308;
 		ACCwriteln(274);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIG _
	p000e0309:
	{
 		if (skipdoall('p000e0309')) break p000e0309;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0309;
 		}
		if (!CNDat(13)) break p000e0309;
		if (!CNDcarried(33)) break p000e0309;
		if (!CNDeq(27,100)) break p000e0309;
 		ACClet(27,200);
 		ACCcreate(7);
 		ACCwriteln(275);
 		ACCpause(150);
 		function anykey00082() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00082);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DIG _
	p000e0310:
	{
 		if (skipdoall('p000e0310')) break p000e0310;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0310;
 		}
		if (!CNDat(13)) break p000e0310;
		if (!CNDeq(27,200)) break p000e0310;
		if (!CNDcarried(33)) break p000e0310;
 		ACClet(27,220);
 		ACCcreate(8);
 		ACCwriteln(276);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00083() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00083);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DIG _
	p000e0311:
	{
 		if (skipdoall('p000e0311')) break p000e0311;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0311;
 		}
		if (!CNDat(13)) break p000e0311;
		if (!CNDcarried(33)) break p000e0311;
		if (!CNDgt(27,219)) break p000e0311;
 		ACCwriteln(277);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIG _
	p000e0312:
	{
 		if (skipdoall('p000e0312')) break p000e0312;
 		if (in_response)
		{
			if (!CNDverb(76)) break p000e0312;
 		}
 		ACCwriteln(278);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// UNDO ROPE
	p000e0313:
	{
 		if (skipdoall('p000e0313')) break p000e0313;
 		if (in_response)
		{
			if (!CNDverb(80)) break p000e0313;
			if (!CNDnoun1(79)) break p000e0313;
 		}
		if (!CNDpresent(2)) break p000e0313;
		if (!CNDzero(20)) break p000e0313;
 		ACClet(20,100);
 		ACCcreate(34);
 		ACCget(34);
		if (!success) break pro000_restart;
 		ACCwriteln(279);
 		ACCpause(100);
 		function anykey00084() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00084);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// UNDO ROPE
	p000e0314:
	{
 		if (skipdoall('p000e0314')) break p000e0314;
 		if (in_response)
		{
			if (!CNDverb(80)) break p000e0314;
			if (!CNDnoun1(79)) break p000e0314;
 		}
		if (!CNDat(29)) break p000e0314;
		if (!CNDzero(21)) break p000e0314;
 		ACCwriteln(280);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// UNDO ROPE
	p000e0315:
	{
 		if (skipdoall('p000e0315')) break p000e0315;
 		if (in_response)
		{
			if (!CNDverb(80)) break p000e0315;
			if (!CNDnoun1(79)) break p000e0315;
 		}
		if (!CNDpresent(2)) break p000e0315;
		if (!CNDeq(20,100)) break p000e0315;
 		ACCwriteln(281);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CUT ROPE
	p000e0316:
	{
 		if (skipdoall('p000e0316')) break p000e0316;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0316;
			if (!CNDnoun1(79)) break p000e0316;
 		}
		if (!CNDpresent(2)) break p000e0316;
		if (!CNDzero(20)) break p000e0316;
 		ACCwriteln(282);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CUT ROPE
	p000e0317:
	{
 		if (skipdoall('p000e0317')) break p000e0317;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0317;
			if (!CNDnoun1(79)) break p000e0317;
 		}
		if (!CNDat(29)) break p000e0317;
		if (!CNDzero(21)) break p000e0317;
		if (!CNDnotcarr(27)) break p000e0317;
 		ACCwriteln(283);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CUT ROPE
	p000e0318:
	{
 		if (skipdoall('p000e0318')) break p000e0318;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0318;
			if (!CNDnoun1(79)) break p000e0318;
 		}
		if (!CNDat(29)) break p000e0318;
		if (!CNDcarried(27)) break p000e0318;
		if (!CNDzero(21)) break p000e0318;
 		ACClet(21,100);
 		ACCwriteln(284);
 		ACCanykey();
 		function anykey00085() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00085);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// CUT ROPE
	p000e0319:
	{
 		if (skipdoall('p000e0319')) break p000e0319;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0319;
			if (!CNDnoun1(79)) break p000e0319;
 		}
		if (!CNDat(29)) break p000e0319;
		if (!CNDcarried(27)) break p000e0319;
		if (!CNDeq(21,100)) break p000e0319;
 		ACCwriteln(285);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LIGH BOX
	p000e0320:
	{
 		if (skipdoall('p000e0320')) break p000e0320;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0320;
			if (!CNDnoun1(20)) break p000e0320;
 		}
		if (!CNDcarried(6)) break p000e0320;
 		ACCwriteln(286);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LIGH BOX
	p000e0321:
	{
 		if (skipdoall('p000e0321')) break p000e0321;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0321;
			if (!CNDnoun1(20)) break p000e0321;
 		}
		if (!CNDcarried(5)) break p000e0321;
 		ACCwriteln(287);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LIGH COMP
	p000e0322:
	{
 		if (skipdoall('p000e0322')) break p000e0322;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0322;
			if (!CNDnoun1(25)) break p000e0322;
 		}
		if (!CNDat(48)) break p000e0322;
		if (!CNDlt(24,150)) break p000e0322;
 		ACCwriteln(288);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LIGH COMP
	p000e0323:
	{
 		if (skipdoall('p000e0323')) break p000e0323;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0323;
			if (!CNDnoun1(25)) break p000e0323;
 		}
		if (!CNDat(48)) break p000e0323;
		if (!CNDeq(24,150)) break p000e0323;
 		ACClet(24,200);
 		ACCcreate(61);
 		ACCpause(25);
 		function anykey00086() 
		{
 		ACCplus(30,2);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00086);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// LIGH LAMP
	p000e0324:
	{
 		if (skipdoall('p000e0324')) break p000e0324;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0324;
			if (!CNDnoun1(39)) break p000e0324;
 		}
		if (!CNDcarried(1)) break p000e0324;
		if (!CNDgt(29,0)) break p000e0324;
		if (!CNDcarried(6)) break p000e0324;
 		ACCswap(0,1);
 		ACCclear(10);
 		ACCwriteln(289);
 		ACCpause(75);
 		function anykey00087() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00087);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// LIGH LAMP
	p000e0325:
	{
 		if (skipdoall('p000e0325')) break p000e0325;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0325;
			if (!CNDnoun1(39)) break p000e0325;
 		}
		if (!CNDcarried(1)) break p000e0325;
		if (!CNDcarried(5)) break p000e0325;
 		ACCwriteln(290);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LIGH LAMP
	p000e0326:
	{
 		if (skipdoall('p000e0326')) break p000e0326;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0326;
			if (!CNDnoun1(39)) break p000e0326;
 		}
		if (!CNDcarried(1)) break p000e0326;
		if (!CNDnotcarr(6)) break p000e0326;
 		ACCwriteln(291);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LIGH LAMP
	p000e0327:
	{
 		if (skipdoall('p000e0327')) break p000e0327;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0327;
			if (!CNDnoun1(39)) break p000e0327;
 		}
		if (!CNDcarried(1)) break p000e0327;
		if (!CNDeq(29,0)) break p000e0327;
		if (!CNDcarried(6)) break p000e0327;
 		ACCwriteln(292);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LIGH GENE
	p000e0328:
	{
 		if (skipdoall('p000e0328')) break p000e0328;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0328;
			if (!CNDnoun1(161)) break p000e0328;
 		}
		if (!CNDat(31)) break p000e0328;
		if (!CNDlt(115,255)) break p000e0328;
 		ACCwriteln(293);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXTI LAMP
	p000e0329:
	{
 		if (skipdoall('p000e0329')) break p000e0329;
 		if (in_response)
		{
			if (!CNDverb(84)) break p000e0329;
			if (!CNDnoun1(39)) break p000e0329;
 		}
		if (!CNDcarried(0)) break p000e0329;
 		ACCswap(0,1);
 		ACClet(10,3);
 		ACCwriteln(294);
 		ACCpause(75);
 		function anykey00088() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00088);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRIS CHAI
	p000e0330:
	{
 		if (skipdoall('p000e0330')) break p000e0330;
 		if (in_response)
		{
			if (!CNDverb(86)) break p000e0330;
			if (!CNDnoun1(50)) break p000e0330;
 		}
		if (!CNDat(8)) break p000e0330;
		if (!CNDcarried(19)) break p000e0330;
		if (!CNDabsent(20)) break p000e0330;
 		ACCwriteln(295);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRIS CUPB
	p000e0331:
	{
 		if (skipdoall('p000e0331')) break p000e0331;
 		if (in_response)
		{
			if (!CNDverb(86)) break p000e0331;
			if (!CNDnoun1(52)) break p000e0331;
 		}
		if (!CNDat(32)) break p000e0331;
		if (!CNDcarried(19)) break p000e0331;
 		ACCpause(25);
 		function anykey00090() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00089() 
		{
 		ACCwriteln(296);
 		ACCgoto(18);
 		ACCdestroy(15);
 		ACCanykey();
 		waitKey(anykey00090);
		}
 		waitKey(anykey00089);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PRIS NAIL
	p000e0332:
	{
 		if (skipdoall('p000e0332')) break p000e0332;
 		if (in_response)
		{
			if (!CNDverb(86)) break p000e0332;
			if (!CNDnoun1(59)) break p000e0332;
 		}
		if (!CNDpresent(25)) break p000e0332;
 		ACCwriteln(297);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BOAR CANO
	p000e0333:
	{
 		if (skipdoall('p000e0333')) break p000e0333;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0333;
			if (!CNDnoun1(13)) break p000e0333;
 		}
		if (!CNDpresent(2)) break p000e0333;
 		ACCwriteln(298);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BOAR RAFT
	p000e0334:
	{
 		if (skipdoall('p000e0334')) break p000e0334;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0334;
			if (!CNDnoun1(74)) break p000e0334;
 		}
		if (!CNDat(29)) break p000e0334;
		if (!CNDlt(21,255)) break p000e0334;
 		ACCwriteln(299);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BOAR RAFT
	p000e0335:
	{
 		if (skipdoall('p000e0335')) break p000e0335;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0335;
			if (!CNDnoun1(74)) break p000e0335;
 		}
		if (!CNDat(29)) break p000e0335;
		if (!CNDeq(21,255)) break p000e0335;
		if (!CNDnotcarr(4)) break p000e0335;
 		ACCwriteln(300);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BOAR RAFT
	p000e0336:
	{
 		if (skipdoall('p000e0336')) break p000e0336;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0336;
			if (!CNDnoun1(74)) break p000e0336;
 		}
		if (!CNDat(29)) break p000e0336;
		if (!CNDcarried(4)) break p000e0336;
 		ACCwriteln(301);
 		ACCgoto(33);
 		ACCplace(32,33);
 		ACCanykey();
 		function anykey00091() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00091);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// BOAR RAFT
	p000e0337:
	{
 		if (skipdoall('p000e0337')) break p000e0337;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0337;
			if (!CNDnoun1(74)) break p000e0337;
 		}
		if (!CNDat(33)) break p000e0337;
		if (!CNDnotcarr(4)) break p000e0337;
 		ACCwriteln(302);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BOAR RAFT
	p000e0338:
	{
 		if (skipdoall('p000e0338')) break p000e0338;
 		if (in_response)
		{
			if (!CNDverb(87)) break p000e0338;
			if (!CNDnoun1(74)) break p000e0338;
 		}
		if (!CNDat(33)) break p000e0338;
		if (!CNDcarried(4)) break p000e0338;
 		ACCgoto(29);
 		ACCplace(32,29);
 		ACCwriteln(303);
 		ACCanykey();
 		function anykey00092() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00092);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// FEED BEAR
	p000e0339:
	{
 		if (skipdoall('p000e0339')) break p000e0339;
 		if (in_response)
		{
			if (!CNDverb(88)) break p000e0339;
			if (!CNDnoun1(90)) break p000e0339;
 		}
		if (!CNDat(34)) break p000e0339;
		if (!CNDcarried(18)) break p000e0339;
 		ACCwriteln(304);
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// THRO HONE
	p000e0340:
	{
 		if (skipdoall('p000e0340')) break p000e0340;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0340;
			if (!CNDnoun1(46)) break p000e0340;
 		}
		if (!CNDat(34)) break p000e0340;
		if (!CNDcarried(18)) break p000e0340;
 		ACCwriteln(305);
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// THRO GRAP
	p000e0341:
	{
 		if (skipdoall('p000e0341')) break p000e0341;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0341;
			if (!CNDnoun1(69)) break p000e0341;
 		}
		if (!CNDat(40)) break p000e0341;
		if (!CNDcarried(38)) break p000e0341;
 		ACCwriteln(306);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// THRO ROPE
	p000e0342:
	{
 		if (skipdoall('p000e0342')) break p000e0342;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0342;
			if (!CNDnoun1(79)) break p000e0342;
 		}
		if (!CNDat(40)) break p000e0342;
		if (!CNDcarried(38)) break p000e0342;
 		ACCswap(38,48);
 		ACCdrop(48);
		if (!success) break pro000_restart;
 		ACCwriteln(307);
 		ACClet(28,1);
 		ACCpause(250);
 		function anykey00094() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00093() 
		{
 		ACCplus(30,3);
 		ACCanykey();
 		waitKey(anykey00094);
		}
 		waitKey(anykey00093);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// THRO ROPE
	p000e0343:
	{
 		if (skipdoall('p000e0343')) break p000e0343;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0343;
			if (!CNDnoun1(79)) break p000e0343;
 		}
		if (!CNDnotat(40)) break p000e0343;
 		ACCwriteln(308);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// THRO ROPE
	p000e0344:
	{
 		if (skipdoall('p000e0344')) break p000e0344;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0344;
			if (!CNDnoun1(79)) break p000e0344;
 		}
		if (!CNDat(40)) break p000e0344;
		if (!CNDcarried(34)) break p000e0344;
 		ACCdestroy(34);
 		ACCwriteln(309);
 		ACCpause(75);
 		function anykey00097() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00096() 
		{
 		ACCanykey();
 		waitKey(anykey00097);
		}
 		function anykey00095() 
		{
 		ACCwriteln(310);
 		ACClet(28,1);
 		ACCpause(200);
 		waitKey(anykey00096);
		}
 		waitKey(anykey00095);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// THRO TRIPSWITCH
	p000e0345:
	{
 		if (skipdoall('p000e0345')) break p000e0345;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0345;
			if (!CNDnoun1(204)) break p000e0345;
 		}
		if (!CNDpresent(53)) break p000e0345;
		if (!CNDworn(67)) break p000e0345;
		if (!CNDeq(24,100)) break p000e0345;
 		ACClet(24,150);
 		ACCclear(0);
 		ACCdestroy(72);
 		ACCwriteln(311);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00098() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00098);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// THRO TRIPSWITCH
	p000e0346:
	{
 		if (skipdoall('p000e0346')) break p000e0346;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0346;
			if (!CNDnoun1(204)) break p000e0346;
 		}
		if (!CNDpresent(53)) break p000e0346;
		if (!CNDnotworn(67)) break p000e0346;
		if (!CNDeq(24,100)) break p000e0346;
 		ACClet(28,4);
 		ACCpause(4);
 		function anykey00099() 
		{
 		ACCwriteln(312);
 		ACCscore();
 		ACCend();
		return;
		}
 		waitKey(anykey00099);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// THRO TRIPSWITCH
	p000e0347:
	{
 		if (skipdoall('p000e0347')) break p000e0347;
 		if (in_response)
		{
			if (!CNDverb(89)) break p000e0347;
			if (!CNDnoun1(204)) break p000e0347;
 		}
		if (!CNDpresent(53)) break p000e0347;
		if (!CNDeq(24,150)) break p000e0347;
 		ACCwriteln(313);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// STAR TRAC
	p000e0348:
	{
 		if (skipdoall('p000e0348')) break p000e0348;
 		if (in_response)
		{
			if (!CNDverb(93)) break p000e0348;
			if (!CNDnoun1(139)) break p000e0348;
 		}
		if (!CNDat(53)) break p000e0348;
 		ACCwriteln(314);
 		ACCpause(100);
 		function anykey00101() 
		{
 		ACCwriteln(316);
 		ACCdone();
		return;
		}
 		function anykey00100() 
		{
 		ACCwriteln(315);
 		ACCpause(100);
 		waitKey(anykey00101);
		}
 		waitKey(anykey00100);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// STAR GENE
	p000e0349:
	{
 		if (skipdoall('p000e0349')) break p000e0349;
 		if (in_response)
		{
			if (!CNDverb(93)) break p000e0349;
			if (!CNDnoun1(161)) break p000e0349;
 		}
		if (!CNDat(31)) break p000e0349;
		if (!CNDlt(115,255)) break p000e0349;
 		ACCwriteln(317);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BURN ROPE
	p000e0350:
	{
 		if (skipdoall('p000e0350')) break p000e0350;
 		if (in_response)
		{
			if (!CNDverb(95)) break p000e0350;
			if (!CNDnoun1(79)) break p000e0350;
 		}
		if (!CNDat(29)) break p000e0350;
		if (!CNDzero(21)) break p000e0350;
		if (!CNDcarried(6)) break p000e0350;
 		ACCwriteln(318);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BURN ROPE
	p000e0351:
	{
 		if (skipdoall('p000e0351')) break p000e0351;
 		if (in_response)
		{
			if (!CNDverb(95)) break p000e0351;
			if (!CNDnoun1(79)) break p000e0351;
 		}
		if (!CNDat(29)) break p000e0351;
		if (!CNDcarried(6)) break p000e0351;
		if (!CNDeq(21,100)) break p000e0351;
 		ACCset(21);
 		ACCwriteln(319);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00102() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00102);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// BURN ROPE
	p000e0352:
	{
 		if (skipdoall('p000e0352')) break p000e0352;
 		if (in_response)
		{
			if (!CNDverb(95)) break p000e0352;
			if (!CNDnoun1(79)) break p000e0352;
 		}
		if (!CNDat(29)) break p000e0352;
		if (!CNDcarried(6)) break p000e0352;
		if (!CNDeq(21,255)) break p000e0352;
 		ACCwriteln(320);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G CANO
	p000e0353:
	{
 		if (skipdoall('p000e0353')) break p000e0353;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0353;
			if (!CNDnoun1(13)) break p000e0353;
 		}
		if (!CNDpresent(2)) break p000e0353;
 		ACCwriteln(321);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G BOX
	p000e0354:
	{
 		if (skipdoall('p000e0354')) break p000e0354;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0354;
			if (!CNDnoun1(20)) break p000e0354;
 		}
		if (!CNDpresent(6)) break p000e0354;
 		ACCget(6);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// G LAMP
	p000e0355:
	{
 		if (skipdoall('p000e0355')) break p000e0355;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0355;
			if (!CNDnoun1(39)) break p000e0355;
 		}
		if (!CNDat(22)) break p000e0355;
		if (!CNDlt(113,200)) break p000e0355;
 		ACCwriteln(322);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G LAMP
	p000e0356:
	{
 		if (skipdoall('p000e0356')) break p000e0356;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0356;
			if (!CNDnoun1(39)) break p000e0356;
 		}
		if (!CNDat(22)) break p000e0356;
		if (!CNDeq(113,200)) break p000e0356;
 		ACCget(1);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// G LAMP
	p000e0357:
	{
 		if (skipdoall('p000e0357')) break p000e0357;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0357;
			if (!CNDnoun1(39)) break p000e0357;
 		}
		if (!CNDpresent(1)) break p000e0357;
 		ACCget(1);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// G LAMP
	p000e0358:
	{
 		if (skipdoall('p000e0358')) break p000e0358;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0358;
			if (!CNDnoun1(39)) break p000e0358;
 		}
		if (!CNDpresent(0)) break p000e0358;
 		ACCget(0);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// G LAMP
	p000e0359:
	{
 		if (skipdoall('p000e0359')) break p000e0359;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0359;
			if (!CNDnoun1(39)) break p000e0359;
 		}
		if (!CNDpresent(60)) break p000e0359;
 		ACCwriteln(323);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G HONE
	p000e0360:
	{
 		if (skipdoall('p000e0360')) break p000e0360;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0360;
			if (!CNDnoun1(46)) break p000e0360;
 		}
		if (!CNDpresent(36)) break p000e0360;
 		ACCwriteln(324);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G HONE
	p000e0361:
	{
 		if (skipdoall('p000e0361')) break p000e0361;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0361;
			if (!CNDnoun1(46)) break p000e0361;
 		}
		if (!CNDpresent(37)) break p000e0361;
		if (!CNDpresent(18)) break p000e0361;
		if (!CNDnotcarr(18)) break p000e0361;
 		ACCwriteln(325);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G HONE
	p000e0362:
	{
 		if (skipdoall('p000e0362')) break p000e0362;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0362;
			if (!CNDnoun1(46)) break p000e0362;
 		}
		if (!CNDpresent(18)) break p000e0362;
		if (!CNDnotcarr(18)) break p000e0362;
		if (!CNDzero(5)) break p000e0362;
		if (!CNDpresent(59)) break p000e0362;
 		ACCwriteln(326);
 		ACCanykey();
 		function anykey00103() 
		{
 		ACCgoto(21);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00103);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// G HONE
	p000e0363:
	{
 		if (skipdoall('p000e0363')) break p000e0363;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0363;
			if (!CNDnoun1(46)) break p000e0363;
 		}
		if (!CNDpresent(62)) break p000e0363;
		if (!CNDgt(5,1)) break p000e0363;
 		ACCplus(30,3);
 		ACCget(18);
		if (!success) break pro000_restart;
 		ACCwriteln(327);
 		ACCanykey();
 		function anykey00104() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00104);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// G HONE
	p000e0364:
	{
 		if (skipdoall('p000e0364')) break p000e0364;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0364;
			if (!CNDnoun1(46)) break p000e0364;
 		}
		if (!CNDpresent(18)) break p000e0364;
		if (!CNDnotcarr(18)) break p000e0364;
		if (!CNDzero(5)) break p000e0364;
		if (!CNDpresent(62)) break p000e0364;
 		ACCwriteln(328);
 		ACCanykey();
 		function anykey00105() 
		{
 		ACCgoto(21);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00105);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// G HONE
	p000e0365:
	{
 		if (skipdoall('p000e0365')) break p000e0365;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0365;
			if (!CNDnoun1(46)) break p000e0365;
 		}
 		ACCget(18);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// G NOTI
	p000e0366:
	{
 		if (skipdoall('p000e0366')) break p000e0366;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0366;
			if (!CNDnoun1(58)) break p000e0366;
 		}
		if (!CNDpresent(25)) break p000e0366;
 		ACCwriteln(329);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G NOTI
	p000e0367:
	{
 		if (skipdoall('p000e0367')) break p000e0367;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0367;
			if (!CNDnoun1(58)) break p000e0367;
 		}
		if (!CNDpresent(26)) break p000e0367;
 		ACCget(26);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// G NAIL
	p000e0368:
	{
 		if (skipdoall('p000e0368')) break p000e0368;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0368;
			if (!CNDnoun1(59)) break p000e0368;
 		}
		if (!CNDpresent(25)) break p000e0368;
 		ACCwriteln(330);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G LADD
	p000e0369:
	{
 		if (skipdoall('p000e0369')) break p000e0369;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0369;
			if (!CNDnoun1(64)) break p000e0369;
 		}
		if (!CNDpresent(28)) break p000e0369;
		if (!CNDgt(1,2)) break p000e0369;
 		ACCwriteln(331);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G LADD
	p000e0370:
	{
 		if (skipdoall('p000e0370')) break p000e0370;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0370;
			if (!CNDnoun1(64)) break p000e0370;
 		}
		if (!CNDpresent(28)) break p000e0370;
 		ACCget(28);
		if (!success) break pro000_restart;
 		ACClet(28,11);
 		ACCpause(3);
 		function anykey00106() 
		{
 		ACCok();
		return;
		}
 		waitKey(anykey00106);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// G LADD
	p000e0371:
	{
 		if (skipdoall('p000e0371')) break p000e0371;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0371;
			if (!CNDnoun1(64)) break p000e0371;
 		}
		if (!CNDpresent(29)) break p000e0371;
		if (!CNDgt(1,2)) break p000e0371;
 		ACCwriteln(332);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G LADD
	p000e0372:
	{
 		if (skipdoall('p000e0372')) break p000e0372;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0372;
			if (!CNDnoun1(64)) break p000e0372;
 		}
		if (!CNDpresent(29)) break p000e0372;
 		ACCswap(28,29);
 		ACCget(28);
		if (!success) break pro000_restart;
 		ACClet(28,11);
 		ACCpause(3);
 		function anykey00107() 
		{
 		ACCok();
		return;
		}
 		waitKey(anykey00107);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// G ALL
	p000e0373:
	{
 		if (skipdoall('p000e0373')) break p000e0373;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0373;
			if (!CNDnoun1(65)) break p000e0373;
 		}
 		ACCwriteln(333);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G GRAP
	p000e0374:
	{
 		if (skipdoall('p000e0374')) break p000e0374;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0374;
			if (!CNDnoun1(69)) break p000e0374;
 		}
		if (!CNDat(36)) break p000e0374;
		if (!CNDzero(18)) break p000e0374;
 		ACCwriteln(334);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G GRAP
	p000e0375:
	{
 		if (skipdoall('p000e0375')) break p000e0375;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0375;
			if (!CNDnoun1(69)) break p000e0375;
 		}
		if (!CNDat(45)) break p000e0375;
		if (!CNDpresent(30)) break p000e0375;
 		ACCwriteln(335);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G ROPE
	p000e0376:
	{
 		if (skipdoall('p000e0376')) break p000e0376;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0376;
			if (!CNDnoun1(79)) break p000e0376;
 		}
		if (!CNDpresent(38)) break p000e0376;
 		ACCget(38);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// G ROPE
	p000e0377:
	{
 		if (skipdoall('p000e0377')) break p000e0377;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0377;
			if (!CNDnoun1(79)) break p000e0377;
 		}
		if (!CNDpresent(2)) break p000e0377;
		if (!CNDzero(20)) break p000e0377;
 		ACCwriteln(336);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G ROPE
	p000e0378:
	{
 		if (skipdoall('p000e0378')) break p000e0378;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0378;
			if (!CNDnoun1(79)) break p000e0378;
 		}
		if (!CNDat(40)) break p000e0378;
		if (!CNDpresent(48)) break p000e0378;
 		ACCwriteln(337);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G ROPE
	p000e0379:
	{
 		if (skipdoall('p000e0379')) break p000e0379;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0379;
			if (!CNDnoun1(79)) break p000e0379;
 		}
		if (!CNDpresent(48)) break p000e0379;
 		ACCwriteln(338);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G DOG
	p000e0380:
	{
 		if (skipdoall('p000e0380')) break p000e0380;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0380;
			if (!CNDnoun1(82)) break p000e0380;
 		}
		if (!CNDpresent(40)) break p000e0380;
 		ACCwriteln(339);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G EGG
	p000e0381:
	{
 		if (skipdoall('p000e0381')) break p000e0381;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0381;
			if (!CNDnoun1(98)) break p000e0381;
 		}
		if (!CNDpresent(44)) break p000e0381;
 		ACCget(44);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// G EGG
	p000e0382:
	{
 		if (skipdoall('p000e0382')) break p000e0382;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0382;
			if (!CNDnoun1(98)) break p000e0382;
 		}
		if (!CNDpresent(45)) break p000e0382;
 		ACCwriteln(340);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G BARK
	p000e0383:
	{
 		if (skipdoall('p000e0383')) break p000e0383;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0383;
			if (!CNDnoun1(109)) break p000e0383;
 		}
		if (!CNDat(39)) break p000e0383;
 		ACCwriteln(341);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G PAPE
	p000e0384:
	{
 		if (skipdoall('p000e0384')) break p000e0384;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0384;
			if (!CNDnoun1(114)) break p000e0384;
 		}
		if (!CNDat(42)) break p000e0384;
		if (!CNDzero(23)) break p000e0384;
 		ACCwriteln(342);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G LIFTER
	p000e0385:
	{
 		if (skipdoall('p000e0385')) break p000e0385;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0385;
			if (!CNDnoun1(203)) break p000e0385;
 		}
		if (!CNDpresent(51)) break p000e0385;
 		ACCget(51);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// G PLAN
	p000e0386:
	{
 		if (skipdoall('p000e0386')) break p000e0386;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0386;
			if (!CNDnoun1(165)) break p000e0386;
 		}
		if (!CNDpresent(68)) break p000e0386;
 		ACCwriteln(343);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// G COTT
	p000e0387:
	{
 		if (skipdoall('p000e0387')) break p000e0387;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0387;
			if (!CNDnoun1(201)) break p000e0387;
 		}
 		ACCget(56);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// G _
	p000e0388:
	{
 		if (skipdoall('p000e0388')) break p000e0388;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0388;
 		}
 		ACCautog();
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP BOX
	p000e0389:
	{
 		if (skipdoall('p000e0389')) break p000e0389;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0389;
			if (!CNDnoun1(20)) break p000e0389;
 		}
		if (!CNDcarried(6)) break p000e0389;
 		ACCdrop(6);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP LAMP
	p000e0390:
	{
 		if (skipdoall('p000e0390')) break p000e0390;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0390;
			if (!CNDnoun1(39)) break p000e0390;
 		}
		if (!CNDcarried(1)) break p000e0390;
 		ACCswap(1,60);
 		ACCdrop(60);
		if (!success) break pro000_restart;
 		ACCwriteln(344);
 		ACCpause(25);
 		function anykey00109() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00108() 
		{
 		ACCwriteln(345);
 		ACCpause(120);
 		waitKey(anykey00109);
		}
 		waitKey(anykey00108);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DROP LAMP
	p000e0391:
	{
 		if (skipdoall('p000e0391')) break p000e0391;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0391;
			if (!CNDnoun1(39)) break p000e0391;
 		}
		if (!CNDcarried(0)) break p000e0391;
 		ACCswap(0,60);
 		ACCdrop(60);
		if (!success) break pro000_restart;
 		ACCwriteln(346);
 		ACCpause(25);
 		function anykey00111() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00110() 
		{
 		ACCwriteln(347);
 		ACCpause(120);
 		waitKey(anykey00111);
		}
 		waitKey(anykey00110);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DROP HONE
	p000e0392:
	{
 		if (skipdoall('p000e0392')) break p000e0392;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0392;
			if (!CNDnoun1(46)) break p000e0392;
 		}
		if (!CNDcarried(18)) break p000e0392;
 		ACCswap(18,36);
 		ACCdrop(36);
		if (!success) break pro000_restart;
 		ACCwriteln(348);
 		ACCpause(25);
 		function anykey00113() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00112() 
		{
 		ACCwriteln(349);
 		ACCpause(120);
 		waitKey(anykey00113);
		}
 		waitKey(anykey00112);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DROP NOTI
	p000e0393:
	{
 		if (skipdoall('p000e0393')) break p000e0393;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0393;
			if (!CNDnoun1(58)) break p000e0393;
 		}
		if (!CNDcarried(26)) break p000e0393;
 		ACCdrop(26);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP LADD
	p000e0394:
	{
 		if (skipdoall('p000e0394')) break p000e0394;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0394;
			if (!CNDnoun1(64)) break p000e0394;
 		}
		if (!CNDcarried(28)) break p000e0394;
 		ACCdrop(28);
		if (!success) break pro000_restart;
 		ACClet(28,11);
 		ACCpause(6);
 		function anykey00115() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00114() 
		{
 		ACCwriteln(350);
 		ACCanykey();
 		waitKey(anykey00115);
		}
 		waitKey(anykey00114);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DROP ALL
	p000e0395:
	{
 		if (skipdoall('p000e0395')) break p000e0395;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0395;
			if (!CNDnoun1(65)) break p000e0395;
 		}
		if (!CNDgt(1,0)) break p000e0395;
 		ACCdropall();
 		ACClet(28,11);
 		ACCpause(6);
 		function anykey00117() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00116() 
		{
 		ACCwriteln(351);
 		ACCpause(150);
 		waitKey(anykey00117);
		}
 		waitKey(anykey00116);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DROP ALL
	p000e0396:
	{
 		if (skipdoall('p000e0396')) break p000e0396;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0396;
			if (!CNDnoun1(65)) break p000e0396;
 		}
		if (!CNDeq(1,0)) break p000e0396;
		if (!CNDnotworn(8)) break p000e0396;
		if (!CNDnotworn(67)) break p000e0396;
 		ACCwriteln(352);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP ROPE
	p000e0397:
	{
 		if (skipdoall('p000e0397')) break p000e0397;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0397;
			if (!CNDnoun1(79)) break p000e0397;
 		}
		if (!CNDcarried(38)) break p000e0397;
 		ACCdrop(38);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP EGG
	p000e0398:
	{
 		if (skipdoall('p000e0398')) break p000e0398;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0398;
			if (!CNDnoun1(98)) break p000e0398;
 		}
		if (!CNDat(38)) break p000e0398;
		if (!CNDcarried(44)) break p000e0398;
 		ACCswap(44,45);
 		ACCdrop(45);
		if (!success) break pro000_restart;
 		ACCplace(45,39);
 		ACCwriteln(353);
 		ACCpause(50);
 		function anykey00118() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00118);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DROP EGG
	p000e0399:
	{
 		if (skipdoall('p000e0399')) break p000e0399;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0399;
			if (!CNDnoun1(98)) break p000e0399;
 		}
		if (!CNDnotat(38)) break p000e0399;
		if (!CNDcarried(44)) break p000e0399;
 		ACCswap(44,45);
 		ACCdrop(45);
		if (!success) break pro000_restart;
 		ACCwriteln(354);
 		ACCpause(50);
 		function anykey00119() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00119);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DROP LIFTER
	p000e0400:
	{
 		if (skipdoall('p000e0400')) break p000e0400;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0400;
			if (!CNDnoun1(203)) break p000e0400;
 		}
		if (!CNDcarried(51)) break p000e0400;
 		ACCdrop(51);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP NOTH
	p000e0401:
	{
 		if (skipdoall('p000e0401')) break p000e0401;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0401;
			if (!CNDnoun1(183)) break p000e0401;
 		}
		if (!CNDeq(1,0)) break p000e0401;
 		ACCwriteln(355);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP COTT
	p000e0402:
	{
 		if (skipdoall('p000e0402')) break p000e0402;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0402;
			if (!CNDnoun1(201)) break p000e0402;
 		}
 		ACCdrop(56);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP _
	p000e0403:
	{
 		if (skipdoall('p000e0403')) break p000e0403;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0403;
 		}
		if (!CNDat(27)) break p000e0403;
 		ACCwriteln(356);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP _
	p000e0404:
	{
 		if (skipdoall('p000e0404')) break p000e0404;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0404;
 		}
		if (!CNDat(30)) break p000e0404;
 		ACCwriteln(357);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP _
	p000e0405:
	{
 		if (skipdoall('p000e0405')) break p000e0405;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0405;
 		}
		if (!CNDat(45)) break p000e0405;
 		ACCwriteln(358);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP _
	p000e0406:
	{
 		if (skipdoall('p000e0406')) break p000e0406;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0406;
 		}
		if (!CNDat(47)) break p000e0406;
 		ACCwriteln(359);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP _
	p000e0407:
	{
 		if (skipdoall('p000e0407')) break p000e0407;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0407;
 		}
 		ACCautod();
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO ROD
	p000e0408:
	{
 		if (skipdoall('p000e0408')) break p000e0408;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0408;
			if (!CNDnoun1(53)) break p000e0408;
 		}
		if (!CNDat(31)) break p000e0408;
		if (!CNDpresent(23)) break p000e0408;
 		ACCwriteln(360);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REMO NEED
	p000e0409:
	{
 		if (skipdoall('p000e0409')) break p000e0409;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0409;
			if (!CNDnoun1(57)) break p000e0409;
 		}
		if (!CNDcarried(12)) break p000e0409;
 		ACCcreate(11);
 		ACCswap(10,12);
 		ACCwriteln(361);
 		ACCanykey();
 		function anykey00120() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00120);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// REMO NOTI
	p000e0410:
	{
 		if (skipdoall('p000e0410')) break p000e0410;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0410;
			if (!CNDnoun1(58)) break p000e0410;
 		}
		if (!CNDpresent(25)) break p000e0410;
 		ACCwriteln(362);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REMO NAIL
	p000e0411:
	{
 		if (skipdoall('p000e0411')) break p000e0411;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0411;
			if (!CNDnoun1(59)) break p000e0411;
 		}
		if (!CNDcarried(51)) break p000e0411;
 		ACCcreate(10);
 		ACCswap(50,51);
 		ACCdrop(50);
		if (!success) break pro000_restart;
 		ACCget(10);
		if (!success) break pro000_restart;
 		ACCwriteln(363);
 		ACCanykey();
 		function anykey00121() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00121);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// REMO NAIL
	p000e0412:
	{
 		if (skipdoall('p000e0412')) break p000e0412;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0412;
			if (!CNDnoun1(59)) break p000e0412;
 		}
		if (!CNDat(14)) break p000e0412;
		if (!CNDnotcarr(57)) break p000e0412;
		if (!CNDpresent(25)) break p000e0412;
 		ACCwriteln(364);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REMO LID
	p000e0413:
	{
 		if (skipdoall('p000e0413')) break p000e0413;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0413;
			if (!CNDnoun1(63)) break p000e0413;
 		}
		if (!CNDcarried(18)) break p000e0413;
		if (!CNDlt(19,220)) break p000e0413;
 		ACCwriteln(365);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REMO GRAP
	p000e0414:
	{
 		if (skipdoall('p000e0414')) break p000e0414;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0414;
			if (!CNDnoun1(69)) break p000e0414;
 		}
		if (!CNDat(45)) break p000e0414;
		if (!CNDzero(18)) break p000e0414;
 		ACCwriteln(366);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REMO ROPE
	p000e0415:
	{
 		if (skipdoall('p000e0415')) break p000e0415;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0415;
			if (!CNDnoun1(79)) break p000e0415;
 		}
		if (!CNDat(29)) break p000e0415;
		if (!CNDlt(21,255)) break p000e0415;
 		ACCwriteln(367);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REMO COTT
	p000e0416:
	{
 		if (skipdoall('p000e0416')) break p000e0416;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0416;
			if (!CNDnoun1(201)) break p000e0416;
 		}
		if (!CNDworn(56)) break p000e0416;
 		ACCremove(56);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO _
	p000e0417:
	{
 		if (skipdoall('p000e0417')) break p000e0417;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0417;
 		}
 		ACCautor();
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// WEAR PANT
	p000e0418:
	{
 		if (skipdoall('p000e0418')) break p000e0418;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0418;
			if (!CNDnoun1(37)) break p000e0418;
 		}
		if (!CNDcarried(14)) break p000e0418;
 		ACCwriteln(368);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// WEAR COTT
	p000e0419:
	{
 		if (skipdoall('p000e0419')) break p000e0419;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0419;
			if (!CNDnoun1(201)) break p000e0419;
 		}
		if (!CNDcarried(56)) break p000e0419;
 		ACCwriteln(369);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// WEAR _
	p000e0420:
	{
 		if (skipdoall('p000e0420')) break p000e0420;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0420;
 		}
 		ACCautow();
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// I _
	p000e0421:
	{
 		if (skipdoall('p000e0421')) break p000e0421;
 		if (in_response)
		{
			if (!CNDverb(104)) break p000e0421;
 		}
 		ACCinven();
		break pro000_restart;
		{}

	}

	// L _
	p000e0422:
	{
 		if (skipdoall('p000e0422')) break p000e0422;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0422;
 		}
 		ACCprocess(3);
		{}

	}

	// L UNDE
	p000e0423:
	{
 		if (skipdoall('p000e0423')) break p000e0423;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0423;
			if (!CNDnoun1(32)) break p000e0423;
 		}
		if (!CNDat(19)) break p000e0423;
		if (!CNDzero(113)) break p000e0423;
 		ACClet(113,100);
 		ACCcreate(17);
 		ACCwriteln(370);
 		ACCplus(30,2);
 		ACCpause(75);
 		function anykey00122() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00122);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// L UNDE
	p000e0424:
	{
 		if (skipdoall('p000e0424')) break p000e0424;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0424;
			if (!CNDnoun1(32)) break p000e0424;
 		}
		if (!CNDat(16)) break p000e0424;
		if (!CNDzero(22)) break p000e0424;
 		ACClet(22,100);
 		ACCcreate(47);
 		ACCwriteln(371);
 		ACCpause(75);
 		function anykey00123() 
		{
 		ACCplus(30,2);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00123);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// L UNDE
	p000e0425:
	{
 		if (skipdoall('p000e0425')) break p000e0425;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0425;
			if (!CNDnoun1(32)) break p000e0425;
 		}
		if (!CNDat(19)) break p000e0425;
		if (!CNDeq(113,100)) break p000e0425;
 		ACCwriteln(372);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// L UNDE
	p000e0426:
	{
 		if (skipdoall('p000e0426')) break p000e0426;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0426;
			if (!CNDnoun1(32)) break p000e0426;
 		}
		if (!CNDat(16)) break p000e0426;
		if (!CNDeq(22,100)) break p000e0426;
 		ACCwriteln(373);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// L UNDE
	p000e0427:
	{
 		if (skipdoall('p000e0427')) break p000e0427;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0427;
			if (!CNDnoun1(32)) break p000e0427;
 		}
 		ACCwriteln(374);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// L BEHI
	p000e0428:
	{
 		if (skipdoall('p000e0428')) break p000e0428;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0428;
			if (!CNDnoun1(92)) break p000e0428;
 		}
		if (!CNDat(52)) break p000e0428;
		if (!CNDzero(17)) break p000e0428;
 		ACCset(17);
 		ACCcreate(64);
 		ACCwriteln(375);
 		ACCplus(30,3);
 		ACCpause(75);
 		function anykey00124() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00124);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// L BEHI
	p000e0429:
	{
 		if (skipdoall('p000e0429')) break p000e0429;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0429;
			if (!CNDnoun1(92)) break p000e0429;
 		}
		if (!CNDat(31)) break p000e0429;
		if (!CNDeq(112,100)) break p000e0429;
 		ACClet(112,200);
 		ACCcreate(39);
 		ACCwriteln(376);
 		ACCplus(30,3);
 		ACCpause(150);
 		function anykey00125() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00125);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// L BEHI
	p000e0430:
	{
 		if (skipdoall('p000e0430')) break p000e0430;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0430;
			if (!CNDnoun1(92)) break p000e0430;
 		}
		if (!CNDat(52)) break p000e0430;
		if (!CNDeq(17,255)) break p000e0430;
 		ACCwriteln(377);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// L BEHI
	p000e0431:
	{
 		if (skipdoall('p000e0431')) break p000e0431;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0431;
			if (!CNDnoun1(92)) break p000e0431;
 		}
		if (!CNDat(31)) break p000e0431;
		if (!CNDeq(112,200)) break p000e0431;
 		ACCwriteln(378);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// L BEHI
	p000e0432:
	{
 		if (skipdoall('p000e0432')) break p000e0432;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0432;
			if (!CNDnoun1(92)) break p000e0432;
 		}
 		ACCwriteln(379);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// L _
	p000e0433:
	{
 		if (skipdoall('p000e0433')) break p000e0433;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0433;
 		}
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// QQ _
	p000e0434:
	{
 		if (skipdoall('p000e0434')) break p000e0434;
 		if (in_response)
		{
			if (!CNDverb(106)) break p000e0434;
 		}
 		ACCquit();
 		function anykey00126() 
		{
 		ACCturns();
 		ACCend();
		return;
		}
 		waitKey(anykey00126);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SAVE _
	p000e0435:
	{
 		if (skipdoall('p000e0435')) break p000e0435;
 		if (in_response)
		{
			if (!CNDverb(107)) break p000e0435;
 		}
 		ACCsave();
		break pro000_restart;
		{}

	}

	// LOAD _
	p000e0436:
	{
 		if (skipdoall('p000e0436')) break p000e0436;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0436;
 		}
 		ACCload();
		break pro000_restart;
		{}

	}

	// MOVE LID
	p000e0437:
	{
 		if (skipdoall('p000e0437')) break p000e0437;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0437;
			if (!CNDnoun1(63)) break p000e0437;
 		}
		if (!CNDcarried(18)) break p000e0437;
		if (!CNDlt(19,220)) break p000e0437;
 		ACCwriteln(380);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MOVE BARK
	p000e0438:
	{
 		if (skipdoall('p000e0438')) break p000e0438;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0438;
			if (!CNDnoun1(109)) break p000e0438;
 		}
		if (!CNDat(39)) break p000e0438;
		if (!CNDabsent(46)) break p000e0438;
 		ACCcreate(46);
 		ACCwriteln(381);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00127() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00127);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// MOVE BARK
	p000e0439:
	{
 		if (skipdoall('p000e0439')) break p000e0439;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0439;
			if (!CNDnoun1(109)) break p000e0439;
 		}
		if (!CNDat(39)) break p000e0439;
		if (!CNDpresent(46)) break p000e0439;
 		ACCwriteln(382);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MOVE KNOB
	p000e0440:
	{
 		if (skipdoall('p000e0440')) break p000e0440;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0440;
			if (!CNDnoun1(134)) break p000e0440;
 		}
		if (!CNDat(49)) break p000e0440;
		if (!CNDnotcarr(57)) break p000e0440;
 		ACCwriteln(383);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MOVE KNOB
	p000e0441:
	{
 		if (skipdoall('p000e0441')) break p000e0441;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0441;
			if (!CNDnoun1(134)) break p000e0441;
 		}
		if (!CNDat(49)) break p000e0441;
		if (!CNDcarried(57)) break p000e0441;
		if (!CNDzero(26)) break p000e0441;
 		ACCset(26);
 		ACCplus(25,10);
 		ACCwriteln(384);
 		ACCplus(30,3);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MOVE KNOB
	p000e0442:
	{
 		if (skipdoall('p000e0442')) break p000e0442;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0442;
			if (!CNDnoun1(134)) break p000e0442;
 		}
		if (!CNDat(49)) break p000e0442;
		if (!CNDeq(26,255)) break p000e0442;
 		ACCwriteln(385);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MOVE _
	p000e0443:
	{
 		if (skipdoall('p000e0443')) break p000e0443;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0443;
 		}
 		ACCturns();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SWIN _
	p000e0444:
	{
 		if (skipdoall('p000e0444')) break p000e0444;
 		if (in_response)
		{
			if (!CNDverb(113)) break p000e0444;
 		}
		if (!CNDat(40)) break p000e0444;
		if (!CNDabsent(48)) break p000e0444;
 		ACCwriteln(386);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SWIN _
	p000e0445:
	{
 		if (skipdoall('p000e0445')) break p000e0445;
 		if (in_response)
		{
			if (!CNDverb(113)) break p000e0445;
 		}
		if (!CNDat(40)) break p000e0445;
		if (!CNDpresent(48)) break p000e0445;
 		ACCgoto(42);
 		ACCwriteln(387);
 		ACCplace(48,42);
 		ACClet(28,5);
 		ACCpause(3);
 		function anykey00129() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00128() 
		{
 		ACCanykey();
 		waitKey(anykey00129);
		}
 		waitKey(anykey00128);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SWIN _
	p000e0446:
	{
 		if (skipdoall('p000e0446')) break p000e0446;
 		if (in_response)
		{
			if (!CNDverb(113)) break p000e0446;
 		}
		if (!CNDat(42)) break p000e0446;
 		ACCwriteln(388);
 		ACCplace(48,40);
 		ACCgoto(40);
 		ACClet(28,5);
 		ACCpause(3);
 		function anykey00131() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00130() 
		{
 		ACCanykey();
 		waitKey(anykey00131);
		}
 		waitKey(anykey00130);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// LIFT PAPE
	p000e0447:
	{
 		if (skipdoall('p000e0447')) break p000e0447;
 		if (in_response)
		{
			if (!CNDverb(116)) break p000e0447;
			if (!CNDnoun1(114)) break p000e0447;
 		}
		if (!CNDat(42)) break p000e0447;
		if (!CNDzero(23)) break p000e0447;
		if (!CNDcarried(51)) break p000e0447;
 		ACClet(23,10);
 		ACCcreate(49);
 		ACCdrop(51);
		if (!success) break pro000_restart;
 		ACCget(49);
		if (!success) break pro000_restart;
 		ACCwriteln(389);
 		ACCplus(30,3);
 		ACCanykey();
 		function anykey00132() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00132);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// LIFT PAPE
	p000e0448:
	{
 		if (skipdoall('p000e0448')) break p000e0448;
 		if (in_response)
		{
			if (!CNDverb(116)) break p000e0448;
			if (!CNDnoun1(114)) break p000e0448;
 		}
		if (!CNDat(42)) break p000e0448;
		if (!CNDeq(23,10)) break p000e0448;
 		ACCwriteln(390);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SWITCH _
	p000e0449:
	{
 		if (skipdoall('p000e0449')) break p000e0449;
 		if (in_response)
		{
			if (!CNDverb(120)) break p000e0449;
 		}
 		ACCprocess(4);
		{}

	}

	// 28S 172W
	p000e0450:
	{
 		if (skipdoall('p000e0450')) break p000e0450;
 		if (in_response)
		{
			if (!CNDverb(124)) break p000e0450;
			if (!CNDnoun1(125)) break p000e0450;
 		}
		if (!CNDabsent(55)) break p000e0450;
 		ACCwriteln(391);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// 28S 172W
	p000e0451:
	{
 		if (skipdoall('p000e0451')) break p000e0451;
 		if (in_response)
		{
			if (!CNDverb(124)) break p000e0451;
			if (!CNDnoun1(125)) break p000e0451;
 		}
		if (!CNDpresent(55)) break p000e0451;
		if (!CNDabsent(58)) break p000e0451;
 		ACCcreate(58);
 		ACCplus(25,10);
 		ACCwriteln(392);
 		ACClet(28,5);
 		ACCpause(4);
 		function anykey00133() 
		{
 		ACCdone();
		return;
		}
 		waitKey(anykey00133);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SWIM _
	p000e0452:
	{
 		if (skipdoall('p000e0452')) break p000e0452;
 		if (in_response)
		{
			if (!CNDverb(128)) break p000e0452;
 		}
		if (!CNDatgt(2)) break p000e0452;
		if (!CNDatlt(5)) break p000e0452;
 		ACCwriteln(393);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SWIM _
	p000e0453:
	{
 		if (skipdoall('p000e0453')) break p000e0453;
 		if (in_response)
		{
			if (!CNDverb(128)) break p000e0453;
 		}
		if (!CNDatgt(39)) break p000e0453;
		if (!CNDatlt(43)) break p000e0453;
 		ACCwriteln(394);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BLOC EAR
	p000e0454:
	{
 		if (skipdoall('p000e0454')) break p000e0454;
 		if (in_response)
		{
			if (!CNDverb(130)) break p000e0454;
			if (!CNDnoun1(131)) break p000e0454;
 		}
		if (!CNDnotat(49)) break p000e0454;
		if (!CNDcarried(56)) break p000e0454;
 		ACCwriteln(395);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BLOC EAR
	p000e0455:
	{
 		if (skipdoall('p000e0455')) break p000e0455;
 		if (in_response)
		{
			if (!CNDverb(130)) break p000e0455;
			if (!CNDnoun1(131)) break p000e0455;
 		}
		if (!CNDat(49)) break p000e0455;
		if (!CNDcarried(56)) break p000e0455;
 		ACCwear(56);
		if (!success) break pro000_restart;
 		ACCwriteln(396);
 		ACCplus(30,3);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// KISS WIFE
	p000e0456:
	{
 		if (skipdoall('p000e0456')) break p000e0456;
 		if (in_response)
		{
			if (!CNDverb(137)) break p000e0456;
			if (!CNDnoun1(136)) break p000e0456;
 		}
		if (!CNDpresent(59)) break p000e0456;
 		ACCwriteln(397);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// KISS WIFE
	p000e0457:
	{
 		if (skipdoall('p000e0457')) break p000e0457;
 		if (in_response)
		{
			if (!CNDverb(137)) break p000e0457;
			if (!CNDnoun1(136)) break p000e0457;
 		}
		if (!CNDpresent(62)) break p000e0457;
		if (!CNDpresent(18)) break p000e0457;
		if (!CNDnotcarr(18)) break p000e0457;
 		ACCwriteln(398);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// KISS WIFE
	p000e0458:
	{
 		if (skipdoall('p000e0458')) break p000e0458;
 		if (in_response)
		{
			if (!CNDverb(137)) break p000e0458;
			if (!CNDnoun1(136)) break p000e0458;
 		}
		if (!CNDpresent(62)) break p000e0458;
		if (!CNDabsent(18)) break p000e0458;
 		ACCwriteln(399);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DRY BOX
	p000e0459:
	{
 		if (skipdoall('p000e0459')) break p000e0459;
 		if (in_response)
		{
			if (!CNDverb(140)) break p000e0459;
			if (!CNDnoun1(20)) break p000e0459;
 		}
		if (!CNDcarried(5)) break p000e0459;
 		ACCwriteln(400);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PAY GUAR
	p000e0460:
	{
 		if (skipdoall('p000e0460')) break p000e0460;
 		if (in_response)
		{
			if (!CNDverb(141)) break p000e0460;
			if (!CNDnoun1(97)) break p000e0460;
 		}
		if (!CNDpresent(43)) break p000e0460;
		if (!CNDcarried(9)) break p000e0460;
 		ACCwriteln(401);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CROS POOL
	p000e0461:
	{
 		if (skipdoall('p000e0461')) break p000e0461;
 		if (in_response)
		{
			if (!CNDverb(143)) break p000e0461;
			if (!CNDnoun1(144)) break p000e0461;
 		}
		if (!CNDat(40)) break p000e0461;
 		ACCwriteln(402);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CROS HIGH
	p000e0462:
	{
 		if (skipdoall('p000e0462')) break p000e0462;
 		if (in_response)
		{
			if (!CNDverb(143)) break p000e0462;
			if (!CNDnoun1(185)) break p000e0462;
 		}
		if (!CNDat(1)) break p000e0462;
 		ACCwriteln(403);
 		ACCend();
		break pro000_restart;
		{}

	}

	// DISM COMP
	p000e0463:
	{
 		if (skipdoall('p000e0463')) break p000e0463;
 		if (in_response)
		{
			if (!CNDverb(145)) break p000e0463;
			if (!CNDnoun1(25)) break p000e0463;
 		}
		if (!CNDcarried(12)) break p000e0463;
 		ACCwriteln(404);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EAT HONE
	p000e0464:
	{
 		if (skipdoall('p000e0464')) break p000e0464;
 		if (in_response)
		{
			if (!CNDverb(149)) break p000e0464;
			if (!CNDnoun1(46)) break p000e0464;
 		}
		if (!CNDcarried(18)) break p000e0464;
		if (!CNDlt(19,220)) break p000e0464;
 		ACCwriteln(405);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EAT HONE
	p000e0465:
	{
 		if (skipdoall('p000e0465')) break p000e0465;
 		if (in_response)
		{
			if (!CNDverb(149)) break p000e0465;
			if (!CNDnoun1(46)) break p000e0465;
 		}
		if (!CNDcarried(18)) break p000e0465;
		if (!CNDeq(19,220)) break p000e0465;
 		ACCwriteln(406);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EAT HERB
	p000e0466:
	{
 		if (skipdoall('p000e0466')) break p000e0466;
 		if (in_response)
		{
			if (!CNDverb(149)) break p000e0466;
			if (!CNDnoun1(148)) break p000e0466;
 		}
		if (!CNDcarried(64)) break p000e0466;
 		ACCdestroy(64);
 		ACCwriteln(407);
 		ACCpause(100);
 		function anykey00134() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00134);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SAVE RAM
	p000e0467:
	{
 		if (skipdoall('p000e0467')) break p000e0467;
 		if (in_response)
		{
			if (!CNDverb(107)) break p000e0467;
			if (!CNDnoun1(151)) break p000e0467;
 		}
 		ACCwriteln(408);
 		ACCpause(50);
 		function anykey00136() 
		{
		}
 		function anykey00135() 
		{
 		ACClet(28,21);
 		ACCpause(1);
 		waitKey(anykey00136);
		}
 		waitKey(anykey00135);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// LOAD RAM
	p000e0468:
	{
 		if (skipdoall('p000e0468')) break p000e0468;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0468;
			if (!CNDnoun1(151)) break p000e0468;
 		}
 		ACCwriteln(409);
 		ACCpause(50);
 		function anykey00138() 
		{
		}
 		function anykey00137() 
		{
 		ACClet(28,21);
 		ACCpause(50);
 		waitKey(anykey00138);
		}
 		waitKey(anykey00137);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// RS _
	p000e0469:
	{
 		if (skipdoall('p000e0469')) break p000e0469;
 		if (in_response)
		{
			if (!CNDverb(152)) break p000e0469;
 		}
 		ACCwriteln(410);
 		ACCpause(50);
 		function anykey00140() 
		{
		}
 		function anykey00139() 
		{
 		ACClet(28,21);
 		ACCpause(1);
 		waitKey(anykey00140);
		}
 		waitKey(anykey00139);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// RL _
	p000e0470:
	{
 		if (skipdoall('p000e0470')) break p000e0470;
 		if (in_response)
		{
			if (!CNDverb(153)) break p000e0470;
 		}
 		ACCwriteln(411);
 		ACCpause(50);
 		function anykey00142() 
		{
		}
 		function anykey00141() 
		{
 		ACClet(28,21);
 		ACCpause(50);
 		waitKey(anykey00142);
		}
 		waitKey(anykey00141);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// FONT 1
	p000e0471:
	{
 		if (skipdoall('p000e0471')) break p000e0471;
 		if (in_response)
		{
			if (!CNDverb(154)) break p000e0471;
			if (!CNDnoun1(155)) break p000e0471;
 		}
 		ACClet(28,7);
 		ACCpause(1);
 		function anykey00143() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00143);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// FONT 2
	p000e0472:
	{
 		if (skipdoall('p000e0472')) break p000e0472;
 		if (in_response)
		{
			if (!CNDverb(154)) break p000e0472;
			if (!CNDnoun1(156)) break p000e0472;
 		}
 		ACClet(28,8);
 		ACCpause(1);
 		function anykey00144() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00144);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DRIN WATE
	p000e0473:
	{
 		if (skipdoall('p000e0473')) break p000e0473;
 		if (in_response)
		{
			if (!CNDverb(157)) break p000e0473;
			if (!CNDnoun1(19)) break p000e0473;
 		}
		if (!CNDat(4)) break p000e0473;
 		ACCwriteln(412);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SHOU _
	p000e0474:
	{
 		if (skipdoall('p000e0474')) break p000e0474;
 		if (in_response)
		{
			if (!CNDverb(160)) break p000e0474;
 		}
		if (!CNDat(18)) break p000e0474;
		if (!CNDpresent(13)) break p000e0474;
 		ACCwriteln(413);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SHOU _
	p000e0475:
	{
 		if (skipdoall('p000e0475')) break p000e0475;
 		if (in_response)
		{
			if (!CNDverb(160)) break p000e0475;
 		}
 		ACCwriteln(414);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LIGH GENE
	p000e0476:
	{
 		if (skipdoall('p000e0476')) break p000e0476;
 		if (in_response)
		{
			if (!CNDverb(83)) break p000e0476;
			if (!CNDnoun1(161)) break p000e0476;
 		}
		if (!CNDat(31)) break p000e0476;
		if (!CNDlt(115,255)) break p000e0476;
 		ACCwriteln(415);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// KILL BEAR
	p000e0477:
	{
 		if (skipdoall('p000e0477')) break p000e0477;
 		if (in_response)
		{
			if (!CNDverb(163)) break p000e0477;
			if (!CNDnoun1(90)) break p000e0477;
 		}
		if (!CNDpresent(37)) break p000e0477;
 		ACCwriteln(416);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// KILL _
	p000e0478:
	{
 		if (skipdoall('p000e0478')) break p000e0478;
 		if (in_response)
		{
			if (!CNDverb(163)) break p000e0478;
 		}
 		ACCwriteln(417);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEAN LADD
	p000e0479:
	{
 		if (skipdoall('p000e0479')) break p000e0479;
 		if (in_response)
		{
			if (!CNDverb(164)) break p000e0479;
			if (!CNDnoun1(64)) break p000e0479;
 		}
		if (!CNDatgt(32)) break p000e0479;
		if (!CNDatlt(40)) break p000e0479;
		if (!CNDcarried(28)) break p000e0479;
 		ACCdrop(28);
		if (!success) break pro000_restart;
 		ACClet(28,11);
 		ACCpause(6);
 		function anykey00146() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00145() 
		{
 		ACCwriteln(418);
 		ACCpause(150);
 		waitKey(anykey00146);
		}
 		waitKey(anykey00145);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PEEL BARK
	p000e0480:
	{
 		if (skipdoall('p000e0480')) break p000e0480;
 		if (in_response)
		{
			if (!CNDverb(167)) break p000e0480;
			if (!CNDnoun1(109)) break p000e0480;
 		}
		if (!CNDat(39)) break p000e0480;
 		ACCwriteln(419);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SMEL PLAN
	p000e0481:
	{
 		if (skipdoall('p000e0481')) break p000e0481;
 		if (in_response)
		{
			if (!CNDverb(168)) break p000e0481;
			if (!CNDnoun1(165)) break p000e0481;
 		}
		if (!CNDat(52)) break p000e0481;
 		ACCwriteln(420);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BRIB GUAR
	p000e0482:
	{
 		if (skipdoall('p000e0482')) break p000e0482;
 		if (in_response)
		{
			if (!CNDverb(170)) break p000e0482;
			if (!CNDnoun1(97)) break p000e0482;
 		}
		if (!CNDpresent(43)) break p000e0482;
		if (!CNDcarried(9)) break p000e0482;
 		ACCwriteln(421);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESP DOG
	p000e0483:
	{
 		if (skipdoall('p000e0483')) break p000e0483;
 		if (in_response)
		{
			if (!CNDverb(171)) break p000e0483;
			if (!CNDnoun1(82)) break p000e0483;
 		}
		if (!CNDat(12)) break p000e0483;
		if (!CNDeq(112,200)) break p000e0483;
		if (!CNDpresent(70)) break p000e0483;
		if (!CNDpresent(40)) break p000e0483;
 		ACCswap(35,70);
 		ACCdestroy(40);
 		ACCwriteln(422);
 		ACCplus(30,2);
 		ACCanykey();
 		function anykey00147() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00147);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DESP DOG
	p000e0484:
	{
 		if (skipdoall('p000e0484')) break p000e0484;
 		if (in_response)
		{
			if (!CNDverb(171)) break p000e0484;
			if (!CNDnoun1(82)) break p000e0484;
 		}
		if (!CNDat(12)) break p000e0484;
		if (!CNDabsent(35)) break p000e0484;
		if (!CNDlt(112,200)) break p000e0484;
 		ACCwriteln(423);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0485:
	{
 		if (skipdoall('p000e0485')) break p000e0485;
 		if (in_response)
		{
			if (!CNDverb(172)) break p000e0485;
 		}
		if (!CNDat(18)) break p000e0485;
		if (!CNDpresent(13)) break p000e0485;
 		ACCwriteln(424);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0486:
	{
 		if (skipdoall('p000e0486')) break p000e0486;
 		if (in_response)
		{
			if (!CNDverb(172)) break p000e0486;
 		}
		if (!CNDat(12)) break p000e0486;
		if (!CNDpresent(40)) break p000e0486;
 		ACCwriteln(425);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0487:
	{
 		if (skipdoall('p000e0487')) break p000e0487;
 		if (in_response)
		{
			if (!CNDverb(172)) break p000e0487;
 		}
		if (!CNDat(26)) break p000e0487;
		if (!CNDcarried(11)) break p000e0487;
		if (!CNDnotcarr(10)) break p000e0487;
 		ACCwriteln(426);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0488:
	{
 		if (skipdoall('p000e0488')) break p000e0488;
 		if (in_response)
		{
			if (!CNDverb(172)) break p000e0488;
 		}
		if (!CNDat(34)) break p000e0488;
		if (!CNDpresent(37)) break p000e0488;
		if (!CNDcarried(18)) break p000e0488;
 		ACCwriteln(427);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0489:
	{
 		if (skipdoall('p000e0489')) break p000e0489;
 		if (in_response)
		{
			if (!CNDverb(172)) break p000e0489;
 		}
		if (!CNDat(35)) break p000e0489;
		if (!CNDabsent(29)) break p000e0489;
 		ACCwriteln(428);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0490:
	{
 		if (skipdoall('p000e0490')) break p000e0490;
 		if (in_response)
		{
			if (!CNDverb(172)) break p000e0490;
 		}
		if (!CNDat(37)) break p000e0490;
		if (!CNDnotcarr(9)) break p000e0490;
		if (!CNDpresent(43)) break p000e0490;
 		ACCwriteln(429);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0491:
	{
 		if (skipdoall('p000e0491')) break p000e0491;
 		if (in_response)
		{
			if (!CNDverb(172)) break p000e0491;
 		}
		if (!CNDat(42)) break p000e0491;
		if (!CNDzero(23)) break p000e0491;
 		ACCwriteln(430);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0492:
	{
 		if (skipdoall('p000e0492')) break p000e0492;
 		if (in_response)
		{
			if (!CNDverb(172)) break p000e0492;
 		}
		if (!CNDat(43)) break p000e0492;
		if (!CNDabsent(66)) break p000e0492;
 		ACCwriteln(431);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0493:
	{
 		if (skipdoall('p000e0493')) break p000e0493;
 		if (in_response)
		{
			if (!CNDverb(172)) break p000e0493;
 		}
		if (!CNDat(24)) break p000e0493;
		if (!CNDpresent(18)) break p000e0493;
		if (!CNDzero(5)) break p000e0493;
 		ACCwriteln(432);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0494:
	{
 		if (skipdoall('p000e0494')) break p000e0494;
 		if (in_response)
		{
			if (!CNDverb(172)) break p000e0494;
 		}
		if (!CNDat(12)) break p000e0494;
		if (!CNDabsent(40)) break p000e0494;
		if (!CNDabsent(35)) break p000e0494;
 		ACCwriteln(433);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0495:
	{
 		if (skipdoall('p000e0495')) break p000e0495;
 		if (in_response)
		{
			if (!CNDverb(172)) break p000e0495;
 		}
		if (!CNDat(39)) break p000e0495;
		if (!CNDabsent(46)) break p000e0495;
 		ACCwriteln(434);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLUE _
	p000e0496:
	{
 		if (skipdoall('p000e0496')) break p000e0496;
 		if (in_response)
		{
			if (!CNDverb(172)) break p000e0496;
 		}
 		ACCwriteln(435);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SCOR _
	p000e0497:
	{
 		if (skipdoall('p000e0497')) break p000e0497;
 		if (in_response)
		{
			if (!CNDverb(173)) break p000e0497;
 		}
 		ACCscore();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PLAY DOG
	p000e0498:
	{
 		if (skipdoall('p000e0498')) break p000e0498;
 		if (in_response)
		{
			if (!CNDverb(174)) break p000e0498;
			if (!CNDnoun1(82)) break p000e0498;
 		}
		if (!CNDpresent(40)) break p000e0498;
 		ACCwriteln(436);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PAT DOG
	p000e0499:
	{
 		if (skipdoall('p000e0499')) break p000e0499;
 		if (in_response)
		{
			if (!CNDverb(175)) break p000e0499;
			if (!CNDnoun1(82)) break p000e0499;
 		}
		if (!CNDpresent(40)) break p000e0499;
 		ACCwriteln(437);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLEA TILL
	p000e0500:
	{
 		if (skipdoall('p000e0500')) break p000e0500;
 		if (in_response)
		{
			if (!CNDverb(179)) break p000e0500;
			if (!CNDnoun1(111)) break p000e0500;
 		}
		if (!CNDat(16)) break p000e0500;
 		ACCwriteln(438);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CLEA CAR
	p000e0501:
	{
 		if (skipdoall('p000e0501')) break p000e0501;
 		if (in_response)
		{
			if (!CNDverb(179)) break p000e0501;
			if (!CNDnoun1(133)) break p000e0501;
 		}
		if (!CNDat(1)) break p000e0501;
 		ACCwriteln(439);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GRAB HONE
	p000e0502:
	{
 		if (skipdoall('p000e0502')) break p000e0502;
 		if (in_response)
		{
			if (!CNDverb(180)) break p000e0502;
			if (!CNDnoun1(46)) break p000e0502;
 		}
		if (!CNDpresent(18)) break p000e0502;
		if (!CNDnotcarr(18)) break p000e0502;
		if (!CNDzero(5)) break p000e0502;
		if (!CNDpresent(59)) break p000e0502;
 		ACCwriteln(440);
 		ACCanykey();
 		function anykey00148() 
		{
 		ACCgoto(21);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00148);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// GRAB HONE
	p000e0503:
	{
 		if (skipdoall('p000e0503')) break p000e0503;
 		if (in_response)
		{
			if (!CNDverb(180)) break p000e0503;
			if (!CNDnoun1(46)) break p000e0503;
 		}
		if (!CNDpresent(18)) break p000e0503;
		if (!CNDnotcarr(18)) break p000e0503;
		if (!CNDzero(5)) break p000e0503;
		if (!CNDpresent(62)) break p000e0503;
 		ACCwriteln(441);
 		ACCanykey();
 		function anykey00149() 
		{
 		ACCgoto(21);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00149);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// FILL BARR
	p000e0504:
	{
 		if (skipdoall('p000e0504')) break p000e0504;
 		if (in_response)
		{
			if (!CNDverb(181)) break p000e0504;
			if (!CNDnoun1(55)) break p000e0504;
 		}
		if (!CNDat(4)) break p000e0504;
		if (!CNDcarried(24)) break p000e0504;
 		ACCwriteln(442);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RIP NOTI
	p000e0505:
	{
 		if (skipdoall('p000e0505')) break p000e0505;
 		if (in_response)
		{
			if (!CNDverb(184)) break p000e0505;
			if (!CNDnoun1(58)) break p000e0505;
 		}
		if (!CNDat(14)) break p000e0505;
		if (!CNDpresent(25)) break p000e0505;
 		ACCwriteln(443);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ _
	p000e0506:
	{
 		if (skipdoall('p000e0506')) break p000e0506;
 		ACChook(444);
		if (done_flag) break pro000_restart;
		{}

	}

	// _ _
	p000e0507:
	{
 		if (skipdoall('p000e0507')) break p000e0507;
 		ACChook(445);
		if (done_flag) break pro000_restart;
		{}

	}


}
}

function pro001()
{
process_restart=true;
pro001_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p001e0000:
	{
 		if (skipdoall('p001e0000')) break p001e0000;
 		ACChook(446);
		if (done_flag) break pro001_restart;
		{}

	}

	// _ _
	p001e0001:
	{
 		if (skipdoall('p001e0001')) break p001e0001;
		if (!CNDat(0)) break p001e0001;
 		ACCbclear(12,5);
		{}

	}

	// _ _
	p001e0002:
	{
 		if (skipdoall('p001e0002')) break p001e0002;
		if (!CNDislight()) break p001e0002;
 		ACClistobj();
 		ACClistnpc(getFlag(38));
		{}

	}


}
}

function pro002()
{
process_restart=true;
pro002_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p002e0000:
	{
 		if (skipdoall('p002e0000')) break p002e0000;
		if (!CNDeq(31,0)) break p002e0000;
		if (!CNDeq(32,0)) break p002e0000;
 		ACCability(6,6);
		{}

	}

	// _ _
	p002e0001:
	{
 		if (skipdoall('p002e0001')) break p002e0001;
 		ACChook(447);
		if (done_flag) break pro002_restart;
		{}

	}

	// _ _
	p002e0002:
	{
 		if (skipdoall('p002e0002')) break p002e0002;
		if (!CNDat(3)) break p002e0002;
		if (!CNDpresent(3)) break p002e0002;
		if (!CNDeq(32,0)) break p002e0002;
		if (!CNDgt(31,20)) break p002e0002;
 		ACCwriteln(448);
 		ACCanykey();
 		function anykey00150() 
		{
 		ACCdestroy(3);
 		ACCplus(31,1);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00150);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0003:
	{
 		if (skipdoall('p002e0003')) break p002e0003;
		if (!CNDat(3)) break p002e0003;
		if (!CNDzero(111)) break p002e0003;
		if (!CNDabsent(3)) break p002e0003;
 		ACCcreate(3);
 		ACClet(111,10);
 		ACCwriteln(449);
 		ACCanykey();
 		function anykey00151() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00151);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0004:
	{
 		if (skipdoall('p002e0004')) break p002e0004;
		if (!CNDatgt(20)) break p002e0004;
		if (!CNDatlt(23)) break p002e0004;
		if (!CNDabsent(13)) break p002e0004;
		if (!CNDchance(20)) break p002e0004;
 		ACCcreate(13);
 		ACCwriteln(450);
 		ACCpause(75);
 		function anykey00152() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00152);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0005:
	{
 		if (skipdoall('p002e0005')) break p002e0005;
		if (!CNDatgt(20)) break p002e0005;
		if (!CNDatlt(23)) break p002e0005;
		if (!CNDpresent(13)) break p002e0005;
		if (!CNDchance(25)) break p002e0005;
 		ACCdestroy(13);
 		ACCwriteln(451);
 		ACCpause(75);
 		function anykey00153() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00153);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0006:
	{
 		if (skipdoall('p002e0006')) break p002e0006;
		if (!CNDcarried(1)) break p002e0006;
		if (!CNDpresent(13)) break p002e0006;
 		ACCwriteln(452);
 		ACCgoto(18);
 		ACCdestroy(13);
 		ACCdestroy(15);
 		ACCdestroy(1);
 		ACCanykey();
 		function anykey00154() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00154);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0007:
	{
 		if (skipdoall('p002e0007')) break p002e0007;
		if (!CNDcarried(0)) break p002e0007;
		if (!CNDpresent(13)) break p002e0007;
 		ACCwriteln(453);
 		ACCgoto(18);
 		ACCdestroy(13);
 		ACCdestroy(15);
 		ACCdestroy(0);
 		ACCanykey();
 		function anykey00155() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00155);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0008:
	{
 		if (skipdoall('p002e0008')) break p002e0008;
		if (!CNDpresent(0)) break p002e0008;
 		ACCminus(29,1);
		{}

	}

	// _ _
	p002e0009:
	{
 		if (skipdoall('p002e0009')) break p002e0009;
		if (!CNDpresent(0)) break p002e0009;
		if (!CNDgt(29,1)) break p002e0009;
		if (!CNDlt(29,6)) break p002e0009;
 		ACCwriteln(454);
		{}

	}

	// _ _
	p002e0010:
	{
 		if (skipdoall('p002e0010')) break p002e0010;
		if (!CNDpresent(0)) break p002e0010;
		if (!CNDeq(29,1)) break p002e0010;
 		ACCswap(0,1);
 		ACClet(10,3);
 		ACCclear(29);
 		ACCdesc();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0011:
	{
 		if (skipdoall('p002e0011')) break p002e0011;
		if (!CNDeq(10,1)) break p002e0011;
 		ACCwriteln(455);
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0012:
	{
 		if (skipdoall('p002e0012')) break p002e0012;
		if (!CNDeq(29,88)) break p002e0012;
 		ACCwriteln(456);
		{}

	}

	// _ _
	p002e0013:
	{
 		if (skipdoall('p002e0013')) break p002e0013;
		if (!CNDat(49)) break p002e0013;
		if (!CNDabsent(72)) break p002e0013;
 		ACCclear(0);
		{}

	}

	// _ _
	p002e0014:
	{
 		if (skipdoall('p002e0014')) break p002e0014;
		if (!CNDat(24)) break p002e0014;
		if (!CNDpresent(18)) break p002e0014;
		if (!CNDnotcarr(18)) break p002e0014;
		if (!CNDpresent(59)) break p002e0014;
		if (!CNDzero(5)) break p002e0014;
		if (!CNDchance(14)) break p002e0014;
 		ACCwriteln(457);
 		ACClet(5,3);
 		ACCswap(59,62);
 		ACCanykey();
 		function anykey00156() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00156);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0015:
	{
 		if (skipdoall('p002e0015')) break p002e0015;
		if (!CNDat(24)) break p002e0015;
		if (!CNDeq(5,1)) break p002e0015;
 		ACCwriteln(458);
 		ACCclear(5);
 		ACCanykey();
 		function anykey00157() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00157);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0016:
	{
 		if (skipdoall('p002e0016')) break p002e0016;
		if (!CNDgt(5,1)) break p002e0016;
 		ACCwriteln(459);
		{}

	}

	// _ _
	p002e0017:
	{
 		if (skipdoall('p002e0017')) break p002e0017;
		if (!CNDat(24)) break p002e0017;
		if (!CNDzero(5)) break p002e0017;
		if (!CNDcarried(18)) break p002e0017;
 		ACCpause(10);
 		function anykey00159() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00158() 
		{
 		ACCwriteln(460);
 		ACCdestroy(18);
 		ACCanykey();
 		waitKey(anykey00159);
		}
 		waitKey(anykey00158);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0018:
	{
 		if (skipdoall('p002e0018')) break p002e0018;
		if (!CNDgt(6,1)) break p002e0018;
		if (!CNDlt(6,5)) break p002e0018;
 		ACCwriteln(461);
		{}

	}

	// _ _
	p002e0019:
	{
 		if (skipdoall('p002e0019')) break p002e0019;
		if (!CNDeq(6,1)) break p002e0019;
		if (!CNDlt(18,150)) break p002e0019;
 		ACCwriteln(462);
 		ACClet(28,3);
 		ACCpause(200);
 		function anykey00160() 
		{
 		ACCscore();
 		ACCend();
		return;
		}
 		waitKey(anykey00160);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0020:
	{
 		if (skipdoall('p002e0020')) break p002e0020;
		if (!CNDeq(6,1)) break p002e0020;
		if (!CNDeq(18,150)) break p002e0020;
 		ACCwriteln(463);
 		ACCgoto(36);
 		ACClet(28,3);
 		ACCpause(250);
 		function anykey00162() 
		{
 		ACCclear(6);
 		ACCdesc();
		return;
		}
 		function anykey00161() 
		{
 		ACCanykey();
 		waitKey(anykey00162);
		}
 		waitKey(anykey00161);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0021:
	{
 		if (skipdoall('p002e0021')) break p002e0021;
		if (!CNDat(41)) break p002e0021;
 		ACCwriteln(464);
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0022:
	{
 		if (skipdoall('p002e0022')) break p002e0022;
		if (!CNDat(41)) break p002e0022;
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0023:
	{
 		if (skipdoall('p002e0023')) break p002e0023;
		if (!CNDat(12)) break p002e0023;
		if (!CNDcarried(0)) break p002e0023;
		if (!CNDcarried(39)) break p002e0023;
		if (!CNDabsent(40)) break p002e0023;
 		ACCdestroy(39);
 		ACCcreate(40);
 		ACCwriteln(465);
 		ACCanykey();
 		function anykey00163() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00163);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0024:
	{
 		if (skipdoall('p002e0024')) break p002e0024;
		if (!CNDpresent(37)) break p002e0024;
		if (!CNDgt(7,1)) break p002e0024;
 		ACCwriteln(466);
		{}

	}

	// _ _
	p002e0025:
	{
 		if (skipdoall('p002e0025')) break p002e0025;
		if (!CNDpresent(37)) break p002e0025;
		if (!CNDeq(7,1)) break p002e0025;
 		ACCwriteln(467);
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0026:
	{
 		if (skipdoall('p002e0026')) break p002e0026;
		if (!CNDzero(32)) break p002e0026;
		if (!CNDeq(31,200)) break p002e0026;
 		ACCcls();
 		ACCwriteln(468);
 		ACCpause(250);
 		function anykey00164() 
		{
 		ACCplus(31,1);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00164);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0027:
	{
 		if (skipdoall('p002e0027')) break p002e0027;
		if (!CNDeq(32,1)) break p002e0027;
		if (!CNDeq(31,144)) break p002e0027;
 		ACCcls();
 		ACCwriteln(469);
 		ACCpause(250);
 		function anykey00165() 
		{
 		ACCplus(31,1);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00165);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0028:
	{
 		if (skipdoall('p002e0028')) break p002e0028;
		if (!CNDeq(32,2)) break p002e0028;
		if (!CNDeq(31,88)) break p002e0028;
 		ACCcls();
 		ACCwriteln(470);
 		ACCpause(250);
 		function anykey00166() 
		{
 		ACCplus(31,1);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00166);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0029:
	{
 		if (skipdoall('p002e0029')) break p002e0029;
		if (!CNDeq(32,3)) break p002e0029;
		if (!CNDeq(31,32)) break p002e0029;
 		ACCcls();
 		ACCwriteln(471);
 		ACCpause(250);
 		function anykey00167() 
		{
 		ACCplus(31,1);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00167);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0030:
	{
 		if (skipdoall('p002e0030')) break p002e0030;
		if (!CNDeq(32,3)) break p002e0030;
		if (!CNDeq(31,232)) break p002e0030;
 		ACCcls();
 		ACCwriteln(472);
 		ACCpause(250);
 		function anykey00168() 
		{
 		ACCplus(31,1);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00168);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0031:
	{
 		if (skipdoall('p002e0031')) break p002e0031;
		if (!CNDeq(32,4)) break p002e0031;
		if (!CNDeq(31,176)) break p002e0031;
 		ACCcls();
 		ACCwriteln(473);
 		ACCpause(250);
 		function anykey00169() 
		{
 		ACCplus(31,1);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00169);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0032:
	{
 		if (skipdoall('p002e0032')) break p002e0032;
		if (!CNDeq(32,5)) break p002e0032;
		if (!CNDeq(31,120)) break p002e0032;
 		ACCcls();
 		ACCwriteln(474);
 		ACCpause(250);
 		function anykey00170() 
		{
 		ACCplus(31,1);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00170);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0033:
	{
 		if (skipdoall('p002e0033')) break p002e0033;
		if (!CNDeq(8,1)) break p002e0033;
 		ACCwriteln(475);
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0034:
	{
 		if (skipdoall('p002e0034')) break p002e0034;
		if (!CNDat(49)) break p002e0034;
		if (!CNDeq(25,255)) break p002e0034;
		if (!CNDnotworn(56)) break p002e0034;
 		ACCwriteln(476);
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0035:
	{
 		if (skipdoall('p002e0035')) break p002e0035;
		if (!CNDat(49)) break p002e0035;
		if (!CNDeq(25,255)) break p002e0035;
		if (!CNDworn(56)) break p002e0035;
 		ACCanykey();
 		function anykey00171() 
		{
 		ACCcls();
 		ACCwriteln(477);
 		ACCplus(30,4);
 		ACCscore();
 		ACCend();
		return;
		}
 		waitKey(anykey00171);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0036:
	{
 		if (skipdoall('p002e0036')) break p002e0036;
		if (!CNDat(5)) break p002e0036;
		if (!CNDpresent(8)) break p002e0036;
		if (!CNDcarried(57)) break p002e0036;
 		ACCwriteln(478);
 		ACCdestroy(8);
 		ACCanykey();
 		function anykey00172() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00172);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0037:
	{
 		if (skipdoall('p002e0037')) break p002e0037;
		if (!CNDat(0)) break p002e0037;
 		ACCgoto(1);
 		ACClet(29,200);
 		ACCanykey();
 		function anykey00173() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00173);
		done_flag=true;
		break pro002_restart;
		{}

	}


}
}

function pro003()
{
process_restart=true;
pro003_restart: while(process_restart)
{
	process_restart=false;
	// ENTER _
	p003e0000:
	{
 		if (skipdoall('p003e0000')) break p003e0000;
 		if (in_response)
		{
			if (!CNDverb(11)) break p003e0000;
 		}
		if (!CNDcarried(24)) break p003e0000;
 		ACCwriteln(479);
 		ACCdone();
		break pro003_restart;
		{}

	}

	// ENTER _
	p003e0001:
	{
 		if (skipdoall('p003e0001')) break p003e0001;
 		if (in_response)
		{
			if (!CNDverb(11)) break p003e0001;
 		}
 		ACCwriteln(480);
 		ACCdone();
		break pro003_restart;
		{}

	}


}
}

function pro004()
{
process_restart=true;
pro004_restart: while(process_restart)
{
	process_restart=false;
	// LIGH _
	p004e0000:
	{
 		if (skipdoall('p004e0000')) break p004e0000;
 		if (in_response)
		{
			if (!CNDverb(83)) break p004e0000;
 		}
		if (!CNDat(48)) break p004e0000;
		if (!CNDlt(24,200)) break p004e0000;
 		ACCwriteln(481);
 		ACCdone();
		break pro004_restart;
		{}

	}


}
}

last_process = 4;
// This file is (C) Carlos Sanchez 2014, released under the MIT license

// This function is called first by the start() function that runs when the game starts for the first time
var h_init = function()
{
}


// This function is called last by the start() function that runs when the game starts for the first time
var h_post =  function()
{
}

// This function is called when the engine tries to write any text
var h_writeText =  function (text)
{
	return text;
}

//This function is called every time the user types any order
var h_playerOrder = function(player_order)
{
	return player_order;
}

// This function is called every time a location is described, just after the location text is written
var h_description_init =  function ()
{
}

// This function is called every time a location is described, just after the process 1 is executed
var h_description_post = function()
{
}


// this function is called when the savegame object has been created, in order to be able to add more custom properties
var h_saveGame = function(savegame_object)
{
	return savegame_object;
}


// this function is called after the restore game function has restored the standard information in savegame, in order to restore any additional data included in a patched (by h_saveGame) savegame.
var h_restoreGame = function(savegame_object)
{
}

// this funcion is called before writing a message about player order beeing impossible to understand
var h_invalidOrder = function(player_order)
{
}

// this function is called when a sequence tag is found giving a chance for any hook library to provide a response
// tagparams receives the params inside the tag as an array  {XXXX|nn|mm|yy} => ['XXXX', 'nn', 'mm', 'yy']
var h_sequencetag = function (tagparams)
{
	return '';
}

// this function is called from certain points in the response or process tables via the HOOK condact. Depending on the string received it can do something or not.
// it's designed to allow direct javascript code to take control in the start database just installing a plugin library (avoiding the wirter need to enter code to activate the library)
var h_code = function(str)
{
	return false;
}


// this function is called from the keydown evente handler used by block and other functions to emulate a pause or waiting for a keypress. It is designed to allow plugin condacts or
// libraries to attend those key presses and react accordingly. In case a hook function decides that the standard keydown functions should not be processed, the hook function should return false.
// Also, any h_keydown replacement should probably do the same.
var h_keydown = function (event)
{
	return true;
}


// this function is called every time a process is called,  either by the internall loop of by the PROCESS condact, just before running it.
var h_preProcess = function(procno)
{

}

// this function is called every time a process is called just after the process exits (no matter which DONE status it has), either by the internall loop of by the PROCESS condact
var h_postProcess= function (procno)
{

}// This file is (C) Carlos Sanchez 2014, and is released under the MIT license
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function ACCdesc()
{
	describe_location_flag = true;
	ACCbreak(); // Cancel doall loop
}


function ACCdone()
{
	done_flag = true;
}

function CNDat(locno)
{
  return (loc_here()==locno);
}

function CNDnotat(locno)
{
	 return (loc_here()!=locno);
}


function CNDatgt(locno)
{
	 return (loc_here()>locno);
}


function CNDatlt(locno)
{
	 return (loc_here()<locno);
}

function CNDpresent(objno)
{
	var loc = getObjectLocation(objno);
	if (loc == loc_here()) return true;
	if (loc == LOCATION_WORN) return true;
	if (loc == LOCATION_CARRIED) return true;
	if ( (!bittest(getFlag(FLAG_PARSER_SETTINGS),7)) && (objectIsContainer(loc) || objectIsSupporter(loc))  &&  (loc<=last_object_number)  && (CNDpresent(loc)) )  // Extended context and object in another object that is present
	{
		if (objectIsSupporter(loc)) return true;  // On supporter
		if ( objectIsContainer(loc) && objectIsAttr(loc, ATTR_OPENABLE) && objectIsAttr(loc, ATTR_OPEN)) return true; // In a openable & open container
		if ( objectIsContainer(loc) && (!objectIsAttr(loc, ATTR_OPENABLE)) ) return true; // In a not openable container
	}
	return false;
}

function CNDabsent(objno)
{
	return !CNDpresent(objno);
}

function CNDworn(objno)
{
	return (getObjectLocation(objno) == LOCATION_WORN);
}

function CNDnotworn(objno)
{
	return !CNDworn(objno);
}

function CNDcarried(objno)
{
	return (getObjectLocation(objno) == LOCATION_CARRIED);	
}

function CNDnotcarr(objno)
{
	return !CNDcarried(objno);
}


function CNDchance(percent)
{
	 var val = Math.floor((Math.random()*101));
	 return (val<=percent);
}

function CNDzero(flagno)
{
	return (getFlag(flagno) == 0);
}

function CNDnotzero(flagno)
{
	 return !CNDzero(flagno)
}


function CNDeq(flagno, value)
{
	return (getFlag(flagno) == value);
}

function CNDnoteq(flagno,value)
{
	return !CNDeq(flagno, value);
}

function CNDgt(flagno, value)
{
	return (getFlag(flagno) > value);
}

function CNDlt(flagno, value)
{
	return (getFlag(flagno) < value);
}


function CNDadject1(wordno)
{
	return (getFlag(FLAG_ADJECT1) == wordno);
}

function CNDadverb(wordno)
{
	return (getFlag(FLAG_ADVERB) == wordno);
}


function CNDtimeout()
{
	 return bittest(getFlag(FLAG_TIMEOUT_SETTINGS),7);
}


function CNDisat(objno, locno)
{
	return (getObjectLocation(objno) == locno);

}


function CNDisnotat(objno, locno)
{
	return !CNDisat(objno, locno);
}



function CNDprep(wordno)
{
	return (getFlag(FLAG_PREP) == wordno);
}




function CNDnoun2(wordno)
{
	return (getFlag(FLAG_NOUN2) == wordno);
}

function CNDadject2(wordno)
{
	return (getFlag(FLAG_ADJECT2) == wordno);
}

function CNDsame(flagno1,flagno2)
{
	return (getFlag(flagno1) == getFlag(flagno2));
}


function CNDnotsame(flagno1,flagno2)
{
	return (getFlag(flagno1) != getFlag(flagno2));
}

function ACCinven()
{
	var count = 0;
	writeSysMessage(SYSMESS_YOUARECARRYING);
	ACCnewline();
	var listnpcs_with_objects = !bittest(getFlag(FLAG_PARSER_SETTINGS),3);
	var i;
	for (i=0;i<num_objects;i++)
	{
		if ((getObjectLocation(i)) == LOCATION_CARRIED)
		{
			
			if ((listnpcs_with_objects) || (!objectIsNPC(i)))
			{
				writeObject(i);
				if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
				ACCnewline();
				count++;
			}
		}
		if (getObjectLocation(i) == LOCATION_WORN)
		{
			if (listnpcs_with_objects || (!objectIsNPC(i)))
			{
				writeObject(i);
				writeSysMessage(SYSMESS_WORN);
				count++;
				ACCnewline();
			}
		}
	}
	if (!count) 
	{
		 writeSysMessage(SYSMESS_CARRYING_NOTHING);
		 ACCnewline();
	}

	if (!listnpcs_with_objects)
	{
		var numNPC = getNPCCountAt(LOCATION_CARRIED);
		if (numNPC)	ACClistnpc(LOCATION_CARRIED);
	}
	done_flag = true;
}

function desc()
{
	describe_location_flag = true;
}


function ACCquit()
{
	inQUIT = true;
	writeSysMessage(SYSMESS_AREYOUSURE);
}


function ACCend()
{
	$('.input').hide();
	inEND = true;
	writeSysMessage(SYSMESS_PLAYAGAIN);
	done_flag = true;
}


function done()
{
	done_flag = true;
}

function ACCok()
{
	writeSysMessage(SYSMESS_OK);
	done_flag = true;
}



function ACCramsave()
{
	ramsave_value = getSaveGameObject();
	var savegame_object = getSaveGameObject();	
	savegame =   JSON.stringify(savegame_object);
	localStorage.setItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME, savegame);
}

function ACCramload()
{
	if (ramsave_value==null) 
	{
		var json_str = localStorage.getItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME);
		if (json_str)
		{
			savegame_object = JSON.parse(json_str.trim());
			restoreSaveGameObject(savegame_object);
			ACCdesc();
			focusInput();
			return;
		}
		else
		{
			writeText (STR_RAMLOAD_ERROR);
			ACCnewline();
			done_flag = true;
			return;
		}
	}
	restoreSaveGameObject(ramsave_value);
	ACCdesc();
}

function ACCsave()
{
	var savegame_object = getSaveGameObject();	
	savegame =   JSON.stringify(savegame_object);
	filename = prompt(getSysMessageText(SYSMESS_SAVEFILE),'');
	if ( filename !== null ) localStorage.setItem('ngpaws_savegame_' + filename.toUpperCase(), savegame);
	ACCok();
}

 
function ACCload() 	
{
	var json_str;
	filename = prompt(getSysMessageText(SYSMESS_LOADFILE),'');
	if ( filename !== null ) json_str = localStorage.getItem('ngpaws_savegame_' + filename.toUpperCase());
	if (json_str)
	{
		savegame_object = JSON.parse(json_str.trim());
		restoreSaveGameObject(savegame_object);
	}
	else
	{
		writeSysMessage(SYSMESS_FILENOTFOUND);
		ACCnewline();
		done_flag = true; return;
	}
	ACCdesc();
	focusInput();
}



function ACCturns()
{
	var turns = getFlag(FLAG_TURNS_HIGH) * 256 +  getFlag(FLAG_TURNS_LOW);
	writeSysMessage(SYSMESS_TURNS_START);
	writeText(turns + '');
	writeSysMessage(SYSMESS_TURNS_CONTINUE);
	if (turns > 1) writeSysMessage(SYSMESS_TURNS_PLURAL);
	writeSysMessage(SYSMESS_TURNS_END);
}

function ACCscore()
{
	var score = getFlag(FLAG_SCORE);
	writeSysMessage(SYSMESS_SCORE_START);
	writeText(score + '');
	writeSysMessage(SYSMESS_SCORE_END);
}


function ACCcls()
{
	clearScreen();
}

function ACCdropall()
{
	// Done in two different loops cause PAW did it like that, just a question of retro compatibility
	var i;
	for (i=0;i<num_objects;i++)	if (getObjectLocation(i) == LOCATION_CARRIED)setObjectLocation(i, getFlag(FLAG_LOCATION));
	for (i=0;i<num_objects;i++)	if (getObjectLocation(i) == LOCATION_WORN)setObjectLocation(i, getFlag(FLAG_LOCATION));
}


function ACCautog()
{
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	if (!bittest(getFlag(FLAG_PARSER_SETTINGS),7))  // Extended context for objects
	for (var i=0; i<num_objects;i++) // Try to find it in present containers/supporters
	{
		if (CNDpresent(i) && (isAccesibleContainer(i) || objectIsAttr(i, ATTR_SUPPORTER)) )  // If there is another object present that is an accesible container or a supporter
		{
			objno =findMatchingObject(i);
			if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
		}
	}
	success = false;
	writeSysMessage(SYSMESS_CANTSEETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautod()
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };  
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautow()
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautor()
{
	var objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOURENOTWEARINGTHAT);
	ACCnewtext();
	ACCdone();
}



function ACCpause(value)
{
 if (value == 0) value = 256;
 pauseRemainingTime = Math.floor(value /50 * 1000);	
 inPause = true;
 showAnykeyLayer();
} 

function ACCgoto(locno)
{
 	setFlag(FLAG_LOCATION,locno);
}

function ACCmessage(mesno)
{
	writeMessage(mesno);
	ACCnewline();
}


function ACCremove(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_CARRIED:  
		case loc_here():
			writeSysMessage(SYSMESS_YOUARENOTWEARINGOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case LOCATION_WORN:
			if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
			{
				writeSysMessage(SYSMESS_CANTREMOVE_TOOMANYOBJECTS);
				ACCnewtext();
				ACCdone();
				return;
			}
			setObjectLocation(objno, LOCATION_CARRIED);
			writeSysMessage(SYSMESS_YOUREMOVEOBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUARENOTWEARINGTHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}


function trytoGet(objno)  // auxiliaty function for ACCget
{
	if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
	{
		writeSysMessage(SYSMESS_CANTCARRYANYMORE);
		ACCnewtext();
		ACCdone();
		doall_flag = false;
		return;
	}
	var weight = 0;
	weight += getObjectWeight(objno);
	weight +=  getLocationObjectsWeight(LOCATION_CARRIED);
	weight +=  getLocationObjectsWeight(LOCATION_WORN);
	if (weight > getFlag(FLAG_MAXWEIGHT_CARRIED))
	{
		writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
		ACCnewtext();
		ACCdone();
		return;
	}
	setObjectLocation(objno, LOCATION_CARRIED);
	writeSysMessage(SYSMESS_YOUTAKEOBJECT);
	success = true;
}


 function ACCget(objno)
 {
 	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_CARRIED:  
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():
			trytoGet(objno);
			break;

		default: 
			if  ((locno<=last_object_number) && (CNDpresent(locno)))    // If it's not here, carried or worn but it present, that means that bit 7 of flag 12 is cleared, thus you can get objects from present containers/supporters
			{
				trytoGet(objno);
			}
			else
			{
				writeSysMessage(SYSMESS_CANTSEETHAT);
				ACCnewtext();
				ACCdone();
				return;
				break;
		    }
	}
 }

function ACCdrop(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():  
			writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;


		case LOCATION_CARRIED:  
			setObjectLocation(objno, loc_here());
			writeSysMessage(SYSMESS_YOUDROPOBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}

function ACCwear(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUAREALREADYWAERINGOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():  
			writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;


		case LOCATION_CARRIED:  
			if (!objectIsWearable(objno))
			{
				writeSysMessage(SYSMESS_YOUCANTWEAROBJECT);
				ACCnewtext();
				ACCdone();
				return;
			}
			setObjectLocation(objno, LOCATION_WORN);
			writeSysMessage(SYSMESS_YOUWEAROBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}



function ACCdestroy(objno)
{
	setObjectLocation(objno, LOCATION_NONCREATED);
}


function ACCcreate(objno)
{
	setObjectLocation(objno, loc_here());
}


function ACCswap(objno1,objno2)
{
	var locno1 = getObjectLocation (objno1);
	var locno2 = getObjectLocation (objno2);
	ACCplace (objno1,locno2);
	ACCplace (objno2,locno1);
	setReferredObject(objno2);
}


function ACCplace(objno, locno)
{
	setObjectLocation(objno, locno);
}

function ACCset(flagno)
{
	setFlag(flagno, SET_VALUE);
}

function ACCclear(flagno)
{
	setFlag(flagno,0);
}

function ACCplus(flagno,value)
{
	var newval = getFlag(flagno) + value;
	setFlag(flagno, newval);
}

function ACCminus(flagno,value)
{
    var newval = getFlag(flagno) - value;
    if (newval < 0) newval = 0;
	setFlag(flagno, newval);
}

function ACClet(flagno,value)
{
	setFlag(flagno,value);
}

function ACCnewline()
{
	writeText(STR_NEWLINE);
}

function ACCprint(flagno)
{
	writeText(getFlag(flagno) +'');
}

function ACCsysmess(sysno)
{
	writeSysMessage(sysno);
}

function ACCcopyof(objno,flagno)
{
	setFlag(flagno, getObjectLocation(objno))
}

function ACCcopyoo(objno1, objno2)
{
	setObjectLocation(objno2,getObjectLocation(objno1));
	setReferredObject(objno2);
}

function ACCcopyfo(flagno,objno)
{
	setObjectLocation(objno, getFlag(flagno));
}

function ACCcopyff(flagno1, flagno2)
{
	setFlag(flagno2, getFlag(flagno1));
}

function ACCadd(flagno1, flagno2)
{
	var newval = getFlag(flagno1) + getFlag(flagno2);
	setFlag(flagno2, newval);
}

function ACCsub(flagno1,flagno2)
{
	var newval = getFlag(flagno2) - getFlag(flagno1);
	if (newval < 0) newval = 0;
	setFlag(flagno2, newval);
}


function CNDparse()
{
	return (!getLogicSentence());
}


function ACClistat(locno, container_objno)   // objno is a container/suppoter number, used to list contents of objects
{
  var listingContainer = false;
  if (arguments.length > 1) listingContainer = true;
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);  
  objscount = objscount - concealed_or_scenery_objcount;
  if (!listingContainer) setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),7)); 
  if (!objscount) return;
  var continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  if (listingContainer) 
  	{
  		writeText(' (');
  		if (objectIsAttr(container_objno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_OVER_YOUCANSEE); else if (objectIsAttr(container_objno, ATTR_CONTAINER)) writeSysMessage(SYSMESS_INSIDE_YOUCANSEE);
  		continouslisting = true;  // listing contents of container always continuous
  	}
  
  if (!listingContainer)
  {
    setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),7)); 
    if (!continouslisting) ACCnewline();
  }
  var progresscount = 0;
  for (var i=0;i<num_objects;i++)
  {
  	if (getObjectLocation(i) == locno)
  		if  ( ((!objectIsNPC(i)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)))  && (!objectIsAttr(i,ATTR_CONCEALED)) && (!objectIsAttr(i,ATTR_SCENERY))   ) // if not an NPC or parser setting say NPCs are considered objects, and object is not concealed nor scenery
  		  { 
  		     writeText(getObjectText(i)); 
  		     if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
  		     progresscount++
  		     if (continouslisting)
  		     {
		  			if (progresscount <= objscount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
  					if (progresscount == objscount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
  					if (!listingContainer) if (progresscount == objscount ) writeSysMessage(SYSMESS_LISTEND);
  			 } else ACCnewline();
  		  }; 
  }
  if (arguments.length > 1) writeText(')');
}


function ACClistnpc(locno)
{
  var npccount =  getNPCCountAt(locno);
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),5)); 
  if (!npccount) return;
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),5)); 
  continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  writeSysMessage(SYSMESS_NPCLISTSTART);
  if (!continouslisting) ACCnewline();
  if (npccount==1)  writeSysMessage(SYSMESS_NPCLISTCONTINUE); else writeSysMessage(SYSMESS_NPCLISTCONTINUE_PLURAL);
  var progresscount = 0;
  var i;
  for (i=0;i<num_objects;i++)
  {
  	if (getObjectLocation(i) == locno)
  		if ( (objectIsNPC(i)) && (!objectIsAttr(i,ATTR_CONCEALED)) ) // only NPCs not concealed
  		  { 
  		     writeText(getObjectText(i)); 
  		     progresscount++
  		     if (continouslisting)
  		     {
		  	 	if (progresscount <= npccount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
  			 	if (progresscount == npccount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
  			 	if (progresscount == npccount ) writeSysMessage(SYSMESS_LISTEND);
  			 } else ACCnewline();
  		  }; 
  }
}


function ACClistobj()
{
  var locno = loc_here();
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);

  objscount = objscount - concealed_or_scenery_objcount;
  if (objscount)
  {
	  writeSysMessage(SYSMESS_YOUCANSEE);
      ACClistat(loc_here());
  }
}

function ACCprocess(procno)
{
	if (procno > last_process) 
	{
		writeText(STR_WRONG_PROCESS);
		ACCnewtext();
		ACCdone();
	}
	callProcess(procno);
    if (describe_location_flag) done_flag = true;
}

function ACCmes(mesno)
{
	writeMessage(mesno);
}

function ACCmode(mode)
{
	setFlag(FLAG_MODE, mode);
}

function ACCtime(length, settings)
{
	setFlag(FLAG_TIMEOUT_LENGTH, length);
	setFlag(FLAG_TIMEOUT_SETTINGS, settings);
}

function ACCdoall(locno)
{
	doall_flag = true;
	if (locno == LOCATION_HERE) locno = loc_here();
	// Each object will be considered for doall loop if is at locno and it's not the object specified by the NOUN2/ADJECT2 pair and it's not a NPC (or setting to consider NPCs as objects is set)
	setFlag(FLAG_DOALL_LOC, locno);
	var doall_obj;
	doall_loop:
	for (doall_obj=0;(doall_obj<num_objects) && (doall_flag);doall_obj++)  
	{
		if (getObjectLocation(doall_obj) == locno)
			if ((!objectIsNPC(doall_obj)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3))) 
 			 if (!objectIsAttr(doall_obj, ATTR_CONCEALED)) 
 			  if (!objectIsAttr(doall_obj, ATTR_SCENERY)) 
				if (!( (objectsNoun[doall_obj]==getFlag(FLAG_NOUN2))  &&    ((objectsAdjective[doall_obj]==getFlag(FLAG_ADJECT2)) || (objectsAdjective[doall_obj]==EMPTY_WORD)) ) ) // implements "TAKE ALL EXCEPT BIG SWORD"
				{
					setFlag(FLAG_NOUN1, objectsNoun[doall_obj]);
					setFlag(FLAG_ADJECT1, objectsAdjective[doall_obj]);
					setReferredObject(doall_obj);
					callProcess(process_in_doall);
					if (describe_location_flag) 
						{
							doall_flag = false;
							entry_for_doall = '';
							break doall_loop;
						}
				}
	}
	doall_flag = false;
	entry_for_doall = '';
	if (describe_location_flag) descriptionLoop();
}

function ACCprompt(value)  // deprecated
{
	setFlag(FLAG_PROMPT, value);
	setInputPlaceHolder();
}


function ACCweigh(objno, flagno)
{
	var weight = getObjectWeight(objno);
	setFlag(flagno, weight);
}

function ACCputin(objno, locno)
{
	success = false;
	setReferredObject(objno);
	if (getObjectLocation(objno) == LOCATION_WORN)
	{
		writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == loc_here())
	{
		writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == LOCATION_CARRIED)
	{
		setObjectLocation(objno, locno);
		if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUPUTOBJECTON); else writeSysMessage(SYSMESS_YOUPUTOBJECTIN);
		writeText(getObjectFixArticles(locno));
		writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
		success = true;
		return;
	}

	writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
	ACCnewtext();
	ACCdone();
}


function ACCtakeout(objno, locno)
{
	success = false;
	setReferredObject(objno);
	if ((getObjectLocation(objno) == LOCATION_WORN) || (getObjectLocation(objno) == LOCATION_CARRIED))
	{
		writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == loc_here())
	{
		if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
		writeText(getObjectFixArticles(locno));
		writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectWeight(objno) + getLocationObjectsWeight(LOCATION_WORN) + getLocationObjectsWeight(LOCATION_CARRIED) >  getFlag(FLAG_MAXWEIGHT_CARRIED))
	{
		writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
	{		
		writeSysMessage(SYSMESS_CANTCARRYANYMORE);
		ACCnewtext();
		ACCdone();
		return;
	}

	setObjectLocation(objno, LOCATION_CARRIED);
	writeSysMessage(SYSMESS_YOUTAKEOBJECT);
	success = true;


}
function ACCnewtext()
{
	player_order_buffer = '';
}

function ACCability(maxObjectsCarried, maxWeightCarried)
{
	setFlag(FLAG_MAXOBJECTS_CARRIED, maxObjectsCarried);
	setFlag(FLAG_MAXWEIGHT_CARRIED, maxWeightCarried);
}

function ACCweight(flagno)
{
	var weight_carried = getLocationObjectsWeight(LOCATION_CARRIED);
	var weight_worn = getLocationObjectsWeight(LOCATION_WORN);
	var total_weight = weight_worn + weight_carried;
	setFlag(flagno, total_weight);
}


function ACCrandom(flagno)
{
	 setFlag(flagno, 1 + Math.floor((Math.random()*100)));
}

function ACCwhato()
{
	var whatofound = getReferredObject();
	if (whatofound != EMPTY_OBJECT) setReferredObject(whatofound);
}

function ACCputo(locno)
{
	setObjectLocation(getFlag(FLAG_REFERRED_OBJECT), locno);
}

function ACCnotdone()
{
	done_flag = false;
}

function ACCautop(locno)
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno = findMatchingObject(null); // anywhere
	if (objno != EMPTY_OBJECT) 
		{ 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return; 
		};

	success = false;
	writeSysMessage(SYSMESS_CANTDOTHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautot(locno)
{

	var objno =findMatchingObject(locno);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };

	objno = findMatchingObject(null); // anywhere
	if (objno != EMPTY_OBJECT) 
		{ 
			if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
			writeText(getObjectFixArticles(locno));
			writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION)
			ACCnewtext();
			ACCdone();
			return; 
		};

	success = false;
	writeSysMessage(SYSMESS_CANTDOTHAT);
	ACCnewtext();
	ACCdone();
	
}


function CNDmove(flagno)
{
	var locno = getFlag(flagno);
	var dirno = getFlag(FLAG_VERB);
	var destination = getConnection( locno,  dirno);
	if (destination != -1) 
		{
			 setFlag(flagno, destination);
			 return true;
		}
	return false;
}


function ACCextern(writeno)
{
	eval(writemessages[writeno]);
}


function ACCpicture(picno)
{
	drawPicture(picno);
}



function ACCgraphic(option)
{
	graphicsON = (option==1);  
	if (!graphicsON) hideGraphicsWindow();	
}

function ACCbeep(sfxno, channelno, times)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxplay(sfxno, channelno, times, 'play');
}

function ACCsound(value)
{
	soundsON = (value==1);  
	if (!soundsON) sfxstopall();
}

function CNDozero(objno, attrno)
{
	if (attrno > 63) return false;
	return !objectIsAttr(objno, attrno);

}

function CNDonotzero(objno, attrno)
{
	return objectIsAttr(objno, attrno);
}

function ACCoset(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		attrs = getObjectLowAttributes(objno);
		var attrs = bitset(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitset(attrs, attrno);
	setObjectHighAttributes(objno, attrs);

}

function ACCoclear(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		var attrs = getObjectLowAttributes(objno);
		attrs = bitclear(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitclear(attrs, attrno);
	setObjectHighAttributes(objno, attrs);

}


function CNDislight()
{
	if (!isDarkHere()) return true;
	return lightObjectsPresent();
}



function CNDisnotlight()
{
	return ! CNDislight();
}

function ACCversion()
{
	writeText(filterText(STR_RUNTIME_VERSION));
}


function ACCwrite(writeno)
{
	writeWriteMessage(writeno);
}

function ACCwriteln(writeno)
{
	writeWriteMessage(writeno);
	ACCnewline();
}

function ACCrestart()
{
  process_restart = true;
}


function ACCtranscript()
{
	$('#transcript_area').html(transcript);
	$('.transcript_layer').show();
	inTranscript = true;
}

function ACCanykey()
{
	writeSysMessage(SYSMESS_PRESSANYKEY);
	inAnykey = true;
}

function ACCgetkey(flagno)
{
	getkey_return_flag = flagno;
	inGetkey = true;
}


//////////////////
//   LEGACY     //
//////////////////

// From PAW PC
function ACCbell()
{
 	// Empty, PAW PC legacy, just does nothing 
}


// From PAW Spectrum
function ACCreset()
{
	// Legacy condact, does nothing now
}


function ACCpaper(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCink(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCborder(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCcharset(value)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCline(lineno)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCinput()
{
	// Legacy condact, does nothing now
}

function ACCsaveat()
{
	// Legacy condact, does nothing now
}

function ACCbackat()
{
	// Legacy condact, does nothing now
}

function ACCprintat()
{
	// Legacy condact, does nothing now
}

function ACCprotect()
{
	// Legacy condact, does nothing now
}

// From Superglus


function ACCdebug()
{
	// Legacy condact, does nothing now		
}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS FOR COMPILER //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function CNDverb(wordno)
{
	return (getFlag(FLAG_VERB) == wordno);
}


function CNDnoun1(wordno)
{
	return (getFlag(FLAG_NOUN1) == wordno);
}

//   PLUGINS    ;

//CND RNDWRITELN A 14 14 14 0

function ACCrndwriteln(writeno1,writeno2,writeno3)
{
	ACCrndwrite(writeno1,writeno2,writeno3);
	ACCnewline();
}
//CND SYNONYM A 15 13 0 0

function ACCsynonym(wordno1, wordno2)
{
   if (wordno1!=EMPTY_WORD) setFlag(FLAG_VERB, wordno1);
   if (wordno2!=EMPTY_WORD)	setFlag(FLAG_NOUN1, wordno2);
}
//CND RNDWRITE A 14 14 14 0

function ACCrndwrite(writeno1,writeno2,writeno3)
{
	var val = Math.floor((Math.random()*3));
	switch (val)
	{
		case 0 : writeWriteMessage(writeno1);break;
		case 1 : writeWriteMessage(writeno2);break;
		case 2 : writeWriteMessage(writeno3);break;
	}
}
//CND SETWEIGHT A 4 2 0 0

function ACCsetweight(objno, value)
{
   objectsWeight[objno] = value;
}

//CND BSET A 1 2 0 0

function ACCbset(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitset(getFlag(flagno),bitno));
}
//CND ISNOTMOV C 0 0 0 0

function CNDisnotmov()
{
	return !CNDismov();	
}

//CND VOLUME A 2 2 0 0

function ACCvolume(channelno, value)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;
	sfxvolume(channelno, value);
}

//CND ISVIDEO C 0 0 0 0

function CNDisvideo()
{
	if (typeof videoElement == 'undefined') return false;
	if (!videoLoopCount) return false;
	if (videoElement.paused) return false;
	return true;
}

//CND GE C 1 2 0 0

function CNDge(flagno, valor)
{
	return (getFlag(flagno)>=valor);
}
//CND ISNOTDOALL C 0 0 0 0

function CNDisnotdoall()
{
	return !CNDisdoall();
}

//CND RESUMEVIDEO A 0 0 0 0


function ACCresumevideo()
{
	if (typeof videoElement != 'undefined') 
		if (videoElement.paused)
		  videoElement.play();
}

//CND PAUSEVIDEO A 0 0 0 0


function ACCpausevideo()
{
	if (typeof videoElement != 'undefined') 
		if (!videoElement.ended) 
		if (!videoElement.paused)
		   videoElement.pause();
}

//CND BZERO C 1 2 0 0

function CNDbzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (!bittest(getFlag(flagno), bitno));
}
//CND BNEG A 1 2 0 0

function ACCbneg(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitneg(getFlag(flagno),bitno));
}
//CND ISDOALL C 0 0 0 0

function CNDisdoall()
{
	return doall_flag;	
}

//CND ONEG A 4 2 0 0

function ACConeg(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		var attrs = getObjectLowAttributes(objno);
		attrs = bitneg(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitneg(attrs, attrno);
	setObjectHighAttributes(objno, attrs);
}

//CND ISDONE C 0 0 0 0

function CNDisdone()
{
	return done_flag;	
}

//CND BLOCK A 14 2 2 0

function ACCblock(writeno, picno, procno)
{
   inBlock = true;
   disableInterrupt();
   $('.block_layer').hide();
   var text = getWriteMessageText(writeno);
   $('.block_text').html(text);
   
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var imgsrc = '<img class="block_picture" src="' + filename + '" />';
		$('.block_graphics').html(imgsrc);
	}
    if (procno == 0 ) unblock_process ==null; else unblock_process = procno;
    $('.block_layer').show();

}
//CND HELP A 0 0 0 0

function ACChelp()
{
	if (getLang()=='EN') EnglishHelp(); else SpanishHelp();
}	

function EnglishHelp()
{
	writeText('HOW DO I SEND COMMANDS TO THE PC?');
	writeText(STR_NEWLINE);
	writeText('Use simple orders: OPEN DOOR, TAKE KEY, GO UP, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I MOVE IN THE MAP?');
	writeText(STR_NEWLINE);
	writeText('Usually you will have to use compass directions as north (shortcut: "N"), south (S), east (E), west (W) or other directions (up, down, enter, leave, etc.). Some games allow complex order like "go to well". Usually you would be able to know avaliable exits by location description, some games also provide the "EXITS" command.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK MY INVENTORY?');
	writeText(STR_NEWLINE);
	writeText('type INVENTORY (shortcut "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I USE THE OBJECTS?');
	writeText(STR_NEWLINE);
	writeText('Use the proper verb, that is, instead of USE KEY type OPEN.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK SOMETHING CLOSELY?');
	writeText(STR_NEWLINE);
	writeText('Use "examine" verb: EXAMINE DISH. (shortcut: EX)');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I SEE AGAIN THE CURRENT LOCATION DSCRIPTION?');
	writeText(STR_NEWLINE);
	writeText('Type LOOK (shortcut "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I TALK TO OTHER CHARACTERS?');
	writeText(STR_NEWLINE);
	writeText('Most common methods are [CHARACTER, SENTENCE] or [SAY CHARACTER "SENTENCE"]. For instance: [JOHN, HELLO] o [SAY JOHN "HELLO"]. Some games also allow just [TALK TO JOHN]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING IN A CONTAINER, HOW CAN I TAKE SOMETHING OUT?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY IN BOX. TAKE KEY OUT OF BOX. INSERT KEY IN BOX. EXTRACT KEY FROM BOX.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING ON SOMETHING ELSE?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY ON TABLE. TAKE KEY FROM TABLE');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I SAVE/RESTORE MY GAME?');
	writeText(STR_NEWLINE);
	writeText('Use SAVE/LOAD commands.');
	writeText(STR_NEWLINE + STR_NEWLINE);

}

function SpanishHelp()
{
	writeText('¿CÓMO DOY ORDENES AL PERSONAJE?');
	writeText(STR_NEWLINE);
	writeText('Utiliza órdenes en imperativo o infinitivo: ABRE PUERTA, COGER LLAVE, SUBIR, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO ME MUEVO POR EL JUEGO?');
	writeText(STR_NEWLINE);
	writeText('Por regla general, mediante los puntos cardinales como norte (abreviado "N"), sur (S), este (E), oeste (O) o direcciones espaciales (arriba, abajo, bajar, subir, entrar, salir, etc.). Algunas aventuras permiten también cosas como "ir a pozo". Normalmente podrás saber en qué dirección puedes ir por la descripción del sitio, aunque algunos juegos facilitan el comando "SALIDAS" que te dirá exactamente cuáles hay.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO SABER QUE OBJETOS LLEVO?');
	writeText(STR_NEWLINE);
	writeText('Teclea INVENTARIO (abreviado "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO USO LOS OBJETOS?');
	writeText(STR_NEWLINE);
	writeText('Utiliza el verbo correcto, en lugar de USAR ESCOBA escribe BARRER.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO MIRAR DE CERCA UN OBJETO U OBSERVARLO MÁS DETALLADAMENTE?');
	writeText(STR_NEWLINE);
	writeText('Con el verbo examinar: EXAMINAR PLATO. Generalmente se puede usar la abreviatura "EX": EX PLATO.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO VER DE NUEVO LA DESCRIPCIÓN DEL SITIO DONDE ESTOY?');
	writeText(STR_NEWLINE);
	writeText('Escribe MIRAR (abreviado "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO HABLO CON LOS PERSONAJES?');
	writeText(STR_NEWLINE);
	writeText('Los modos más comunes son [PERSONAJE, FRASE] o [DECIR A PERSONAJE "FRASE"]. Por ejemplo: [LUIS, HOLA] o [DECIR A LUIS "HOLA"]. En algunas aventuras también se puede utilizar el formato [HABLAR A LUIS]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO METO ALGO EN UN CONTENEDOR? ¿CÓMO LO SACO?');
	writeText(STR_NEWLINE);
	writeText('METER LLAVE EN CAJA. SACAR LLAVE DE CAJA');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PONGO ALGO SOBRE ALGO? ¿CÓMO LO QUITO?');
	writeText(STR_NEWLINE);
	writeText('PONER LLAVE EN MESA. COGER LLAVE DE MESA');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO GRABO Y CARGO LA PARTIDA?');
	writeText(STR_NEWLINE);
	writeText('Usa las órdenes SAVE y LOAD, o GRABAR y CARGAR.');
	writeText(STR_NEWLINE + STR_NEWLINE);
}

//CND BNOTZERO C 1 2 0 0

function CNDbnotzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (bittest(getFlag(flagno), bitno));
}

//CND PLAYVIDEO A 14 2 2 0

var videoLoopCount;
var videoEscapable;
var videoElement;

function ACCplayvideo(strno, loopCount, settings)
{
	videoEscapable = settings & 1; // if bit 0 of settings is 1, video can be interrupted with ESC key
	if (loopCount == 0) loopCount = -1;
	videoLoopCount = loopCount;

	str = '<video id="videoframe" height="100%">';
	str = str + '<source src="dat/' + writemessages[strno] + '.mp4" type="video/mp4" codecs="avc1.4D401E, mp4a.40.2">';
	str = str + '<source src="dat/' + writemessages[strno] + '.webm" type="video/webm" codecs="vp8.0, vorbis">';
	str = str + '<source src="dat/' + writemessages[strno] + '.ogg" type="video/ogg" codecs="theora, vorbis">';
	str = str + '</video>';
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#videoframe').css('height','100%');
	$('#videoframe').css('display','block');
	$('#videoframe').css('margin-left','auto');
	$('#videoframe').css('margin-right','auto');
	$('#graphics').show();
	videoElement = document.getElementById('videoframe');
	videoElement.onended = function() 
	{
    	if (videoLoopCount == -1) videoElement.play();
    	else
    	{
    		videoLoopCount--;
    		if (videoLoopCount) videoElement.play();
    	}
	};
	videoElement.play();

}

// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_video_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#videoframe").length > 0) $("#videoframe").remove();	
	old_video_h_description_init();
}

// Hook into keypress to cancel video playing if ESC is pressed and video is skippable

var old_video_h_keydown =  h_keydown;
h_keydown = function (event)
{
 	if ((event.keyCode == 27) && (typeof videoElement != 'undefined') && (!videoElement.ended) && (videoEscapable)) 
 	{
 		videoElement.pause(); 
 		return false;  // we've finished attending ESC press
 	}
 	else return old_video_h_keydown(event);
}




//CND TEXTPIC A 2 2 0 0

function ACCtextpic(picno, align)
{
	var style = '';
	var post = '';
	var pre = '';
	switch(align)
	{
		case 0: post='<br style="clear:left">';break;
		case 1: style = 'float:left'; break;
		case 2: style = 'float:right'; break;
		case 3: pre='<center>';post='</center><br style="clear:left">';break;
	}
	filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var texto = pre + "<img alt='' class='textpic' style='"+style+"' src='"+filename+"' />" + post;
		writeText(texto);
		$(".text").scrollTop($(".text")[0].scrollHeight);
	}
}
//CND OBJFOUND C 2 9 0 0

function CNDobjfound(attrno, locno)
{

	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return true; }
	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return false;
}

//CND PICTUREAT A 2 2 2 0

/*
In order to determine the actual size of both background image and pictureat image they should be loaded, thus two chained "onload" are needed. That is, 
background image is loaded to determine its size, then pictureat image is loaded to determine its size. Size of currently displayed background image cannot
be used as it may have been already stretched.
*/

function ACCpictureat(x,y,picno)
{
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (!filename) return;

	// Check location has a picture, otherwise exit
	var currentBackgroundScreenImage = $('.location_picture');
	if (!currentBackgroundScreenImage) return;

	// Create a new image with the contents of current background image, to be able to calculate original height of image
	var virtualBackgroundImage = new Image();
	// Pass required data as image properties in order to be avaliable at "onload" event
	virtualBackgroundImage.bg_data=[];
	virtualBackgroundImage.bg_data.filename = filename; 
	virtualBackgroundImage.bg_data.x = x;
	virtualBackgroundImage.bg_data.y = y;
	virtualBackgroundImage.bg_data.picno = picno;
	virtualBackgroundImage.bg_data.currentBackgroundScreenImage = currentBackgroundScreenImage;


	// Event triggered when virtual background image is loaded
	virtualBackgroundImage.onload = function()
		{
			var originalBackgroundImageHeight = this.height;
			var scale = this.bg_data.currentBackgroundScreenImage.height() / originalBackgroundImageHeight;

			// Create a new image with the contents of picture to show with PICTUREAT, to be able to calculate height of image
			var virtualPictureAtImage = new Image();
			// Also pass data from background image as property so they are avaliable in the onload event
			virtualPictureAtImage.pa_data = [];
			virtualPictureAtImage.pa_data.x = this.bg_data.x;
			virtualPictureAtImage.pa_data.y = this.bg_data.y;
			virtualPictureAtImage.pa_data.picno = this.bg_data.picno;
			virtualPictureAtImage.pa_data.filename = this.bg_data.filename;
			virtualPictureAtImage.pa_data.scale = scale;
			virtualPictureAtImage.pa_data.currentBackgroundImageWidth = this.bg_data.currentBackgroundScreenImage.width();
			
			// Event triggered when virtual PCITUREAT image is loaded
			virtualPictureAtImage.onload = function ()
			{
		    		var imageHeight = this.height; 
					var x = Math.floor(this.pa_data.x * this.pa_data.scale);
					var y = Math.floor(this.pa_data.y * this.pa_data.scale);
					var newimageHeight = Math.floor(imageHeight * this.pa_data.scale);
					var actualBackgroundImageX = Math.floor((parseInt($('.graphics').width()) - this.pa_data.currentBackgroundImageWidth)/2);;
					var id = 'pictureat_' + this.pa_data.picno;

					// Add new image, notice we are not using the virtual image, but creating a new one
					$('.graphics').append('<img  alt="" id="'+id+'" style="display:none" />');				
					$('#' + id).css('position','absolute');
					$('#' + id).css('left', actualBackgroundImageX + x  + 'px');
					$('#' + id).css('top',y + 'px');
					$('#' + id).css('z-index','100');
					$('#' + id).attr('src', this.pa_data.filename);
					$('#' + id).css('height',newimageHeight + 'px');
					$('#' + id).show();
			}

			// Assign the virtual pictureat image the destinationsrc to trigger the "onload" event
			virtualPictureAtImage.src = this.bg_data.filename;
			};

	// Assign the virtual background image same src as current background to trigger the "onload" event
	virtualBackgroundImage.src = currentBackgroundScreenImage.attr("src");

}

//CND ISNOTRESP C 0 0 0 0

function CNDisnotresp()
{
	return !in_response;	
}

//CND ISSOUND C 1 0 0 0

function CNDissound(channelno)
{
	if ((channelno <1 ) || (channelno > MAX_CHANNELS)) return false;
    return channelActive(channelno);
}
//CND ZONE C 8 8 0 0

function CNDzone(locno1, locno2)
{

	if (loc_here()<locno1) return false;
	if (loc_here()>locno2) return false;
	return true;
}
//CND FADEOUT A 2 2 0 0

function ACCfadeout(channelno, value)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxfadeout(channelno, value);
}
//CND CLEAREXIT A 2 0 0 0

function ACCclearexit(wordno)
{
	if ((wordno >= NUM_CONNECTION_VERBS) || (wordno< 0 )) return;
	setConnection(loc_here(),wordno, -1);
}
//CND WHATOX2 A 1 0 0 0

function ACCwhatox2(flagno)
{	
	var auxNoun = getFlag(FLAG_NOUN1);
	var auxAdj = getFlag(FLAG_ADJECT1);
	setFlag(FLAG_NOUN1, getFlag(FLAG_NOUN2));
	setFlag(FLAG_ADJECT1, getFlag(FLAG_ADJECT2));
	var whatox2found = getReferredObject();
	setFlag(flagno,whatox2found);
	setFlag(FLAG_NOUN1, auxNoun);
	setFlag(FLAG_ADJECT1, auxAdj);
}
//CND COMMAND A 2 0 0 0

function ACCcommand(value)
{
	if (value) {$('.input').show();$('.input').focus();} else $('.input').hide();
}
//CND TITLE A 14 0 0 0

function ACCtitle(writeno)
{
	document.title = writemessages[writeno];
}
//CND LE C 1 2 0 0

function CNDle(flagno, valor)
{
	return (getFlag(flagno) <= valor);
}
//CND WARNINGS A 2 0 0 0

function ACCwarnings(value)
{
	if (value) showWarnings = true; else showWarnings = false;
}
//CND BCLEAR A 1 2 0 0

function ACCbclear(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitclear(getFlag(flagno), bitno));
}
//CND DIV A 1 2 0 0

function ACCdiv(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) / valor));
}
//CND OBJAT A 9 1 0 0

function ACCobjat(locno, flagno)
{
	setFlag(flagno, getObjectCountAt(locno));
}
//CND SILENCE A 2 0 0 0

function ACCsilence(channelno)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;
	sfxstop(channelno);
}
//CND SETEXIT A 2 2 0 0

function ACCsetexit(value, locno)
{
	if (value < NUM_CONNECTION_VERBS) setConnection(loc_here(), value, locno);
}
//CND EXITS A 8 5 0 0

function ACCexits(locno,mesno)
{
  writeText(getExitsText(locno,mesno));
}

//CND HOOK A 14 0 0 5

function ACChook(writeno)
{
	h_code(writemessages[writeno]);
}
//CND RANDOMX A 1 2 0 0

function ACCrandomx(flagno, value)
{
	 setFlag(flagno, 1 + Math.floor((Math.random()*value)));
}
//CND ISNOTDONE C 0 0 0 0

function CNDisnotdone()
{
	return !CNDisdone();
}

//CND ATGE C 8 0 0 0

function CNDatge(locno)
{
	return (getFlag(FLAG_LOCATION) >= locno);
}

//CND ATLE C 8 0 0 0

function CNDatle(locno)
{
	return (getFlag(FLAG_LOCATION) <= locno);
}

//CND RESP A 0 0 0 0

function ACCresp()
{
	in_response = true;
}	

//CND ISNOTSOUND C 1 0 0 0

function CNDisnotsound(channelno)
{
  if ((channelno <1) || (channelno >MAX_CHANNELS)) return false;
  return !(CNDissound(channelno));
}
//CND ASK W 14 14 1 0

// Global vars for ASK


var inAsk = false;
var ask_responses = null;
var ask_flagno = null;



function ACCask(writeno, writenoOptions, flagno)
{
	inAsk = true;
	writeWriteMessage(writeno);
	ask_responses = getWriteMessageText(writenoOptions);
	ask_flagno = flagno;
}



// hook replacement
var old_ask_h_keydown  = h_keydown;
h_keydown  = function (event)
{
	if (inAsk)
	{
		var keyCodeAsChar = String.fromCharCode(event.keyCode).toLowerCase();
		if (ask_responses.indexOf(keyCodeAsChar)!= -1)
		{
			setFlag(ask_flagno, ask_responses.indexOf(keyCodeAsChar));
			inAsk = false;
			event.preventDefault();
            $('.input').show();
		    $('.input').focus();
		    hideBlock();
			waitKeyCallback();
		};
		return false; // if we are in ASK condact, no keypress should be considered other than ASK response
	} else return old_ask_h_keydown(event);
}

//CND MUL A 1 2 0 0

function ACCmul(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) * valor));
}
//CND NPCAT A 9 1 0 0

function ACCnpcat(locno, flagno)
{
	setFlag(flagno,getNPCCountAt(locno));
}

//CND SOFTBLOCK A 2 0 0 0

function ACCsoftblock(procno)
{
   inBlock = true;
   disableInterrupt();

   $('.block_layer').css('display','none');
   $('.block_text').html('');
   $('.block_graphics').html('');
   $('.block_layer').css('background','transparent');
   if (procno == 0 ) unblock_process ==null; else unblock_process = procno;
   $('.block_layer').css('display','block');
}
//CND LISTCONTENTS A 9 0 0 0

function ACClistcontents(locno)
{
   ACClistat(locno, locno)
}
//CND SPACE A 0 0 0 0

function ACCspace()
{
	writeText(' ');
}
//CND ISNOTMUSIC C 0 0 0 0

function CNDisnotmusic()
{
  return !CNDismusic();
}

//CND BREAK A 0 0 0 0

function ACCbreak()
{
	doall_flag = false; 
	entry_for_doall = '';
}
//CND NORESP A 0 0 0 0

function ACCnoresp()
{
	in_response = false;
}	

//CND ISMUSIC C 0 0 0 0

function CNDismusic()
{
	return (CNDissound(0));	
}

//CND ISRESP C 0 0 0 0

function CNDisresp()
{
	return in_response;	
}

//CND MOD A 1 2 0 0

function ACCmod(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) % valor));
}
//CND FADEIN A 2 2 2 0

function ACCfadein(sfxno, channelno, times)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxplay(sfxno, channelno, times, 'fadein');
}
//CND WHATOX A 1 0 0 0

function ACCwhatox(flagno)
{
	var whatoxfound = getReferredObject();
	setFlag(flagno,whatoxfound);
}

//CND GETEXIT A 2 2 0 0

function ACCgetexit(value,flagno)
{
	if (value >= NUM_CONNECTION_VERBS) 
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	var locno = getConnection(loc_here(),value);
	if (locno == -1)
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	setFlag(flagno,locno);
}
//CND LOG A 14 0 0 0

function ACClog(writeno)
{
  console_log(writemessages[writeno]);
}
//CND VOLUMEVIDEO A 2 0 0 0


function ACCvolumevideo(value)
{
	if (typeof videoElement != 'undefined') 
		videoElement.volume = value  / 65535;
}

//CND OBJNOTFOUND C 2 9 0 0

function CNDobjnotfound(attrno, locno)
{
	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return false; }

	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return true;
}
//CND ISMOV C 0 0 0 0

function CNDismov()
{
	if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)==EMPTY_WORD)) return true;

	if ((getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_VERB)==EMPTY_WORD)) return true;

    if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS)) return true;
    
    return false;
}

//CND YOUTUBE A 14 0 0 0

function ACCyoutube(strno)
{

	var str = '<iframe id="youtube" width="560" height="315" src="http://www.youtube.com/embed/' + writemessages[strno] + '?autoplay=1&controls=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>'
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#youtube').css('height','100%');
	$('#youtube').css('display','block');
	$('#youtube').css('margin-left','auto');
	$('#youtube').css('margin-right','auto');
	$('.graphics').show();
}


// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_youtube_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#youtube").length > 0) $("#youtube").remove();	
	old_youtube_h_description_init();
}
//CND LISTSAVEDGAMES A 0 0 0 0

function ACClistsavedgames()
{
  var numberofgames = 0;
  for(var savedgames in localStorage)
  {
    gamePrefix = savedgames.substring(0,16); // takes out ngpaws_savegame_
    if (gamePrefix == "ngpaws_savegame_")
    {
      gameName = savedgames.substring(16);
      writelnText(gameName);
      numberofgames++;
    }
  }
  if (numberofgames == 0) 
  {
     if (getLang()=='EN') writelnText("No saved games found."); else writelnText("No hay ninguna partida guardada.");
  }
}


// This file is (C) Carlos Sanchez 2014, released under the MIT license


// IMPORTANT: Please notice this file must be encoded with the same encoding the index.html file is, so the "normalize" function works properly.
//            As currently the ngpwas compiler generates utf-8, and the index.html is using utf-8 also, this file must be using that encoding.


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                         Auxiliary functions                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General functions
String.prototype.rights= function(n){
    if (n <= 0)
       return "";
    else if (n > String(this).length)
       return this;
    else {
       var iLen = String(this).length;
       return String(this).substring(iLen, iLen - n);
    }
}


String.prototype.firstToLower= function()
{
	return  this.charAt(0).toLowerCase() + this.slice(1);	
}


// Returns true if using Internet Explorer 9 or below, where some features are not supported
function isBadIE () {
  var myNav = navigator.userAgent.toLowerCase();
  if (myNav.indexOf('msie') == -1) return false;
  ieversion =  parseInt(myNav.split('msie')[1]);
  return (ieversion<10);
}


function runningLocal()
{
	return (window.location.protocol == 'file:');
}


// Levenshtein function

function getLevenshteinDistance (a, b)
{
  if(a.length == 0) return b.length; 
  if(b.length == 0) return a.length; 
 
  var matrix = [];
 
  // increment along the first column of each row
  var i;
  for(i = 0; i <= b.length; i++){
    matrix[i] = [i];
  }
 
  // increment each column in the first row
  var j;
  for(j = 0; j <= a.length; j++){
    matrix[0][j] = j;
  }
 
  // Fill in the rest of the matrix
  for(i = 1; i <= b.length; i++){
    for(j = 1; j <= a.length; j++){
      if(b.charAt(i-1) == a.charAt(j-1)){
        matrix[i][j] = matrix[i-1][j-1];
      } else {
        matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
                                Math.min(matrix[i][j-1] + 1, // insertion
                                         matrix[i-1][j] + 1)); // deletion
      }
    }
  }
 
  return matrix[b.length][a.length];
};

// waitKey helper for all key-wait condacts

function waitKey(callbackFunction)
{
	waitkey_callback_function.push(callbackFunction);
	showAnykeyLayer();
}

function waitKeyCallback()
{
 	var callback = waitkey_callback_function.pop();
	if ( callback ) callback();
	if (describe_location_flag) descriptionLoop();  		
}


// Check DOALL entry

function skipdoall(entry)
{
	return  ((doall_flag==true) && (entry_for_doall!='') && (current_process==process_in_doall) && (entry_for_doall > entry));
}

// Dynamic attribute use functions
function getNextFreeAttribute()
{
	var value = nextFreeAttr;
	nextFreeAttr++;
	return value;
}


// Gender functions

function getSimpleGender(objno)  // Simple, for english
{
 	isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
 	if (isPlural) return "P";
 	isFemale = objectIsAttr(objno, ATTR_FEMALE);
 	if (isFemale) return "F";
 	isMale = objectIsAttr(objno, ATTR_MALE);
 	if (isMale) return "M";
    return "N"; // Neuter
}

function getAdvancedGender(objno)  // Complex, for spanish
{
 	var isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
 	var isFemale = objectIsAttr(objno, ATTR_FEMALE);
 	var isMale = objectIsAttr(objno, ATTR_MALE);

 	if (!isPlural) 
 	{
	 	if (isFemale) return "F";
	 	if (isMale) return "M";
	    return "N"; // Neuter
 	}
 	else
 	{
	 	if (isFemale) return "PF";
	 	if (isMale) return "PM";
	 	return "PN"; // Neuter plural
 	}

}

function getLang()
{
	var value = bittest(getFlag(FLAG_PARSER_SETTINGS),5);
	if (value) return "ES"; else return "EN";
}

function getObjectFixArticles(objno)
{
	var object_text = getObjectText(objno);
	var object_words = object_text.split(' ');
	if (object_words.length == 1) return object_text;
	var candidate = object_words[0];
	object_words.splice(0, 1);
	if (getLang()=='EN')
	{
		if ((candidate!='an') && (candidate!='a') && (candidate!='some')) return object_text;
		return 'the ' + object_words.join(' ');
	}
	else
	{
		if ( (candidate!='un') && (candidate!='una') && (candidate!='unos') && (candidate!='unas') && (candidate!='alguna') && (candidate!='algunos') && (candidate!='algunas') && (candidate!='algun')) return object_text;
		var gender = getAdvancedGender(objno);
		if (gender == 'F') return 'la ' + object_words.join(' ');
		if (gender == 'M') return 'el ' + object_words.join(' ');
		if (gender == 'N') return 'el ' + object_words.join(' ');
		if (gender == 'PF') return 'las ' + object_words.join(' ');
		if (gender == 'PM') return 'los ' + object_words.join(' ');
		if (gender == 'PN') return 'los ' + object_words.join(' ');
	}	


}



// JS level log functions
function console_log(string)
{
	if (typeof console != "undefined") console.log(string);
}


// Resources functions
function getResourceById(resource_type, id)
{
	for (var i=0;i<resources.length;i++)
	 if ((resources[i][0] == resource_type) && (resources[i][1]==id)) return resources[i][2];
	return false; 
}

// Flag read/write functions
function getFlag(flagno)
{
	 return flags[flagno];
}

function setFlag(flagno, value)
{
	 flags[flagno] = value;
}

// Locations functions
function loc_here()  // Returns current location, avoid direct use of flags
{
	 return getFlag(FLAG_LOCATION);
}


// Connections functions

function setConnection(locno1, dirno, locno2)
{
	connections[locno1][dirno] = locno2;
}

function getConnection(locno, dirno)
{
	return connections[locno][dirno];
}

// Objects text functions

function getObjectText(objno)
{
	return filterText(objects[objno]);
}


// Message text functions
function getMessageText(mesno)
{
	return filterText(messages[mesno]);
}

function getSysMessageText(sysno)
{
	return filterText(sysmessages[sysno]);
}

function getWriteMessageText(writeno)
{
	return filterText(writemessages[writeno]);
}

function getExitsText(locno,mesno)
{
  if ( locno === undefined ) return ''; // game hasn't fully initialised yet
  if ((getFlag(FLAG_LIGHT) == 0) || ((getFlag(FLAG_LIGHT) != 0) && lightObjectsPresent()))
  {
  		var exitcount = 0;
  		for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1) exitcount++;
      if (exitcount)
      {
    		var message = getMessageText(mesno);
    		var exitcountprogress = 0;
    		for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1)
    		{ 
    			exitcountprogress++;
    			message += getMessageText(mesno + 2 + i);
    			if (exitcountprogress == exitcount) message += getSysMessageText(SYSMESS_LISTEND);
    			if (exitcountprogress == exitcount-1) message += getSysMessageText(SYSMESS_LISTLASTSEPARATOR);
    			if (exitcountprogress <= exitcount-2) message += getSysMessageText(SYSMESS_LISTSEPARATOR);
  		  }
  		  return message;
      } else return getMessageText(mesno + 1);
  } else return getMessageText(mesno + 1);
}


// Location text functions
function getLocationText(locno)
{
	return  filterText(locations[locno]);
}



// Output processing functions
function implementTag(tag)
{
	tagparams = tag.split('|');
	for (var tagindex=0;tagindex<tagparams.length-1;tagindex++) tagparams[tagindex] = tagparams[tagindex].trim();
	if (tagparams.length == 0) {writeWarning(STR_INVALID_TAG_SEQUENCE_EMPTY); return ''}

	var resolved_hook_value = h_sequencetag(tagparams);
	if (resolved_hook_value!='') return resolved_hook_value;

	switch(tagparams[0].toUpperCase())
	{
		case 'URL': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					return '<a target="newWindow" href="' + tagparams[1]+ '">' + tagparams[2] + '</a>'; // Note: _blank would get the underscore character replaced by current selected object so I prefer to use a different target name as most browsers will open a new window
					break;
		case 'CLASS': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span class="' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'STYLE': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span style="' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'INK': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span style="color:' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'PAPER': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span style="background-color:' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'OBJECT': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(objects[getFlag(tagparams[1])]) return getObjectFixArticles(getFlag(tagparams[1])); else return '';
					   break;
		case 'WEIGHT': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(objectsWeight[getFlag(tagparams[1])]) return objectsWeight[getFlag(tagparams[1])]; else return '';
					   break;
		case 'OLOCATION': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					      if(objectsLocation[getFlag(tagparams[1])]) return objectsLocation[getFlag(tagparams[1])]; else return '';
					      break;
		case 'MESSAGE':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(messages[getFlag(tagparams[1])]) return getMessageText(getFlag(tagparams[1])); else return '';
					   break;
		case 'SYSMESS':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(sysmessages[getFlag(tagparams[1])]) return getSysMessageText(getFlag(tagparams[1])); else return '';
					   break;
		case 'LOCATION':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(locations[getFlag(tagparams[1])]) return getLocationText(getFlag(tagparams[1])); else return '';
		case 'EXITS':if (tagparams.length != 3 ) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   return getExitsText(/^@\d+/.test(tagparams[1]) ? getFlag(tagparams[1].substr(1)) : tagparams[1],parseInt(tagparams[2],10));
					   break;
		case 'PROCESS':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   callProcess(tagparams[1]);
					   return "";
					   break;
		case 'ACTION': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   return '<a href="type: ' + tagparams[1] + '" onmouseup="orderEnteredLoop(\'' + tagparams[1]+ '\');return false;">' + tagparams[2] + '</a>';
					   break;
		case 'RESTART': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					    return '<a href="javascript: void(0)" onmouseup="restart()">' + tagparams[1] + '</a>';
					    break;
		case 'EXTERN': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					    return '<a href="javascript: void(0)" onmouseup="' + tagparams[1] + ' ">' + tagparams[2] + '</a>';
					    break;
		case 'TEXTPIC': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
						var style = '';
						var post = '';
						var pre = '';
						align = tagparams[2];
						switch(align)
						{
							case 1: style = 'float:left'; break;
							case 2: style = 'float:right'; break;
							case 3: post = '<br />';
							case 4: pre='<center>';post='</center>';break;
						}
						return pre + "<img class='textpic' style='"+style+"' src='"+ RESOURCES_DIR + tagparams[1]+"' />" + post;
					    break;
		case 'HTML': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
						return tagparams[1];
					    break;
		case 'FLAG': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
						return getFlag(tagparams[1]);
					    break;
		case 'OREF': if (tagparams.length != 1) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
   			        if(objects[getFlag(FLAG_REFERRED_OBJECT)]) return getObjectFixArticles(getFlag(FLAG_REFERRED_OBJECT)); else return '';
					break;
		case 'TT':  
		case 'TOOLTIP':
					if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					var title = $('<span>'+tagparams[1]+'</span>').text().replace(/'/g,"&apos;").replace(/\n/g, "&#10;");
					var text = tagparams[2];
					return "<span title='"+title+"'>"+text+"</span>";
					break;
		case 'OPRO': if (tagparams.length != 1) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};  // returns the pronoun for a given object, used for english start database
					 switch (getSimpleGender(getFlag(FLAG_REFERRED_OBJECT)))
					 {
					 	case 'M' : return "him";
					 	case "F" : return "her";
					 	case "N" : return "it";
					 	case "P" : return "them";  // plural returns them
					 }
					break;

		default : return '[[[' + STR_INVALID_TAG_SEQUENCE_BADTAG + ' : ' + tagparams[0] + ']]]';
	}
}

function processTags(text)
{
	//Apply the {} tags filtering
	var pre, post, innerTag;
	tagfilter:
	while (text.indexOf('{') != -1)
	{
		if (( text.indexOf('}') == -1 ) || ((text.indexOf('}') < text.indexOf('{'))))
		{
			writeWarning(STR_INVALID_TAG_SEQUENCE + text);
			break tagfilter;
		}
		pre = text.substring(0,text.indexOf('{'));
		var openbracketcont = 1;
		pointer = text.indexOf('{') + 1;
		innerTag = ''
		while (openbracketcont>0)
		{
			if (text.charAt(pointer) == '{') openbracketcont++;
			if (text.charAt(pointer) == '}') openbracketcont--;
			if ( text.length <= pointer )
			{
				writeWarning(STR_INVALID_TAG_SEQUENCE + text);
				break tagfilter;
			}
			innerTag = innerTag + text.charAt(pointer);
			pointer++;
		}
		innerTag = innerTag.substring(0,innerTag.length - 1);
		post = text.substring(pointer);
		if (innerTag.indexOf('{') != -1 ) innerTag = processTags(innerTag); 
		innerTag = implementTag(innerTag);
		text = pre + innerTag + post;
	}
	return text;
}

function filterText(text)
{
	// ngPAWS sequences
	text = processTags(text);


	// Superglus sequences (only \n remains)
    text = text.replace(/\n/g, STR_NEWLINE);

	// PAWS sequences (only underscore)
	objno = getFlag(FLAG_REFERRED_OBJECT);
	if ((objno != EMPTY_OBJECT) && (objects[objno]))	text = text.replace(/_/g,objects[objno].firstToLower()); else text = text.replace(/_/g,'');
	text = text.replace(/¬/g,' ');

	return text;
}


// Text Output functions
function writeText(text, skipAutoComplete)
{
	if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
	text = h_writeText(text); // hook
	$('.text').append(text);
	$('.text').scrollTop($('.text')[0].scrollHeight);
	addToTranscript(text);
	if (!skipAutoComplete) addToAutoComplete(text);
	focusInput();
}

function writeWarning(text)
{
	if (showWarnings) writeText(text)
}

function addToTranscript(text)
{
	transcript = transcript + text;		
}

function writelnText(text, skipAutoComplete)
{
	if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
	writeText(text + STR_NEWLINE, skipAutoComplete);
}

function writeMessage(mesno)
{
	if (messages[mesno]!=null) writeText(getMessageText(mesno)); else writeWarning(STR_NEWLINE + STR_WRONG_MESSAGE + ' [' + mesno + ']');
}

function writeSysMessage(sysno)
{
		if (sysmessages[sysno]!=null) writeText(getSysMessageText(sysno)); else writeWarning(STR_NEWLINE + STR_WRONG_SYSMESS + ' [' + sysno + ']');
		$(".text").scrollTop($(".text")[0].scrollHeight);
}

function writeWriteMessage(writeno)
{
		writeText(getWriteMessageText(writeno)); 
}

function writeObject(objno)
{
	writeText(getObjectText(objno));
}

function clearTextWindow()
{
	$('.text').empty();
}


function clearInputWindow()
{
	$('.prompt').val('');
}


function writeLocation(locno)
{
	if (locations[locno]!=null) writeText(getLocationText(locno) + STR_NEWLINE); else writeWarning(STR_NEWLINE + STR_WRONG_LOCATION + ' [' + locno + ']');
}

// Screen control functions

function clearGraphicsWindow()
{
	$('.graphics').empty();	
}


function clearScreen()
{
	clearInputWindow();
	clearTextWindow();
	clearGraphicsWindow();
}

function copyOrderToTextWindow(player_order)
{

	last_player_orders.push(player_order);
	last_player_orders_pointer = 0;
	clearInputWindow();
	writelnText(STR_PROMPT_START + player_order + STR_PROMPT_END, false);
}

function get_prev_player_order()
{
	if (!last_player_orders.length) return '';
	var last = last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];
	if (last_player_orders_pointer < last_player_orders.length - 1) last_player_orders_pointer++;
	return last;
}

function get_next_player_order()
{
	if (!last_player_orders.length || last_player_orders_pointer == 0) return '';
	last_player_orders_pointer--;
	return last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];

}



// Graphics functions


function hideGraphicsWindow()
{
		$('.text').removeClass('half_text');
		$('.text').addClass('all_text');
		$('.graphics').removeClass('half_graphics');
		$('.graphics').addClass('hidden');
		if ($('.location_picture')) $('.location_picture').remove();
}



function drawPicture(picno)  
{
	var pictureDraw = false;
	if (graphicsON) 
	{
		if ((isDarkHere()) && (!lightObjectsPresent())) picno = 0;
		var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
		if (filename)
		{
			$('.graphics').removeClass('hidden');
			$('.graphics').addClass('half_graphics');
			$('.text').removeClass('all_text');
			$('.text').addClass('half_text');
			$('.graphics').html('<img alt="" class="location_picture" src="' +  filename + '" />');
			$('.location_picture').css('height','100%');
			pictureDraw = true;
		}
	}

	if (!pictureDraw) hideGraphicsWindow();
}




function clearPictureAt() // deletes all pictures drawn by "pictureAT" condact
{
	$.each($('.graphics img'), function () {
		if ($(this)[0].className!= 'location_picture') $(this).remove();
	});

}

// Turns functions

function incTurns()
{
	turns = getFlag(FLAG_TURNS_LOW) + 256 * getFlag(FLAG_TURNS_HIGH)  + 1;
	setFlag(FLAG_TURNS_LOW, turns % 256);
	setFlag(FLAG_TURNS_HIGH, Math.floor(turns / 256));
}

// input box functions

function disableInput()
{
	$(".input").prop('disabled', true); 
}

function enableInput()
{
	$(".input").prop('disabled', false); 
}

function focusInput()
{
	$(".prompt").focus();
	timeout_progress = 0;
}

// Object default attributes functions

function objectIsNPC(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_NPC);
}

function objectIsLight(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_LIGHT);
}

function objectIsWearable(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_WEARABLE);
}

function objectIsContainer(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_CONTAINER);
}

function objectIsSupporter(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_SUPPORTER);
}


function objectIsAttr(objno, attrno)
{
	if (attrno > 63) return false;
	var attrs = getObjectLowAttributes(objno);
	if (attrno > 31)
	{
		attrs = getObjectHighAttributes(objno);
		attrno = attrno - 32;
	}
	return bittest(attrs, attrno);
}

function isAccesibleContainer(objno)
{
	if (objectIsSupporter(objno)) return true;   // supporter
	if ( objectIsContainer(objno) && !objectIsAttr(objno, ATTR_OPENABLE) ) return true;  // No openable container
	if ( objectIsContainer(objno) && objectIsAttr(objno, ATTR_OPENABLE) && objectIsAttr(objno, ATTR_OPEN)  )  return true;  // No openable & open container
	return false;
}

//Objects and NPC functions

function findMatchingObject(locno)
{
	for (var i=0;i<num_objects;i++)
		if ((locno==-1) || (getObjectLocation(i) == locno))
		 if (((objectsNoun[i]) == getFlag(FLAG_NOUN1)) && (((objectsAdjective[i]) == EMPTY_WORD) || ((objectsAdjective[i]) == getFlag(FLAG_ADJECT1))))  return i;
	return EMPTY_OBJECT;
}

function getReferredObject()
{
	var objectfound = EMPTY_OBJECT; 
	refobject_search: 
	{
		object_id = findMatchingObject(LOCATION_CARRIED);
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	

		object_id = findMatchingObject(LOCATION_WORN);
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	

		object_id = findMatchingObject(loc_here());
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	

		object_id = findMatchingObject(-1);
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	
	}
	return objectfound;
}


function getObjectLowAttributes(objno)
{
	return objectsAttrLO[objno];
}

function getObjectHighAttributes(objno)
{
	return objectsAttrHI[objno]
}


function setObjectLowAttributes(objno, attrs)
{
	objectsAttrLO[objno] = attrs;
}

function setObjectHighAttributes(objno, attrs)
{
	objectsAttrHI[objno] = attrs;
}


function getObjectLocation(objno)
{
	if (objno > last_object_number) 
		writeWarning(STR_INVALID_OBJECT + ' [' + objno + ']');
	return objectsLocation[objno];
}

function setObjectLocation(objno, locno)
{
	if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) - 1);
	objectsLocation[objno] = locno;
	if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) + 1);
}



// Sets all flags associated to  referred object by current LS  
function setReferredObject(objno) 
{
	if (objno == EMPTY_OBJECT)
	{
		setFlag(FLAG_REFERRED_OBJECT, EMPTY_OBJECT);
		setFlag(FLAG_REFERRED_OBJECT_LOCATION, LOCATION_NONCREATED);
		setFlag(FLAG_REFERRED_OBJECT_WEIGHT, 0);
		setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, 0);
		setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, 0);
		return;
	}
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setFlag(FLAG_REFERRED_OBJECT_LOCATION, getObjectLocation(objno));
	setFlag(FLAG_REFERRED_OBJECT_WEIGHT, getObjectWeight(objno));
	setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, getObjectLowAttributes(objno));
	setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, getObjectHighAttributes(objno));

}


function getObjectWeight(objno) 
{
	var weight = objectsWeight[objno];
	if ( ((objectIsContainer(objno)) || (objectIsSupporter(objno))) && (weight!=0)) // Container with zero weigth are magic boxes, anything you put inside weigths zero
  		weight = weight + getLocationObjectsWeight(objno);
	return weight;
}


function getLocationObjectsWeight(locno) 
{
	var weight = 0;
	for (var i=0;i<num_objects;i++)
	{
		if (getObjectLocation(i) == locno) 
		{
			objweight = objectsWeight[i];
			weight += objweight;
			if (objweight > 0)
			{
				if (  (objectIsContainer(i)) || (objectIsSupporter(i)) )
				{	
					weight += getLocationObjectsWeight(i);
				}
			}
		}
	}
	return weight;
}

function getObjectCountAt(locno) 
{
	var count = 0;
	for (i=0;i<num_objects;i++)
	{
		if (getObjectLocation(i) == locno) 
		{
			attr = getObjectLowAttributes(i);
			if (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)) count ++;  // Parser settings say we should include NPCs as objects
			 else if (!objectIsNPC(i)) count++;     // or object is not an NPC
		}
	}
	return count;
}


function getObjectCountAtWithAttr(locno, attrnoArray) 
{
	var count = 0;
	for (var i=0;i<num_objects;i++)
		if (getObjectLocation(i) == locno)  
			for (var j=0;j<attrnoArray.length;j++)
				if (objectIsAttr(i, attrnoArray[j])) count++;
	return count;
}


function getNPCCountAt(locno) 
{
	var count = 0;
	for (i=0;i<num_objects;i++)
		if ((getObjectLocation(i) == locno) &&  (objectIsNPC(i))) count++;
	return count;
}


// Location light function

function lightObjectsAt(locno) 
{
	return getObjectCountAtWithAttr(locno, [ATTR_LIGHT]) > 0;
}


function lightObjectsPresent() 
{
  if (lightObjectsAt(LOCATION_CARRIED)) return true;
  if (lightObjectsAt(LOCATION_WORN)) return true;
  if (lightObjectsAt(loc_here())) return true;
  return false;
}


function isDarkHere()
{
	return (getFlag(FLAG_LIGHT) != 0);
}

// Sound functions


function preloadsfx()
{
	for (var i=0;i<resources.length;i++)
	 	if (resources[i][0] == 'RESOURCE_TYPE_SND') 
	 	{
	 		var fileparts = resources[i][2].split('.');
			var basename = fileparts[0];
			var mySound = new buzz.sound( basename, {  formats: [ "ogg", "mp3" ] , preload: true} );
	 	}
}

function sfxplay(sfxno, channelno, times, method)
{

	if (!soundsON) return;
	if ((channelno <0) || (channelno >MAX_CHANNELS)) return;
	if (times == 0) times = -1; // more than 4000 million times
	var filename = getResourceById(RESOURCE_TYPE_SND, sfxno);
	if (filename)
	{
		var fileparts = filename.split('.');
		var basename = fileparts[0];
		var mySound = new buzz.sound( basename, {  formats: [ "ogg", "mp3" ] });
		if (soundChannels[channelno]) soundChannels[channelno].stop();
		soundLoopCount[channelno] = times;
		mySound.bind("ended", function(e) {
			for (sndloop=0;sndloop<MAX_CHANNELS;sndloop++)
				if (soundChannels[sndloop] == this)
				{
					if (soundLoopCount[sndloop]==-1) {this.play(); return }
					soundLoopCount[sndloop]--;
					if (soundLoopCount[sndloop] > 0) {this.play(); return }
					sfxstop(sndloop);
					return;
				}
		});
		soundChannels[channelno] = mySound;
		if (method=='play')	mySound.play(); else mySound.fadeIn(2000);
	}
}

function playLocationMusic(locno)
{
	if (soundsON) 
		{
			sfxstop(0);
			sfxplay(locno, 0, 0, 'play');
		}
}

function musicplay(musicno, times)  
{
	sfxplay(musicno, 0, times);
}

function channelActive(channelno)
{
	if (soundChannels[channelno]) return true; else return false;
}


function sfxstopall() 
{
	for (channelno=0;channelno<MAX_CHANNELS;channelno++) sfxstop(channelno);

}


function sfxstop(channelno)
{
	if (soundChannels[channelno]) 
		{
			soundChannels[channelno].unbind('ended');
			soundChannels[channelno].stop();
			soundChannels[channelno] = null;
		}
}

function sfxvolume(channelno, value)
{
	if (soundChannels[channelno]) soundChannels[channelno].setVolume(Math.floor( value * 100 / 65535)); // Inherited volume condact uses a number among 0 and 65535, buzz library uses 0-100.
}

function isSFXPlaying(channelno)
{
	if (!soundChannels[channelno]) return false;
	return true;
}


function sfxfadeout(channelno, value)
{
	if (!soundChannels[channelno]) return;
	soundChannels[channelno].fadeOut(value, function() { sfxstop(channelno) });
}

// *** Process functions ***

function callProcess(procno)
{
	if (inEND) return;
	current_process = procno;
	var prostr = procno.toString(); 
	while (prostr.length < 3) prostr = "0" + prostr;
	if (procno==0) in_response = true;
	if (doall_flag && in_response) done_flag = false;
	if (!in_response) done_flag = false;
	h_preProcess(procno);
    eval("pro" + prostr + "()");
	h_postProcess(procno);
	if (procno==0) in_response = false;
}

// Bitwise functions

function bittest(value, bitno)
{
	mask = 1 << bitno;
	return ((value & mask) != 0);
}

function bitset(value, bitno)
{

	mask = 1 << bitno;
	return value | mask;
}

function bitclear(value, bitno)
{
	mask = 1 << bitno;
	return value & (~mask);
}


function bitneg(value, bitno) 
{
	mask = 1 << bitno;
	return value ^ mask;

}

// Savegame functions
function getSaveGameObject()
{
	var savegame_object = new Object();
	// Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
	savegame_object.flags = flags.slice();
	savegame_object.objectsLocation = objectsLocation.slice();
	savegame_object.objectsWeight = objectsWeight.slice();
	savegame_object.objectsAttrLO = objectsAttrLO.slice();
	savegame_object.objectsAttrHI = objectsAttrHI.slice();
	savegame_object.connections = connections.slice();
	savegame_object.last_player_orders = last_player_orders.slice();
	savegame_object.last_player_orders_pointer = last_player_orders_pointer;
	savegame_object.transcript = transcript;
	savegame_object = h_saveGame(savegame_object);
	return savegame_object;
}

function restoreSaveGameObject(savegame_object)
{
	flags = savegame_object.flags;
	// Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
	objectsLocation = savegame_object.objectsLocation.slice();
	objectsWeight = savegame_object.objectsWeight.slice();
	objectsAttrLO = savegame_object.objectsAttrLO.slice();
	objectsAttrHI = savegame_object.objectsAttrHI.slice();
	connections = savegame_object.connections.slice();
	last_player_orders = savegame_object.last_player_orders.slice();
	last_player_orders_pointer = savegame_object.last_player_orders_pointer;
	transcript = savegame_object.transcript;
	h_restoreGame(savegame_object);
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        The parser                                                      //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function loadPronounSufixes()
{

    var swapped;

	for (var j=0;j<vocabulary.length;j++) if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_PRONOUN)
			 pronoun_suffixes.push(vocabulary[j][VOCABULARY_WORD]);
	// Now sort them so the longest are first, so you rather replace SELOS in (COGESELOS=>COGE SELOS == >TAKE THEM) than LOS (COGESELOS==> COGESE LOS ==> TAKExx THEM) that woul not be understood (COGESE is not a verb, COGE is)
    do {
        swapped = false;
        for (var i=0; i < pronoun_suffixes.length-1; i++) 
        {
            if (pronoun_suffixes[i].length < pronoun_suffixes[i+1].length) 
            {
                var temp = pronoun_suffixes[i];
                pronoun_suffixes[i] = pronoun_suffixes[i+1];
                pronoun_suffixes[i+1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
}


function findVocabulary(word, forceDisableLevenshtein)  
{
	// Pending: in general this function is not very efficient. A solution where the vocabulary array is sorted by word so the first search can be binary search
	//          and possible typos are precalculated, so the distance is a lookup table instead of a function, would be much more efficient. On the other hand,
	//          the current solution is fast enough with a 1000+ words game that I don't consider improving this function to have high priority now.

	// Search word in vocabulary
	for (var j=0;j<vocabulary.length;j++)
		if (vocabulary[j][VOCABULARY_WORD] == word)
			 return vocabulary[j];

	if (forceDisableLevenshtein) return null;

	if (word.length <=4) return null; // Don't try to fix typo for words with less than 5 length

	if (bittest(getFlag(FLAG_PARSER_SETTINGS), 8)) return null; // If matching is disabled, we won't try to use levhenstein distance

	// Search words in vocabulary with a Levenshtein distance of 1
	var distance2_match = null;
	for (var k=0;k<vocabulary.length;k++)
	{
		if ([WORDTYPE_VERB,WORDTYPE_NOUN,WORDTYPE_ADJECT,WORDTYPE_ADVERB].indexOf(vocabulary[k][VOCABULARY_TYPE])  != -1 )
		{
			var distance = getLevenshteinDistance(vocabulary[k][VOCABULARY_WORD], word);
			if ((!distance2_match) && (distance==2)) distance2_match = vocabulary[k]; // Save first word with distance=2, in case we don't find any word with distance 1
			if (distance <= 1) return vocabulary[k];
		}
	} 

	// If we found any word with distance 2, return it, only if word was at least 7 characters long
	if ((distance2_match) &&  (word.length >6)) return distance2_match;

	// Word not found
	return null;
}

function normalize(player_order)   
// Removes accented characters and makes sure every sentence separator (colon, semicolon, quotes, etc.) has one space before and after. Also, all separators are converted to comma
{
	var originalchars = 'áéíóúäëïöüâêîôûàèìòùÁÉÍÓÚÄËÏÖÜÂÊÎÔÛÀÈÌÒÙ';
	var i;
	var output = '';
	var pos;

	for (i=0;i<player_order.length;i++) 
	{
		pos = originalchars.indexOf(player_order.charAt(i));
		if (pos!=-1) output = output + "aeiou".charAt(pos % 5); else 
		{
			ch = player_order.charAt(i);
				if ((ch=='.') || (ch==',') || (ch==';') || (ch=='"') || (ch=='\'') || (ch=='«') || (ch=='»')) output = output + ' , '; else output = output + player_order.charAt(i);
		}

	}
	return output;
}

function toParserBuffer(player_order)  // Converts a player order in a list of sentences separated by dot.
{
     player_order = normalize(player_order);
     player_order = player_order.toUpperCase();
    
	 var words = player_order.split(' ');
	 for (var q=0;q<words.length;q++)
	 {
	 	words[q] = words[q].trim();
	 	if  (words[q]!=',')
	 	{
	 		words[q] = words[q].trim();
	 		foundWord = findVocabulary(words[q], false);
	 		if (foundWord)
	 		{
	 			if (foundWord[VOCABULARY_TYPE]==WORDTYPE_CONJUNCTION)
	 			{
	 			words[q] = ','; // Replace conjunctions with commas
		 		} 
	 		}
	 	}
	 }

	 var output = '';
	 for (q=0;q<words.length;q++)
	 {
	 	if (words[q] == ',') output = output + ','; else output = output + words[q] + ' ';
	 }
	 output = output.replace(/ ,/g,',');
	 output = output.trim();
	 player_order_buffer = output;
}

function getSentencefromBuffer()
{
	var sentences = player_order_buffer.split(',');
	var result = sentences[0];
	sentences.splice(0,1);
	player_order_buffer = sentences.join();
	return result;
}

function processPronounSufixes(words)  
{
	// This procedure will split pronominal sufixes into separated words, so COGELA will become COGE LA at the end, and work exactly as TAKE IT does.
	// it's only for spanish so if lang is english then it makes no changes
	if (getLang() == 'EN') return words;
	var verbFound = false;
	if (!bittest(getFlag(FLAG_PARSER_SETTINGS),0)) return words;  // If pronoun sufixes inactive, just do nothing
	// First, we clear the word list from any match with pronouns, cause if we already have something that matches pronouns, probably is just concidence, like in COGE LA LLAVE
	var filtered_words = [];
	for (var q=0;q < words.length;q++)
	{
		foundWord = findVocabulary(words[q], false);
		if (foundWord) 
			{
				if (foundWord[VOCABULARY_TYPE] != WORDTYPE_PRONOUN) filtered_words[filtered_words.length] = words[q];
			}
			else filtered_words[filtered_words.length] = words[q];
	}
	words = filtered_words;

	// Now let's start trying to get sufixes
	new_words = [];
	for (var k=0;k < words.length;k++)
	{
		words[k] = words[k].trim();
		foundWord = findVocabulary(words[k], true); // true to disable Levenshtein distance applied
		if (foundWord) if (foundWord[VOCABULARY_TYPE] == WORDTYPE_VERB) verbFound = true;  // If we found a verb, we don't look for pronoun sufixes, as they have to come together with verb
		suffixFound = false;
		pronunsufix_search:
		for (var l=0;(l<pronoun_suffixes.length) && (!suffixFound) && (!verbFound);l++)
		{

			if (pronoun_suffixes[l] == words[k].rights(pronoun_suffixes[l].length))
			{
				var verb_part = words[k].substring(0,words[k].length - pronoun_suffixes[l].length);
				var checkWord = findVocabulary(verb_part, false);
				if ((!checkWord)  || (checkWord[VOCABULARY_TYPE] != WORDTYPE_VERB))  // If the part before the supposed-to-be pronoun sufix is not a verb, then is not a pronoun sufix
				{
					new_words.push(words[k]);	
					continue pronunsufix_search;
				}
				new_words.push(verb_part);  // split the word in two parts: verb + pronoun. Since that very moment it works like in english (COGERLO ==> COGER LO as of TAKE IT)
				new_words.push(pronoun_suffixes[l]);
				suffixFound = true;
				verbFound = true;
			}
		}
		if (!suffixFound) new_words.push(words[k]);
	}
	return new_words;
}

function getLogicSentence()
{
	parser_word_found = false; ;
	aux_verb = -1;
	aux_noun1 = -1;
	aux_adject1 = -1;
	aux_adverb = -1;
	aux_pronoun = -1
	aux_pronoun_adject = -1
	aux_preposition = -1;
	aux_noun2 = -1;
	aux_adject2 = -1;
	initializeLSWords();
	SL_found = false;

	var order = getSentencefromBuffer();
	setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS),1)); // Initialize flag that says an unknown word was found in the sentence


	words = order.split(" ");
	words = processPronounSufixes(words);
	wordsearch_loop:
	for (var i=0;i<words.length;i++)
	{
		original_word = currentword = words[i];
		if (currentword.length>10) currentword = currentword.substring(0,MAX_WORD_LENGHT);
		foundWord = findVocabulary(currentword, false);
		if (foundWord)
		{
			wordtype = foundWord[VOCABULARY_TYPE];
			word_id = foundWord[VOCABULARY_ID];

			switch (wordtype)
			{
				case WORDTYPE_VERB: if (aux_verb == -1)  aux_verb = word_id; 
				        			break;

				case WORDTYPE_NOUN: if (aux_noun1 == -1) aux_noun1 = word_id; else if (aux_noun2 == -1) aux_noun2 = word_id;
									break;

				case WORDTYPE_ADJECT: if (aux_adject1 == -1) aux_adject1 = word_id; else if (aux_adject2 == -1) aux_adject2 = word_id;
									  break;

				case WORDTYPE_ADVERB: if (aux_adverb == -1) aux_adverb = word_id;
				        			  break;

				case WORDTYPE_PRONOUN: if (aux_pronoun == -1) 
											{
												aux_pronoun = word_id;
												if ((previous_noun != EMPTY_WORD) && (aux_noun1 == -1))
												{
													aux_noun1 = previous_noun;
													if (previous_adject != EMPTY_WORD) aux_adject1 = previous_adject;
												}
											}

				        			   break;

				case WORDTYPE_CONJUNCTION: break wordsearch_loop; // conjunction or nexus. Should not appear in this function, just added for security
				
				case WORDTYPE_PREPOSITION: if (aux_preposition == -1) aux_preposition = word_id;
										   if (aux_noun1!=-1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS),2));  // Set bit that determines that a preposition word was found after first noun
										   break;
			}

			// Nouns that can be converted to verbs
			if ((aux_noun1!=-1) && (aux_verb==-1) && (aux_noun1 < NUM_CONVERTIBLE_NOUNS))
			{
				aux_verb = aux_noun1;
				aux_noun1 = -1;
			}

			if ((aux_verb==-1) && (aux_noun1!=-1) && (previous_verb!=EMPTY_WORD)) aux_verb = previous_verb;  // Support "TAKE SWORD AND SHIELD" --> "TAKE WORD AND TAKE SHIELD"

			if ((aux_verb!=-1) || (aux_noun1!=-1) || (aux_adject1!=-1 || (aux_preposition!=-1) || (aux_adverb!=-1))) SL_found = true;



		} else if (aux_verb!=-1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS),1));  // Set bit that determines that an unknown word was found after the verb
	}

	if (SL_found)
	{
		if (aux_verb != -1) setFlag(FLAG_VERB, aux_verb);
		if (aux_noun1 != -1) setFlag(FLAG_NOUN1, aux_noun1);
		if (aux_adject1 != -1) setFlag(FLAG_ADJECT1, aux_adject1);
		if (aux_adverb != -1) setFlag(FLAG_ADVERB, aux_adverb);
		if (aux_pronoun != -1) 
			{
				setFlag(FLAG_PRONOUN, aux_noun1);
				setFlag(FLAG_PRONOUN_ADJECT, aux_adject1);
			}
			else
			{
				setFlag(FLAG_PRONOUN, EMPTY_WORD);
				setFlag(FLAG_PRONOUN_ADJECT, EMPTY_WORD);
			}
		if (aux_preposition != -1) setFlag(FLAG_PREP, aux_preposition);
		if (aux_noun2 != -1) setFlag(FLAG_NOUN2, aux_noun2);
		if (aux_adject2 != -1) setFlag(FLAG_ADJECT2, aux_adject2);
		setReferredObject(getReferredObject());
		previous_verb = aux_verb;
		if ((aux_noun1!=-1) && (aux_noun1>=NUM_PROPER_NOUNS))
		{
			previous_noun = aux_noun1;
			if (aux_adject1!=-1) previous_adject = aux_adject1;
		}
		
	}
	if ((aux_verb + aux_noun1+ aux_adject1 + aux_adverb + aux_pronoun + aux_preposition + aux_noun2 + aux_adject2) != -8) parser_word_found = true;

	return SL_found;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        Main functions and main loop                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Interrupt functions

function enableInterrupt()
{
	interruptDisabled = false;
}

function disableInterrupt()
{
	interruptDisabled = true;
}

function timer()
{
	// Timeout control
	timeout_progress=  timeout_progress + 1/32;  //timer happens every 40 milliseconds, but timeout counter should only increase every 1.28 seconds (according to PAWS documentation)
	timeout_length = getFlag(FLAG_TIMEOUT_LENGTH);
	if ((timeout_length) && (timeout_progress> timeout_length))  // time for timeout
	{
		timeout_progress = 0;
		if (($('.prompt').val() == '')  || (($('.prompt').val()!='') && (!bittest(getFlag(FLAG_TIMEOUT_SETTINGS),0))) )  // but first check there is no text type, or is allowed to timeout when text typed already
		{
			setFlag(FLAG_TIMEOUT_SETTINGS, bitset(getFlag(FLAG_TIMEOUT_SETTINGS),7)); // Clears timeout bit
			writeSysMessage(SYSMESS_TIMEOUT);	
			callProcess(PROCESS_TURN);
		}
	}	

	// PAUSE condact control
	if (inPause)
	{
		pauseRemainingTime = pauseRemainingTime - 40; // every tick = 40 milliseconds
		if (pauseRemainingTime<=0)
		{
			inPause = false;
			hideAnykeyLayer();
			waitKeyCallback()
		}
	}

	// Interrupt process control
	if (!interruptDisabled)
	if (interruptProcessExists)
	{
		callProcess(interrupt_proc);
		setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened 
	}

}

// Initialize and finalize functions

function farewell()
{
	writeSysMessage(SYSMESS_FAREWELL);
	ACCnewline();
}


function initializeConnections()
{
  connections = [].concat(connections_start);
}

function initializeObjects()
{
  for (i=0;i<objects.length;i++)
  {
  	objectsAttrLO = [].concat(objectsAttrLO_start);
  	objectsAttrHI = [].concat(objectsAttrHI_start);
  	objectsLocation = [].concat(objectsLocation_start);
  	objectsWeight = [].concat(objectsWeight_start);
  }
}

function  initializeLSWords()
{
  setFlag(FLAG_PREP,EMPTY_WORD);
  setFlag(FLAG_NOUN2,EMPTY_WORD);
  setFlag(FLAG_ADJECT2,EMPTY_WORD);
  setFlag(FLAG_PRONOUN,EMPTY_WORD);
  setFlag(FLAG_ADJECT1,EMPTY_WORD);
  setFlag(FLAG_VERB,EMPTY_WORD);
  setFlag(FLAG_NOUN1,EMPTY_WORD);
  setFlag(FLAG_ADJECT1,EMPTY_WORD);
  setFlag(FLAG_ADVERB,EMPTY_WORD);
}


function initializeFlags()
{
  flags = [];
  for (var  i=0;i<FLAG_COUNT;i++) flags.push(0);
  setFlag(FLAG_MAXOBJECTS_CARRIED,4);
  setFlag(FLAG_PARSER_SETTINGS,9); // Pronoun sufixes active, DOALL and others ignore NPCs, etc. 00001001
  setFlag(FLAG_MAXWEIGHT_CARRIED,10);
  initializeLSWords();
  setFlag(FLAG_OBJECT_LIST_FORMAT,64); // List objects in a single sentence (comma separated)
  setFlag(FLAG_OBJECTS_CARRIED_COUNT,carried_objects);  // FALTA: el compilador genera esta variable, hay que cambiarlo en el compilador, ERA numero_inicial_de_objetos_llevados
}

function initializeInternalVars()
{
	num_objects = last_object_number + 1;
	transcript = '';
	timeout_progress = 0;
	previous_noun = EMPTY_WORD;
	previous_verb = EMPTY_WORD;
	previous_adject = EMPTY_WORD;
	player_order_buffer = '';
	last_player_orders = [];
	last_player_orders_pointer = 0;
	graphicsON = true; 
	soundsON = true; 
	interruptDisabled = false;
	unblock_process = null;
	done_flag = false;
	describe_location_flag =false;
	in_response = false;
	success = false;
	doall_flag = false;
	entry_for_doall	= '';
}

function initializeSound()
{
	sfxstopall();
}




function initialize()
{
	preloadsfx();
	initializeInternalVars();
	initializeSound();
	initializeFlags();
	initializeObjects();
	initializeConnections();
}



// Main loops

function descriptionLoop()
{
	do
	{
		describe_location_flag = false;
		if (!getFlag(FLAG_MODE)) clearTextWindow();
		if ((isDarkHere()) && (!lightObjectsPresent())) writeSysMessage(SYSMESS_ISDARK); else writeLocation(loc_here()); 
		h_description_init();
		playLocationMusic(loc_here());
		if (loc_here()) drawPicture(loc_here()); else hideGraphicsWindow(); // Don't show picture at location 0
		ACCminus(FLAG_AUTODEC2,1);
		if (isDarkHere()) ACCminus(FLAG_AUTODEC3,1);
		if ((isDarkHere()) && (lightObjectsAt(loc_here())==0)) ACCminus(FLAG_AUTODEC4,1);
		callProcess(PROCESS_DESCRIPTION);
		h_description_post();
		if (describe_location_flag) continue; // descriptionLoop() again without nesting
		describe_location_flag = false;
		callProcess(PROCESS_TURN);
		if (describe_location_flag) continue; 
		describe_location_flag = false;
		focusInput();
		break; // Dirty trick to make this happen just one, but many times if descriptioLoop() should be repeated
	} while (true);

}

function orderEnteredLoop(player_order)
{
	previous_verb = EMPTY_WORD;
	setFlag(FLAG_TIMEOUT_SETTINGS, bitclear(getFlag(FLAG_TIMEOUT_SETTINGS),7)); // Clears timeout bit
	if (player_order == '') {writeSysMessage(SYSMESS_SORRY); ACCnewline(); return; };	
	player_order = h_playerOrder(player_order); //hook
	copyOrderToTextWindow(player_order);
	toParserBuffer(player_order);
	do 
	{
		describe_location_flag = false;
		ACCminus(FLAG_AUTODEC5,1);
		ACCminus(FLAG_AUTODEC6,1);
		ACCminus(FLAG_AUTODEC7,1);
		ACCminus(FLAG_AUTODEC8,1);
		if (isDarkHere()) ACCminus(FLAG_AUTODEC9,1);
		if ((isDarkHere()) && (lightObjectsAt(loc_here())==0)) ACCminus(FLAG_AUTODEC10,1);
		
		if (describe_location_flag) 
		{
			descriptionLoop();
			return;
		};

		if (getLogicSentence())
		{
			incTurns();
			done_flag = false;
			callProcess(PROCESS_RESPONSE); // Response table
			if (describe_location_flag) 
			{
				descriptionLoop();
				return;
			};
			if (!done_flag) 
			{
				if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (CNDmove(FLAG_LOCATION)))
				{
					descriptionLoop();
					return;
				} else if (getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) {writeSysMessage(SYSMESS_WRONGDIRECTION);ACCnewline();}	else {writeSysMessage(SYSMESS_CANTDOTHAT);ACCnewline();};

			}
		} else
		{
			h_invalidOrder(player_order);
			if (parser_word_found) {writeSysMessage(SYSMESS_IDONTUNDERSTAND);   ACCnewline() }
			    		      else {writeSysMessage(SYSMESS_NONSENSE_SENTENCE); ACCnewline() };	
		}  
		callProcess(PROCESS_TURN);
	} while (player_order_buffer !='');
	previous_verb = ''; // Can't use previous verb if a new order is typed (we keep previous noun though, it can be used)
	focusInput();
}


function restart()
{
	location.reload();	
}


function hideBlock()
{
	clearInputWindow();
    $('.block_layer').hide('slow');
    enableInterrupt();   	
    $('.input').show();  
    focusInput();
}

function hideAnykeyLayer()
{
	$('.anykey_layer').hide();
    $('.input').show();  
    focusInput();   
}

function showAnykeyLayer()
{
	$('.anykey_layer').show();
    $('.input').hide();  
}

//called when the block layer is closed
function closeBlock()
{
	if (!inBlock) return;
	inBlock = false;
	hideBlock();
    var proToCall = unblock_process;
	unblock_process = null;
	callProcess(proToCall);
	if (describe_location_flag) descriptionLoop();
}

function setInputPlaceHolder()
{
	var prompt_msg = getFlag(FLAG_PROMPT);
	if (!prompt_msg)
	{
		var random = Math.floor((Math.random()*100));
		if (random<30) prompt_msg = SYSMESS_PROMPT0; else
		if ((random>=30) && (random<60)) prompt_msg = SYSMESS_PROMPT1; else
		if ((random>=60) && (random<90)) prompt_msg = SYSMESS_PROMPT2; else
		if (random>=90) prompt_msg = SYSMESS_PROMPT3;
	}
	$('.prompt').attr('placeholder', $('<div>'+getSysMessageText(prompt_msg).replace(/(?:<br>)*$/,'').replace( /<br>/g, ', ' )+'</div>').text());
}


function divTextScrollUp()
{
   	var currentPos = $('.text').scrollTop();
	if (currentPos>=DIV_TEXT_SCROLL_STEP) $('.text').scrollTop(currentPos - DIV_TEXT_SCROLL_STEP); else $('.text').scrollTop(0);
}

function divTextScrollDown()
{
   	var currentPos = $('.text').scrollTop();
   	if (currentPos <= ($('.text')[0].scrollHeight - DIV_TEXT_SCROLL_STEP)) $('.text').scrollTop(currentPos + DIV_TEXT_SCROLL_STEP); else $('.text').scrollTop($('.text')[0].scrollHeight);
}

// Autocomplete functions

function predictiveText(currentText)
{
	if (currentText == '') return currentText;
	var wordToComplete;
	var words = currentText.split(' ');
	if (autocompleteStep!=0) wordToComplete = autocompleteBaseWord; else wordToComplete = words[words.length-1];
	words[words.length-1] = completedWord(wordToComplete);
	return words.join(' ');
}


function initAutoComplete()
{
	for (var j=0;j<vocabulary.length;j++)
		if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_VERB)
			if (vocabulary[j][VOCABULARY_WORD].length >= 3)
				autocomplete.push(vocabulary[j][VOCABULARY_WORD].toLowerCase());
}

function addToAutoComplete(sentence)
{
	var words = sentence.split(' ');
	for (var i=0;i<words.length;i++)
	{
		var finalWord = '';
		for (var j=0;j<words[i].length;j++)
		{
			var c = words[i][j].toLowerCase();
			if ("abcdefghijklmnopqrstuvwxyzáéíóúàèìòùçäëïÖüâêîôû".indexOf(c) != -1) finalWord = finalWord + c;
			else break;
		}
	
		if (finalWord.length>=3) 
		{
			var index = autocomplete.indexOf(finalWord);
			if (index!=-1) autocomplete.splice(index,1);
			autocomplete.push(finalWord);
		}
	}
}

function completedWord(word)
{
	if (word=='') return '';
   autocompleteBaseWord  =word;
   var foundCount = 0;
   for (var i = autocomplete.length-1;i>=0; i--)
   {
   	  if (autocomplete[i].length > word.length) 
   	  	 if (autocomplete[i].indexOf(word)==0) 
   	  	 	{
   	  	 		foundCount++;
   	  	 		if (foundCount>autocompleteStep)
   	  	 		{
   	  	 			autocompleteStep++;
   	  	 			return autocomplete[i];
   	  	 		}
   	  	 	}
   }
   return word;
}


// Exacution starts here, called by the html file on document.ready()
function start()
{
	h_init(); //hook
	$('.graphics').addClass('half_graphics');
	$('.text').addClass('half_text');
	if (isBadIE()) alert(STR_BADIE)
	loadPronounSufixes();	
    setInputPlaceHolder();
    initAutoComplete();

	// Assign keypress action for input box (detect enter key press)
	$('.prompt').keypress(function(e) {  
    	if (e.which == 13) 
    	{ 
    		setInputPlaceHolder();
    		player_order = $('.prompt').val();
    		if (player_order.charAt(0) == '#')
    		{
    			addToTranscript(player_order + STR_NEWLINE);
    			clearInputWindow();
    		} 
    		else
    		if (player_order!='') 
    				orderEnteredLoop(player_order);
    	}
    });

	// Assign arrow up key press to recover last order
    $('.prompt').keyup( function(e) {
    	if (e.which  == 38) $('.prompt').val(get_prev_player_order());
    	if (e.which  == 40) $('.prompt').val(get_next_player_order());
    });


    // Assign tab keydown to complete word
    $('.prompt').keydown( function(e) {
    	if (e.which == 9) 
    		{
    			$('.prompt').val(predictiveText($('.prompt').val()));
    			e.preventDefault();
    		} else 
    		{
		    	autocompleteStep = 0;
    			autocompleteBaseWord = ''; // Any keypress other than tab resets the autocomplete feature
    		}
    });

    //Detect resize to change flag 12
     $(window).resize(function () {
     	setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened 
     	clearPictureAt();
     	return;
     });


     // assign any click on block layer --> close it
     $(document).click( function(e) {

	// if waiting for END response
	if (inEND)
	{
		restart();
		return;
	}

     	if (inBlock)
     	{
     		closeBlock();
     		e.preventDefault();
     		return;
     	}

     	if (inAnykey)  // return for ANYKEY, accepts mouse click
     	{
     		inAnykey = false;
     		hideAnykeyLayer();
     		waitKeyCallback();
     		e.preventDefault();
     		return;
    	}

     });

     //Make tap act as click
    //document.addEventListener('touchstart', function(e) {$(document).click(); }, false);   
     
     
	$(document).keydown(function(e) {

		if (!h_keydown(e)) return; // hook

		// if waiting for END response
		if (inEND)
		{
			var endYESresponse = getSysMessageText(SYSMESS_YES);
			var endNOresponse = getSysMessageText(SYSMESS_NO);
			if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
			if (!endNOresponse.length) endNOresponse = 'N'; 
			var endYESresponseCode = endYESresponse.charCodeAt(0);
			var endNOresponseCode = endNOresponse.charCodeAt(0);

			switch ( e.keyCode )
			{
				case endYESresponseCode:
				case 13: // return
				case 32: // space
					location.reload();
					break;
				case endNOresponseCode:
					inEND = false;
					sfxstopall();
					$('body').hide('slow');
					break;
			}
			return;
		}


		// if waiting for QUIT response
		if (inQUIT)
		{
			var endYESresponse = getSysMessageText(SYSMESS_YES);
			var endNOresponse = getSysMessageText(SYSMESS_NO);
			if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
			if (!endNOresponse.length) endNOresponse = 'N'; 
			var endYESresponseCode = endYESresponse.charCodeAt(0);
			var endNOresponseCode = endNOresponse.charCodeAt(0);

			switch ( e.keyCode )
			{
				case endYESresponseCode:
				case 13: // return
				case 32: // space
					inQUIT=false;
					e.preventDefault();
					waitKeyCallback();
					return;
				case endNOresponseCode:
					inQUIT=false;
					waitkey_callback_function.pop();
					hideAnykeyLayer();
					e.preventDefault();
					break;
			}
		}

		// ignore uninteresting keys
		switch ( e.keyCode )
		{
			case 9:  // tab   \ keys used during
			case 13: // enter / keyboard navigation
			case 16: // shift
			case 17: // ctrl
			case 18: // alt
			case 20: // caps lock
			case 91: // left Windows key
			case 92: // left Windows key
			case 93: // left Windows key
			case 225: // right alt
				// do not focus the input - the user was probably doing something else
				// (e.g. alt-tab'ing to another window)
				return;
		}


		if (inGetkey)  // return for getkey
     	{
     		setFlag(getkey_return_flag, e.keyCode);
     		getkey_return_flag = null;
     		inGetkey = false;
     		hideAnykeyLayer();
     		e.preventDefault();
     		waitKeyCallback();
     		return;
      	}

     	// Scroll text window using PgUp/PgDown
        if (e.keyCode==33)  // PgUp
        {
        	divTextScrollUp();
        	e.preventDefault();
        	return;
        }
        if (e.keyCode==34)  // PgDown
        {
        	divTextScrollDown();
        	return;
        }


     	if (inAnykey)  // return for anykey
     	{
     		inAnykey = false;
     		hideAnykeyLayer();
     		e.preventDefault();
     		waitKeyCallback();
     		return;
     	}

		// if keypress and block displayed, close it
     	if (inBlock)
     		{
     			closeBlock();
     			e.preventDefault();
     			return;
     		}


     	// if ESC pressed and transcript layer visible, close it
     	if ((inTranscript) &&  (e.keyCode == 27)) 
     		{
     			$('.transcript_layer').hide();
     			inTranscript = false;
     			e.preventDefault();
     			return;
     		}

	// focus the input if the user is likely to expect it
	// (but not if they're e.g. ctrl+c'ing some text)
	switch ( e.keyCode )
	{
		case 8: // backspace
		case 9: // tab
		case 13: // enter
			break;
		default:
			if ( !e.ctrlKey && !e.altKey ) focusInput();
	}

	});


    $(document).bind('wheel mousewheel',function(e)
    {
  		if((e.originalEvent.wheelDelta||-e.originalEvent.deltaY) > 0) divTextScrollUp(); else divTextScrollDown();
    });


	initialize();
	descriptionLoop();
	focusInput();
	
	h_post();  //hook

    // Start interrupt process
    setInterval( timer, TIMER_MILLISECONDS );

}

$('document').ready(
	function ()
	{
		start();
	}
	);

// VOCABULARY

vocabulary = [];
vocabulary.push([155, "1", 1]);
vocabulary.push([125, "172W", 1]);
vocabulary.push([156, "2", 1]);
vocabulary.push([124, "28S", 0]);
vocabulary.push([65, "ALL", 1]);
vocabulary.push([2, "AND", 5]);
vocabulary.push([67, "ASCE", 0]);
vocabulary.push([70, "ATTA", 0]);
vocabulary.push([48, "BAR", 1]);
vocabulary.push([109, "BARK", 1]);
vocabulary.push([55, "BARR", 1]);
vocabulary.push([90, "BEAR", 1]);
vocabulary.push([92, "BEHI", 1]);
vocabulary.push([130, "BLOC", 0]);
vocabulary.push([87, "BOAR", 0]);
vocabulary.push([94, "BONE", 1]);
vocabulary.push([123, "BOOK", 1]);
vocabulary.push([20, "BOX", 1]);
vocabulary.push([49, "BREA", 0]);
vocabulary.push([170, "BRIB", 0]);
vocabulary.push([115, "BROO", 1]);
vocabulary.push([95, "BURN", 0]);
vocabulary.push([126, "BUTT", 1]);
vocabulary.push([51, "CALL", 0]);
vocabulary.push([13, "CANO", 1]);
vocabulary.push([133, "CAR", 1]);
vocabulary.push([50, "CHAI", 1]);
vocabulary.push([29, "CHAT", 0]);
vocabulary.push([179, "CLEA", 0]);
vocabulary.push([67, "CLIM", 0]);
vocabulary.push([172, "CLUE", 0]);
vocabulary.push([24, "COIN", 1]);
vocabulary.push([25, "COMP", 1]);
vocabulary.push([146, "COOR", 1]);
vocabulary.push([21, "CORN", 1]);
vocabulary.push([201, "COTT", 1]);
vocabulary.push([143, "CROS", 0]);
vocabulary.push([48, "CROW", 1]);
vocabulary.push([52, "CUPB", 1]);
vocabulary.push([81, "CUT", 0]);
vocabulary.push([10, "D", 1]);
vocabulary.push([61, "DAGG", 1]);
vocabulary.push([10, "DESC", 0]);
vocabulary.push([171, "DESP", 0]);
vocabulary.push([76, "DIG", 0]);
vocabulary.push([112, "DISC", 1]);
vocabulary.push([112, "DISK", 1]);
vocabulary.push([145, "DISM", 0]);
vocabulary.push([82, "DOG", 1]);
vocabulary.push([42, "DOOR", 1]);
vocabulary.push([10, "DOWN", 0]);
vocabulary.push([40, "DRIE", 1]);
vocabulary.push([157, "DRIN", 0]);
vocabulary.push([101, "DROP", 0]);
vocabulary.push([140, "DRY", 0]);
vocabulary.push([40, "DRYE", 1]);
vocabulary.push([3, "E", 0]);
vocabulary.push([131, "EAR", 1]);
vocabulary.push([131, "EARS", 1]);
vocabulary.push([3, "EAST", 0]);
vocabulary.push([149, "EAT", 0]);
vocabulary.push([98, "EGG", 1]);
vocabulary.push([11, "ENTE", 1]);
vocabulary.push([11, "ENTER", 0]);
vocabulary.push([15, "EXAM", 0]);
vocabulary.push([12, "EXIT", 0]);
vocabulary.push([84, "EXTI", 0]);
vocabulary.push([28, "FARM", 1]);
vocabulary.push([88, "FEED", 0]);
vocabulary.push([181, "FILL", 0]);
vocabulary.push([85, "FIRE", 1]);
vocabulary.push([85, "FIREPLACE", 1]);
vocabulary.push([154, "FONT", 0]);
vocabulary.push([18, "FOUN", 1]);
vocabulary.push([100, "G", 0]);
vocabulary.push([200, "GALO", 1]);
vocabulary.push([147, "GARD", 1]);
vocabulary.push([26, "GATE", 1]);
vocabulary.push([161, "GENE", 1]);
vocabulary.push([100, "GET", 0]);
vocabulary.push([30, "GIVE", 0]);
vocabulary.push([202, "GLOV", 1]);
vocabulary.push([180, "GRAB", 0]);
vocabulary.push([69, "GRAP", 1]);
vocabulary.push([119, "GREE", 1]);
vocabulary.push([97, "GUAR", 1]);
vocabulary.push([172, "H", 0]);
vocabulary.push([158, "HAND", 1]);
vocabulary.push([75, "HEAP", 1]);
vocabulary.push([172, "HELP", 0]);
vocabulary.push([148, "HERB", 1]);
vocabulary.push([185, "HIGH", 1]);
vocabulary.push([172, "HINT", 0]);
vocabulary.push([162, "HOLE", 1]);
vocabulary.push([46, "HONE", 1]);
vocabulary.push([69, "HOOK", 1]);
vocabulary.push([82, "HOT", 1]);
vocabulary.push([176, "HOTP", 1]);
vocabulary.push([142, "HUT", 1]);
vocabulary.push([104, "I", 0]);
vocabulary.push([95, "IGNI", 0]);
vocabulary.push([95, "IGNITION", 1]);
vocabulary.push([11, "IN", 0]);
vocabulary.push([43, "INSE", 0]);
vocabulary.push([11, "INSI", 0]);
vocabulary.push([104, "INVE", 0]);
vocabulary.push([46, "JAR", 1]);
vocabulary.push([73, "JUMP", 0]);
vocabulary.push([176, "KETT", 1]);
vocabulary.push([150, "KEY", 1]);
vocabulary.push([163, "KILL", 0]);
vocabulary.push([137, "KISS", 0]);
vocabulary.push([134, "KNOB", 1]);
vocabulary.push([105, "L", 0]);
vocabulary.push([64, "LADD", 1]);
vocabulary.push([39, "LAMP", 1]);
vocabulary.push([66, "LAY", 0]);
vocabulary.push([164, "LEAN", 0]);
vocabulary.push([73, "LEAP", 0]);
vocabulary.push([12, "LEAV", 0]);
vocabulary.push([12, "LEAVE", 0]);
vocabulary.push([117, "LEDG", 1]);
vocabulary.push([54, "LEVE", 1]);
vocabulary.push([63, "LID", 1]);
vocabulary.push([66, "LIE", 0]);
vocabulary.push([116, "LIFT", 0]);
vocabulary.push([203, "LIFTER", 1]);
vocabulary.push([83, "LIGH", 0]);
vocabulary.push([83, "LIGHT", 1]);
vocabulary.push([108, "LOAD", 0]);
vocabulary.push([105, "LOOK", 0]);
vocabulary.push([159, "LOUD", 1]);
vocabulary.push([122, "MACH", 1]);
vocabulary.push([31, "MAGA", 1]);
vocabulary.push([60, "MAKE", 0]);
vocabulary.push([28, "MAN", 1]);
vocabulary.push([20, "MATC", 1]);
vocabulary.push([110, "MOVE", 0]);
vocabulary.push([1, "N", 0]);
vocabulary.push([59, "NAIL", 1]);
vocabulary.push([5, "NE", 0]);
vocabulary.push([57, "NEED", 1]);
vocabulary.push([96, "NICH", 1]);
vocabulary.push([1, "NORTH", 0]);
vocabulary.push([5, "NORTHEAST", 0]);
vocabulary.push([123, "NOTE", 1]);
vocabulary.push([183, "NOTH", 1]);
vocabulary.push([58, "NOTI", 1]);
vocabulary.push([6, "NW", 0]);
vocabulary.push([12, "O", 0]);
vocabulary.push([84, "OFF", 0]);
vocabulary.push([30, "OFFE", 0]);
vocabulary.push([83, "ON", 0]);
vocabulary.push([83, "ONTO", 1]);
vocabulary.push([27, "OPEN", 0]);
vocabulary.push([12, "OUT", 0]);
vocabulary.push([16, "P", 0]);
vocabulary.push([17, "PADD", 1]);
vocabulary.push([37, "PANT", 1]);
vocabulary.push([114, "PAPE", 1]);
vocabulary.push([71, "PARA", 1]);
vocabulary.push([175, "PAT", 0]);
vocabulary.push([132, "PATT", 1]);
vocabulary.push([141, "PAY", 0]);
vocabulary.push([167, "PEEL", 0]);
vocabulary.push([175, "PET", 0]);
vocabulary.push([201, "PIEC", 1]);
vocabulary.push([62, "PIER", 0]);
vocabulary.push([75, "PILE", 1]);
vocabulary.push([22, "PIT", 1]);
vocabulary.push([44, "PLAC", 1]);
vocabulary.push([165, "PLAN", 1]);
vocabulary.push([176, "PLAT", 1]);
vocabulary.push([174, "PLAY", 0]);
vocabulary.push([135, "PLIE", 1]);
vocabulary.push([130, "PLUG", 0]);
vocabulary.push([35, "POCK", 1]);
vocabulary.push([144, "POOL", 1]);
vocabulary.push([129, "POT", 1]);
vocabulary.push([24, "POUN", 1]);
vocabulary.push([47, "PRES", 0]);
vocabulary.push([62, "PRIC", 0]);
vocabulary.push([86, "PRIS", 0]);
vocabulary.push([164, "PROP", 0]);
vocabulary.push([86, "PRY", 0]);
vocabulary.push([47, "PULL", 0]);
vocabulary.push([47, "PUSH", 0]);
vocabulary.push([44, "PUT", 0]);
vocabulary.push([106, "QQ", 0]);
vocabulary.push([106, "QUIT", 0]);
vocabulary.push([105, "R", 0]);
vocabulary.push([74, "RAFT", 1]);
vocabulary.push([151, "RAM", 1]);
vocabulary.push([56, "RATT", 0]);
vocabulary.push([34, "READ", 0]);
vocabulary.push([118, "RED", 1]);
vocabulary.push([105, "REDE", 0]);
vocabulary.push([102, "REMO", 0]);
vocabulary.push([72, "RIDG", 1]);
vocabulary.push([184, "RIP", 0]);
vocabulary.push([153, "RL", 0]);
vocabulary.push([185, "ROAD", 1]);
vocabulary.push([53, "ROD", 1]);
vocabulary.push([79, "ROPE", 1]);
vocabulary.push([152, "RS", 0]);
vocabulary.push([75, "RUBB", 1]);
vocabulary.push([2, "S", 0]);
vocabulary.push([107, "SAVE", 0]);
vocabulary.push([173, "SCOR", 0]);
vocabulary.push([33, "SCRE", 1]);
vocabulary.push([138, "SCRO", 1]);
vocabulary.push([138, "SCROLL", 1]);
vocabulary.push([7, "SE", 0]);
vocabulary.push([23, "SEAR", 0]);
vocabulary.push([171, "SEND", 0]);
vocabulary.push([177, "SHAF", 1]);
vocabulary.push([56, "SHAK", 0]);
vocabulary.push([114, "SHEE", 1]);
vocabulary.push([160, "SHOU", 0]);
vocabulary.push([91, "SIGN", 1]);
vocabulary.push([182, "SKET", 1]);
vocabulary.push([49, "SMAS", 0]);
vocabulary.push([168, "SMEL", 0]);
vocabulary.push([168, "SNIF", 0]);
vocabulary.push([2, "SOUTH", 0]);
vocabulary.push([8, "SOUTHWEST", 0]);
vocabulary.push([77, "SPAD", 1]);
vocabulary.push([29, "SPEA", 0]);
vocabulary.push([148, "SPIC", 1]);
vocabulary.push([62, "STAB", 0]);
vocabulary.push([178, "STAL", 1]);
vocabulary.push([164, "STAN", 0]);
vocabulary.push([93, "STAR", 0]);
vocabulary.push([173, "STAT", 0]);
vocabulary.push([180, "STEA", 0]);
vocabulary.push([115, "STIC", 1]);
vocabulary.push([176, "STOV", 1]);
vocabulary.push([79, "STRA", 1]);
vocabulary.push([185, "STRE", 1]);
vocabulary.push([175, "STRO", 0]);
vocabulary.push([8, "SW", 0]);
vocabulary.push([128, "SWIM", 0]);
vocabulary.push([113, "SWIN", 0]);
vocabulary.push([180, "SWIP", 0]);
vocabulary.push([120, "SWIT", 1]);
vocabulary.push([120, "SWITCH", 0]);
vocabulary.push([100, "T", 0]);
vocabulary.push([45, "TABL", 1]);
vocabulary.push([100, "TAKE", 0]);
vocabulary.push([29, "TALK", 0]);
vocabulary.push([184, "TEAR", 0]);
vocabulary.push([2, "THEN", 5]);
vocabulary.push([89, "THRO", 0]);
vocabulary.push([70, "TIE", 0]);
vocabulary.push([18, "TIER", 1]);
vocabulary.push([111, "TILL", 1]);
vocabulary.push([172, "TIP", 0]);
vocabulary.push([139, "TRAC", 1]);
vocabulary.push([68, "TREE", 1]);
vocabulary.push([120, "TRIP", 0]);
vocabulary.push([204, "TRIPSWITCH", 1]);
vocabulary.push([37, "TROU", 1]);
vocabulary.push([68, "TRUN", 1]);
vocabulary.push([110, "TURN", 0]);
vocabulary.push([9, "U", 1]);
vocabulary.push([32, "UNDE", 1]);
vocabulary.push([80, "UNDO", 0]);
vocabulary.push([27, "UNLO", 0]);
vocabulary.push([38, "UNSC", 0]);
vocabulary.push([80, "UNTI", 0]);
vocabulary.push([9, "UP", 0]);
vocabulary.push([169, "VACU", 1]);
vocabulary.push([4, "W", 0]);
vocabulary.push([16, "WAIT", 0]);
vocabulary.push([121, "WALL", 1]);
vocabulary.push([36, "WARD", 1]);
vocabulary.push([179, "WASH", 0]);
vocabulary.push([19, "WATE", 1]);
vocabulary.push([14, "WAVE", 0]);
vocabulary.push([103, "WEAR", 0]);
vocabulary.push([4, "WEST", 0]);
vocabulary.push([136, "WIFE", 1]);
vocabulary.push([201, "WOOL", 1]);
vocabulary.push([15, "X", 0]);



// SYS MESSAGES

total_sysmessages=68;

sysmessages = [];

sysmessages[0] = "It's too dark to see anything.<br>";
sysmessages[1] = "\nYOU NOTICE:<br>";
sysmessages[2] = "What do you want to do now?";
sysmessages[3] = "{EXITS|@38|1000}";
sysmessages[4] = "Please enter your next order";
sysmessages[5] = "Please type in your orders";
sysmessages[6] = "Sorry, not understood.<br>";
sysmessages[7] = "No path leads in that direction.<br>";
sysmessages[8] = "Command can't be executed.<br>";
sysmessages[9] = "INVENTORY:";
sysmessages[10] = " (worn)";
sysmessages[11] = "Nothing<br>";
sysmessages[12] = "Are you certain?<br>";
sysmessages[13] = "\nTry again?<br>";
sysmessages[14] = "Thanks for playing a game from  {CLASS|center|ZENOBI SOFTWARE\n\n}<br>";
sysmessages[15] = "DONE<br>";
sysmessages[16] = "                               ?<br>";
sysmessages[17] = "You have typed<br>";
sysmessages[18] = " turns have been attempted.";
sysmessages[19] = "s<br>";
sysmessages[20] = ".<br>";
sysmessages[21] = "\nYour rating is ";
sysmessages[22] = " per cent.<br>";
sysmessages[23] = "You're not wearing it.<br>";
sysmessages[24] = "You can't remove it as both your hands are full.<br>";
sysmessages[25] = "Are there two?<br>";
sysmessages[26] = "Do you see it ?<br>";
sysmessages[27] = "You're carrying the limit.<br>";
sysmessages[28] = "Command can't be executed.<br>";
sysmessages[29] = "You're already wearing it.<br>";
sysmessages[30] = "Y<br>";
sysmessages[31] = "N<br>";
sysmessages[32] = "More...<br>";
sysmessages[33] = "><br>";
sysmessages[34] = "<br>";
sysmessages[35] = "Time passes...<br>";
sysmessages[36] = "";
sysmessages[37] = "";
sysmessages[38] = "";
sysmessages[39] = "";
sysmessages[40] = "Command can't be executed.<br>";
sysmessages[41] = "Command can't be executed.<br>";
sysmessages[42] = "You can't remove it as both your hands are full.<br>";
sysmessages[43] = "You're carrying the limit.<br>";
sysmessages[44] = "You put {OREF} into<br>";
sysmessages[45] = "{OREF} is not into<br>";
sysmessages[46] = "<br>";
sysmessages[47] = "<br>";
sysmessages[48] = "<br>";
sysmessages[49] = "Command can't be executed.<br>";
sysmessages[50] = "Command can't be executed.<br>";
sysmessages[51] = ".<br>";
sysmessages[52] = "That is not into<br>";
sysmessages[53] = "nothing at all<br>";
sysmessages[54] = "File not found.<br>";
sysmessages[55] = "File corrupt.<br>";
sysmessages[56] = "I/O error. File not saved.<br>";
sysmessages[57] = "Directory full.<br>";
sysmessages[58] = "Please enter savegame name you used when saving the game status.";
sysmessages[59] = "Invalid savegame name. Please check the name you entered is correct, and make sure you are trying to load the game from the same browser you saved it.<br>";
sysmessages[60] = "Please enter savegame name. Remember to note down the name you choose, as it will be requested in order to restore the game status.";
sysmessages[61] = "<br>";
sysmessages[62] = "Sorry? Please try other words.<br>";
sysmessages[63] = "Here<br>";
sysmessages[64] = "you can see<br>";
sysmessages[65] = "you can see<br>";
sysmessages[66] = "inside you see<br>";
sysmessages[67] = "on top you see<br>\n\n\n\n\n\n";

// USER MESSAGES

total_messages=12;

messages = [];

messages[1000] = "Exits: ";
messages[1001] = "You can't see any exits\n";
messages[1003] = "{ACTION|nort|nort}";
messages[1004] = "{ACTION|sout|sout}";
messages[1005] = "{ACTION|east|east}";
messages[1006] = "{ACTION|west|west}";
messages[1007] = "{ACTION|ne|ne}";
messages[1009] = "{ACTION|se|se}";
messages[1010] = "{ACTION|sw|sw}";
messages[1012] = "{ACTION|down|down}";
messages[1013] = "{ACTION|insi|insi}";
messages[1014] = "{ACTION|out|out}\n\n\n\n\n\n";

// WRITE MESSAGES

total_writemessages=482;

writemessages = [];

writemessages[0] = "RESPONSE_START";
writemessages[1] = "RESPONSE_USER";
writemessages[2] = "The path is on the other side of the locked gate.";
writemessages[3] = "OOPS! Stalagmites pierce your feet and you start to bleed to death.";
writemessages[4] = "The ladder's rungs protect your feet from the lethal stalagmites";
writemessages[5] = "You can't do that...yet.";
writemessages[6] = "Don't be crazy!";
writemessages[7] = "The path is on the other side of the locked gate.";
writemessages[8] = "OOPS! Stalagmites pierce your feet and you start to bleed to death.";
writemessages[9] = "The ladder's rungs protect your feet from the lethal stalagmites";
writemessages[10] = "You can't do that...yet.";
writemessages[11] = "The guard squares his shoulders and gives you an icy stare.";
writemessages[12] = "How ?";
writemessages[13] = "\nstOh dear, you misjudge the spee d of an oncoming Porsche and you                 are sent into ob livion!";
writemessages[14] = "You'll get lost if you do.";
writemessages[15] = "With the aid of the compass, you manage to get through the forest and find yourself on the shore of a large lake.";
writemessages[16] = "You'll get lost if you do.";
writemessages[17] = "Using the compass to help you, you retrace your steps back to the outskirts of the forest.";
writemessages[18] = "You slip on the oily steps. With a severely bruised ego, you climb back out.";
writemessages[19] = "The galoshes prevent you from slipping as you enter the pit.";
writemessages[20] = "stYou can't do that yet, it's on                 the far side of the dam.";
writemessages[21] = "stEntering the canoe, you paddle                 to the other sid e...                            ";
writemessages[22] = "The canoe hits a hidden rock and sinks. You leap onto the bank just in time.";
writemessages[23] = "Don't be crazy!";
writemessages[24] = "You enter the trunk, and stumble down a very steep path to an underground cavern.";
writemessages[25] = "What for, you've just started?!";
writemessages[26] = "Why ?";
writemessages[27] = "Could you put that another way ?";
writemessages[28] = "stYou can't - it's far too narro w.";
writemessages[29] = "The farmer says you may only enter when you find his missing key.";
writemessages[30] = "There's a strong chain holding the door closed.";
writemessages[31] = "Please be more specific...";
writemessages[32] = "You can't do that...yet.";
writemessages[33] = "&#34;Oy, I didn't say you could take me trousers,&#34; roars the farmer taking them from you. Sheepishly you leave.";
writemessages[34] = "&#34;Oy, I didn't say you could take me trousers,&#34; roars the farmer taking them from you. Sheepishly you leave.";
writemessages[35] = "st&#34;And you can return my pound,&#34;                 he scowls, takin g it from you.";
writemessages[36] = "The man waves back, climbs into the canoe and paddles across. He gives you the paddle and wishes you luck with your quest.";
writemessages[37] = "Why ?";
writemessages[38] = "Its mooring rope dangles in the water.";
writemessages[39] = "stYou can't do that yet, it's on                 the far side of the dam.";
writemessages[40] = "Cascading water over the tiers is almost mesmerizing.";
writemessages[41] = "Floating in the water is a soggy box of matches.";
writemessages[42] = "You've done that.";
writemessages[43] = "stWet and soggy.";
writemessages[44] = "Nice and dry.";
writemessages[45] = "It's just what it says it is....";
writemessages[46] = "You can't do that...yet.";
writemessages[47] = "It's just what it says it is....";
writemessages[48] = "Standard Royal Mint issue.";
writemessages[49] = "On-screen instructions instruct you to enter the coordinates thus:-\n\nstLeave one space between the tw o coordinates  e.g. 123N 123E";
writemessages[50] = "The lit screen is blank.";
writemessages[51] = "It hasn't been switched on yet.";
writemessages[52] = "Suitable for getting you through a forest.";
writemessages[53] = "He's holding a reel from a fish- ing rod in his hand, which he seems to be mending.";
writemessages[54] = "It's titled: &#34;FISHING DIGEST&#34;. At the top left hand corner is a sketch of a reddish-coloured fish.";
writemessages[55] = "You really want a description?";
writemessages[56] = "You've found something...";
writemessages[57] = "You've done that.";
writemessages[58] = "It's locked.";
writemessages[59] = "They belong to the farmer.";
writemessages[60] = "A small tag tells you that there is only a little paraffin in the lamp.";
writemessages[61] = "A small tag tells you that there is only a little paraffin in the lamp.";
writemessages[62] = "It's empty.";
writemessages[63] = "Has a hinged front door through which clothing is placed to dry.";
writemessages[64] = "A small heap of soil on the other side stops it opening wide enough to enter. The gap is a mere few inches.";
writemessages[65] = "It needs a key.";
writemessages[66] = "Standing alongside the fireplace is a crowbar.";
writemessages[67] = "You've done that.";
writemessages[68] = "&#34;Stop fiddling about with my table.&#34; complains the wife.";
writemessages[69] = "A label states &#34;BEST QUALITY&#34;   {CLASS|center|           VACUUM SEALED}           Break vacuum to open.";
writemessages[70] = "A label states &#34;BEST QUALITY&#34;   {CLASS|center|           VACUUM SEALED}           Break vacuum to open.";
writemessages[71] = "The lid has been pierced and removed.";
writemessages[72] = "Looks very strong.";
writemessages[73] = "You've found something...";
writemessages[74] = "You've done that.";
writemessages[75] = "Closed with a simple lock.";
writemessages[76] = "About 30cm long and has a 2cm, square end.";
writemessages[77] = "stSmall and light.";
writemessages[78] = "There is an stN at the end, and a 2mm hole underneath.";
writemessages[79] = "It is a 2mm nail.";
writemessages[80] = "It is a 2mm nail.";
writemessages[81] = "Very sharp and pointed.";
writemessages[82] = "It's just what it says it is....";
writemessages[83] = "stGiving the tree a sound tap, y oudeduce (correctly) that it is hollow.";
writemessages[84] = "A sunshade very much like an umbrella.";
writemessages[85] = "You've found something...";
writemessages[86] = "You've done that.";
writemessages[87] = "stUnlikely to fall apart or sink !";
writemessages[88] = "It's just a tall heap of rubble which appears worthless.";
writemessages[89] = "It's just what it says it is....";
writemessages[90] = "It has been cut through, except for one last resistant strand.";
writemessages[91] = "It's just what it says it is....";
writemessages[92] = "The adventurer's best friend!";
writemessages[93] = "Standing alongside the fireplace is a crowbar.";
writemessages[94] = "You've done that.";
writemessages[95] = "Big and mean.";
writemessages[96] = "Not much meat on it.";
writemessages[97] = "It needs a key.";
writemessages[98] = "You've found something...";
writemessages[99] = "You've done that.";
writemessages[100] = "Nothing less than a Sherman tank will get past him!";
writemessages[101] = "Would make a nice omelette.";
writemessages[102] = "stIt seems to be movable.";
writemessages[103] = "Ancient. Mounted on thick stubby legs, and is stuck on 4/7&#124;d.";
writemessages[104] = "For booting up a computer.";
writemessages[105] = "It has what appears to be some coordinates written on it.";
writemessages[106] = "It has what appears to be some coordinates written on it.";
writemessages[107] = "It has two holes: one near the bottom, and the other dead centre at the bottom.  Perhaps something should go in one hole and out the other...";
writemessages[108] = "Has a long nail sticking out the bottom.";
writemessages[109] = "The &#34;up and down&#34; variety.";
writemessages[110] = "stOn it lies a sheet of paper.";
writemessages[111] = "Old and rusty. Looks decidedly unsafe, especially for the naked hand.";
writemessages[112] = "You've found something...";
writemessages[113] = "You've done that.";
writemessages[114] = "It's the infamous Repelling-Beam Machine. On its side is a bright red button and a fine-adjustment knob.";
writemessages[115] = "It contains a stew and is fragrant with many spices.";
writemessages[116] = "There are many scuff marks on the pattern, intimating that the tree is not your normal variety!";
writemessages[117] = "Dirty: needs a good wash.";
writemessages[118] = "The &#34;long nose&#34; sort.  Can reach where your fingers can't.";
writemessages[119] = "Seems to be some form of papyrus";
writemessages[120] = "It seems to fill the whole shed. Its wheels are huge, giving it plenty of clearance over bumpy terrain. There is a foothold to facilitate climbing onto the tractor.";
writemessages[121] = "A nearby thermometer reads 2@C.";
writemessages[122] = "You see many different herbs\nall neatly labelled.";
writemessages[123] = "It would enhance any respectable dish.";
writemessages[124] = "It's marked &#34;OBSERVATORY&#34;";
writemessages[125] = "Powerful enough to drive a lift";
writemessages[126] = "but it is off at present.";
writemessages[127] = "Powerful enough to drive a lift";
writemessages[128] = "and it is now working.";
writemessages[129] = "It's just what it says it is....";
writemessages[130] = "It is very fragrant.";
writemessages[131] = "stFor use during the long wee hours of work.";
writemessages[132] = "Just wide enough for the dog.";
writemessages[133] = "stPointed and sharp&#59; they could easily puncture your shoes and pierce the soles of your feet.";
writemessages[134] = "It's just what it says it is....";
writemessages[135] = "Made of supple rubber.";
writemessages[136] = "The farmer used them to exclude unwanted noise.";
writemessages[137] = "Something's going right: it's your size!";
writemessages[138] = "Nothing further is revealed.";
writemessages[139] = "Old Father Time taps his foot patiently...";
writemessages[140] = "You've found something...";
writemessages[141] = "You've done that.";
writemessages[142] = "You've found something...";
writemessages[143] = "You've done that.";
writemessages[144] = "You've found something...";
writemessages[145] = "You've done that.";
writemessages[146] = "Nothing further is revealed.";
writemessages[147] = "Could you put that another way ?";
writemessages[148] = "Could you put that another way ?";
writemessages[149] = "&#34;Oy, I didn't say you could come onto my property,&#34; cries the farmer.  You flee as he aims a shotgun at you.";
writemessages[150] = "stNo thanks, looks too boring.";
writemessages[151] = "Adventures should be SO simple!";
writemessages[152] = "How ?";
writemessages[153] = "How ?";
writemessages[154] = "How ?";
writemessages[155] = "With what ?";
writemessages[156] = "You insert the key in the lock and turn it.  The door creaks open....";
writemessages[157] = "How ?";
writemessages[158] = "With what ?";
writemessages[159] = "Nothing further is revealed.";
writemessages[160] = "The farmer says you may only enter when you find his missing key.";
writemessages[161] = "The farmer seems not to have heard you.";
writemessages[162] = "You can't do that...yet.";
writemessages[163] = "&#34;Are you the one who's going to save London?&#34; he asks.  Seeing you nod humbly he goes on:\n&#34;It's usually `2 to pass through here, but Zenobi travellers can go through for a quid.&#34;";
writemessages[164] = "&#34;Chatty sort, aren't ya?&#34; says the guard. &#34;`1 or stay here...&#34;";
writemessages[165] = "She ignores you and gets on with her chores.";
writemessages[166] = "&#34;Ah, now I can hear you,&#34; says the farmer removing some pieces of cotton wool from his ears. &#34;If you can help me find the key to my tractor, you may enter my house&#59; meanwhile you may come on to my land.&#34;  And with that he opens the gate and bids you enter.";
writemessages[167] = "Nothing to say.";
writemessages[168] = "st&#34;Thanks guv,&#34; says the guard a s               he pockets the c oin, &#34;I'm off   duty now&#59; you ca n come and go asyou please.&#34;";
writemessages[169] = "With what ?";
writemessages[170] = "The farmer accepts the magazine and opens it. There is a tinkle as the tractor key falls out of the magazine onto the ground. Smiling sheepishly as he stoops to retrieve it, he says: &#34;I forgot I'd wedged it in there&#59; you may enter my house, and call me if I can be of service.&#34;";
writemessages[171] = "&#34;What the blazes are you doing with my trousers?&#34; grumbles the farmer taking them from you.";
writemessages[172] = "Could you put that another way ?";
writemessages[173] = "You give the rare herb to the farmer's wife. She thanks you profusely and adds it to the pot over the fire.\nst&#34;Take this key - it should hel p you.  The professor lodged her e and I found it in his room aft erthe poor man died.&#34;";
writemessages[174] = "You give the rare herb to the farmer's wife. She thanks you profusely and adds it to the pot over the fire.\nst&#34;Take this key - it should hel p you.  The professor lodged her e and I found it in his room aft erthe poor man died.&#34;";
writemessages[175] = "&#34;I don't see it,&#34; she cries.";
writemessages[176] = "Adventures should be SO simple!";
writemessages[177] = "Not wanted.";
writemessages[178] = "stNo thanks, looks too boring.";
writemessages[179] = "Do not dig - leaking Methane gas";
writemessages[180] = "Do not dig - leaking Methane gas";
writemessages[181] = "PLEASE DON'T FEED THE ANIMALS.";
writemessages[182] = "You can't do that...yet.";
writemessages[183] = "{CLASS|center|28@S  172@W}";
writemessages[184] = "Inside, in a neat handwriting is a page of instructions:-\n\n1. Punch the coordinates into\n   the computer.\n\n2. Open the dome.\n\n3. Switch on machine.\n\n4. Turn fine adjustment knob\n\n5. Don't forget to bl\n\nThe advisory stops dead, and you wonder what is missing...";
writemessages[185] = "QQ   =  QUIT    I   =  INVENTORY R?L  =  LOOK    G?T =  GET?TAKE X    =  EXAMINE O   =  OUT?LEAVE P    =  WAIT\nRS   =  RAM SAVE  RL=  RAM LOAD\n\nNOTE:-\n\nThis game employs occasional inputs of THREE words (although only the first two are processed by the Quill).\n\n\nTyping   FONT 1    and    FONT 2 gives alternative typeface.\n\n\nHandle fragile objects carefully";
writemessages[186] = "{CLASS|center|28@S  172@W}";
writemessages[187] = "With what ?";
writemessages[188] = "As surreptitiously as possible you unscrew the lamp.";
writemessages[189] = "&#34;What the hell are you doing to my lamp?&#34; says the farmer.  He chases you off his land and says never to return.";
writemessages[190] = "You've done that.";
writemessages[191] = "You insert the wet box into the drier and close the door.  The tumble drier spins around for a while, and when it stops you open the door and remove the box which is now dry.";
writemessages[192] = "Why ?";
writemessages[193] = "You've done that.";
writemessages[194] = "Inserting the crowbar under the chain, you give a mighty push and the chain snaps&#59; the door creaks open....";
writemessages[195] = "You've done that.";
writemessages[196] = "You fit the rod into the recess, effectively making a lever.";
writemessages[197] = "stPushing the nail into the hole                 on the broomstic k, it emerges   out the bottom m aking a useful  paper-lifter.";
writemessages[198] = "You can't do that...yet.";
writemessages[199] = "Nice try, but nothing happens!";
writemessages[200] = "Could you put that another way ?";
writemessages[201] = "Could you put that another way ?";
writemessages[202] = "Carefully you put it down.";
writemessages[203] = "Carefully you put it down.";
writemessages[204] = "Carefully you put it down.";
writemessages[205] = "stYou place the jar near the bea r               and retreat. You  see the bear   vainly attemptin g to remove the lid.";
writemessages[206] = "You place the jar gently on the ground and take a few steps back.  The bear picks up the jar and lopes out of sight to enjoy his feast.";
writemessages[207] = "You lay the ladder over the stalagmites.";
writemessages[208] = "You stand the ladder in front of you.";
writemessages[209] = "Carefully you put it down.";
writemessages[210] = "Nice try, but nothing happens!";
writemessages[211] = "stThe lift begins to move.";
writemessages[212] = "There is no power.";
writemessages[213] = "stThe lift begins to move.";
writemessages[214] = "Nice try, but nothing happens!";
writemessages[215] = "Adventures should be SO simple!";
writemessages[216] = "The generator hums into life, indicating that the lift now has power.";
writemessages[217] = "You've done that.";
writemessages[218] = "Adventures should be SO simple!";
writemessages[219] = "With what ?";
writemessages[220] = "You pull the nail out of the wall with the pliers.";
writemessages[221] = "You've done that.";
writemessages[222] = "stStanding on top of the ladder,                 you pull the hoo k out of the    rock. The inerti a from doing so,causes the ladde r to slip from  under you and yo u drop the hook.Displaying a spl it-second reflexaction, you grab  hold of a      convenient ridge , but you can   feel your finger s slipping....";
writemessages[223] = "How ?";
writemessages[224] = "Nice try, but nothing happens!";
writemessages[225] = "There is a rumble as the ceiling moves.";
writemessages[226] = "There is no power.";
writemessages[227] = "There is a rumble as the ceiling moves.";
writemessages[228] = "Could you put that another way ?";
writemessages[229] = "There is no power.";
writemessages[230] = "You can't do that...yet.";
writemessages[231] = "stThe machine starts up with a l owgrowl, and quickly builds up t o               a high-pitched s cream.";
writemessages[232] = "Nice try, but nothing happens!";
writemessages[233] = "With what ?";
writemessages[234] = "How ?";
writemessages[235] = "How ?";
writemessages[236] = "&#34;I see you've been trying to open my cupboard,&#34; the farmer says, responding to your call. &#34;Heard you shaking it a mile away - it's simpler with a key!&#34; He produces a key and opens the cupboard for you.";
writemessages[237] = "The farmer appears.\n&#34;You don't need my help&#59; stop wasting my time.&#34;";
writemessages[238] = "The farmer appears.\n&#34;You don't need my help&#59; stop wasting my time.&#34;";
writemessages[239] = "Why ?";
writemessages[240] = "Giving the seemingly empty barrel a good shake, you are rewarded by an object falling out...";
writemessages[241] = "You've done that.";
writemessages[242] = "Wobbling the compass needle on the rusty nail, you make a crude compass.";
writemessages[243] = "With what ?";
writemessages[244] = "Oops!";
writemessages[245] = "You pierce the lid with the dagger.  There is a hiss as air fills the vacuum, and the lid unscrews easily which you discard.";
writemessages[246] = "You've done that.";
writemessages[247] = "With what ?";
writemessages[248] = "You lay the ladder over the stalagmites.";
writemessages[249] = "Not here.";
writemessages[250] = "Why ?";
writemessages[251] = "&#34;Oy, I didn't say you could come onto my property,&#34; cries the farmer.  You flee as he aims a shotgun at you.";
writemessages[252] = "Why ?";
writemessages[253] = "Not here.";
writemessages[254] = "stWhile you're holding it ?";
writemessages[255] = "stWhile you're holding it ?";
writemessages[256] = "With what ?";
writemessages[257] = "Taking a deep breath, you trudge up the steep slope to the very top.";
writemessages[258] = "Too late!";
writemessages[259] = "Why ?";
writemessages[260] = "Could you put that another way ?";
writemessages[261] = "You tie the rope to the hook.";
writemessages[262] = "You've done that.";
writemessages[263] = "With what ?";
writemessages[264] = "If you insist.";
writemessages[265] = "\nstYou fall to the ground breakin g your legs and arms.\n                ";
writemessages[266] = "If you insist.";
writemessages[267] = "You do an Olympic jump, but miss the hook by a whisker!";
writemessages[268] = "Don't be crazy!";
writemessages[269] = "stYou reduce the pile of rubble toa worthless heap, and in doing                 so uncover a sho rt dagger.";
writemessages[270] = "With what ?";
writemessages[271] = "You've done that.";
writemessages[272] = "With what ?";
writemessages[273] = "BOOM! A badly-sealed methane pipe explodes, blowing off your feet.";
writemessages[274] = "Please be more specific...";
writemessages[275] = "You dig a hole and find nothing.";
writemessages[276] = "Digging deeper you find a pair of old galoshes.";
writemessages[277] = "You've done that.";
writemessages[278] = "Not here.";
writemessages[279] = "You untie the rope and pick it up.";
writemessages[280] = "The knot's too tight, you will need to cut it.";
writemessages[281] = "You've done that.";
writemessages[282] = "With what ?";
writemessages[283] = "With what ?";
writemessages[284] = "The dagger slices through most of the rope. There is however, a resistant strand that will not succumb to the dagger's edge.";
writemessages[285] = "Nice try, but nothing happens!";
writemessages[286] = "Could you put that another way ?";
writemessages[287] = "You can't do that...yet.";
writemessages[288] = "There is no power.";
writemessages[289] = "You light the lamp.";
writemessages[290] = "You can't do that...yet.";
writemessages[291] = "With what ?";
writemessages[292] = "The paraffin is finished.";
writemessages[293] = "How ?";
writemessages[294] = "You extinguish the lamp.";
writemessages[295] = "Could you put that another way ?";
writemessages[296] = "stThe farmer appears in a rage. &#34;Oy, what do you think you're doing?  You can't just walk into people's homes and smash their bleedin' cupboards.&#34;\nHe chucks you off his property.";
writemessages[297] = "stYou'll need to pull it out - n otprise it.";
writemessages[298] = "Could you put that another way ?";
writemessages[299] = "The mooring rope is still tied.";
writemessages[300] = "What's the point of that without a paddle?";
writemessages[301] = "Clambering onto the raft, you paddle it across to the other side of the lake.";
writemessages[302] = "What's the point of that without a paddle?";
writemessages[303] = "Clambering onto the raft, you paddle it across to the other side of the lake.";
writemessages[304] = "\nThe bear lifts you like a toy and crushes you to death.";
writemessages[305] = "KLONK! The jar hits the bear on the head. The bear gives a loud roar of pain and chases you. With one powerful squeeze it breaks every bone in your body.";
writemessages[306] = "Could you put that another way ?";
writemessages[307] = "stThe hook catches onto the rock y               protuberance, an d the rope      dangles just wit hin reach.";
writemessages[308] = "Why ?";
writemessages[309] = "If you insist.";
writemessages[310] = "That was dumb.  You throw the rope into the air and it lands in the pool with a splash.";
writemessages[311] = "With the glove protecting your hand, you throw the tripswitch to the &#34;ON&#34; position. The power is restored and all the lights come on.";
writemessages[312] = "stYou receive a violent electric                 shock from the t ripswitch.";
writemessages[313] = "You've done that.";
writemessages[314] = "How ?";
writemessages[315] = "With what ?";
writemessages[316] = "Why ?";
writemessages[317] = "How ?";
writemessages[318] = "A match is not going to burn through such a thick rope.";
writemessages[319] = "Striking a match, you hold it beneath the resistant strand, which smoulders...and finally breaks.";
writemessages[320] = "You've done that.";
writemessages[321] = "Don't be crazy!";
writemessages[322] = "stIt's screwed to the table.";
writemessages[323] = "Why ?";
writemessages[324] = "Don't be crazy!";
writemessages[325] = "Not with that mad bear around!";
writemessages[326] = "&#34;You leave that jar alone,&#34; says the farmer's wife chasing you out of the kitchen.";
writemessages[327] = "stAs soon as her back is turned,                 you realize that  this is your   golden opportuni ty. You reach   out and take the  jar of honey.  But watch out if  she catches youwith it....";
writemessages[328] = "&#34;You leave that jar alone,&#34; says the farmer's wife chasing you out of the kitchen.";
writemessages[329] = "You can't do that...yet.";
writemessages[330] = "How ?";
writemessages[331] = "It's large and heavy&#59; you need to drop something first.";
writemessages[332] = "It's large and heavy&#59; you need to drop something first.";
writemessages[333] = "stThis facility is regrettably n otavailable on The Quill - sorry !";
writemessages[334] = "You can't reach it&#59; it's high above you embedded in the rock.";
writemessages[335] = "How ?";
writemessages[336] = "Please be more specific...";
writemessages[337] = "Don't be crazy!";
writemessages[338] = "stAfter all this trouble?! Leave                 it be - you'll n eed it.";
writemessages[339] = "Why ?";
writemessages[340] = "Why ?";
writemessages[341] = "Why ?";
writemessages[342] = "How ?";
writemessages[343] = "st&#34;Please don't touch the flora, &#34;               booms a voice fr om above.";
writemessages[344] = "Oops!";
writemessages[345] = "That wasn't very bright...";
writemessages[346] = "Oops!";
writemessages[347] = "That wasn't very bright...";
writemessages[348] = "Oops!";
writemessages[349] = "That wasn't very bright...";
writemessages[350] = "Carefully you put it down.";
writemessages[351] = "stYou carefully put everything o n               the ground.";
writemessages[352] = "Nice try, but nothing happens!";
writemessages[353] = "Oops!";
writemessages[354] = "Oops!";
writemessages[355] = "Don't be crazy!";
writemessages[356] = "Not here.";
writemessages[357] = "Not here.";
writemessages[358] = "Not here.";
writemessages[359] = "Not here.";
writemessages[360] = "Too late!";
writemessages[361] = "You remove the needle from the nail and put it on the ground.";
writemessages[362] = "How ?";
writemessages[363] = "You remove the nail from the broom.";
writemessages[364] = "How ?";
writemessages[365] = "How ?";
writemessages[366] = "How ?";
writemessages[367] = "Adventures should be SO simple!";
writemessages[368] = "They're about four sizes too big for you.";
writemessages[369] = "Could you put that another way ?";
writemessages[370] = "You've found something...";
writemessages[371] = "You've found something...";
writemessages[372] = "You've done that.";
writemessages[373] = "You've done that.";
writemessages[374] = "Nothing further is revealed.";
writemessages[375] = "You've found something...";
writemessages[376] = "You find an old bone behind the generator.";
writemessages[377] = "You've done that.";
writemessages[378] = "You've done that.";
writemessages[379] = "Nothing further is revealed.";
writemessages[380] = "Adventures should be SO simple!";
writemessages[381] = "Throwing your arms around the large tree, you move the bark and reveal an opening...";
writemessages[382] = "You've done that.";
writemessages[383] = "It's deeply recessed&#59; you can't reach it with your fingers.";
writemessages[384] = "You turn the knurled knob with the pair of pliers.";
writemessages[385] = "You've done that.";
writemessages[386] = "With what ?";
writemessages[387] = "You swing yourself over the icy pool and land on the other side. You secure the rope in readiness for a possible return journey.";
writemessages[388] = "You swing yourself over the icy pool and land on the other side. You secure the rope in readiness for a possible return journey.";
writemessages[389] = "You lower the paper-lifter over the edge and lift the sheet of paper with the nail.  Removing the paper from the nail, you drop the paper-lifter, having no further use for it.";
writemessages[390] = "You've done that.";
writemessages[391] = "You can't do that...yet.";
writemessages[392] = "You enter the coordinates into the computer.  The machine in the adjoining room swings into the correct position.";
writemessages[393] = "stNo thanks! Water looks too col d.";
writemessages[394] = "stNo thanks! Water looks too col d.";
writemessages[395] = "Not here.";
writemessages[396] = "You plug your ears with the two pieces of cotton wool.";
writemessages[397] = "&#34;Well I never,&#34; declares the wife, &#34;you have swept me off my feet - but you still can't have my honey!&#34;";
writemessages[398] = "&#34;Well I never,&#34; declares the wife, &#34;you have swept me off my feet - but you still can't have my honey!&#34;";
writemessages[399] = "If you insist.";
writemessages[400] = "How ?";
writemessages[401] = "Could you put that another way ?";
writemessages[402] = "How ?";
writemessages[403] = "\nstOh dear, you misjudge the spee d of an oncoming Porsche and you                 are sent into ob livion!";
writemessages[404] = "Could you put that another way ?";
writemessages[405] = "You can't do that...yet.";
writemessages[406] = "You're sweet enough&#59; you don't need the honey!";
writemessages[407] = "stThat was delicious.";
writemessages[408] = "Data placed in memory.";
writemessages[409] = "Data recalled.";
writemessages[410] = "Data placed in memory.";
writemessages[411] = "Data recalled.";
writemessages[412] = "Just what's needed on a hot day!";
writemessages[413] = "st&#34;Oy, why you shouting?  I'm no t               that deaf&#34; says the farmer.";
writemessages[414] = "Why ?";
writemessages[415] = "How ?";
writemessages[416] = "With what ?";
writemessages[417] = "stIt is this writer's considered                 opinion that all  adventurers    have homicidal t endencies!";
writemessages[418] = "You stand the ladder in front of you.";
writemessages[419] = "Why ?";
writemessages[420] = "It is very fragrant.";
writemessages[421] = "Don't be crazy!";
writemessages[422] = "stHot takes his bone and squeeze s               through the gap.  He gnaws on it for a while, the n digs away at  the heap looking  for another. Byso doing, he lev els the heap andthe door swings open.           Hot notices the ventilation pipethrough which he 'd arrived. He  disappears into it, and with a  yelp is gone.";
writemessages[423] = "You can't do that...yet.";
writemessages[424] = "Raise your voice&#59; he's deaf but he won't admit it!";
writemessages[425] = "stHot could probably flatten tha t pile if he could be persuaded tosqueeze through the door again .";
writemessages[426] = "You'll need to wobble the needle on something....";
writemessages[427] = "Just leave the jar of honey for him!";
writemessages[428] = "Make an improvised pathway.";
writemessages[429] = "Now where would we usually find a coin?";
writemessages[430] = "Tidy up like a park-keeper.";
writemessages[431] = "Ingratiate yourself to the lady of the house....";
writemessages[432] = "Wait until she's distracted.";
writemessages[433] = "Have you checked the generator room thoroughly?";
writemessages[434] = "The bark must be moved out of the way...";
writemessages[435] = "Not here.";
writemessages[436] = "stHot raises a paw. Seems it's t heonly trick he knows.";
writemessages[437] = "stLick lick he responds!";
writemessages[438] = "Nice try, but nothing happens!";
writemessages[439] = "Old Father Time taps his foot patiently...";
writemessages[440] = "&#34;You leave that jar alone,&#34; says the farmer's wife chasing you out of the kitchen.";
writemessages[441] = "&#34;You leave that jar alone,&#34; says the farmer's wife chasing you out of the kitchen.";
writemessages[442] = "Why ?";
writemessages[443] = "Why ?";
writemessages[444] = "RESPONSE_DEFAULT_START";
writemessages[445] = "RESPONSE_DEFAULT_END";
writemessages[446] = "PRO1";
writemessages[447] = "PRO2";
writemessages[448] = "\nstThe man turns around and leave s.";
writemessages[449] = "\nstAcross the dam a man appears&#59; helooks in your direction.";
writemessages[450] = "st\nThe farmer appears.";
writemessages[451] = "\nThe farmer wanders off.";
writemessages[452] = "\n&#34;What the hades are you holding? My lovely lamp from the lounge?&#34; screams the farmer.  &#34;Get off my land you thief you!&#34;";
writemessages[453] = "\n&#34;What the hades are you holding? My lovely lamp from the lounge?&#34; screams the farmer.  &#34;Get off my land you thief you!&#34;";
writemessages[454] = "\nstThe lamp is getting dimmer...";
writemessages[455] = "\nstYou walk into a wall and crack{CLASS|center|           your skull.}";
writemessages[456] = "\nA needle on the lamp points to the &#124; mark.  Half your paraffin has been used.";
writemessages[457] = "\nst&#34;Oh dear, my stew is burning,&#34;                 cries the farmer 's wife.  She   turns to tend to  the pot.";
writemessages[458] = "\nstHaving saved the stew, the goo d               lady returns to her chores.";
writemessages[459] = "\nThe farmer's wife is preoccupied with saving her stew...";
writemessages[460] = "\n&#34;Oh, so you're the one who stole my jar of honey,&#34; scolds the farmer's wife.  &#34;I'll have it back if you don't mind - and this time I'm going to lock it away.&#34;  She takes the jar from you, muttering something about adventurers who go into people's homes and just take things as soon as a back is turned.";
writemessages[461] = "\nstYour fingers are slipping...";
writemessages[462] = "\nstYou fall to the ground breakin g your legs and arms.\n                ";
writemessages[463] = "\nWheeeeee!! Look at Mary Poppins floating down to safety!";
writemessages[464] = "\nBefore long you die a painful death resulting from hypothermia";
writemessages[465] = "\nA small dog suddenly comes into your life by squeezing through the partly-open steel door. He jumps up and grabs the bone from out of your hand.  Proud of himself, he sits there panting and waiting....";
writemessages[466] = "st\nThe bear advances towards you...";
writemessages[467] = "\nThe bear lifts you like a toy and crushes you to death.";
writemessages[468] = "\nThe asteroid is getting closer!!";
writemessages[469] = "\nThe asteroid is getting closer!!";
writemessages[470] = "\nThe asteroid is getting closer!!";
writemessages[471] = "\nThe asteroid is getting closer!!";
writemessages[472] = "\nThe asteroid is getting closer!!";
writemessages[473] = "\nThe asteroid is getting closer!!";
writemessages[474] = "\nThe asteroid is getting closer!!";
writemessages[475] = "st\nThe ledge creaks and groans and finally snaps. You fall into a dark and bottomless abyss.";
writemessages[476] = "\nIt tears at your unprotected ear drums. You collapse with pain as the shriek goes far beyond the normal threshold of pain.";
writemessages[477] = "The machine comes to life with a supersonic scream that could burst an eardrum, but the cotton wool protects you.  An invisible beam is sent skywards towards the errant asteroid.\nstThe repelling beam hits the asteroid, sending it into a new orbit away from Earth.\nPicking up a nearby phone, you report your success. You beam with pride as congratulations on your success shower on you. London has been saved, and your credibility left intact.\n\n{CLASS|center|CONGRATULATIONS}";
writemessages[478] = "\nstA passing tramp stops you and begs for the galoshes. Having no further need for them, you give them to him.";
writemessages[479] = "stIt seems to be empty, but you can't be sure.";
writemessages[480] = "Please be more specific...";
writemessages[481] = "Could you put that another way ?";

// LOCATION MESSAGES

total_location_messages=55;

locations = [];

locations[0] = "{CLASS|center|IMPACT\n}The exhausted scientists looked up wearily from their equations and calculations, and looking at each other nodded a silent concurrence.\nstAn asteroid measuring almost t enmiles in diameter is in an orb itwhich will cause an impact wit h the city of London.\nA laser beam is tried unsuccess- fully to destroy it.\nIt has been reported that a repelling beam machine has been built by a now-dead inventor. You must find it and deflect the asteroid away from Earth before it destroys most of London.\n\nby Laurence Creighton June 1992 using the Quill  the Press";
locations[1] = "You are next to your car on a busy highway. The road itself runs north/south, and a narrow path leads off to the {ACTION|east|east}. Exit: {ACTION|east|east}";
locations[2] = "You are on a narrow path.\nExits: {ACTION|west|west} and {ACTION|east|east}";
locations[3] = "You are beside a small dam. The path ends abruptly at the water, and continues on the far side. A small canoe can be seen.\nExit: {ACTION|west|west}";
locations[4] = "You are in a very small field. In the centre stands a large stone fountain over which water cascades from one tier to the next. The dam is to the west. Exit: {ACTION|east|east}";
locations[5] = "You are at some crossroads. To the {ACTION|north|north} you can see the outskirts of a dense forest in which you could easily get lost. To the {ACTION|east|east} is a service station and a short distance to the {ACTION|south|south} a small shop can be seen. Exits: {ACTION|north|north}, {ACTION|south|south}, {ACTION|west|west}, {ACTION|east|east}";
locations[6] = "You're inside a service station. The floor is covered with oil, and steps leading down to the service pit look very slippery. Exit: {ACTION|west|west} and down";
locations[7] = "You are at a junction of paths. Exits: {ACTION|west|west}, {ACTION|east|east} and northeast";
locations[8] = "You have come to the grounds of a disused mine.  Off to one side is a small room.\nExits: {ACTION|west|west}, {ACTION|east|east} and in";
locations[9] = "You are by a mineshaft. A lift stands ready to descend to the lower depths.\nExits: {ACTION|west|west} and {ACTION|in|in}";
locations[10] = "The path comes to a dead end. This is a small but very high cavern and appears to be a dump for rubble.\nA path leads {ACTION|west|west}";
locations[11] = "You're in an underground passage with a lift ready to ascend to the surface.\nExits: {ACTION|west|west}, {ACTION|east|east} and in";
locations[12] = "You are in a damp underground passage. You can see a large steel door.\nExits: {ACTION|east|east} and west";
locations[13] = "You have come to a new digging. Exits: {ACTION|south|south} and east";
locations[14] = "You are at the southern side of the new digging.\nExit: {ACTION|north|north}";
locations[15] = "You are on a forked path.\nExits: {ACTION|north|north}, {ACTION|south|south} and {ACTION|east|east}";
locations[16] = "You are in a general dealer's store. The shelves are filled with merchandise and an ancient till full of dust is obviously broken.\nExit: {ACTION|north|north}";
locations[17] = "You are on a farm road wending from southwest to {ACTION|north|north}.";
locations[18] = "You are at a high wooden gate. Beyond can be seen the farmhouse and westwards, a shed.\nExits: north, {ACTION|south|south} and west";
locations[19] = "You are in a tractor shed. Next to you is a large red tractor which almost fills the entire shed.\nExit: {ACTION|east|east}";
locations[20] = "You are standing at an open door which leads into the farmhouse. Exits: {ACTION|south|south} and in";
locations[21] = "You are in the hallway. Doors lead off in every direction. Exits: {ACTION|north|north}, {ACTION|west|west}, {ACTION|east|east}, out";
locations[22] = "You are in a cozy living room. Armchairs are in a semicircle around a low coffee table.\nExit: {ACTION|south|south}";
locations[23] = "You are standing in a room which is being used as a laundry. The rubber pipe leading to the washing machine is broken, but the tumble drier has not met the same fate and seems to be in good order.\nExits: {ACTION|south|south} and northeast";
locations[24] = "You're in a typical farm kitchen with a huge pot bubbling over a roaring fire. A large table is standing in the centre in front of the fireplace.\nExits: {ACTION|north|north} and {ACTION|west|west}";
locations[25] = "You're in a bedroom. Long velvet curtains cover the wide window, and a large wardrobe is built into the wall to the side of the bed.\nExit: {ACTION|east|east}";
locations[26] = "You are on the outskirts of an impenetrable forest. A well- defined path leads {ACTION|south|south} and a rudimentary path swings off to the {ACTION|northwest|northwest}, but you're apt to go round in a circle if you don't know where you're going...";
locations[27] = "You're inside the lift at the surface.  Two buttons marked U and D can be seen.\nExit: {ACTION|out|out}";
locations[28] = "You are in a service pit. Steps lead up to the interior of the service station.\nExit: up";
locations[29] = "You are on the shore of a large lake.\nExit: southeast";
locations[30] = "You're inside the lift deep inside the mine.  Two buttons marked U and D can be seen. Exit: out";
locations[31] = "You are in a small room housing a powerful generator. In the front of the generator is a 2cm square-shaped recess, and behind the generator are wires and lots of dust.\nExit: {ACTION|out|out}";
locations[32] = "This is obviously the farmer's workshop. A cupboard marked &#34;BITS  PIECES&#34; stands alongsidea  dark corner.                  E xit: southwest";
locations[33] = "You are on the far shore of the lake.\nExits: north and {ACTION|west|west}";
locations[34] = "You are by the entrance to a cave. A flashing red sign tries to tell you something.\nExits: south and east";
locations[35] = "You are inside the cave.  The floor to the east is covered with sharp, pointed stalagmites. Exits: west and east";
locations[36] = "You are in a high-walled cavern. The walls are slimy and too slippery to climb, and the only path seems to run west - over the stalagmites.";
locations[37] = "You are on a west?{ACTION|east|east} path. A tollgate stands next to a small wooden hut.";
locations[38] = "You are on a ladder at the top of the tree.  Looking around and admiring the view, you wonder whether the effort was worth it! Exit: {ACTION|down|down}";
locations[39] = "You are at a large tree. The bark seems to have a mystical pattern, and high above a nest can be seen.\nExit: {ACTION|east|east}";
locations[40] = "You're in an underground cavern. To the west is an icy pool above which is a rocky protuberance. Beyond the pool you can see that the path continues. A steep path leads up out of the tree.\nExit: {ACTION|north|north}";
locations[41] = "You are in an icy pool. A nearby thermometer reads 2@C - which explains why your legs and arms have cramped and you feel your- self drowning...";
locations[42] = "You are on a path which goes {ACTION|north|north}. A very steep path leads down to a flimsy-looking ledge&#59; and to the east is the icy pool.";
locations[43] = "You are before the door of a big round building.\nExits: in and {ACTION|south|south}";
locations[44] = "You are on a rocky ledge. It is groaning under your weight and you realize that the end is upon you!\nExit: up";
locations[45] = "You're on top of the ladder, high above the floor.  Next to you is a grappling hook which is embedded in the rock.\nExit: down";
locations[46] = "You are dangling high above the cave&#59; your fingers tenaciously gripping the crumbling ridge....";
locations[47] = "You are atop a pile of rubble. Next to you is a small niche. Exit: {ACTION|down|down}";
locations[48] = "You are in a small side room. It appears to be an office, and on the desk stands a computer. Exit: east";
locations[49] = "You're in a large observatory. A telescope points at the ceiling, and next to it is the repelling machine, also pointing at the ceiling. High above your head, is a catwalk which is accessed by a narrow flight of steps. Exits: west, east, out and up";
locations[50] = "You are in a tiny kitchenette. There is a kettle and a hotplate against the wall - probably for the long nights of observing. Exit: west";
locations[51] = "You're on a narrow catwalk high above the observatory floor. You notice the ceiling is bisected, and next to you are two buttons: one red, the other green.\nExit: down";
locations[52] = "You're in an underground herb garden. The aroma fills your nostrils with delight.\nExit: {ACTION|south|south}";
locations[53] = "You are sitting on the driver's seat of the tractor. In front of you is the ignition but the key is missing.\nExit: {ACTION|down|down}";
locations[54] = "";

// CONNECTIONS

connections = [];
connections_start = [];

connections[0] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[1] = [ -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[2] = [ -1, -1, -1, 3, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[3] = [ -1, -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[4] = [ -1, -1, -1, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[5] = [ -1, 26, 15, 6, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[6] = [ -1, -1, -1, -1, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[7] = [ -1, -1, -1, 8, 15, 17, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[8] = [ -1, -1, -1, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[9] = [ -1, -1, -1, -1, 8, -1, -1, -1, -1, -1, -1, 27, -1, -1, -1, -1 ];
connections[10] = [ -1, -1, -1, -1, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[11] = [ -1, -1, -1, 10, 12, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[12] = [ -1, -1, -1, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[13] = [ -1, -1, 14, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[14] = [ -1, 13, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[15] = [ -1, 5, 16, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[16] = [ -1, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[17] = [ -1, 18, -1, -1, -1, -1, -1, -1, 7, -1, -1, -1, -1, -1, -1, -1 ];
connections[18] = [ -1, -1, 17, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[19] = [ -1, -1, -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[20] = [ -1, -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[21] = [ -1, 22, -1, 24, 25, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[22] = [ -1, -1, 21, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[23] = [ -1, -1, 24, -1, -1, 32, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[24] = [ -1, 23, -1, -1, 21, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[25] = [ -1, -1, -1, 21, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[26] = [ -1, 26, 5, 26, 26, 26, -1, 26, 26, -1, -1, -1, -1, -1, -1, -1 ];
connections[27] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 9, -1, -1, -1 ];
connections[28] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[29] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[30] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[31] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 8, -1, -1, -1 ];
connections[32] = [ -1, -1, -1, -1, -1, -1, -1, -1, 23, -1, -1, -1, -1, -1, -1, -1 ];
connections[33] = [ -1, -1, -1, -1, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[34] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[35] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[36] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[37] = [ -1, -1, -1, 33, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[38] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 39, -1, -1, -1, -1, -1 ];
connections[39] = [ -1, -1, -1, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[40] = [ -1, 52, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[41] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[42] = [ -1, 43, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[43] = [ -1, -1, 42, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[44] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[45] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[46] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[47] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 10, -1, -1, -1, -1, -1 ];
connections[48] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[49] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[50] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[51] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[52] = [ -1, -1, 40, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[53] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 19, -1, -1, -1, -1, -1 ];
connections[54] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];

connections_start[0] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[1] = [ -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[2] = [ -1, -1, -1, 3, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[3] = [ -1, -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[4] = [ -1, -1, -1, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[5] = [ -1, 26, 15, 6, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[6] = [ -1, -1, -1, -1, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[7] = [ -1, -1, -1, 8, 15, 17, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[8] = [ -1, -1, -1, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[9] = [ -1, -1, -1, -1, 8, -1, -1, -1, -1, -1, -1, 27, -1, -1, -1, -1 ];
connections_start[10] = [ -1, -1, -1, -1, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[11] = [ -1, -1, -1, 10, 12, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[12] = [ -1, -1, -1, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[13] = [ -1, -1, 14, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[14] = [ -1, 13, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[15] = [ -1, 5, 16, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[16] = [ -1, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[17] = [ -1, 18, -1, -1, -1, -1, -1, -1, 7, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[18] = [ -1, -1, 17, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[19] = [ -1, -1, -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[20] = [ -1, -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[21] = [ -1, 22, -1, 24, 25, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[22] = [ -1, -1, 21, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[23] = [ -1, -1, 24, -1, -1, 32, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[24] = [ -1, 23, -1, -1, 21, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[25] = [ -1, -1, -1, 21, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[26] = [ -1, 26, 5, 26, 26, 26, -1, 26, 26, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[27] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 9, -1, -1, -1 ];
connections_start[28] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[29] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[30] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[31] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 8, -1, -1, -1 ];
connections_start[32] = [ -1, -1, -1, -1, -1, -1, -1, -1, 23, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[33] = [ -1, -1, -1, -1, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[34] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[35] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[36] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[37] = [ -1, -1, -1, 33, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[38] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 39, -1, -1, -1, -1, -1 ];
connections_start[39] = [ -1, -1, -1, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[40] = [ -1, 52, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[41] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[42] = [ -1, 43, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[43] = [ -1, -1, 42, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[44] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[45] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[46] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[47] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 10, -1, -1, -1, -1, -1 ];
connections_start[48] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[49] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[50] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[51] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[52] = [ -1, -1, 40, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[53] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 19, -1, -1, -1, -1, -1 ];
connections_start[54] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];


resources=[];


 //OBJECTS

objects = [];
objectsAttrLO = [];
objectsAttrHI = [];
objectsLocation = [];
objectsNoun = [];
objectsAdjective = [];
objectsWeight = [];
objectsAttrLO_start = [];
objectsAttrHI_start = [];
objectsLocation_start = [];
objectsWeight_start = [];

objects[0] = "A LIT paraffin lamp";
objectsNoun[0] = 255;
objectsAdjective[0] = 255;
objectsLocation[0] = 252;
objectsLocation_start[0] = 252;
objectsWeight[0] = 1;
objectsWeight_start[0] = 1;
objectsAttrLO[0] = 1;
objectsAttrLO_start[0] = 1;
objectsAttrHI[0] = 0;
objectsAttrHI_start[0] = 0;

objects[1] = "A paraffin lamp";
objectsNoun[1] = 255;
objectsAdjective[1] = 255;
objectsLocation[1] = 22;
objectsLocation_start[1] = 22;
objectsWeight[1] = 1;
objectsWeight_start[1] = 1;
objectsAttrLO[1] = 0;
objectsAttrLO_start[1] = 0;
objectsAttrHI[1] = 0;
objectsAttrHI_start[1] = 0;

objects[2] = "A canoe";
objectsNoun[2] = 255;
objectsAdjective[2] = 255;
objectsLocation[2] = 252;
objectsLocation_start[2] = 252;
objectsWeight[2] = 1;
objectsWeight_start[2] = 1;
objectsAttrLO[2] = 0;
objectsAttrLO_start[2] = 0;
objectsAttrHI[2] = 0;
objectsAttrHI_start[2] = 0;

objects[3] = "A man standing on the far side";
objectsNoun[3] = 255;
objectsAdjective[3] = 255;
objectsLocation[3] = 252;
objectsLocation_start[3] = 252;
objectsWeight[3] = 1;
objectsWeight_start[3] = 1;
objectsAttrLO[3] = 0;
objectsAttrLO_start[3] = 0;
objectsAttrHI[3] = 0;
objectsAttrHI_start[3] = 0;

objects[4] = "A paddle";
objectsNoun[4] = 17;
objectsAdjective[4] = 255;
objectsLocation[4] = 252;
objectsLocation_start[4] = 252;
objectsWeight[4] = 1;
objectsWeight_start[4] = 1;
objectsAttrLO[4] = 0;
objectsAttrLO_start[4] = 0;
objectsAttrHI[4] = 0;
objectsAttrHI_start[4] = 0;

objects[5] = "A box of matches";
objectsNoun[5] = 20;
objectsAdjective[5] = 255;
objectsLocation[5] = 252;
objectsLocation_start[5] = 252;
objectsWeight[5] = 1;
objectsWeight_start[5] = 1;
objectsAttrLO[5] = 0;
objectsAttrLO_start[5] = 0;
objectsAttrHI[5] = 0;
objectsAttrHI_start[5] = 0;

objects[6] = "A box of matches";
objectsNoun[6] = 255;
objectsAdjective[6] = 255;
objectsLocation[6] = 252;
objectsLocation_start[6] = 252;
objectsWeight[6] = 1;
objectsWeight_start[6] = 1;
objectsAttrLO[6] = 0;
objectsAttrLO_start[6] = 0;
objectsAttrHI[6] = 0;
objectsAttrHI_start[6] = 0;

objects[7] = "A hole";
objectsNoun[7] = 255;
objectsAdjective[7] = 255;
objectsLocation[7] = 252;
objectsLocation_start[7] = 252;
objectsWeight[7] = 1;
objectsWeight_start[7] = 1;
objectsAttrLO[7] = 0;
objectsAttrLO_start[7] = 0;
objectsAttrHI[7] = 0;
objectsAttrHI_start[7] = 0;

objects[8] = "A pair of galoshes";
objectsNoun[8] = 200;
objectsAdjective[8] = 255;
objectsLocation[8] = 252;
objectsLocation_start[8] = 252;
objectsWeight[8] = 1;
objectsWeight_start[8] = 1;
objectsAttrLO[8] = 2;
objectsAttrLO_start[8] = 2;
objectsAttrHI[8] = 0;
objectsAttrHI_start[8] = 0;

objects[9] = "A `1 coin";
objectsNoun[9] = 24;
objectsAdjective[9] = 255;
objectsLocation[9] = 252;
objectsLocation_start[9] = 252;
objectsWeight[9] = 1;
objectsWeight_start[9] = 1;
objectsAttrLO[9] = 0;
objectsAttrLO_start[9] = 0;
objectsAttrHI[9] = 0;
objectsAttrHI_start[9] = 0;

objects[10] = "A rusty nail";
objectsNoun[10] = 59;
objectsAdjective[10] = 255;
objectsLocation[10] = 252;
objectsLocation_start[10] = 252;
objectsWeight[10] = 1;
objectsWeight_start[10] = 1;
objectsAttrLO[10] = 0;
objectsAttrLO_start[10] = 0;
objectsAttrHI[10] = 0;
objectsAttrHI_start[10] = 0;

objects[11] = "A needle";
objectsNoun[11] = 57;
objectsAdjective[11] = 255;
objectsLocation[11] = 252;
objectsLocation_start[11] = 252;
objectsWeight[11] = 1;
objectsWeight_start[11] = 1;
objectsAttrLO[11] = 0;
objectsAttrLO_start[11] = 0;
objectsAttrHI[11] = 0;
objectsAttrHI_start[11] = 0;

objects[12] = "A crude compass";
objectsNoun[12] = 25;
objectsAdjective[12] = 255;
objectsLocation[12] = 252;
objectsLocation_start[12] = 252;
objectsWeight[12] = 1;
objectsWeight_start[12] = 1;
objectsAttrLO[12] = 0;
objectsAttrLO_start[12] = 0;
objectsAttrHI[12] = 0;
objectsAttrHI_start[12] = 0;

objects[13] = "A farmer";
objectsNoun[13] = 255;
objectsAdjective[13] = 255;
objectsLocation[13] = 18;
objectsLocation_start[13] = 18;
objectsWeight[13] = 1;
objectsWeight_start[13] = 1;
objectsAttrLO[13] = 0;
objectsAttrLO_start[13] = 0;
objectsAttrHI[13] = 0;
objectsAttrHI_start[13] = 0;

objects[14] = "A pair of trousers";
objectsNoun[14] = 37;
objectsAdjective[14] = 255;
objectsLocation[14] = 25;
objectsLocation_start[14] = 25;
objectsWeight[14] = 1;
objectsWeight_start[14] = 1;
objectsAttrLO[14] = 0;
objectsAttrLO_start[14] = 0;
objectsAttrHI[14] = 0;
objectsAttrHI_start[14] = 0;

objects[15] = "An open gate";
objectsNoun[15] = 255;
objectsAdjective[15] = 255;
objectsLocation[15] = 252;
objectsLocation_start[15] = 252;
objectsWeight[15] = 1;
objectsWeight_start[15] = 1;
objectsAttrLO[15] = 0;
objectsAttrLO_start[15] = 0;
objectsAttrHI[15] = 0;
objectsAttrHI_start[15] = 0;

objects[16] = "A magazine";
objectsNoun[16] = 31;
objectsAdjective[16] = 255;
objectsLocation[16] = 19;
objectsLocation_start[16] = 19;
objectsWeight[16] = 1;
objectsWeight_start[16] = 1;
objectsAttrLO[16] = 0;
objectsAttrLO_start[16] = 0;
objectsAttrHI[16] = 0;
objectsAttrHI_start[16] = 0;

objects[17] = "A screwdriver";
objectsNoun[17] = 33;
objectsAdjective[17] = 255;
objectsLocation[17] = 252;
objectsLocation_start[17] = 252;
objectsWeight[17] = 1;
objectsWeight_start[17] = 1;
objectsAttrLO[17] = 0;
objectsAttrLO_start[17] = 0;
objectsAttrHI[17] = 0;
objectsAttrHI_start[17] = 0;

objects[18] = "A jar of honey";
objectsNoun[18] = 255;
objectsAdjective[18] = 255;
objectsLocation[18] = 24;
objectsLocation_start[18] = 24;
objectsWeight[18] = 1;
objectsWeight_start[18] = 1;
objectsAttrLO[18] = 0;
objectsAttrLO_start[18] = 0;
objectsAttrHI[18] = 0;
objectsAttrHI_start[18] = 0;

objects[19] = "A crowbar";
objectsNoun[19] = 48;
objectsAdjective[19] = 255;
objectsLocation[19] = 252;
objectsLocation_start[19] = 252;
objectsWeight[19] = 1;
objectsWeight_start[19] = 1;
objectsAttrLO[19] = 0;
objectsAttrLO_start[19] = 0;
objectsAttrHI[19] = 0;
objectsAttrHI_start[19] = 0;

objects[20] = "An open door";
objectsNoun[20] = 255;
objectsAdjective[20] = 255;
objectsLocation[20] = 252;
objectsLocation_start[20] = 252;
objectsWeight[20] = 1;
objectsWeight_start[20] = 1;
objectsAttrLO[20] = 0;
objectsAttrLO_start[20] = 0;
objectsAttrHI[20] = 0;
objectsAttrHI_start[20] = 0;

objects[21] = "An open cupboard";
objectsNoun[21] = 255;
objectsAdjective[21] = 255;
objectsLocation[21] = 252;
objectsLocation_start[21] = 252;
objectsWeight[21] = 1;
objectsWeight_start[21] = 1;
objectsAttrLO[21] = 0;
objectsAttrLO_start[21] = 0;
objectsAttrHI[21] = 0;
objectsAttrHI_start[21] = 0;

objects[22] = "An iron rod";
objectsNoun[22] = 53;
objectsAdjective[22] = 255;
objectsLocation[22] = 252;
objectsLocation_start[22] = 252;
objectsWeight[22] = 1;
objectsWeight_start[22] = 1;
objectsAttrLO[22] = 0;
objectsAttrLO_start[22] = 0;
objectsAttrHI[22] = 0;
objectsAttrHI_start[22] = 0;

objects[23] = "An iron lever";
objectsNoun[23] = 255;
objectsAdjective[23] = 255;
objectsLocation[23] = 252;
objectsLocation_start[23] = 252;
objectsWeight[23] = 1;
objectsWeight_start[23] = 1;
objectsAttrLO[23] = 0;
objectsAttrLO_start[23] = 0;
objectsAttrHI[23] = 0;
objectsAttrHI_start[23] = 0;

objects[24] = "A small wooden barrel";
objectsNoun[24] = 55;
objectsAdjective[24] = 255;
objectsLocation[24] = 16;
objectsLocation_start[24] = 16;
objectsWeight[24] = 1;
objectsWeight_start[24] = 1;
objectsAttrLO[24] = 0;
objectsAttrLO_start[24] = 0;
objectsAttrHI[24] = 0;
objectsAttrHI_start[24] = 0;

objects[25] = "A notice nailed to the wall";
objectsNoun[25] = 255;
objectsAdjective[25] = 255;
objectsLocation[25] = 14;
objectsLocation_start[25] = 14;
objectsWeight[25] = 1;
objectsWeight_start[25] = 1;
objectsAttrLO[25] = 0;
objectsAttrLO_start[25] = 0;
objectsAttrHI[25] = 0;
objectsAttrHI_start[25] = 0;

objects[26] = "A notice";
objectsNoun[26] = 255;
objectsAdjective[26] = 255;
objectsLocation[26] = 252;
objectsLocation_start[26] = 252;
objectsWeight[26] = 1;
objectsWeight_start[26] = 1;
objectsAttrLO[26] = 0;
objectsAttrLO_start[26] = 0;
objectsAttrHI[26] = 0;
objectsAttrHI_start[26] = 0;

objects[27] = "A short dagger";
objectsNoun[27] = 61;
objectsAdjective[27] = 255;
objectsLocation[27] = 252;
objectsLocation_start[27] = 252;
objectsWeight[27] = 1;
objectsWeight_start[27] = 1;
objectsAttrLO[27] = 0;
objectsAttrLO_start[27] = 0;
objectsAttrHI[27] = 0;
objectsAttrHI_start[27] = 0;

objects[28] = "A ladder";
objectsNoun[28] = 255;
objectsAdjective[28] = 255;
objectsLocation[28] = 39;
objectsLocation_start[28] = 39;
objectsWeight[28] = 1;
objectsWeight_start[28] = 1;
objectsAttrLO[28] = 0;
objectsAttrLO_start[28] = 0;
objectsAttrHI[28] = 0;
objectsAttrHI_start[28] = 0;

objects[29] = "A ladder over the stalagmites";
objectsNoun[29] = 255;
objectsAdjective[29] = 255;
objectsLocation[29] = 252;
objectsLocation_start[29] = 252;
objectsWeight[29] = 1;
objectsWeight_start[29] = 1;
objectsAttrLO[29] = 0;
objectsAttrLO_start[29] = 0;
objectsAttrHI[29] = 0;
objectsAttrHI_start[29] = 0;

objects[30] = "A grappling hook (in the rock)";
objectsNoun[30] = 255;
objectsAdjective[30] = 255;
objectsLocation[30] = 36;
objectsLocation_start[30] = 36;
objectsWeight[30] = 1;
objectsWeight_start[30] = 1;
objectsAttrLO[30] = 0;
objectsAttrLO_start[30] = 0;
objectsAttrHI[30] = 0;
objectsAttrHI_start[30] = 0;

objects[31] = "A pink parasol";
objectsNoun[31] = 71;
objectsAdjective[31] = 255;
objectsLocation[31] = 252;
objectsLocation_start[31] = 252;
objectsWeight[31] = 1;
objectsWeight_start[31] = 1;
objectsAttrLO[31] = 0;
objectsAttrLO_start[31] = 0;
objectsAttrHI[31] = 0;
objectsAttrHI_start[31] = 0;

objects[32] = "A small raft";
objectsNoun[32] = 255;
objectsAdjective[32] = 255;
objectsLocation[32] = 29;
objectsLocation_start[32] = 29;
objectsWeight[32] = 1;
objectsWeight_start[32] = 1;
objectsAttrLO[32] = 0;
objectsAttrLO_start[32] = 0;
objectsAttrHI[32] = 0;
objectsAttrHI_start[32] = 0;

objects[33] = "A spade";
objectsNoun[33] = 77;
objectsAdjective[33] = 255;
objectsLocation[33] = 252;
objectsLocation_start[33] = 252;
objectsWeight[33] = 1;
objectsWeight_start[33] = 1;
objectsAttrLO[33] = 0;
objectsAttrLO_start[33] = 0;
objectsAttrHI[33] = 0;
objectsAttrHI_start[33] = 0;

objects[34] = "A rope";
objectsNoun[34] = 79;
objectsAdjective[34] = 255;
objectsLocation[34] = 252;
objectsLocation_start[34] = 252;
objectsWeight[34] = 1;
objectsWeight_start[34] = 1;
objectsAttrLO[34] = 0;
objectsAttrLO_start[34] = 0;
objectsAttrHI[34] = 0;
objectsAttrHI_start[34] = 0;

objects[35] = "An open steel door";
objectsNoun[35] = 255;
objectsAdjective[35] = 255;
objectsLocation[35] = 252;
objectsLocation_start[35] = 252;
objectsWeight[35] = 1;
objectsWeight_start[35] = 1;
objectsAttrLO[35] = 0;
objectsAttrLO_start[35] = 0;
objectsAttrHI[35] = 0;
objectsAttrHI_start[35] = 0;

objects[36] = "A mixture of glass and honey";
objectsNoun[36] = 255;
objectsAdjective[36] = 255;
objectsLocation[36] = 252;
objectsLocation_start[36] = 252;
objectsWeight[36] = 1;
objectsWeight_start[36] = 1;
objectsAttrLO[36] = 0;
objectsAttrLO_start[36] = 0;
objectsAttrHI[36] = 0;
objectsAttrHI_start[36] = 0;

objects[37] = "A bear";
objectsNoun[37] = 255;
objectsAdjective[37] = 255;
objectsLocation[37] = 34;
objectsLocation_start[37] = 34;
objectsWeight[37] = 1;
objectsWeight_start[37] = 1;
objectsAttrLO[37] = 0;
objectsAttrLO_start[37] = 0;
objectsAttrHI[37] = 0;
objectsAttrHI_start[37] = 0;

objects[38] = "A long rope with grappling hook";
objectsNoun[38] = 255;
objectsAdjective[38] = 255;
objectsLocation[38] = 252;
objectsLocation_start[38] = 252;
objectsWeight[38] = 1;
objectsWeight_start[38] = 1;
objectsAttrLO[38] = 0;
objectsAttrLO_start[38] = 0;
objectsAttrHI[38] = 0;
objectsAttrHI_start[38] = 0;

objects[39] = "A bone";
objectsNoun[39] = 94;
objectsAdjective[39] = 255;
objectsLocation[39] = 252;
objectsLocation_start[39] = 252;
objectsWeight[39] = 1;
objectsWeight_start[39] = 1;
objectsAttrLO[39] = 0;
objectsAttrLO_start[39] = 0;
objectsAttrHI[39] = 0;
objectsAttrHI_start[39] = 0;

objects[40] = "Hot, the dog";
objectsNoun[40] = 255;
objectsAdjective[40] = 255;
objectsLocation[40] = 252;
objectsLocation_start[40] = 252;
objectsWeight[40] = 1;
objectsWeight_start[40] = 1;
objectsAttrLO[40] = 0;
objectsAttrLO_start[40] = 0;
objectsAttrHI[40] = 0;
objectsAttrHI_start[40] = 0;

objects[41] = "A pile of rubble";
objectsNoun[41] = 255;
objectsAdjective[41] = 255;
objectsLocation[41] = 10;
objectsLocation_start[41] = 10;
objectsWeight[41] = 1;
objectsWeight_start[41] = 1;
objectsAttrLO[41] = 0;
objectsAttrLO_start[41] = 0;
objectsAttrHI[41] = 0;
objectsAttrHI_start[41] = 0;

objects[42] = "A flattened pile of rubble";
objectsNoun[42] = 255;
objectsAdjective[42] = 255;
objectsLocation[42] = 252;
objectsLocation_start[42] = 252;
objectsWeight[42] = 1;
objectsWeight_start[42] = 1;
objectsAttrLO[42] = 0;
objectsAttrLO_start[42] = 0;
objectsAttrHI[42] = 0;
objectsAttrHI_start[42] = 0;

objects[43] = "A guard";
objectsNoun[43] = 255;
objectsAdjective[43] = 255;
objectsLocation[43] = 37;
objectsLocation_start[43] = 37;
objectsWeight[43] = 1;
objectsWeight_start[43] = 1;
objectsAttrLO[43] = 0;
objectsAttrLO_start[43] = 0;
objectsAttrHI[43] = 0;
objectsAttrHI_start[43] = 0;

objects[44] = "An egg";
objectsNoun[44] = 255;
objectsAdjective[44] = 255;
objectsLocation[44] = 38;
objectsLocation_start[44] = 38;
objectsWeight[44] = 1;
objectsWeight_start[44] = 1;
objectsAttrLO[44] = 0;
objectsAttrLO_start[44] = 0;
objectsAttrHI[44] = 0;
objectsAttrHI_start[44] = 0;

objects[45] = "A broken egg";
objectsNoun[45] = 255;
objectsAdjective[45] = 255;
objectsLocation[45] = 252;
objectsLocation_start[45] = 252;
objectsWeight[45] = 1;
objectsWeight_start[45] = 1;
objectsAttrLO[45] = 0;
objectsAttrLO_start[45] = 0;
objectsAttrHI[45] = 0;
objectsAttrHI_start[45] = 0;

objects[46] = "An opening in the trunk";
objectsNoun[46] = 255;
objectsAdjective[46] = 255;
objectsLocation[46] = 252;
objectsLocation_start[46] = 252;
objectsWeight[46] = 1;
objectsWeight_start[46] = 1;
objectsAttrLO[46] = 0;
objectsAttrLO_start[46] = 0;
objectsAttrHI[46] = 0;
objectsAttrHI_start[46] = 0;

objects[47] = "A computer disc";
objectsNoun[47] = 112;
objectsAdjective[47] = 255;
objectsLocation[47] = 252;
objectsLocation_start[47] = 252;
objectsWeight[47] = 1;
objectsWeight_start[47] = 1;
objectsAttrLO[47] = 0;
objectsAttrLO_start[47] = 0;
objectsAttrHI[47] = 0;
objectsAttrHI_start[47] = 0;

objects[48] = "A dangling rope";
objectsNoun[48] = 255;
objectsAdjective[48] = 255;
objectsLocation[48] = 252;
objectsLocation_start[48] = 252;
objectsWeight[48] = 1;
objectsWeight_start[48] = 1;
objectsAttrLO[48] = 0;
objectsAttrLO_start[48] = 0;
objectsAttrHI[48] = 0;
objectsAttrHI_start[48] = 0;

objects[49] = "A sheet of paper";
objectsNoun[49] = 114;
objectsAdjective[49] = 255;
objectsLocation[49] = 44;
objectsLocation_start[49] = 44;
objectsWeight[49] = 1;
objectsWeight_start[49] = 1;
objectsAttrLO[49] = 0;
objectsAttrLO_start[49] = 0;
objectsAttrHI[49] = 0;
objectsAttrHI_start[49] = 0;

objects[50] = "An old broomstick";
objectsNoun[50] = 115;
objectsAdjective[50] = 255;
objectsLocation[50] = 32;
objectsLocation_start[50] = 32;
objectsWeight[50] = 1;
objectsWeight_start[50] = 1;
objectsAttrLO[50] = 0;
objectsAttrLO_start[50] = 0;
objectsAttrHI[50] = 0;
objectsAttrHI_start[50] = 0;

objects[51] = "A paper-lifter#The &#34;up and down&#34; variety.";
objectsNoun[51] = 255;
objectsAdjective[51] = 255;
objectsLocation[51] = 252;
objectsLocation_start[51] = 252;
objectsWeight[51] = 1;
objectsWeight_start[51] = 1;
objectsAttrLO[51] = 0;
objectsAttrLO_start[51] = 0;
objectsAttrHI[51] = 0;
objectsAttrHI_start[51] = 0;

objects[52] = "That the ceiling has opened";
objectsNoun[52] = 255;
objectsAdjective[52] = 255;
objectsLocation[52] = 252;
objectsLocation_start[52] = 252;
objectsWeight[52] = 1;
objectsWeight_start[52] = 1;
objectsAttrLO[52] = 0;
objectsAttrLO_start[52] = 0;
objectsAttrHI[52] = 0;
objectsAttrHI_start[52] = 0;

objects[53] = "A metal tripswitch";
objectsNoun[53] = 255;
objectsAdjective[53] = 255;
objectsLocation[53] = 252;
objectsLocation_start[53] = 252;
objectsWeight[53] = 1;
objectsWeight_start[53] = 1;
objectsAttrLO[53] = 0;
objectsAttrLO_start[53] = 0;
objectsAttrHI[53] = 0;
objectsAttrHI_start[53] = 0;

objects[54] = "A notebook";
objectsNoun[54] = 123;
objectsAdjective[54] = 255;
objectsLocation[54] = 48;
objectsLocation_start[54] = 48;
objectsWeight[54] = 1;
objectsWeight_start[54] = 1;
objectsAttrLO[54] = 0;
objectsAttrLO_start[54] = 0;
objectsAttrHI[54] = 0;
objectsAttrHI_start[54] = 0;

objects[55] = "that the computer is booted up";
objectsNoun[55] = 255;
objectsAdjective[55] = 255;
objectsLocation[55] = 252;
objectsLocation_start[55] = 252;
objectsWeight[55] = 1;
objectsWeight_start[55] = 1;
objectsAttrLO[55] = 0;
objectsAttrLO_start[55] = 0;
objectsAttrHI[55] = 0;
objectsAttrHI_start[55] = 0;

objects[56] = "Pieces of cotton wool";
objectsNoun[56] = 255;
objectsAdjective[56] = 255;
objectsLocation[56] = 252;
objectsLocation_start[56] = 252;
objectsWeight[56] = 1;
objectsWeight_start[56] = 1;
objectsAttrLO[56] = 0;
objectsAttrLO_start[56] = 0;
objectsAttrHI[56] = 0;
objectsAttrHI_start[56] = 0;

objects[57] = "A pair of pliers";
objectsNoun[57] = 135;
objectsAdjective[57] = 255;
objectsLocation[57] = 252;
objectsLocation_start[57] = 252;
objectsWeight[57] = 1;
objectsWeight_start[57] = 1;
objectsAttrLO[57] = 0;
objectsAttrLO_start[57] = 0;
objectsAttrHI[57] = 0;
objectsAttrHI_start[57] = 0;

objects[58] = "that the coordinates have been entered.";
objectsNoun[58] = 255;
objectsAdjective[58] = 255;
objectsLocation[58] = 252;
objectsLocation_start[58] = 252;
objectsWeight[58] = 1;
objectsWeight_start[58] = 1;
objectsAttrLO[58] = 0;
objectsAttrLO_start[58] = 0;
objectsAttrHI[58] = 0;
objectsAttrHI_start[58] = 0;

objects[59] = "The farmer's wife";
objectsNoun[59] = 255;
objectsAdjective[59] = 255;
objectsLocation[59] = 24;
objectsLocation_start[59] = 24;
objectsWeight[59] = 1;
objectsWeight_start[59] = 1;
objectsAttrLO[59] = 0;
objectsAttrLO_start[59] = 0;
objectsAttrHI[59] = 0;
objectsAttrHI_start[59] = 0;

objects[60] = "A smashed lamp";
objectsNoun[60] = 255;
objectsAdjective[60] = 255;
objectsLocation[60] = 252;
objectsLocation_start[60] = 252;
objectsWeight[60] = 1;
objectsWeight_start[60] = 1;
objectsAttrLO[60] = 0;
objectsAttrLO_start[60] = 0;
objectsAttrHI[60] = 0;
objectsAttrHI_start[60] = 0;

objects[61] = "The computer awaits a disc....";
objectsNoun[61] = 255;
objectsAdjective[61] = 255;
objectsLocation[61] = 252;
objectsLocation_start[61] = 252;
objectsWeight[61] = 1;
objectsWeight_start[61] = 1;
objectsAttrLO[61] = 0;
objectsAttrLO_start[61] = 0;
objectsAttrHI[61] = 0;
objectsAttrHI_start[61] = 0;

objects[62] = "The farmer's wife";
objectsNoun[62] = 255;
objectsAdjective[62] = 255;
objectsLocation[62] = 252;
objectsLocation_start[62] = 252;
objectsWeight[62] = 1;
objectsWeight_start[62] = 1;
objectsAttrLO[62] = 0;
objectsAttrLO_start[62] = 0;
objectsAttrHI[62] = 0;
objectsAttrHI_start[62] = 0;

objects[63] = "A battered scroll";
objectsNoun[63] = 138;
objectsAdjective[63] = 255;
objectsLocation[63] = 1;
objectsLocation_start[63] = 1;
objectsWeight[63] = 1;
objectsWeight_start[63] = 1;
objectsAttrLO[63] = 0;
objectsAttrLO_start[63] = 0;
objectsAttrHI[63] = 0;
objectsAttrHI_start[63] = 0;

objects[64] = "A rare herb";
objectsNoun[64] = 148;
objectsAdjective[64] = 255;
objectsLocation[64] = 252;
objectsLocation_start[64] = 252;
objectsWeight[64] = 1;
objectsWeight_start[64] = 1;
objectsAttrLO[64] = 0;
objectsAttrLO_start[64] = 0;
objectsAttrHI[64] = 0;
objectsAttrHI_start[64] = 0;

objects[65] = "A key";
objectsNoun[65] = 150;
objectsAdjective[65] = 255;
objectsLocation[65] = 252;
objectsLocation_start[65] = 252;
objectsWeight[65] = 1;
objectsWeight_start[65] = 1;
objectsAttrLO[65] = 0;
objectsAttrLO_start[65] = 0;
objectsAttrHI[65] = 0;
objectsAttrHI_start[65] = 0;

objects[66] = "An open door";
objectsNoun[66] = 255;
objectsAdjective[66] = 255;
objectsLocation[66] = 252;
objectsLocation_start[66] = 252;
objectsWeight[66] = 1;
objectsWeight_start[66] = 1;
objectsAttrLO[66] = 0;
objectsAttrLO_start[66] = 0;
objectsAttrHI[66] = 0;
objectsAttrHI_start[66] = 0;

objects[67] = "A rubber glove";
objectsNoun[67] = 202;
objectsAdjective[67] = 255;
objectsLocation[67] = 252;
objectsLocation_start[67] = 252;
objectsWeight[67] = 1;
objectsWeight_start[67] = 1;
objectsAttrLO[67] = 2;
objectsAttrLO_start[67] = 2;
objectsAttrHI[67] = 0;
objectsAttrHI_start[67] = 0;

objects[68] = "A large plant";
objectsNoun[68] = 255;
objectsAdjective[68] = 255;
objectsLocation[68] = 52;
objectsLocation_start[68] = 52;
objectsWeight[68] = 1;
objectsWeight_start[68] = 1;
objectsAttrLO[68] = 0;
objectsAttrLO_start[68] = 0;
objectsAttrHI[68] = 0;
objectsAttrHI_start[68] = 0;

objects[69] = "A narrow ventilation shaft.";
objectsNoun[69] = 255;
objectsAdjective[69] = 255;
objectsLocation[69] = 13;
objectsLocation_start[69] = 13;
objectsWeight[69] = 1;
objectsWeight_start[69] = 1;
objectsAttrLO[69] = 0;
objectsAttrLO_start[69] = 0;
objectsAttrHI[69] = 0;
objectsAttrHI_start[69] = 0;

objects[70] = "A steel door (slightly ajar)";
objectsNoun[70] = 255;
objectsAdjective[70] = 255;
objectsLocation[70] = 12;
objectsLocation_start[70] = 12;
objectsWeight[70] = 1;
objectsWeight_start[70] = 1;
objectsAttrLO[70] = 0;
objectsAttrLO_start[70] = 0;
objectsAttrHI[70] = 0;
objectsAttrHI_start[70] = 0;

objects[71] = "A grappling hook";
objectsNoun[71] = 69;
objectsAdjective[71] = 255;
objectsLocation[71] = 252;
objectsLocation_start[71] = 252;
objectsWeight[71] = 1;
objectsWeight_start[71] = 1;
objectsAttrLO[71] = 0;
objectsAttrLO_start[71] = 0;
objectsAttrHI[71] = 0;
objectsAttrHI_start[71] = 0;

objects[72] = "That the overhead lights are off\n\n\n\n\n\n";
objectsNoun[72] = 255;
objectsAdjective[72] = 255;
objectsLocation[72] = 49;
objectsLocation_start[72] = 49;
objectsWeight[72] = 1;
objectsWeight_start[72] = 1;
objectsAttrLO[72] = 0;
objectsAttrLO_start[72] = 0;
objectsAttrHI[72] = 0;
objectsAttrHI_start[72] = 0;

last_object_number =  72; 
carried_objects = 0;
total_object_messages=73;

