# Impact!
© 1992 by Laurence Creighton

Genre: Science Fiction

YUID: vx3r9spcp7yqq1ok

IFDB: https://ifdb.org/viewgame?id=vx3r9spcp7yqq1ok

## Info
Tools: UnQuill, The Inker, ngPAWS
- N Game fully tested?
- Y Missing images?
- Y Errors rendering images?
- Y Modern code for ngPAWS?
- 1 Edit txp (1) or sce (2)

## Test ngPAWS port

https://interactivefiction.gitlab.io/Impact-1992-Laurence_Creighton/
